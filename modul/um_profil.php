<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// mendefinisikan folder upload
define("UPLOAD_DIR", "img/foto_profil/");

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul = 'modul.php?ke=um_profil';
$theme = 'themes/conquer';
$pageTitle = 'Profil <small>Pegawai</small>';

$pageBreadcrumb = '
<ul class="page-breadcrumb">
<li>
	<i class="fa fa-home"></i> 
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Data Kepegawaian</span>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Profil Pegawai</span>
</li>
</ul>
<div class="page-toolbar">
	<div class="btn-group pull-right">
		<!-- <button type="button" class="btn btn-fit-height default dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true"> -->
		<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" data-delay="1000" data-close-others="true">
			Actions <i class="fa fa-angle-down"></i>
		</button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li><a href="#">Action</a></li>
			<li><a href="#">Another action</a></li>
			<li><a href="#">Something else here</a></li>
			<li class="divider"></li>
			<li><a href="#">Separated link</a></li>
		</ul>
	</div>
</div>
';

$pageStyles = '<script type="text/javascript" src="modul/um_profil.js"></script>';
// $pageStyles = '<script type="text/javascript" src="'.$theme.'/assets/scripts/um_profil.js"></script>';
$initA = '';
$initB = 'Profil.init();';
$initC = '';

$nip = $_SESSION['nip'];

$res = $db->prepare("SELECT * FROM um_users WHERE nip = :nip");
$res->bindParam(":nip", $nip, PDO::PARAM_STR);
$res->execute();
$r = $res->fetch(PDO::FETCH_ASSOC);
extract($r);

if (isset($_POST['aksi']) ) {
	
	try {
		if ($_POST['aksi'] == "userpass") {
			$sql = "UPDATE `um_users` SET `user` = :user";
			if(isset($_POST['pass']) && $_POST['pass'] != '') {
				$sql.=", `pass` = :pass";
			}
			$sql.= " WHERE `id` = :uid";
			$res = $db->prepare($sql);
			
			if(isset($_POST['pass']) && $_POST['pass'] != '') {
				$res->bindParam(":pass", md5($_POST['pass']), PDO::PARAM_STR);
			}			
			$res->bindParam(":user", $_POST['user'], PDO::PARAM_STR);
			$res->bindParam(":uid", $_POST['uid'], PDO::PARAM_INT);
			$res->execute();
			$response = array( 'status'=>'sukses', 'pesan'=>'Berhasil menyimpan.' );
		}		
	
	} catch(PDOException $e) {
		$response = array(
			'status'=>'gagal',
			'pesan'=>'<span class="label label-danger">Error:</span>&nbsp;'.$e->getMessage()
		);
	}
	echo json_encode($response); // Tampilkan hasil Eksekusi
	$db = null; // Tutup koneksi
}

else if(isset($_GET['lihat']) && $_GET['lihat'] == "profil") {
	$isi = 'Halloo';
	echo $isi;
}

else if(isset($_GET['upload']) && $_GET['upload'] == "foto") {
	if(!empty($_FILES["imfotoa"])) {
		$imfotoa	= $_FILES["imfotoa"];
		$ext		= pathinfo($_FILES["imfotoa"]["name"], PATHINFO_EXTENSION);
		$size		= $_FILES["imfotoa"]["size"];
		$tgl		= date("Y-m-d");		
	}
	
	if($imfotoa["error"] !== UPLOAD_ERR_OK) {
		echo '<div class="alert alert-warning">Gagal mengupload</div>';
		exit;
	}
	
	// Nama File yang aman
	$name = $_SESSION['nip'].".".$ext; // preg_replace("/[^A-Z0-9._-]/i","_",$imfotoa["name"]);
	
	// mencegah overwrite
	// $i = 0;
	// $parts = pathinfo($name);
	// $filefoto = UPLOAD_DIR . $name ;
	// if(file_exists($filfoto)) {
	// 	unlink(UPLOAD_DIR . $name . $ext);
	// } else {
	// 	$name = $name . $ext;
	// }
	/*
	while(file_exists(UPLOAD_DIR . $name)) {
		$i++;
		$name = $parts["filename"]."-".$i.".".$parts["extension"];
	}
	*/
	
	$success = move_uploaded_file($imfotoa["tmp_name"], UPLOAD_DIR . $name);
	if ($success) {
		$res = $db->prepare("UPDATE `um_pegawai` SET `imfotoa` = :imfotoa WHERE nip = :nip");
		$res->bindParam(":imfotoa", $name, PDO::PARAM_STR);
		$res->bindParam(":nip", $_SESSION['nip'], PDO::PARAM_STR);
		$res->execute();
		echo "Wooooyyyy";
		exit;
	}
	chmod(UPLOAD_DIR . $name, 0644);
}

else {
	
	/*
	$isi = '
	<div class="row profile">
		<div class="col-md-12">
			<!--BEGIN TABS-->
			<div class="tabbable tabbable-custom">

				<ul class="nav nav-tabs" id="profil">
					<li class="active"><a href="#tab_pegawai" data-url="'.$modul.'&lihat=profil" data-toggle="tab">Profil</a></li>
					<li><a href="#tab_akun" data-toggle="tab">Akun</a></li>
				</ul>

				<div class="tab-content">					
					<div id="tab_pegawai" class="tab-pane active fade in">
					
					</div>
					
					<div id="tab_akun" class="tab-pane fade in">				
						<div class="row profile-account">
							<div class="col-md-3">
								<ul id="akun" class="ver-inline-menu tabbable margin-bottom-10">
									<li class="active">
									  <a data-toggle="tab" href="#tab_user" data-url=""><i class="fa fa-cog"></i> Nama User</a>
									</li>
									<li>
									  <a data-toggle="tab" href="#tab_foto" data-url=""><i class="fa fa-picture-o"></i> Foto</a>
									</li>';
									if(isset($_SESSION['level']) && $_SESSION['level'] == 'admin') {
									$isi.='
									<li>
										<a data-toggle="tab" href="#tab_akses" data-url="">
										<i class="fa fa-eye"></i> Pengaturan Otoritas</a>
									</li>';
									}
									$isi.='
								</ul>
							</div>
											
							<div class="col-md-9">
								<div class="tab-content">								
									<div id="tab_user" class="tab-pane active fade in">
										
										<!-- GANTI USER DAN PASSWORD -->
										<form class="userpass-form" action="#" method="post">
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button><span></span>
										</div>
										<div class="form-group">
											<label class="control-label">Nama User</label>
											<div class="input-icon">
												<i class="fa fa-user"></i>
												<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Nama User" name="user" id="user" value="'.$user.'" />
											</div>
											<div class="help-block">Format Penulisan: huruf kecil semua dan tanpa spasi</div>
										</div>
										<div class="form-group">
											<label class="control-label">Password (*)</label>
											<div class="input-icon">
												<i class="fa fa-lock"></i>
												<input type="password" name="pass" id="pass" placeholder="Password" class="form-control" autocomplete="off" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label">Tulis Kembali Password (*)</label>
											<div class="input-icon">
												<i class="fa fa-check"></i>
												<input type="password" name="rpass" id="rpass" placeholder="Tulis kembali" class="form-control" autocomplete="off" />
											</div>
										</div>
										<div class="note note-warning">
										* Kalo Password ga diganti kosongin aja inputan selain <span class="label label-primary">Nama User</span> dan ga perlu juga nekan tombol <span class="label label-success">Simpan</span>
										</div>
										<div class="margin-top-10">
											
										</div>
										<div class="margin-top-10">
											<input type="hidden" id="uid" name="uid" value="'.$id.'" readonly="readonly">
											<input type="hidden" id="aksi" name="aksi" value="userpass" readonly="readonly">
											<button type="submit" id="submituser" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
										</div>
										</form>								
									
									</div> <!-- END USER -->

									
									<!-- GANTI FOTO PROFIL -->
									<div id="tab_foto" class="tab-pane fade in">
										<div class="note note-warning">
										Foto yang dibaca sistem pake format JPG dengan kondisi baru atau bagus.<br>
										Tidak disarankan pake foto Jadul apalagi foto Jaman Jahiliyah.<br>#moveondonk.
										</div>
										
										<div class="progress progress-striped display-hide">
											<div id="progressBar" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
												<span class="sr-only">0% Complete </span>
											</div>
										</div>
										
										<div class="msg alert alert-info text-left hidden">Hallo</div>
										
										<form id="userfoto-form" name="userfoto-form" action="#" method="post" role="form">
											<div class="form-group">
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">';
													$sql = "SELECT imfotoa FROM um_pegawai WHERE nip = :nip";
													$res = $db->prepare($sql);
													$res->bindParam(":nip", $_SESSION['nip'], PDO::PARAM_STR);
													$res->execute();
													$r = $res->fetch(PDO::FETCH_ASSOC);
													extract($r);
													if($imfotoa != "") {
														$isi.='<img src="'. UPLOAD_DIR . $imfotoa .'" alt=""/>';
													} else {
														$isi.='<img src="img/no_image.png" alt=""/>';
													}
													$isi.='
													</div>
													<div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
													<div>
														<span class="btn btn-default btn-file">
															<span class="fileinput-new">Pilih</span>
															<span class="fileinput-exists">Ganti</span>
															<input type="file" id="imfotoa" name="imfotoa" accept=".jpg">
														</span>
														<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput"> Hapus</a>
													</div>
												</div>
											</div>
											<div class="margin-top-10">
												<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
											</div>
										</form>
									</div> <!-- END GANTI FOTO -->
									
									
									<!-- USER AKSES -->
									<div id="tab_akses" class="tab-pane fade in">
										<form action="#" >
										<table class="table table-bordered table-striped">
										<!--
										<tr>
										  <td>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus..</td>
										  <td><label class="uniform-inline"><input type="radio" name="optionsRadios1" value="option1"/>Yes </label>
										  <label class="uniform-inline"><input type="radio" name="optionsRadios1" value="option2" checked/>No </label>
										  </td>
										</tr>
										-->
										<tr>
											<td>Level Akses<br><span class="help-block">untuk level Admin tidak perlu centang otoritas dibawah ini.</span></td>
											<td><select name="level" id="level" class="form-control input-sm">
											<option value="">-- Pilih --</option>';
											$arr = array("user"=>"User", "admin"=>"Admin");
											foreach($arr as $key => $val) {
												$isi.='<option value="'.$key.'"'.(($key == $level) ? ' selected="selecter"':'').'>'.$val.'</option>';
											}
											$isi.='</select></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-Kepegawaian</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-SKP</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-Surat Tugas Pengawasan</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-Auditor</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										</table>
										<!--end profile-settings-->
										
										<div class="margin-top-10">
										  <a href="#" class="btn btn-success">Simpan</a>
										</div>
										</form>
									</div> <!-- END USER AKSES -->
									
									
								</div>
							</div>
							<!--end col-md-9-->	
					
						</div>
					</div>
					<!--end tab-pane-->
				</div>
			</div>
			<!--END TABS-->
		</div>
	</div>
	';
	*/
	
	$isi = '
	<div class="row profile">
		<div class="col-md-12">
			<!--BEGIN TABS-->
			<div class="tabbable tabbable-custom">
			
				<ul class="nav nav-tabs" id="profil">
					<li class="active"><a href="#tab_pegawai" data-url="'.$modul.'&lihat=profil" data-toggle="tab">Profil</a></li>
					<li><a href="#tab_akun" data-toggle="tab">Akun</a></li>
				</ul>			
			
				<div class="tab-content">					
				
					<div id="tab_pegawai" class="tab-pane active fade in">					
					</div>
					
					<div id="tab_akun" class="tab-pane fade in">				
						<div class="row profile-account">
							
							<div class="col-md-3">
								<ul id="akun" class="ver-inline-menu tabbable margin-bottom-10">
									<li class="active">
									  <a data-toggle="tab" href="#tab_user" data-url=""><i class="fa fa-cog"></i> Nama User</a>
									</li>
									<li>
									  <a data-toggle="tab" href="#tab_foto" data-url=""><i class="fa fa-picture-o"></i> Foto</a>
									</li>';
									if(isset($_SESSION['level']) && $_SESSION['level'] == 'admin') {
									$isi.='
									<li>
										<a data-toggle="tab" href="#tab_akses" data-url="">
										<i class="fa fa-eye"></i> Pengaturan Otoritas</a>
									</li>';
									}
									$isi.='
								</ul>
							</div>
							
							<div class="col-md-9">
								<div class="tab-content">								
									<div id="tab_user" class="tab-pane active fade in">
										
										<!-- GANTI USER DAN PASSWORD -->
										<form class="userpass-form" action="#" method="post">
										<div class="alert alert-danger display-hide">
											<button class="close" data-close="alert"></button><span></span>
										</div>
										<div class="form-group">
											<label class="control-label">Nama User</label>
											<div class="input-icon">
												<i class="fa fa-user"></i>
												<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Nama User" name="user" id="user" value="'.$user.'" />
											</div>
											<div class="help-block">Format Penulisan: huruf kecil semua dan tanpa spasi</div>
										</div>
										<div class="form-group">
											<label class="control-label">Password (*)</label>
											<div class="input-icon">
												<i class="fa fa-lock"></i>
												<input type="password" name="pass" id="pass" placeholder="Password" class="form-control" autocomplete="off" />
											</div>
										</div>
										<div class="form-group">
											<label class="control-label">Tulis Kembali Password (*)</label>
											<div class="input-icon">
												<i class="fa fa-check"></i>
												<input type="password" name="rpass" id="rpass" placeholder="Tulis kembali" class="form-control" autocomplete="off" />
											</div>
										</div>
										<div class="note note-warning">
										* Jika Password tidak diganti kosongin aja inputan selain <span class="label label-primary">Nama User</span> dan tidak perlu menekan tombol <span class="label label-success">Simpan</span>
										</div>
										<div class="margin-top-10">
											
										</div>
										<div class="margin-top-10">
											<input type="hidden" id="uid" name="uid" value="'.$id.'" readonly="readonly">
											<input type="hidden" id="aksi" name="aksi" value="userpass" readonly="readonly">
											<button type="submit" id="submituser" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
										</div>
										</form>								
									
									</div> <!-- END USER -->
									
									
									<!-- GANTI FOTO PROFIL -->
									<div id="tab_foto" class="tab-pane fade in">
										<div class="note note-warning">Foto yang dibaca sistem pake format JPG dengan kondisi baru atau bagus</div>
										
										<div class="progress progress-striped display-hide">
											<div id="progressBar" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
												<span class="sr-only">0% Complete </span>
											</div>
										</div>
										
										<div class="msg alert alert-info text-left hidden">Hallo</div>
										
										<form id="userfoto-form" name="userfoto-form" action="#" method="post" role="form">
											<div class="form-group">
												<div class="fileinput fileinput-new" data-provides="fileinput">
													<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">';													
													$sql = "SELECT um_pegawai.imfotob FROM um_pegawai WHERE nip = :nip";
													$res = $db->prepare($sql);
													$res->bindParam(":nip", $_SESSION['nip'], PDO::PARAM_STR);
													$res->execute();
													$r = $res->fetch(PDO::FETCH_ASSOC);
													if($r['imfotob'] != "") {
														$isi.='<img src="'. UPLOAD_DIR . $imfotoa .'" alt=""/>';
													} else {
														$isi.='<img src="img/no_image.png" alt=""/>';
													}
													$isi.='
													</div>
													<div class="fileinput-preview fileinput-exists thumbnail" style="width: 200px; height: 150px;"></div>
													<div>
														<span class="btn btn-default btn-file">
															<span class="fileinput-new">Pilih</span>
															<span class="fileinput-exists">Ganti</span>
															<input type="file" id="imfotoa" name="imfotoa" accept=".jpg">
														</span>
														<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput"> Hapus</a>
													</div>
												</div>
											</div>
											<div class="margin-top-10">
												<button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Simpan</button>
											</div>
										</form>
									</div> <!-- END GANTI FOTO -->
									
									
									
									<!-- USER AKSES -->
									<div id="tab_akses" class="tab-pane fade in">
										<form action="#" >
										<table class="table table-bordered table-striped">
										<!--
										<tr>
										  <td>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus..</td>
										  <td><label class="uniform-inline"><input type="radio" name="optionsRadios1" value="option1"/>Yes </label>
										  <label class="uniform-inline"><input type="radio" name="optionsRadios1" value="option2" checked/>No </label>
										  </td>
										</tr>
										-->
										<tr>
											<td>Level Akses<br><span class="help-block">untuk level Admin tidak perlu centang otoritas dibawah ini.</span></td>
											<td><select name="level" id="level" class="form-control input-sm">
											<option value="">-- Pilih --</option>';
											$arr = array("user"=>"User", "admin"=>"Admin");
											foreach($arr as $key => $val) {
												$isi.='<option value="'.$key.'"'.(($key == $level) ? ' selected="selecter"':'').'>'.$val.'</option>';
											}
											$isi.='</select></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-Kepegawaian</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-SKP</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-Surat Tugas Pengawasan</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										<tr>
										  <td>Akses Menu <em>e</em>-Auditor</td>
										  <td><label class="uniform-inline"><input type="checkbox" value=""/> Ya </label></td>
										</tr>
										</table>
										<!--end profile-settings-->
										
										<div class="margin-top-10">
										  <a href="#" class="btn btn-success">Simpan</a>
										</div>
										</form>
									</div> <!-- END USER AKSES -->
								
								</div>
							</div> <!--end col-md-9 -->
							
						</div>
					</div>
					
				</div> <!--end tab-pane-->
				
			</div> <!--END TABS-->			
		</div>
	</div>
	';
	
	$tpl = new template;
	$tpl->load('themes/conquer/page_profile.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>