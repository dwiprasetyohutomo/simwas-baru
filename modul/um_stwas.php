<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$theme 		= 'themes/conquer';
$tabelData	= 'um_stwas';
$modul		= 'modul.php?ke=um_stwas';
$pageTitle	= 'Surat Tugas (Pengawasan)';

$pageBreadcrumb = '
<li><i class="fa fa-home"></i><a href="index.php">Beranda</a> <i class="fa fa-angle-right"></i></li>
<li><span>Umum Kepegawaian</span> <i class="fa fa-angle-right"></i></li>
<li><span>Surat Tugas</span> <i class="fa fa-angle-right"></i></li>
<li><span>Pengawasan</span></li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-um-stwas.js"></script>
<!-- <script type="text/javascript" src="'.$theme.'/assets/scripts/form-um-stwas-tim.js"></script> -->
<script type="text/javascript" src="'.$theme.'/assets/scripts/tasks.js"></script>
';
$initB = '
	SuratTugasWas.init();
	// SuratTugasWasTim.init();
	Tasks.initDashboardWidget();
';

// MAIN CODE
if (isset($_POST['aksi'])) {
	try {
		if ($_POST['aksi'] == "stwas_simpan") {
			$sql = "INSERT INTO 
			  `um_stwas`(
			  `idst`,
			  `nomor`,
			  `jenis`,
			  `kegiatan`,
			  `tahun`,
			  `lama`,
			  `anggaran`,
			  `anggaran2`,
			  `tglbrkt`,
			  `tglkmbl`,
			  `tglsrt`,
			  `inspnip`,
			  `inspnama`,
			  `inspgol`
			) VALUE (
			  :idst,
			  :nomor,
			  :jenis,
			  :kegiatan,
			  :tahun,
			  :lama,
			  :anggaran,
			  :anggaran2,
			  :tglbrkt,
			  :tglkmbl,
			  :tglsrt,
			  :inspnip,
			  :inspnama,
			  :inspgol)";
		}
		if ($_POST['aksi'] == "stwas_update") {
			$sql = "UPDATE `um_stwas` SET
			`idst` = :idst,
			`nomor` = :nomor,
			`jenis` = :jenis,			
			`kegiatan` = :kegiatan,
			`tahun` = :tahun,
			`lama` = :lama,
			`anggaran` = :anggaran,
			`anggaran2` = :anggaran2,
			`tglbrkt` = :tglbrkt,
			`tglkmbl` = :tglkmbl,
			`tglsrt` = :tglsrt,
			`inspnip` = :inspnip,
			`inspnama` = :inspnama,
			`inspgol` = :inspgol
			WHERE `id` = :uid";
		}				
		$res = $db->prepare($sql);
		
		$nomor 		= Koleksi::EmptyPostVal($_POST['nomor']);
		$jenis 		= Koleksi::EmptyPostVal($_POST['jenis']);
		$kegiatan	= Koleksi::EmptyPostVal($_POST['kegiatan']);
		$tahun		= Koleksi::EmptyPostVal($_POST['tahun']);
		$lama		= Koleksi::EmptyPostVal($_POST['lama']);
		$anggaran	= Koleksi::EmptyPostVal($_POST['anggaran']);
		if(!empty($anggaran) && $anggaran == 'Rutin') $anggaran2 = null;
		if(!empty($anggaran) && $anggaran == 'Lain') $anggaran2	= Koleksi::EmptyPostVal($_POST['anggaran2']);
		$tglbrkt	= Koleksi::EmptyTglVal($_POST['tglbrkt']);
		$tglkmbl	= Koleksi::EmptyTglVal($_POST['tglkmbl']);
		$tglsrt		= Koleksi::EmptyTglVal($_POST['tglsrt']);
		$inspnip	= Koleksi::EmptyPostVal($_POST['inspnip']);
		$inspnama	= Koleksi::EmptyPostVal($_POST['inspnama']);
		$inspgol	= Koleksi::EmptyPostVal($_POST['inspgol']);
		
		// ID Unik
		if($nomor <> '' || $tahun <> '') {
			$idst	= $nomor.'/'.$tahun;
		} else {
			$idst	= null;
		}
				
		// $tahun		= strtok($tglsrt,'-');
		// $inspnip 	= preg_replace('/\D/','',$_POST['inspnip']);
		
		// Pasangkan dan Eksekusi
		$res->bindParam(":idst", $idst, PDO::PARAM_STR);
		$res->bindParam(":nomor", $nomor, PDO::PARAM_STR);
		$res->bindParam(":jenis", $jenis, PDO::PARAM_STR);
		$res->bindParam(":kegiatan", $kegiatan, PDO::PARAM_STR);
		$res->bindParam(":tahun", $tahun, PDO::PARAM_STR);
		$res->bindParam(":lama", $lama, PDO::PARAM_STR);
		$res->bindParam(":anggaran", $anggaran, PDO::PARAM_STR);
		$res->bindParam(":anggaran2", $anggaran2, PDO::PARAM_STR);
		$res->bindParam(":tglbrkt", $tglbrkt, PDO::PARAM_STR);
		$res->bindParam(":tglkmbl", $tglkmbl, PDO::PARAM_STR);
		$res->bindParam(":tglsrt", $tglsrt, PDO::PARAM_STR);
		$res->bindParam(":inspnip", $inspnip, PDO::PARAM_STR);
		$res->bindParam(":inspnama", $inspnama, PDO::PARAM_STR);
		$res->bindParam(":inspgol", $inspgol, PDO::PARAM_STR);

		if($_POST['aksi'] == "stwas_update")
			$res->bindParam(":uid", $_POST['uid'], PDO::PARAM_INT);
		
		$res->execute();
		
		if ($_POST['aksi'] == "stwas_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Proses menyimpan data berhasil tanpa kendala.' );
		if ($_POST['aksi'] == "stwas_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa kendala.' );		

		
		// Melihat isian
		/*
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}
		$response = array( 'status'=>'gagal','pesan'=>$sql.'<br><br>'.$err );
		*/
	}
	catch(PDOException $e) {
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}
		$response = array(
			'status'=>'gagal',
			'pesan'=>'<span class="label label-danger">Error:</span>&nbsp;'.$e->getMessage().'<hr>'.$sql.'<hr>'.$err
		);
	}
	echo json_encode($response); // Tampilkan hasil Eksekusi
	$db = null; // Tutup koneksi
}

else if(isset($_GET['form']) && $_GET['form'] == 'baru') {
		
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Terdapat isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	
	<div class="form-group">
		
		<!-- Kiri -->
		<div class="col-md-12">';
			
			// Auto Inspektur
			$q1 = "SELECT
			  i.nip AS inspnip, 
			  CONCAT(IF(ISNULL(i.gelardp), '', CONCAT(i.gelardp, '. ')), UPPER(i.nama), IF(ISNULL(i.gelarbk), '', CONCAT(', ', i.gelarbk))) AS inspnama,
			  t.golru AS inspgol
			FROM
			  um_pegawai i
			  INNER JOIN um_jabatan n ON (i.nip = n.nip)
			  INNER JOIN(SELECT nip, MAX(tmtjab) AS maxjab FROM um_jabatan GROUP BY nip) AS n2 ON i.nip = n2.nip AND n.tmtjab = n2.maxjab
			  INNER JOIN um_pangkat t ON (i.nip = t.nip)
			  INNER JOIN(SELECT nip, MAX(golru) AS maxgol FROM um_pangkat GROUP BY nip) AS t2 ON i.nip = t2.nip AND t.golru = t2.maxgol
			WHERE
			  n.jabatan = 'Inspektur'";
			$res1 = $db->prepare($q1);
			$res1->execute();
			$r1 = $res1->fetch(PDO::FETCH_ASSOC);
			
			$inspnip = $r1['inspnip'];
			$inspnama = $r1['inspnama'];
			$inspgol = $r1['inspgol'];
			
			$isi.='
			<!-- <h4 class="form-section">Surat Tugas</h4> -->
			
			<div class="form-group">
				<label class="col-md-3 control-label">Kegiatan<span class="required"></span></label>
				<div class="col-md-9">
					<select id="jenis" name="jenis" class="form-control input-medium" data-placeholder="Pilih...">
					<option value=""></option>';
					$pil = array(
						"reguler"=>"Reguler",
						"monev"=>"Monev",
						"reviu"=>"Reviu",
						"riksus"=>"Riksus",
						"sakip"=>"Sakip",
						"spip"=>"SPIP",
						"tl"=>"Tindak Lanjut");
					foreach($pil as $key => $val) {
						$isi.='<option value="'.$key.'">'.$val.'</option>';
					}
					$isi.='</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">No. Surat Tugas</label>
				<div class="col-md-9">
					<span class="label label-info inline">700/</span>
					<input type="text" id="nomor" name="nomor" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" autocomplete="on">
					<span class="label label-info">/Inspektorat</span>
				</div>
			</div>		
			<div class="form-group">
				<label class="col-md-3 control-label">Uraian Kegiatan</label>
				<div class="col-md-9">
					<textarea id="kegiatan" name="kegiatan" class="form-control input-sm" style="resize:none; overflow-y:scroll" rows="2"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Tahun Anggaran</label>
				<div class="col-md-9">
					<div class="input-group input-small date year-picker" data-date="" data-date-format="yyyy">
						<input type="text" id="tahun" name="tahun" value="'.date('Y').'" class="form-control">
						<span class="input-group-btn"><button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Lama (hari)</label>
				<div class="col-md-9">
					<div id="spinlama" style="float:left">
						<div class="input-group input-small">
							<input type="text" id="lama" name="lama" class="form-control spinner-input" maxlength="3">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn btn-xs spinner-up btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn btn-xs spinner-down btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
					<!-- <div style="float:left; margin: 8px 5px 0 5px"><span class="label label-danger">hari</span></div> -->					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Beban Anggaran</label>
				<div class="col-md-9">
					<select id="anggaran" name="anggaran" class="form-control input-large" data-placeholder="Pilih...">
					<option value=""></option>';
					$jen = array ( "rutin"=>"Rutin Inspektorat Kota Tarakan", "lain"=>"Sumber Lain..." );
					foreach($jen as $key => $val) {
						$p = (($key == 'Rutin') ? ' selected="selected"' : '');
						$isi.='<option value="'.$key.'"'.$p.'>'.$val.'</option>';
					}
					$isi.='</select>
					<input type="text" id="anggaran2" name="anggaran2" placeholder="" class="form-control input-large margin-top-15 placeholder-no-fix" autocomplete="on">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Berangkat/Kembali</label>
				<div class="col-md-9">
					<div class="input-group input-large date-picker input-daterange" data-date="" data-date-format="dd-mm-yyyy">
						<input type="text" name="tglbrkt" id="tglbrkt" class="form-control">
						<span class="input-group-addon">s/d</span>
						<input type="text" name="tglkmbl" id="tglkmbl" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Surat Tugas Tgl</label>
				<div class="col-md-9">
					<div class="input-group input-small date date-picker" data-date="" data-date-format="dd-mm-yyyy">
						<input type="text" name="tglsrt" id="tglsrt" class="form-control input-small">
						<span class="input-group-btn"><button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button></span>
					</div>					
				</div>
			</div>
			
			<div class="panel panel-info hidden">
				<div class="panel-heading">
					<h3 class="panel-title">Informasi Inspektur / Penyesuaian Surat Tugas</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
						<label class="col-md-3 control-label">Inspektur</label>
						<div class="col-md-9">
							<select id="inspnip" name="inspnip" class="form-control input-large" data-placeholder="Pilih...">
							<option value=""></option>';
							$resA = $db->prepare("SELECT
							p.nip,
							CONCAT(IF(p.gelardp != '', CONCAT(p.gelardp, '. '), ''), p.nama, IF(p.gelarbk != '', CONCAT(', ', p.gelarbk), '')) AS nama
							FROM um_pegawai p
							INNER JOIN um_jabatan j ON (p.nip = j.nip)
							WHERE j.`jabatan` = 'Inspektur'");
							$resA->execute();
							while($rowA = $resA->fetch(PDO::FETCH_ASSOC)) {
								$p = (($inspnip == $rowA['nip']) ? ' selected="selected"' : '');
								$isi.='<option value="'.$rowA['nip'].'"'.$p.'>'.$rowA['nama'].'</option>';
							}			
							$isi.='</select>
							<input type="hidden" id="inspnama" name="inspnama" value="'.$inspnama.'" placeholder="" class="form-control margin-top-15 placeholder-no-fix" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Gol.Ruang</label>
						<div class="col-md-9">
							<select id="inspgol" name="inspgol" class="form-control input-medium"  data-placeholder="Pilih...">
							<option value=""></option>';
							$resB = $db->prepare("SELECT kdgol, pangkat FROM kd_golru");
							$resB->execute();
							while($rowB = $resB->fetch(PDO::FETCH_ASSOC)) {
								$p = (($inspgol == $rowB['kdgol']) ? ' selected="selected"' : '');
								$isi.='<option value="'.$rowB['kdgol'].'"'.$p.'>'.$rowB['pangkat'].'</option>';
							}
							$isi.='</select>
						</div>
					</div>
					
				</div> <!-- panel-body -->
			</div> <!-- panel -->
			
			
		</div> <!-- end Kolom Kiri -->
		
		<!-- Kanan -->
		<!--
		<div class="col-md-6">
		<h4 class="form-section">Tim Pemeriksa <span class="pull-right">Testing</span></h4>
		-->
		
	</div> <!-- end form-group -->
	';
	echo $isi;
}

else if(isset($_GET['form']) && $_GET['form'] == 'edit') {
	
	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid']; else $uid = '';
	$res = $db->prepare("SELECT * FROM `um_stwas` WHERE id = :uid");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Terdapat isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	
	<div class="form-group">
		
		<!-- Kiri -->
		<div class="col-md-12">
		
			<!-- <h4 class="form-section">Surat Tugas</h4> -->
					
			<div class="form-group">
				<label class="col-md-3 control-label">Kegiatan</label>
				<div class="col-md-9">
					<select id="jenis" name="jenis" class="form-control input-medium" data-placeholder="Pilih...">
					<option value=""></option>';
					$pil = array(
						"reguler"=>"Reguler",
						"monev"=>"Monev",
						"reviu"=>"Reviu",
						"riksus"=>"Riksus",
						"sakip"=>"Sakip",
						"spip"=>"SPIP",
						"tl"=>"Tindak Lanjut");
					foreach($pil as $key => $val) {
						$p = (($r['jenis'] == $key) ? ' selected="selected"' : '');
						$isi.='<option value="'.$key.'"'.$p.'>'.$val.'</option>';
					}
					$isi.='</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">No. Surat Tugas</label>
				<div class="col-md-9">
					<span class="label label-info inline">700/</span>
					<input type="text" id="nomor" name="nomor" value="'.$r['nomor'].'" placeholder="700/...../Inspektorat" class="form-control placeholder-no-fix input-xsmall inline" autocomplete="on">
					<span class="label label-info inline">/Inspektorat</span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Uraian Kegiatan</label>
				<div class="col-md-9">
					<textarea id="kegiatan" name="kegiatan" class="form-control input-sm" style="resize:none; overflow-y:scroll" rows="2">'.$r['kegiatan'].'</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Tahun Anggaran</label>
				<div class="col-md-9">
					<div class="input-group input-small date year-picker" data-date="" data-date-format="yyyy">
						<input type="text" id="tahunk" name="tahun" value="'.$r['tahun'].'" class="form-control">
						<span class="input-group-btn"><button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button></span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Lama (hari)</label>
				<div class="col-md-9">
					<div id="spinlama" style="float:left">
						<div class="input-group input-small">
							<input type="text" id="lama" name="lama" value="'.$r['lama'].'" class="form-control spinner-input" maxlength="3">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn spinner-up btn-xs btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn spinner-down btn-xs btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
					<!-- <div style="float:left; margin: 8px 5px 0 5px"><span class="label label-danger">hari</span></div> -->					
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Beban Anggaran</label>
				<div class="col-md-9">
					<select id="anggaran" name="anggaran" class="form-control input-large" data-placeholder="Pilih...">
					<option value=""></option>';
					$jen = array (
						"rutin"=>"Rutin Inspektorat Kota Tarakan",
						"lain"=>"Sumber Lain..."
					);
					foreach($jen as $key => $val) {
						$p = (($r['anggaran'] == $key) ? ' selected="selected"' : '');
						$isi.='<option value="'.$key.'"'.$p.'>'.$val.'</option>';
					}
					$isi.='</select>
					<input type="text" id="anggaran2" name="anggaran2" value="'.$r['anggaran2'].'" placeholder="" class="form-control input-large margin-top-15 placeholder-no-fix" autocomplete="on">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Berangkat/Kembali</label>
				<div class="col-md-9">
					<div class="input-group input-large date-picker input-daterange" data-date="" data-date-format="dd-mm-yyyy">
						<input type="text" name="tglbrkt" id="tglbrkt" value="'.Koleksi::tanggalSQL($r['tglbrkt']).'" class="form-control">
						<span class="input-group-addon">s/d</span>
						<input type="text" name="tglkmbl" id="tglkmbl" value="'.Koleksi::tanggalSQL($r['tglkmbl']).'" class="form-control">
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Surat Tugas Tgl.</label>
				<div class="col-md-9">
					<div class="input-group input-small date date-picker" data-date="" data-date-format="dd-mm-yyyy">
						<input type="text" name="tglsrt" id="tglsrt" value="'.Koleksi::tanggalSQL($r['tglsrt']).'" class="form-control input-small">
						<span class="input-group-btn"><button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button></span>
					</div>
				</div>
			</div>	
			
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Informasi Inspektur / Penyesuaian Surat Tugas</h3>
				</div>
				<div class="panel-body">
				
					<div class="form-group">
						<label class="col-md-3 control-label">Inspektur</label>
						<div class="col-md-9">
							<select id="inspnip" name="inspnip" class="form-control input-large" data-placeholder="Pilih...">
							<option value=""></option>';
							$resA = $db->prepare("SELECT
							p.nip,
							CONCAT(IF(p.gelardp != '', CONCAT(p.gelardp, '. '), ''), p.nama, IF(p.gelarbk != '', CONCAT(', ', p.gelarbk), '')) AS nama
							FROM um_pegawai p
							INNER JOIN um_jabatan j ON (p.nip = j.nip)
							WHERE j.`jabatan` = 'Inspektur'");
							$resA->execute();
							while($rowA = $resA->fetch(PDO::FETCH_ASSOC)) {
								$p = (($r['inspnip'] == $rowA['nip']) ? ' selected="selected"' : '');
								$isi.='<option value="'.$rowA['nip'].'"'.$p.'>'.$rowA['nama'].'</option>';
							}			
							$isi.='</select>
							<input type="hidden" id="inspnama" name="inspnama" placeholder="" class="form-control margin-top-15 placeholder-no-fix" readonly>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Gol.Ruang</label>
						<div class="col-md-9">
							<select id="inspgol" name="inspgol" class="form-control input-medium"  data-placeholder="Pilih...">
							<option value=""></option>';
							$resB = $db->prepare("SELECT kdgol, pangkat FROM kd_golru");
							$resB->execute();
							while($rowB = $resB->fetch(PDO::FETCH_ASSOC)) {
								$p = (($r['inspgol'] == $rowB['kdgol']) ? ' selected="selected"' : '');
								$isi.='<option value="'.$rowB['kdgol'].'"'.$p.'>'.$rowB['pangkat'].'</option>';
							}
							$isi.='</select>
						</div>
					</div>
					
				</div> <!-- panel-body -->
			</div> <!-- panel -->

		</div> <!-- end Kolom Kiri -->
		
		<!-- Kanan -->
		<!--
		<div class="col-md-6">
		<h4 class="form-section">Tim Pemeriksa <span class="pull-right">Testing</span></h4>
		-->
		
	</div> <!-- end form-group -->
	';

	echo $isi;
}

else if(isset($_GET['form']) && $_GET['form'] == 'hapus') {
	
	header("Content-type: application/json; charset=UTF-8");
	
	// if(isset($_POST['uid'])) {
	if(isset($_POST['uid']) && $_POST['uid'] != '') {
		try {		
			$res = $db->prepare("DELETE FROM $tabelData WHERE `id`=:uid");
			$res->bindParam(':uid', $_POST['uid']);
			$res->execute();
			$response = array( 'status'=>'sukses','pesan'=>'' );
		} catch(PDOException $e) {
			$response = array( 'status'=>'gagal','pesan'=>'<h4>Perhatian!</h4><p>'.$sql.'<br><br>'.$e->getMessage().'</p>' );
		}
		echo json_encode($response);
		$db = null;
	}
}

else if(isset($_GET['tabel']) && $_GET['tabel'] == "kosongkan") {
	header("Content-type: application/json; charset=UTF-8");
	try {
		$res = $db->prepare("TRUNCATE TABLE $tabelData");
		$res->execute();
		if ($res->rowCount() == 0) {
			$response = array( 'status'=>'sukses', 'pesan'=>'' );
		} else {
			$response = array( 'status'=>'gagal', 'pesan'=>'Ada yang salah saat mengosongkan tabel' );
		}
	} catch(PDOException $e) {
		$response = array( 'status'=>'gagal', 'pesan'=>'Error: '.$e->getMessage().'' );
	}
	echo json_encode($response);
}

else if(isset($_GET['lihat']) && $_GET['lihat'] == 'tabeldata') {
	
	header("Content-type:application/json");
	
	// FIX untuk satu tabel
	/*	
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/
	
	// Kolom yang bisa di Sortir
	$columns = array(
		1=>'nomor',
		5=>'tglsrt'
	);

	// Untuk 2 Tabel	
	$sql = "SELECT * FROM um_stwas WHERE jenis <> ''";
	
	// Filter berdasarkan kategori tambahan
	if(isset($_REQUEST['ftahun']) && $_REQUEST['ftahun'] != '') $sql.=" AND tahun='".$_REQUEST['ftahun']."'";
	if(isset($_REQUEST['fjenis']) && $_REQUEST['fjenis'] != '') $sql.=" AND jenis='".$_REQUEST['fjenis']."'";
	
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" AND nomor LIKE '%".$_REQUEST['search']['value']."%'";
		$sql.=" OR kegiatan LIKE '%".$_REQUEST['search']['value']."%'";
	}
			
	$res = $db->prepare($sql);
	$res->execute();	
	
	$totalData = $res->rowCount();
	$totalFiltered = $totalData;
		
	if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];

	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	$p = '';
	while($r = $res->fetch(PDO::FETCH_BOTH)) {		
		//$no++;
		$nestedData = array();
		// Nomor Urut
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		// Isi Tabel
		$nestedData[] = '700/'.$r['nomor'].'/inspektorat';
		$nestedData[] = '<center>
		<a class="btn btn-xs btn-primary" href="#" id="personil" role="button" data-id="'.$r['id'].'" data-idst="'.$r['idst'].'" data-thn="'.$r['tahun'].'" data-hari="'.$r['lama'].'" data-target="#daftartim-modal" data-toggle="modal">
		<i class="fa fa-users"></i> <span class="hidden-480"> Personil</span>
		</a></center>';
		$jns = array("monev"=>"Monev","reguler"=>"Reguler","reviu"=>"Reviu","riksus"=>"Riksus","sakip"=>"Sakip","spip"=>"SPIP","tl"=>"Tindak Lanjut");
		$jenis = isset($jns[$r['jenis']]) ? $jns[$r['jenis']] : null;
		$nestedData[] = '<span class="label label-sm label-success">'.$jenis.'</span>';
		$nestedData[] = $r['kegiatan'];
		// $nestedData[] = '<center>'.Koleksi::tanggalIndo($row['tglbrkt']).'<br>s/d<br>'.Koleksi::tanggalIndo($row['tglkmbl']).'<br><span class="label label-sm label-info">'.$row['lama'].' hari</span>';
		$nestedData[] = '<center>'.Koleksi::tanggalSQL($r['tglbrkt']).' s/d '.Koleksi::tanggalSQL($r['tglkmbl']).' <span class="label label-sm label-info">'.$r['lama'].' Hari</span></center>';
		// $nestedData[] = '<center>'.Koleksi::tanggalIndo($row['tglsrt']).'</center>';
		$nestedData[] = '<center>'.Koleksi::tanggalSQL($r['tglsrt']).'</center>';
		// Opsi
		$nestedData[] = '<center><div style="width:85px; border:0px solid #000">
		<div class="task-config">

			<div class="task-config-btn btn-group">
				<a href="#" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" data-close-others="true">
					Data <i class="fa fa-angle-down"></i>
				</a>
				<ul class="dropdown-menu pull-right" role="menu">
					<li><a href="#" id="view" data-id="'.$r['id'].'" data-target="#report-modal" data-toggle="modal"><i class="fa fa-file-text-o"></i> Lihat</a></li>
					<li class="divider"></li>
					<li><a href="#" id="edit" data-id="'.$r['id'].'" data-target="#form-modal" data-toggle="modal"><i class="fa fa-edit"></i> Ubah</a></li>
					<li><a href="#" id="hapus" data-id="'.$r['id'].'"><i class="fa fa-times"></i> Hapus</a></li>
				</ul>
			</div>
		</div>
		
		</div></center>';
		
		$data[] = $nestedData;
		$no++;
	}	
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {
	$isi ='
	<div class="note note-info">
		<p>Daftar Surat Tugas Pengawasan berdasarkan Program Kerja Pemeriksaan Tahunan (PKPT)</p>
	</div>
	
	<div class="row">	
		<div class="col-md-12 col-sm-12">
	
			<div class="portlet tasks-widget">
				<div class="portlet-title">
					<div class="caption">Daftar Surat Tugas Pengawasan</div>
					<div class="actions">
						<div class="btn-group">
							<button id="menubaru" data-target="#form-modal" data-toggle="modal" type="button" class="btn btn-xs btn-primary">
								<i class="fa fa-plus"></i> <span class="hidden-480">Surat Tugas Baru</span>
							</button>
							<button class="btn btn-xs btn-primary dropdown-toggle" type="button" data-toggle="dropdown" data-hover="dropdown"><i class="fa fa-angle-down"></i></button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li><a href="#" id="menurefresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li><a href="#" id="menulaporan"><i class="fa fa-print"></i> Laporan</a></li>
								<li class="divider"></li>
								<li><a href="#" id="menuimport" data-target="#import-modal" data-toggle="modal"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="#" id="menuexport"><i class="glyphicon glyphicon-export"></i> Export</a></li>';
								if(isset($_SESSION['level']) && $_SESSION['level'] == 'admin') {
									$isi.='<li class="divider"></li>
									<li><a href="#" id="menukosongkan"><i class="fa fa-recycle"></i> Kosongkan</a></li>';
								}
							$isi.='
							</ul>
						</div> <!-- end btn-group -->
						<!--
						<div class="btn-group">
							<a class="btn btn-info btn-sm dropdown-toggle" href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							More <i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="#"><i class="i"></i> All Project</a></li>
								<li class="divider"></li>
								<li><a href="#">AirAsia</a>
								<li class="divider"></li>
								<li><a href="#">Pending <span class="badge badge-important"> 4 </span></a></li>
							</ul>
						</div>
						-->
					</div> <!-- end actions -->
				</div>
				
				<div class="portlet-body">
					<div class="task-content">
						
						<!-- <div class="scroller" style="height: 305px;" data-always-visible="1" data-rail-visible1="1"> -->

							<div class="table-container">
								
								<!-- Filtering -->
								<div class="table-actions-wrapper">
									<span></span>
									<form id="form-filter" class="form-horizontal">
										<select name="ftahun" id="ftahun" class="table-group-action-input form-control form-filter input-inline input-small input-sm">
										<option value="">Tahun...</option>';
										$resA = $db->prepare("SELECT tahun FROM um_stwas GROUP BY tahun ORDER BY tahun DESC");
										$resA->execute();
										while($rowA = $resA->fetch(PDO::FETCH_ASSOC)) {
											$isi.='<option value="'.$rowA['tahun'].'">'.$rowA['tahun'].'</option>';
										}
										$isi.='</select>
										<select name="fjenis" id="fjenis" class="table-group-action-input form-control form-filter input-inline input-small input-sm">
										<option value="">Jenis...</option>';
										$jenis = array("reguler"=>"Reguler","monev"=>"Monev","reviu"=>"Reviu","riksus"=>"Riksus","spip"=>"SPIP","tl"=>"Tindak Lanjut");
										foreach ($jenis as $key => $val) {
											$isi.='<option value="'.$key.'">'.$val.'</option>';
										}
										$isi.='</select>
										<button class="btn btn-sm btn-warning table-group-action-submit tooltips" data-placement="top" data-original-title="Filter"><i class="fa fa-filter"></i> <span class="hidden-480">Filter</span></button>
										<button class="btn btn-sm btn-danger table-group-action-reset tooltips" data-placement="top" data-original-title="Reset"><i class="fa fa-refresh"></i> <span class="hidden-480">Reset</span></button>
									</form>
								</div> <!-- End table-actions-wrapper -->
								<!-- End Filtering -->
								
								<!-- <div class="table-responsive"> -->
								<table id="tableAjax" width="100%" class="table table-striped table-hover dataTable" style="border: 1px solid #ddd">
								<thead>
								<tr class="heading">
									<th width="1%" class="cellcenter"></th>
									<th width="15%">Nomor ST.</th>
									<th width="8%" class="cellcenter">Tim</th>
									<th width="4%"></th>
									<th width="35%">Kegiatan</th>
									<th width="18%" class="cellcenter">Waktu</th>
									<th width="10%" class="cellcenter">Tgl.ST.</th>
									<th width="9%"></th>
								</tr>					
								</thead>
								<tbody>
								</tbody>
								</table>
								<!-- </div> --> <!-- end table-responsive -->
							</div> <!-- table-container -->
						<!-- </div> --> <!-- Scroller -->
					</div>
					
					<div class="task-footer">
						<span class="pull-right"><a href="javascript:void()">#AppsKita</a> &nbsp; </span>
					</div>
					
				</div> <!-- End portlet-body -->
			</div> <!-- End portlet -->
	
		</div><!-- /.col-md-12 col-sm-12 -->
	</div><!-- /.row -->
	
	<!--
	=============================
	MODAL DAFTAR TIM PEMERIKSA
	============================= -->
	<div id="daftartim-modal" class="modal fade" tabindex="-1" data-focus-on="input:first">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">				
					<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
					
					<div class="pull-right" style="padding-right:20px">
						<button id="timbaru" data-target="#tim-modal" data-toggle="modal" type="button" class="btn btn-sm btn-warning">
						<i class="fa fa-plus"></i> <span class="hidden-480">Tim Personil</span> <i class="fa fa-user"></i>
						</button>
					</div>
					<h4 class="modal-title"><i class="fa fa-group"></i> Daftar Tim Pemeriksa</h4>
					
				</div>
				
				<div class="modal-body form">
					<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
					<div class="form-body" style="max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden">

					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.form-modal -->
		
	<!--
	=============================
	MODAL TAMBAH / SIMPAN
	============================= -->
	<div id="tim-modal" class="modal fade" tabindex="-1" data-focus-on="input:first">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="tim-modal-form" name="tim-modal-form" class="form-horizontal" method="post" enctype="multipart/form-data">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="fa fa-group"></i> Manajemen Tim Pemeriksa</h4>
					</div>
					<div class="modal-body form">
						<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
						<div class="form-body">
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" id="uid" name="uid" readonly="readonly">
						<input type="hidden" id="aksi" name="aksi" readonly">
						<button type="submit" id="submit" name="submit" class="btn btn-success">Simpan</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
					</div>
				</form>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.form-modal -->

	<!--
	=============================
	MODAL ANGKA KREDIT PENGAWASAN
	============================= -->
	<div id="modal-tblpoin" class="modal fade" tabindex="-1" data-focus-on="input:first">
		<div class="modal-dialog">
			<div class="modal-content">			
				<div class="modal-header">
					<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Tabel Kegiatan dan Poin</h4>
				</div>
				<div class="modal-body" style="max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden">
					<table id="tblKegiatan" class="table table-hover">
					<thead>
					<tr>
					<th width="1%">Kode</th>
					<th width="90%">Kegiatan</th>
					<th width="9%">Poin</th>
					</tr>
					</thead>
					<tbody>
					</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<!-- <span class="label label-success">Sumber</span> Permenpan No: PER/60/M.PAN/6/2015, Perka BKN No: 28 Tahun 2015 -->
					<button type="button" data-dismiss="modal" class="btn btn-default">Tutup</button>
				</div>			
			</div>			
		</div>			
	</div>
	';
	
	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA','');
	$tpl->set('initB',$initB);
	$tpl->set('initC','');
	$tpl->publish();
}
?>