<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=um_keluar';
$theme		= 'themes/conquer';
$tabelData	= 'was_rinci';
$pageTitle	= 'Umum <small>Surat Keluar</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i>
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Pengawasan</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Rincian Rekomendasi</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-um-keluar.js"></script>
';
$initA = '
// var modul = "modul.php?ke=was_rinci";
var modul = window.location.href;
var pageTitle = "Rincian Rekomendasi";

// CRUD
var menuBaru 	= modul+"&rinci=baru";
var menuEdit	= modul+"&rinci=edit";

// Table Data
var srcUrl 	= modul+"&was=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
umst.init();
';
$initC = '';
// MAIN CODE

//DATA KD SIFAT SURAT
$kdsifat = array();
$query = "select * from kd_sifatsurat";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdsifat += [$sifat_id => $sifat_ket];
}

//DATA KD DISPOSISI
$kddispo = array();
$query = "select * from kd_disposisi";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kddispo += [$dispo_id => $dispo_ket];
}

$blank="";
if (isset($_POST['submit'])) {
	try {
		// Query Simpan
		if ($_POST['aksi'] == "keluar_simpan") {
			$sql = "INSERT INTO `um_keluar`
			(`keluar_tujuan`,
			`keluar_no,`
			`keluar_tgl,`
			`keluar_kirim,`
			`keluar_perihal,`			
			`keluar_catatan,`
			`keluar_file,`
			`keluar_arsip`
			`keluar_nourut`
			)
			VALUES
			(:tujuan,
			:no,
			:tgl,
			:kirim,
			:perihal,
			:catatan,
			:file,
			:arsip
			:nourut
            )";
            $res = $db->prepare($sql);
            $tahun = date("Y");
            $query = "select max(keluar_nourut) as urut from um_keluar where keluar_tgl like '%$tahun%' ";
            $stmt1 = $db->prepare($query);
            $stmt1->execute();
            while ($row1= $stmt1->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
                extract($row1);
                 if (is_null($st_no)){
                     $no=1;
                 }
                 else {
                   $no=$urut+1;
                 }
            }
            $res->bindParam(":nourut",$nourut);
		}
		// Query Update
		if ($_POST['aksi'] == "keluar_update") {
			$sql = "UPDATE `um_keluar` SET
			`keluar_tujuan` = :tujuan,
			`keluar_no` = :no,
			`keluar_tgl` = :tgl,
			`keluar_kirim` = :kirim,
			`keluar_perihal` = :perihal,
			`keluar_catatan` = :catatan,
			`keluar_file` = :file,
			`keluar_arsip` = :arsip
			WHERE `keluar_id` = :id";
            $res = $db->prepare($sql);
            $res->bindParam(":id", Koleksi::EmptyPostVal($_POST['id']));
		}

		// Persiapkan Query
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		//$nip = preg_replace('/\D/','', $_POST['nip']);
		//$nip = $_POST['nip'];

		// Pasangkan dan Eksekusi
		//if ($_POST['aksi'] == "cuti_simpan")
			//$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		if ($_POST['aksi'] == "keluar_simpan" || $_POST['aksi'] == "keluar_update"){
            $res->bindParam(":tujuan", Koleksi::EmptyPostVal($_POST['tujuan']));
            $res->bindParam(":no", Koleksi::EmptyPostVal($_POST['no']));
            $res->bindParam(":tgl", Koleksi::EmptyPostVal($_POST['tgl']));
            $res->bindParam(":diterima", Koleksi::EmptyPostVal($_POST['diterima']));        
            $res->bindParam(":perihal", Koleksi::EmptyPostVal($_POST['perihal']));
            $res->bindParam(":dispo", Koleksi::EmptyPostVal($_POST['dispo']));
            $res->bindParam(":catatan", Koleksi::EmptyPostVal($_POST['catatan']));
            $res->bindParam(":arsip", Koleksi::EmptyPostVal($_POST['arsip']));
            if ($_POST['aksi'] == "keluar_simpan") {
                if (empty($_FILES["file"]["name"])) {
                    $res->bindParam(":file",$blank);
                }
            }
            if ($_POST['aksi'] == "keluar_update") {
                if (empty($_FILES["file"]["name"])) {
                    $res->bindParam(":file", Koleksi::EmptyPostVal($_POST['filemasuk']));
                }
                else {
                    unlink($_POST['filemasuk']);
                }
            }
            //=================== ini upload file sts ====================
            if (empty($_FILES["file"]["name"])) {
            } else {
                $str =$_POST['no'];
                $str=preg_replace("/[^a-zA-Z0-9\s\.]/", ".", $str);
                $temp = explode(".", $_FILES['file']['name']);
                $nama = $_POST['dari']." no ".$str;
                $target_dir = "upload/surat/masuk/";
                $extension = end($temp);
                $target_file = $target_dir.$nama.".".$extension;
                move_uploaded_file($_FILES["file"]["tmp_name"], $target_file);
                $file=$target_file;
                $res->bindParam(":file",$file);
            }
            //=============================================================

            $res->execute();
        }

		// Diperlukan dalam proses update
		// Eksekusi
		// Tampilkan Informasi Sukses
		if ($_POST['aksi'] == "keluar_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "keluar_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}

	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}

		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;

		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['keluar']) && $_GET['keluar'] == 'baru') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
		<div class="tabbable tabbable-custom">
		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Asal </label>
					<div class="col-md-9">
						<input type="text" id="tujuan" name="tujuan" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Nomor </label>
					<div class="col-md-9">
						<input type="text" id="no" name="no" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal </label>
					<div class="col-md-9">
						<input type="date" id="tgl" name="tgl" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Dikirim </label>
					<div class="col-md-9">
						<input type="date" id="kirim" name="kirim" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Perihal</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="perihal" name="perihal" class="form-control">
                        </textarea>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-3 control-label">Catatan</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="catatan" name="catatan" class="form-control">
                        </textarea>
                    </div>
                </div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan Surat</label>
					<div class="col-md-9">
						<input id="file" name="file" class="input-file" type="file">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Posisi Arsip</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="arsip" name="arsip" class="form-control">
                        </textarea>
                    </div>
                </div>
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['keluar']) && $_GET['keluar'] == 'edit') {

	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `um_keluar` WHERE keluar_id = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Surat Keluar</a></li>
		</ul>
        
        <div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <input type="hidden" id="filemasuk" name="filemasuk" value="'.$r['keluar_file'].'">
                <input type="hidden" id="id" name="id" value="'.$r['keluar_id'].'">
                <div class="form-group">
					<label class="col-md-3 control-label">Tujuan </label>
					<div class="col-md-9">
						<input type="text" id="tujuan" name="tujuan" value="'.$r['keluar_tujuan'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Nomor </label>
					<div class="col-md-9">
						<input type="text" id="no" name="no" value="'.$r['keluar_no'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal </label>
					<div class="col-md-9">
						<input type="date" id="tgl" name="tgl" value="'.$r['keluar_tgl'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Diterima </label>
					<div class="col-md-9">
						<input type="date" id="kirim" name="kirim" value="'.$r['keluar_kirim'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Perihal</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="perihal" name="perihal" class="form-control">'.$r['keluar_perihal'].'
                        </textarea>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-3 control-label">Catatan</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="catatan" name="catatan" class="form-control">'.$r['keluar_catatan'].'
                        </textarea>
                    </div>
                </div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan Surat</label>
					<div class="col-md-9">
						<input id="file" name="file" class="input-file" type="file">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Posisi Arsip</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="arsip" name="arsip" class="form-control"> '.$r['keluar_arsip'].'
                        </textarea>
                    </div>
                </div>
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'tabeldata') {

	header("Content-type:application/json");

	// FIX untuk satu tabel
	/*
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/

    $columns = array(
            4=>'keluar_tgl',
            5=>'keluar_diterima',
        );


	// Untuk 2 Tabel
	/*
	$sql = "SELECT
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/
$sql = "SELECT
*
FROM
  um_keluar
    ";

	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" WHERE `keluar_no` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `keluar_tgl` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `keluar_kirim` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `keluar_tujuan` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `keluar_perihal` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `keluar_catatan` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `keluar_arsip` LIKE '%".$_REQUEST['search']['value']."%' ";
	}
	$res = $db->prepare($sql);
	$res->execute();

	$totalData = $res->rowCount();
	$totalFiltered = $totalData;

	//if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];
    
	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
		$nestedData = array();
        
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		$nestedData[] = $row['keluar_tujuan'];
		$nestedData[] = $row['keluar_no'];
		$nestedData[] = $row['keluar_perihal'];
		$nestedData[] = $row['keluar_tgl'];
		$nestedData[] = $row['keluar_kirim'];
		$nestedData[] = $row['keluar_catatan'];
		$nestedData[] = $row['keluar_arsip'];
		$nestedData[] = '
							<a href="'.$row['keluar_file'].'"><center><span class="label label-sm label-success"> Download </span></center> 
                            <br>
                            <a href="#" id="edit" data-id="'.$row['keluar_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Update </span></center> 
                            <br>
							';                
		$data[] = $nestedData;
		$no++;
	}
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">

		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Surat Masuk
					</div>
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
				</div> <!-- portlet-title -->
                    
				<div class="portlet-body">
					<div class="table-container">
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th><center>No</center></th>
						  <th>Tujuan</th>
						  <th>Nomor</th>
						  <th>Perihal</th>
						  <th>Tanggal Surat</th>
						  <th>Tanggal Dikirim</th>
						  <th>Catatan</th>
						  <th>Posisi Arsip</th>
						  <th>Aksi</th>
						</tr>
						</thead>
						<tbody></tbody>
						</table>

					</div> <!-- table-container -->
				</div> <!-- portlet-body -->

			</div>
		</div>

		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->

	</div>
	';

	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>
