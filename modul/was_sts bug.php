<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=was_sts';
$theme		= 'themes/conquer';
$tabelData	= 'sts_data';
$pageTitle	= 'Pengawasan <small>Daftar STS</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i>
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Pengawasan</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Daftar Kegiatan Pengawasan</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-was-sts.js"></script>
';
$initA = '
// var modul = "modul.php?ke=was_sts";
var modul = window.location.href;
var pageTitle = "Form Data STS";

// CRUD
var menuBaru 	= modul+"&sts=baru";
var menuEdit	= modul+"&sts=edit";

// Table Data
var srcUrl 	= modul+"&sts=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
wasdata.init();
';
$initC = '';
// MAIN CODE

//DATA KD JENIS IN ARRAY
$kdstatus = array();
$query = "select * from was_kd_status";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdstatus += [$status_id => $status_ket];
}
//
//DATA KD GOLRU IN ARRAY
$kdgolru = array();
$query = "select * from kd_golru";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdgolru += [$kdgol => $ket3];
}
//
//DATA KD JENIS IN ARRAY
$kdjenis = array();
$query = "select * from was_kd_jenis";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdjenis += [$jenis_id => $jenis_ket];
}
//
//DATA KD PEMERIKSa IN ARRAY
$kdpemeriksa = array();
$query = "select * from was_pemeriksa";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdpemeriksa += [$pemeriksa_id => $pemeriksa_nama];
}
//
// kode pegawai
$peg = array();
$query = "select nip, nama from um_pegawai";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $peg += [$nip => $nama];
}
// kode posisi st
$kdposisi = array();
$query = "select * from was_st_posisi";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdposisi += [$posisi_id => $posisi_ket];
}
//kode opd
$opd = array();
$query = "select * from opd";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $opd += [$opd_id => $opd_nama];
}
$blank="";
if (isset($_POST['submit'])) {
	try {
		// Query Simpan
		if ($_POST['aksi'] == "sts_simpan") {
			$sql = "INSERT INTO `was_sts`
			(`sts_kode`,
			`sts_jumlah`,
			`sts_tgl`,
			`sts_file`,
			`sts_ket`
			)
			VALUES
			(:kode,
			:jumlah,
			:tgl,
			:file,
			:ket
            )";
            $res = $db->prepare($sql);
            $key=1;
            do {
                $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 10; $i++) {
                    $randstring .= $char[rand(0, strlen($char)-1)];
                }
                $query2 = "select count(*) from was_st where st_key='$randstring'";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute();
                $key= $stmt2->fetchColumn();
            } while ($key > 0);
            $res->bindParam(":kode",$randstring);
            $kode=$randstring;

		}
		// Query Update
		if ($_POST['aksi'] == "sts_update") {
			$sql = "UPDATE `was_sts` SET
			`sts_jumlah` = :jumlah,
			`sts_tgl` = :tgl,
			`sts_file` = :file,
			`sts_ket` = :ket
			WHERE `sts_kode` = :sts_kode";
            $res = $db->prepare($sql);
            $res->bindParam(":kode", Koleksi::EmptyPostVal($_POST['kode']));
            $kode=$_POST['kode'];
		}

		// Persiapkan Query
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		//$nip = preg_replace('/\D/','', $_POST['nip']);
		//$nip = $_POST['nip'];

		// Pasangkan dan Eksekusi
		//if ($_POST['aksi'] == "cuti_simpan")
			//$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		if ($_POST['aksi'] == "sts_simpan" || $_POST['aksi'] == "sts_update"){
            $res->bindParam(":jumlah", Koleksi::EmptyPostVal($_POST['jumlah']));
            $res->bindParam(":tgl", Koleksi::EmptyPostVal($_POST['tanggal']));
            $res->bindParam(":ket", Koleksi::EmptyPostVal($_POST['ket']));
            if ($_POST['aksi'] == "sts_simpan") {
                if (empty($_FILES["file"]["name"])) {
                    $res->bindParam(":file",$blank);
                }
            }
            if ($_POST['aksi'] == "sts_update") {
                if (empty($_FILES["sts"]["name"])) {
                    $res->bindParam(":file",Koleksi::EmptyPostVal($_POST['sts']));
                }
                else {
                    unlink($_POST['sts']);
                }
            }
            //=================== ini upload file sts ====================
            if (empty($_FILES["sts"]["name"])) {
            } else {
                $temp = explode(".", $_FILES['sts']['name']);
                $nama = $kode;
                $target_dir = "upload/sts/";
                $extension = end($temp);
                $target_file = $target_dir.$nama.".".$extension;
                move_uploaded_file($_FILES["sts"]["tmp_name"], $target_file);
                $file=$target_file;
                $res->bindParam(":file",$file);
            }
            //=============================================================

            $res->execute();
        }

		// Diperlukan dalam proses update
		// Eksekusi
		// Tampilkan Informasi Sukses
		if ($_POST['aksi'] == "sts_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "sts_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}

	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}

		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;

		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['sts']) && $_GET['sts'] == 'baru') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data STS</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal STS </label>
					<div class="col-md-9">
						<input type="date" id="tanggal" name="tanggal" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jumlah</label>
					<div class="col-md-9">
						<input type="text" id="jumlah" name="jumlah" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan Bukti</label>
					<div class="col-md-9">
						<input id="sts" name="sts" class="input-file" type="file">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="ket" name="ket" class="form-control">
                        </textarea>
                    </div>
                </div> 
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['sts']) && $_GET['sts'] == 'edit') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">EDIT DA?TA STS</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal STS </label>
					<div class="col-md-9">
						<input type="date" id="tanggal" name="tanggal" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jumlah</label>
					<div class="col-md-9">
						<input type="text" id="jumlah" name="jumlah" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan Bukti</label>
					<div class="col-md-9">
						<input id="sts" name="sts" class="input-file" type="file">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="ket" name="ket" class="form-control">
                        </textarea>
                    </div>
                </div> 
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['sts']) && $_GET['sts'] == 'tabeldata') {

	header("Content-type:application/json");

	// FIX untuk satu tabel
	/*
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/

	// Kolom yang bisa di Sortir
	$columns = array(
		1=>'sts_kode',
		2=>'sts_tgl',
		3=>'sts_jumlah',
	);

	// Untuk 2 Tabel
	/*
	$sql = "SELECT
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/

$sql = "SELECT
        *
        FROM
        `was_sts` ";

	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" WHERE `sts_jumlah` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `sts_tgl` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `sts_ket` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `sts_kode` LIKE '%".$_REQUEST['search']['value']."%' ";
	}
	$res = $db->prepare($sql);
	$res->execute();

	$totalData = $res->rowCount();
	$totalFiltered = $totalData;

	if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];

	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
		$nestedData = array();
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		$nestedData[] = $row['sts_kode'];
		$nestedData[] = $row['sts_tgl'];
		$nestedData[] = $row['sts_jumlah'];
		$nestedData[] = '<a href="'.$row['sts_file'].'" target="_blank">'.$row['sts_file'].'</a>';
		$nestedData[] = $row['sts_ket'];
		/*
		$arr = array("Y"=>"Aktif", "T"=>"Tidak Aktif", "P"=>"Pindah");
		$aktif = isset($arr[$row[6]]) ? $arr[$row[6]] : null;
		*/

		// Opsi
		// $otoritas =


		$nestedData[] = '
							<a href="#" id="edit" data-id="'.$row['sts_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Update STS </span></center> 
                            <br>
							'; 

		$data[] = $nestedData;
		$no++;
	}
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">

		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Pembayaran STS Inspektorat Provinsi Kalimantan Utara
					</div>
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
				</div> <!-- portlet-title -->

				<div class="portlet-body">
					<div class="table-container">
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th><center>No</center></th>
						  <th>NIP</th>
						  <th>Nama</th>
						  <th>Nilai</th>
						  <th>Bayar</th>
						  <th>Keterangan</th>
						  <th>Aksi</th>
						</tr>
						</thead>
						<tbody></tbody>
						</table>

					</div> <!-- table-container -->
				</div> <!-- portlet-body -->

			</div>
		</div>

		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->

	</div>
	';

	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>
