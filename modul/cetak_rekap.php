
<?php
$tahun=$_POST['tahun'];
$obyek=$_POST['obyek'];
require 'configcetak.php';
//$password = "73Q8sr8Gau4JifEW";
$conn = new mysqli($servername, $username, $password, $dbname);


header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Rekapitulasi ".date('d-m-Y H:i:s').".xls");
echo "<br>";
echo "<br>";
echo "<br>";
echo "<table>";
echo "<tr>";
echo "<th colspan=\"1\"></th> ";
echo "<th colspan=\"36\">INSPEKTORAT DAERAH PROVINSI KALIMANTAN UTARA</th> ";
echo "</tr>";
echo "<tr>";
echo "<th colspan=\"1\"></th> ";
echo "<th colspan=\"36\"><b>LAPORAN REKAPITULASI PEMERIKSAAN <b></th> ";
echo "</tr>";
echo "</table>";
 echo "<br>";
if ($obyek!='0'){
     echo "OPD :".$obyek;
    echo "<br>";
}
if ($tahun!='0'){
     echo "Tahun :".$tahun;
    echo "<br>";
}
// obyek dan tahun kosong
if ($obyek=='0' && $tahun=='0' ){
    $sql="SELECT was_tahun
        FROM was_data
        GROUP BY was_tahun
        ORder BY was_tahun ASC" ;     
    echo "<br>";
    echo "<table border=1>";
    echo "<tr>";
    echo "<th rowspan=3>No</th>";
    echo "<th rowspan=3>Tahun</th>";
    echo "<th colspan=11>Kode Temuan</th>";
    echo "<th rowspan=3>Jumlah Temuan</th>";
    echo "<th rowspan=2 colspan=14>Kode Tindak Lanjut</th>";
    echo "<th rowspan=3>Jumlah Tindak Lanjut</th>";
    echo "<th rowspan=2 colspan=4>STATUS</th>";
    echo "<th rowspan=2 colspan=3>Nilai Setor Kepada Negara/Daerah</th>";
    echo "<th rowspan=3>Keterangan</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th colspan=5>1</th>";
    echo "<th colspan=3>2</th>";
    echo "<th colspan=3>3</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>06</th>";
    echo "<th>07</th>";
    echo "<th>08</th>";
    echo "<th>09</th>";
    echo "<th>10</th>";
    echo "<th>11</th>";
    echo "<th>12</th>";
    echo "<th>13</th>";
    echo "<th>14</th>";
    echo "<th>TS</th>";
    echo "<th>TB</th>";
    echo "<th>BT</th>";
    echo "<th>TT</th>";
    echo "<th>Nilai Temuan</th>";
    echo "<th>Setor</th>";
    echo "<th>Sisa</th>";
    echo "</tr>";
    $no=1;
    $temuan=0;
    $tl=0;
    $data=array();
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $nesteddata=array ();
            $temuan=0;
            $rekomendasi=0;
            echo "<tr>";
            echo "<td>".$no."</td>";
            echo "<td>".$row['was_tahun']."</td>";
            $tahun=$row['was_tahun'];
            //temuan kelompok 1
            for($x=1; $x <= 5; $x++){
                $sql1="SELECT
                        count(`was_tl_data`.`tl_subkel`) as kode
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                    WHERE (was_data.was_tahun = '$tahun' and was_tl_data.tl_kelompok=1 and was_tl_data.tl_subkel='$x')
                    GROUP BY was_tahun
                    ORder BY was_tahun ASC";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                $temuan=$temuan+$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
            }
            //Temuan kelompok 2 dan 3
            for($x=2; $x <= 3; $x++){
                for($y=1; $y <= 3; $y++){
                    $sql1="SELECT
                            count(`was_tl_data`.`tl_subkel`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        WHERE (was_data.was_tahun = '$tahun' and was_tl_data.tl_kelompok='$x' and was_tl_data.tl_subkel='$y')
                        GROUP BY was_tahun
                        ORder BY was_tahun ASC";
                    $result1 = $conn->query($sql1);
                    $row1 = $result1->fetch_assoc();
                    $nesteddata []=$row1['kode'];
                    $temuan=$temuan+$row1['kode'];
                    echo "<td>".$row1['kode']."</td>";
                }
            }
            //Jumlah Temuan
            echo "<td>".$temuan."</td>";
            $nesteddata []=$temuan;
            //Data Rekoemndasi
            for($x=1; $x <= 14; $x++){
                $sql1="SELECT
                            COUNT(`was_rekomendasi`.`rek_kode`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                            INNER JOIN `was_rekomendasi` 
                                ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        WHERE (`was_data`.`was_tahun` ='$tahun' AND
                            `was_rekomendasi`.`rek_kode` ='$x')";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
                $rekomendasi=$rekomendasi+$row1['kode'];
            }
            //Total REkoemndasi
            echo "<td>".$rekomendasi."</td>";
            $nesteddata []=$rekomendasi;
            //Status Tindak Lanjut
            for($x=1; $x <= 4; $x++){
                $sql1="SELECT
                            COUNT(`was_rekomendasi`.`rek_status`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                            INNER JOIN `was_rekomendasi` 
                                ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        WHERE (`was_data`.`was_tahun` ='$tahun' AND
                            `was_rekomendasi`.`rek_status` ='$x')";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
            }
            //Nilai Temuan
            $sql1="SELECT
                        SUM(`was_rekomendasi`.`rek_nilai`) as nilai
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        INNER JOIN `was_rekomendasi` 
                            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                    WHERE (`was_data`.`was_tahun` ='$tahun')";
            $result1 = $conn->query($sql1);
            $row1 = $result1->fetch_assoc();
            $nesteddata []=$row1['nilai'];
            echo "<td>".number_format($row1['nilai'],2,",",".")."</td>";
            //setor
            $sql2="SELECT
                        SUM(`was_tlrinci`.`rinci_bayar`) as bayar
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        INNER JOIN `was_rekomendasi` 
                            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        INNER JOIN `was_tlrinci` 
                            ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
                    WHERE (`was_data`.`was_tahun` ='$tahun')";
            $result2 = $conn->query($sql2);
            $row2 = $result2->fetch_assoc();
            $nesteddata []=$row2['bayar'];
            echo "<td>".number_format($row2['bayar'],2,",",".")."</td>";
            //sisa
            $sisa=$row1['nilai']-$row2['bayar'];
            $nesteddata []=$sisa;
            echo "<td>". number_format($sisa,2,",",".")."</td>";
            echo "<td> </td>";
            echo "</tr>";
            $data[]=$nesteddata;
            $no ++;
        }
    }
    echo "<tr>"; 
    echo "<th colspan=2>Jumlah Keseluruhan</th>";
    //sum array multidimensional untuk vertikal key
    for($x=0; $x <= 30; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>".$sum."</th>";
    }
    for($x=31; $x <= 33; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>". number_format($sum,2,",",".")."</th>";
    }
    echo "</tr>"; 
}
// obyek dan tahun ada isinya
elseif ($obyek!='0'&& $tahun!='0' ){    
    echo "<br>";
    echo "<table border=1>";
    echo "<tr>";
    echo "<th rowspan=3>No</th>";
    echo "<th rowspan=3>Obyek Pemeriksaan</th>";
    echo "<th colspan=11>Kode Temuan</th>";
    echo "<th rowspan=3>Jumlah Temuan</th>";
    echo "<th rowspan=2 colspan=14>Kode Tindak Lanjut</th>";
    echo "<th rowspan=3>Jumlah Tindak Lanjut</th>";
    echo "<th rowspan=2 colspan=4>STATUS</th>";
    echo "<th rowspan=2 colspan=3>Nilai Setor Kepada Negara/Daerah</th>";
    echo "<th rowspan=3>Keterangan</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th colspan=5>1</th>";
    echo "<th colspan=3>2</th>";
    echo "<th colspan=3>3</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>06</th>";
    echo "<th>07</th>";
    echo "<th>08</th>";
    echo "<th>09</th>";
    echo "<th>10</th>";
    echo "<th>11</th>";
    echo "<th>12</th>";
    echo "<th>13</th>";
    echo "<th>14</th>";
    echo "<th>TS</th>";
    echo "<th>TB</th>";
    echo "<th>BT</th>";
    echo "<th>TT</th>";
    echo "<th>Nilai Temuan</th>";
    echo "<th>Setor</th>";
    echo "<th>Sisa</th>";
    echo "</tr>";
    $no=1;
    $temuan=0;
    $tl=0;
    $data=array();
    $nesteddata=array ();
    $temuan=0;
    $rekomendasi=0;
    echo "<tr>";
    echo "<td>".$no."</td>";
    echo "<td>".$obyek."</td>";
    //temuan kelompok 1
    for($x=1; $x <= 5; $x++){
        $sql1="SELECT
                count(`was_tl_data`.`tl_subkel`) as kode
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
            WHERE (was_data.was_tahun = '$tahun' and was_data.was_obyek='$obyek' and was_tl_data.tl_kelompok=1 and was_tl_data.tl_subkel='$x')
            GROUP BY was_tahun
            ORder BY was_tahun ASC";
        $result1 = $conn->query($sql1);
        $row1 = $result1->fetch_assoc();
        $nesteddata []=$row1['kode'];
        $temuan=$temuan+$row1['kode'];
        echo "<td>".$row1['kode']."</td>";
    }
    //Temuan kelompok 2 dan 3
    for($x=2; $x <= 3; $x++){
        for($y=1; $y <= 3; $y++){
            $sql1="SELECT
                    count(`was_tl_data`.`tl_subkel`) as kode
                FROM
                    `was_tl_data`
                    INNER JOIN `was_data` 
                        ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                WHERE (was_data.was_tahun = '$tahun' and was_data.was_obyek='$obyek' and was_tl_data.tl_kelompok='$x' and was_tl_data.tl_subkel='$y')
                GROUP BY was_tahun
                ORder BY was_tahun ASC";
            $result1 = $conn->query($sql1);
            $row1 = $result1->fetch_assoc();
            $nesteddata []=$row1['kode'];
            $temuan=$temuan+$row1['kode'];
            echo "<td>".$row1['kode']."</td>";
        }
    }
    //Jumlah Temuan
    echo "<td>".$temuan."</td>";
    $nesteddata []=$temuan;
    //Data Rekoemndasi
    for($x=1; $x <= 14; $x++){
        $sql1="SELECT
                    COUNT(`was_rekomendasi`.`rek_kode`) as kode
                FROM
                    `was_tl_data`
                    INNER JOIN `was_data` 
                        ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                    INNER JOIN `was_rekomendasi` 
                        ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' AND
                    `was_rekomendasi`.`rek_kode` ='$x')";
        $result1 = $conn->query($sql1);
        $row1 = $result1->fetch_assoc();
        $nesteddata []=$row1['kode'];
        echo "<td>".$row1['kode']."</td>";
        $rekomendasi=$rekomendasi+$row1['kode'];
    }
    //Total REkoemndasi
    echo "<td>".$rekomendasi."</td>";
    $nesteddata []=$rekomendasi;
    //Status Tindak Lanjut
    for($x=1; $x <= 4; $x++){
        $sql1="SELECT
                    COUNT(`was_rekomendasi`.`rek_status`) as kode
                FROM
                    `was_tl_data`
                    INNER JOIN `was_data` 
                        ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                    INNER JOIN `was_rekomendasi` 
                        ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' AND
                    `was_rekomendasi`.`rek_status` ='$x')";
        $result1 = $conn->query($sql1);
        $row1 = $result1->fetch_assoc();
        $nesteddata []=$row1['kode'];
        echo "<td>".$row1['kode']."</td>";
    }
    //Nilai Temuan
    $sql1="SELECT
                SUM(`was_rekomendasi`.`rek_nilai`) as nilai
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                INNER JOIN `was_rekomendasi` 
                    ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
            WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek')";
    $result1 = $conn->query($sql1);
    $row1 = $result1->fetch_assoc();
    $nesteddata []=$row1['nilai'];
    echo "<td>".number_format($row1['nilai'],2,",",".")."</td>";
    //setor
    $sql2="SELECT
                SUM(`was_tlrinci`.`rinci_bayar`) as bayar
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                INNER JOIN `was_rekomendasi` 
                    ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                INNER JOIN `was_tlrinci` 
                    ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
            WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek')";
    $result2 = $conn->query($sql2);
    $row2 = $result2->fetch_assoc();
    $nesteddata []=$row2['bayar'];
    echo "<td>".number_format($row2['bayar'],2,",",".")."</td>";
    //sisa
    $sisa=$row1['nilai']-$row2['bayar'];
    $nesteddata []=$sisa;
    echo "<td>". number_format($sisa,2,",",".")."</td>";
    echo "<td> </td>";
    echo "</tr>";
    $data[]=$nesteddata;
    $no ++;
        
    echo "<tr>"; 
    echo "<th colspan=2>Jumlah Keseluruhan</th>";
    //sum array multidimensional untuk vertikal key
    for($x=0; $x <= 30; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>".$sum."</th>";
    }
    for($x=31; $x <= 33; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>". number_format($sum,2,",",".")."</th>";
    }
    echo "</tr>"; 
}
// obyek ada isinya tahun kosong
elseif($obyek!='0' && $tahun=='0' ){
    $sql="SELECT was_tahun
        FROM was_data
        WHERE was_obyek='$obyek'
        GROUP BY was_tahun 
        ORder BY was_tahun ASC" ;  
    echo $sql;
    echo "<br>";
    echo "<table border=1>";
    echo "<tr>";
    echo "<th rowspan=3>No</th>";
    echo "<th rowspan=3>Tahun</th>";
    echo "<th colspan=11>Kode Temuan</th>";
    echo "<th rowspan=3>Jumlah Temuan</th>";
    echo "<th rowspan=2 colspan=14>Kode Tindak Lanjut</th>";
    echo "<th rowspan=3>Jumlah Tindak Lanjut</th>";
    echo "<th rowspan=2 colspan=4>STATUS</th>";
    echo "<th rowspan=2 colspan=3>Nilai Setor Kepada Negara/Daerah</th>";
    echo "<th rowspan=3>Keterangan</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th colspan=5>1</th>";
    echo "<th colspan=3>2</th>";
    echo "<th colspan=3>3</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>06</th>";
    echo "<th>07</th>";
    echo "<th>08</th>";
    echo "<th>09</th>";
    echo "<th>10</th>";
    echo "<th>11</th>";
    echo "<th>12</th>";
    echo "<th>13</th>";
    echo "<th>14</th>";
    echo "<th>TS</th>";
    echo "<th>TB</th>";
    echo "<th>BT</th>";
    echo "<th>TT</th>";
    echo "<th>Nilai Temuan</th>";
    echo "<th>Setor</th>";
    echo "<th>Sisa</th>";
    echo "</tr>";
    $no=1;
    $temuan=0;
    $tl=0;
    $data=array();
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $nesteddata=array ();
            $temuan=0;
            $rekomendasi=0;
            echo "<tr>";
            echo "<td>".$no."</td>";
            echo "<td>".$row['was_tahun']."</td>";
            $tahun=$row['was_tahun'];
            //temuan kelompok 1
            for($x=1; $x <= 5; $x++){
                $sql1="SELECT
                        count(`was_tl_data`.`tl_subkel`) as kode
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                    WHERE (was_data.was_tahun = '$tahun' and was_data.was_obyek='$obyek' and was_tl_data.tl_kelompok=1 and was_tl_data.tl_subkel='$x')
                    GROUP BY was_tahun
                    ORder BY was_tahun ASC";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                $temuan=$temuan+$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
            }
            //Temuan kelompok 2 dan 3
            for($x=2; $x <= 3; $x++){
                for($y=1; $y <= 3; $y++){
                    $sql1="SELECT
                            count(`was_tl_data`.`tl_subkel`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        WHERE (was_data.was_tahun = '$tahun' and was_data.was_obyek='$obyek' and was_tl_data.tl_kelompok='$x' and was_tl_data.tl_subkel='$y')
                        GROUP BY was_tahun
                        ORder BY was_tahun ASC";
                    $result1 = $conn->query($sql1);
                    $row1 = $result1->fetch_assoc();
                    $nesteddata []=$row1['kode'];
                    $temuan=$temuan+$row1['kode'];
                    echo "<td>".$row1['kode']."</td>";
                }
            }
            //Jumlah Temuan
            echo "<td>".$temuan."</td>";
            $nesteddata []=$temuan;
            //Data Rekoemndasi
            for($x=1; $x <= 14; $x++){
                $sql1="SELECT
                            COUNT(`was_rekomendasi`.`rek_kode`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                            INNER JOIN `was_rekomendasi` 
                                ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' AND
                            `was_rekomendasi`.`rek_kode` ='$x')";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
                $rekomendasi=$rekomendasi+$row1['kode'];
            }
            //Total REkoemndasi
            echo "<td>".$rekomendasi."</td>";
            $nesteddata []=$rekomendasi;
            //Status Tindak Lanjut
            for($x=1; $x <= 4; $x++){
                $sql1="SELECT
                            COUNT(`was_rekomendasi`.`rek_status`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                            INNER JOIN `was_rekomendasi` 
                                ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' AND
                            `was_rekomendasi`.`rek_status` ='$x')";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
            }
            //Nilai Temuan
            $sql1="SELECT
                        SUM(`was_rekomendasi`.`rek_nilai`) as nilai
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        INNER JOIN `was_rekomendasi` 
                            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                    WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek')";
            $result1 = $conn->query($sql1);
            $row1 = $result1->fetch_assoc();
            $nesteddata []=$row1['nilai'];
            echo "<td>".number_format($row1['nilai'],2,",",".")."</td>";
            //setor
            $sql2="SELECT
                        SUM(`was_tlrinci`.`rinci_bayar`) as bayar
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        INNER JOIN `was_rekomendasi` 
                            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        INNER JOIN `was_tlrinci` 
                            ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
                    WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' )";
            $result2 = $conn->query($sql2);
            $row2 = $result2->fetch_assoc();
            $nesteddata []=$row2['bayar'];
            echo "<td>".number_format($row2['bayar'],2,",",".")."</td>";
            //sisa
            $sisa=$row1['nilai']-$row2['bayar'];
            $nesteddata []=$sisa;
            echo "<td>". number_format($sisa,2,",",".")."</td>";
            echo "<td> </td>";
            echo "</tr>";
            $data[]=$nesteddata;
            $no ++;
        }
    }
    echo "<tr>"; 
    echo "<th colspan=2>Jumlah Keseluruhan</th>";
    //sum array multidimensional untuk vertikal key
    for($x=0; $x <= 30; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>".$sum."</th>";
    }
    for($x=31; $x <= 33; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>". number_format($sum,2,",",".")."</th>";
    }
    echo "</tr>"; 
}
// tahun ada isinya obyeknya kosong
elseif ($obyek=='0'&& $tahun!='0' ){
    $sql="SELECT was_obyek
        FROM was_data
        WHERE was_tahun='$tahun'
        GROUP BY was_obyek";     
    echo "<br>";
    echo "<table border=1>";
    echo "<tr>";
    echo "<th rowspan=3>No</th>";
    echo "<th rowspan=3>Obyek Pemeriksaan</th>";
    echo "<th colspan=11>Kode Temuan</th>";
    echo "<th rowspan=3>Jumlah Temuan</th>";
    echo "<th rowspan=2 colspan=14>Kode Tindak Lanjut</th>";
    echo "<th rowspan=3>Jumlah Tindak Lanjut</th>";
    echo "<th rowspan=2 colspan=4>STATUS</th>";
    echo "<th rowspan=2 colspan=3>Nilai Setor Kepada Negara/Daerah</th>";
    echo "<th rowspan=3>Keterangan</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th colspan=5>1</th>";
    echo "<th colspan=3>2</th>";
    echo "<th colspan=3>3</th>";
    echo "</tr>";
    echo "<tr>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>01</th>";
    echo "<th>02</th>";
    echo "<th>03</th>";
    echo "<th>04</th>";
    echo "<th>05</th>";
    echo "<th>06</th>";
    echo "<th>07</th>";
    echo "<th>08</th>";
    echo "<th>09</th>";
    echo "<th>10</th>";
    echo "<th>11</th>";
    echo "<th>12</th>";
    echo "<th>13</th>";
    echo "<th>14</th>";
    echo "<th>TS</th>";
    echo "<th>TB</th>";
    echo "<th>BT</th>";
    echo "<th>TT</th>";
    echo "<th>Nilai Temuan</th>";
    echo "<th>Setor</th>";
    echo "<th>Sisa</th>";
    echo "</tr>";
    $no=1;
    $temuan=0;
    $tl=0;
    $data=array();
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $nesteddata=array ();
            $temuan=0;
            $rekomendasi=0;
            echo "<tr>";
            echo "<td>".$no."</td>";
            echo "<td>".$row['was_obyek']."</td>";
            $obyek=$row['was_obyek'];
            //temuan kelompok 1
            for($x=1; $x <= 5; $x++){
                $sql1="SELECT
                        count(`was_tl_data`.`tl_subkel`) as kode
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                    WHERE (was_data.was_tahun = '$tahun' and was_data.was_obyek='$obyek' and was_tl_data.tl_kelompok=1 and was_tl_data.tl_subkel='$x')
                    GROUP BY was_tahun
                    ORder BY was_tahun ASC";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                $temuan=$temuan+$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
            }
            //Temuan kelompok 2 dan 3
            for($x=2; $x <= 3; $x++){
                for($y=1; $y <= 3; $y++){
                    $sql1="SELECT
                            count(`was_tl_data`.`tl_subkel`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        WHERE (was_data.was_tahun = '$tahun' and was_data.was_obyek='$obyek' and was_tl_data.tl_kelompok='$x' and was_tl_data.tl_subkel='$y')
                        GROUP BY was_tahun
                        ORder BY was_tahun ASC";
                    $result1 = $conn->query($sql1);
                    $row1 = $result1->fetch_assoc();
                    $nesteddata []=$row1['kode'];
                    $temuan=$temuan+$row1['kode'];
                    echo "<td>".$row1['kode']."</td>";
                }
            }
            //Jumlah Temuan
            echo "<td>".$temuan."</td>";
            $nesteddata []=$temuan;
            //Data Rekoemndasi
            for($x=1; $x <= 14; $x++){
                $sql1="SELECT
                            COUNT(`was_rekomendasi`.`rek_kode`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                            INNER JOIN `was_rekomendasi` 
                                ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' AND
                            `was_rekomendasi`.`rek_kode` ='$x')";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
                $rekomendasi=$rekomendasi+$row1['kode'];
            }
            //Total REkoemndasi
            echo "<td>".$rekomendasi."</td>";
            $nesteddata []=$rekomendasi;
            //Status Tindak Lanjut
            for($x=1; $x <= 4; $x++){
                $sql1="SELECT
                            COUNT(`was_rekomendasi`.`rek_status`) as kode
                        FROM
                            `was_tl_data`
                            INNER JOIN `was_data` 
                                ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                            INNER JOIN `was_rekomendasi` 
                                ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' AND
                            `was_rekomendasi`.`rek_status` ='$x')";
                $result1 = $conn->query($sql1);
                $row1 = $result1->fetch_assoc();
                $nesteddata []=$row1['kode'];
                echo "<td>".$row1['kode']."</td>";
            }
            //Nilai Temuan
            $sql1="SELECT
                        SUM(`was_rekomendasi`.`rek_nilai`) as nilai
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        INNER JOIN `was_rekomendasi` 
                            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                    WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' )";
            $result1 = $conn->query($sql1);
            $row1 = $result1->fetch_assoc();
            $nesteddata []=$row1['nilai'];
            echo "<td>".number_format($row1['nilai'],2,",",".")."</td>";
            //setor
            $sql2="SELECT
                        SUM(`was_tlrinci`.`rinci_bayar`) as bayar
                    FROM
                        `was_tl_data`
                        INNER JOIN `was_data` 
                            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                        INNER JOIN `was_rekomendasi` 
                            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                        INNER JOIN `was_tlrinci` 
                            ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
                    WHERE (`was_data`.`was_tahun` ='$tahun' and was_data.was_obyek='$obyek' )";
            $result2 = $conn->query($sql2);
            $row2 = $result2->fetch_assoc();
            $nesteddata []=$row2['bayar'];
            echo "<td>".number_format($row2['bayar'],2,",",".")."</td>";
            //sisa
            $sisa=$row1['nilai']-$row2['bayar'];
            $nesteddata []=$sisa;
            echo "<td>". number_format($sisa,2,",",".")."</td>";
            echo "<td> </td>";
            echo "</tr>";
            $data[]=$nesteddata;
            $no ++;
        }
    }
    echo "<tr>"; 
    echo "<th colspan=2>Jumlah Keseluruhan</th>";
    //sum array multidimensional untuk vertikal key
    for($x=0; $x <= 30; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>".$sum."</th>";
    }
    for($x=31; $x <= 33; $x++){
        $sum = 0;
        foreach($data as $value) {
            $sum += $value[$x];
        }
        echo "<th>". number_format($sum,2,",",".")."</th>";
    }
    echo "</tr>"; 
}

echo "<table>";

?>
