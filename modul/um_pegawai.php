<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=um_pegawai';
$theme		= 'themes/conquer';
$tabelData	= 'um_pegawai';
$pageTitle	= 'Umum Kepegawaian <small>Daftar Pegawai</small>';

$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i> 
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Umum Kepegawaian</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Daftar Pegawai</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-um-pegawai.js"></script>
';
$initA = '
// var modul = "modul.php?ke=um_pegawai";
var modul = window.location.href;
var pageTitle = "Form Data Pegawai";
	
// CRUD
var menuBaru 	= modul+"&pegawai=baru";
var menuEdit	= modul+"&pegawai=edit";
var menuHapus 	= modul+"&pegawai=hapus";
	
// Table Data
var srcUrl 	= modul+"&pegawai=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
pegawai.init();
';
$initC = '';
// MAIN CODE

if (isset($_POST['submit'])) {
	
	/*
	$avname		= $_FILES['imavatar']['name'];
	$avsize		= $_FILES['imavatar']['size'];
	$avtype		= $_FILES['imavatar']['type'];
	$avtmp 		= $_FILES['imavatar']['tmp_name'];

	$ftname		= $_FILES['imfoto']['name'];
	$ftsize		= $_FILES['imfoto']['size'];
	$fttype		= $_FILES['imfoto']['type'];
	$ftmp 		= $_FILES['imfoto']['tmp_name'];
	
	$ekstensi	= array('jpg','jpeg');
	
	$lok_avfile	= "img/profil/".$avname;
	$lok_ftfile	= "img/profil/".$avname;
	
	$file_av	= explode(".", $avname);
	$ext_av		= array_pop($file_av);
	
	$file_ft	= explode(".", $ftname);
	$ext_ft		= array_pop($file_ft);	
	*/
		
	try {
		// Query Simpan
		if ($_POST['submit'] == "pegawai_simpan") {
			$sql = "INSERT INTO `um_pegawai`
			(`nip`, `aktif`, `nama`, `gelardp`, `gelarbk`, `lahirtgl`, `lahirtmp`, `jenkel`, `agama`, `status`, `goldarah`,
			 `karpeg`, `npwp`, `alamat`, `telp`, `ponsel`, `email`)
			VALUES
			(:nip, :aktif, :nama, :gelardp, :gelarbk, :lahirtgl, :lahirtmp, :jenkel, :agama, :status, :goldarah,
			 :karpeg, :npwp, :alamat, :telp, :ponsel, :email)";
		}
		
		// Query Update
		if ($_POST['submit'] == "pegawai_update") {
			$sql = "UPDATE `um_pegawai` SET 
			`aktif` = :aktif,
			`nama` = :nama,
			`gelardp` = :gelardp,
			`gelarbk` = :gelarbk,
			`lahirtgl` = :lahirtgl,
			`lahirtmp` = :lahirtmp,
			`jenkel` = :jenkel,
			`agama` = :agama,
			`status` = :status,
			`goldarah` = :goldarah,
			`karpeg` = :karpeg,
			`npwp` = :npwp,
			`alamat` = :alamat,
			`telp` = :telp,
			`ponsel` = :ponsel,
			`email` = :email
			WHERE `id` = :uid";
		}
		
		// Persiapkan Query
		$res = $db->prepare($sql);
	
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		$nip = preg_replace('/\D/','', $_POST['nip']);
		
		// Pasangkan dan Eksekusi
		if ($_POST['submit'] == "pegawai_simpan")			
			$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		
		$res->bindParam(":aktif", Koleksi::EmptyPostVal($_POST['aktif']), PDO::PARAM_STR);
		$res->bindParam(":nama", Koleksi::EmptyPostVal($_POST['nama']), PDO::PARAM_STR);
		$res->bindParam(":gelardp", Koleksi::EmptyPostVal($_POST['gelardp']), PDO::PARAM_STR);
		$res->bindParam(":gelarbk", Koleksi::EmptyPostVal($_POST['gelarbk']), PDO::PARAM_STR);
		$res->bindParam(":lahirtgl", Koleksi::extractTglDariNip($nip), PDO::PARAM_STR);
		$res->bindParam(":lahirtmp", Koleksi::EmptyPostVal($_POST['lahirtmp']), PDO::PARAM_STR);
		$res->bindParam(":jenkel", Koleksi::extractJenkelDariNip($nip), PDO::PARAM_STR);
		$res->bindParam(":agama", Koleksi::EmptyPostVal($_POST['agama']), PDO::PARAM_STR);
		$res->bindParam(":status", Koleksi::EmptyPostVal($_POST['status']), PDO::PARAM_STR);
		$res->bindParam(":goldarah", Koleksi::EmptyPostVal($_POST['goldarah']), PDO::PARAM_STR);
		$res->bindParam(":karpeg", Koleksi::EmptyPostVal($_POST['karpeg']), PDO::PARAM_STR);
		$res->bindParam(":npwp", Koleksi::EmptyPostVal($_POST['npwp']), PDO::PARAM_STR);
		$res->bindParam(":alamat", Koleksi::EmptyPostVal($_POST['alamat']), PDO::PARAM_STR);
		$res->bindParam(":telp", Koleksi::EmptyPostVal($_POST['telp']), PDO::PARAM_STR);
		$res->bindParam(":ponsel", Koleksi::EmptyPostVal($_POST['ponsel']), PDO::PARAM_STR);
		$res->bindParam(":email", Koleksi::EmptyPostVal($_POST['email']), PDO::PARAM_STR);
		
		// Diperlukan dalam proses update
		if ($_POST['submit'] == "pegawai_update")
			$res->bindParam(":uid", $_POST['uid'], PDO::PARAM_INT);

		// Eksekusi
		$res->execute();
		
		// Tampilkan Informasi Sukses
		if ($_POST['submit'] == "pegawai_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['submit'] == "pegawai_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}
	
	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}
		
		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;
		
		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}	
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['isian'])) {

	if($_GET['isian'] == "edit") {
		if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
		$res = $db->prepare("SELECT * FROM `um_pegawai` WHERE id = :uid");
		$res->bindParam(":uid", $uid);
		$res->execute();		
		$r = $res->fetch(PDO::FETCH_ASSOC);
	}
	
	$value = '';
	
	$isi = '
<div class="alert alert-danger display-hide">
	<button class="close" data-close="alert"></button>
	Ada isian Form yang belum di isi. Mohon di periksa kembali.
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><strong>Data Pegawai</strong></h3>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<label class="col-md-3 control-label">NIP Baru</label>
			<div class="col-md-9">
				<input type="text" id="nip" name="nip" placeholder="" class="form-control placeholder-no-fix input-medium" autocomplete="on"'.(($_GET['isian'] == "edit") ? ' readonly="readonly"':'').'>
			</div>
		</div>
	
		<div class="form-group">
			<label class="col-md-3 control-label">Nama Lengkap</label>
			<div class="col-md-9">
				<input type="text" id="nama" name="nama" value="'.(($_GET['isian']=="edit") ? $r['nama'] : '').'" placeholder="" class="form-control placeholder-no-fix input-medium inline" autocomplete="on">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Gelar Depan</label>
			<div class="col-md-9">
				<input type="text" id="gelardp" name="gelardp" value="'.(($_GET['isian']=="edit") ? $r['gelardp'] : '').'" placeholder="" class="form-control placeholder-no-fix input-small inline" autocomplete="on">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Gelar Belakang</label>
			<div class="col-md-9">
				<input type="text" id="gelarbk" name="gelarbk" value="'.(($_GET['isian']=="edit") ? $r['gelarbk'] : '').'" placeholder="" class="form-control placeholder-no-fix input-small inline" autocomplete="on">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Jenis Kelamin</label>
			<div class="col-md-9">
				<div class="radio-list">';
				$pil = array("L"=>"Laki-laki","P"=>"Perempuan");
				foreach($pil as $key => $val) {
					$isi.='<label class="radio-inline">
					<input type="radio" name="jenkel" id="jenkel" value="'.$key.'"'.(($_GET['isian']=="edit") ? (($r['jenkel'] == $key) ? ' checked="checked"' : '') : '').'> '.$val.'
					</label>';
				}
				$isi.='
				</div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Agama</span></label>
			<div class="col-md-9">
				<select id="agama" name="agama" class="form-control input-medium">
				<option value="">Pilih...</option>';
				$pil = array("I"=>"Islam","P"=>"Protestan","K"=>"Katholik","H"=>"Hindu","B"=>"Budha","C"=>"Kong Hu Cu");
				foreach($pil as $key => $val) {
					$isi.='<option value="'.$key.'"'.(($_GET['isian']=="edit") ? (($r['agama'] == $key) ? ' selected=selected' : '') : '').'>'.$val.'</option>';
				}
				$isi.='
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Golongan Darah</label>
			<div class="col-md-9">
				<select id="goldarah" name="goldarah" class="form-control input-small">
				<option value="">Pilih...</option>';
				$pil = array("A"=>"A","B"=>"B","AB"=>"AB","O"=>"O");
				foreach($pil as $key => $val) {
					$isi.='<option value="'.$key.'"'.(($_GET['isian']=="edit") ? (($r['goldarah'] == $key) ? ' selected=selected' : '') : '').'>'.$val.'</option>';
				}
				$isi.='
				</select>
			</div>
		</div>	
		<div class="form-group">
			<label class="col-md-3 control-label">Sts. Pernikahan</span></label>
			<div class="col-md-9">
				<select id="status" name="status" class="form-control input-medium">
				<option value="">Pilih...</option>';
				$pil = array("K"=>"Kawin","B"=>"Belum Kawin","D"=>"Duda","J"=>"Janda");
				foreach($pil as $key => $val) {
					$isi.='<option value="'.$key.'"'.(($_GET['isian']=="edit") ? (($r['status'] == $key) ? ' selected=selected' : '') : '').'>'.$val.'</option>';
				}
				$isi.='
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">Status Pegawai</label>
			<div class="col-md-9">
				<select id="aktif" name="aktif" class="form-control input-small">
				<option value="">Pilih...</option>';
				$pil = array("Y"=>"Aktif","P"=>"Pindah","T"=>"Tidak Aktif");
				foreach($pil as $key => $val) {
					$isi.='<option value="'.$key.'"'.(($_GET['isian']=="edit") ? (($r['aktif'] == $key) ? ' selected=selected' : '') : '').'>'.$val.'</option>';
				}
				$isi.='
				</select>
			</div>
		</div>		
	</div> <!-- panel-body -->
</div> <!-- panel -->

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><strong>Kelahiran</strong></h3>
	</div>
	<div class="panel-body">
		
	<div class="form-group">
		<label class="col-md-3 control-label">Tempat Lahir</label>
		<div class="col-md-9">
			<input type="text" id="lahirtmp" name="lahirtmp" value="'.(($_GET['isian']=="edit") ? $r['lahirtmp'] : '').'" placeholder="" class="form-control placeholder-no-fix" autocomplete="on">
		</div>
	</div>	
	<div class="form-group">
		<label class="col-md-3 control-label">Tanggal Lahir</label>
		<div class="col-md-9">
			<div class="input-group input-medium date date-picker" data-date="" data-date-format="dd-mm-yyyy">
				<input type="text" name="lahirtgl" id="lahirtgl" value="'.(($_GET['isian']=="edit") ? Koleksi::tanggalSQL($r['lahirtgl']) : '').'" class="form-control">
				<span class="input-group-btn"><button class="btn btn-info" type="button"><i class="fa fa-calendar"></i></button></span>
			</div>
		</div>
	</div>
	</div> <!-- panel-body -->
</div> <!-- panel -->

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><strong>Alamat</strong></h3>
	</div>
	<div class="panel-body">

		<div class="form-group">
			<label class="col-md-3 control-label">Rumah</label>
			<div class="col-md-9">
				<textarea id="alamat" name="alamat" class="form-control input-sm" style="resize:none; overflow-y:scroll" rows="2">'.(($_GET['isian']=="edit") ? $r['alamat'] : '').'</textarea>
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">No. Telp</label>
			<div class="col-md-9">
				<input type="text" id="altelp" name="altelp" value="'.(($_GET['isian']=="edit") ? $r['altelp'] : '').'" placeholder="" class="form-control placeholder-no-fix input-medium" autocomplete="on">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">No. Handphone</label>
			<div class="col-md-9">
				<input type="text" id="alponsel" name="alponsel" value="'.(($_GET['isian']=="edit") ? $r['alponsel'] : '').'" placeholder="" class="form-control placeholder-no-fix input-medium" autocomplete="on">
			</div>
		</div>
		<div class="form-group">
			<label class="col-md-3 control-label">E-Mail</label>
			<div class="col-md-9">
				<input type="text" id="alemail" name="alemail" value="'.(($_GET['isian']=="edit") ? $r['alemail'] : '').'" placeholder="" class="form-control placeholder-no-fix" autocomplete="on">
			</div>
		</div>
	
	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><strong>Nomor Kartu</strong></h3>
	</div>
	<div class="panel-body">
	
		<div class="form-group">
			<label class="col-md-3 control-label">Nomor Karpeg</label>
			<div class="col-md-9">
				<input type="text" id="nokarpeg" name="nokarpeg" value="'.(($_GET['isian']=="edit") ? $r['nokarpeg'] : '').'" placeholder="" class="form-control placeholder-no-fix input-medium" autocomplete="on">
			</div>
		</div>
		
	</div>
</div>

<div class="panel panel-success">
	<div class="panel-heading">
		<h3 class="panel-title"><strong>Gambar</strong></h3>
	</div>
	<div class="panel-body">
	
		<div class="form-group">
			<label class="col-md-3 control-label">Avatar</label>
			<div class="col-md-9">		
				<div class="fileinput fileinput-new" data-provides="fileinput">			
					<div class="input-group">
						<div class="form-control uneditable-input span3" data-trigger="fileinput">
							<i class="fa fa-file fileinput-exists"></i>&nbsp;
							<span class="fileinput-filename"></span>
						</div>
						<span class="input-group-addon btn default btn-file">
						<span class="fileinput-new">Pilih...</span>
						<span class="fileinput-exists">Ganti</span>
						<input type="file" id="imavatar" name="imavatar" value="'.(($_GET['isian']=="edit") ? $r['imavatar'] : '').'"></span>
						<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
					</div>
					<div class="clearfix margin-top-10">
						<span class="label label-danger">Catatan!</span> Bisa nanti saja (Ukuran avatar 29 x 29 pixel!)
					</div>
				</div>
			</div>
		</div>
	
		<div class="form-group">
			<label class="col-md-3 control-label">Foto Pegawai</label>
			<div class="col-md-9">				
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
					<div>
						<span class="btn btn-default btn-file">
							<span class="fileinput-new">Pilih... </span>
							<span class="fileinput-exists">Ganti</span>
							<input type="file" id="imfoto" name="imfoto" value="'.(($_GET['isian']=="edit") ? $r['imfoto'] : '').'">
						</span>
						<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
					</div>
				</div>
				<div class="clearfix margin-top-10">
					<span class="label label-danger">Catatan!</span> Bisa nanti saja (Ukuran Foto ... pixel!)
				</div>			
			</div>
		</div>
	
	</div>
</div>
	';	
	echo $isi;
}

else if(isset($_GET['pegawai']) && $_GET['pegawai'] == 'baru') {
	
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	
	<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Biodata Diri</a></li>
			<li><a href="#tab_alamat" data-toggle="tab">Alamat</a></li>
			<li><a href="#tab_kartu" data-toggle="tab">Kartu</a></li>
			<li><a href="#tab_foto" data-toggle="tab">Foto</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
				<div class="form-group">
					<label class="col-md-3 control-label">NIP Baru</label>
					<div class="col-md-9">
						<input type="text" id="nip" name="nip" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Nama</label>
					<div class="col-md-9">
						<input type="text" id="nama" name="nama" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Gelar Depan</label>
					<div class="col-md-9">
						<input type="text" id="gelardp" name="gelardp" class="form-control input-small" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Gelar Belakang</label>
					<div class="col-md-9">
						<input type="text" id="gelarbk" name="gelarbk" class="form-control input-small" autocomplete="on">
					</div>
				</div>
				<!--
				<div class="form-group">
					<label class="col-md-3 control-label">Jenis Kelamin</label>
					<div class="col-md-9">
						<div class="radio-list">';
						$pil = array("L"=>"Laki-laki","P"=>"Perempuan");
						foreach($pil as $key => $val) {
							$isi.='<label class="radio-inline">
							<input type="radio" name="jenkel" id="jenkel" value="'.$key.'"> '.$val.'
							</label>';
						}
						$isi.='
						</div>
					</div>
				</div>
				-->
				<div class="form-group">
					<label class="col-md-3 control-label">Agama</span></label>
					<div class="col-md-9">
						<select id="agama" name="agama" class="form-control input-medium">
						<option value="">Pilih...</option>';
						$pil = array("I"=>"Islam","P"=>"Protestan","K"=>"Katholik","H"=>"Hindu","B"=>"Budha","C"=>"Kong Hu Cu");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Golongan Darah</label>
					<div class="col-md-9">
						<select id="goldarah" name="goldarah" class="form-control input-small">
						<option value="">Pilih...</option>';
						$pil = array("A"=>"A","B"=>"B","AB"=>"AB","O"=>"O");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-md-3 control-label">Sts. Pernikahan</span></label>
					<div class="col-md-9">
						<select id="status" name="status" class="form-control input-medium">
						<option value="">Pilih...</option>';
						$pil = array("K"=>"Kawin","B"=>"Belum Kawin","D"=>"Duda","J"=>"Janda");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Status Pegawai</label>
					<div class="col-md-9">
						<select id="aktif" name="aktif" class="form-control input-small">
						<option value="">Pilih...</option>';
						$pil = array("Y"=>"Aktif","P"=>"Pindah Wilayah Kerja","R"=>"Purna Bhakti","T"=>"Tidak Aktif");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>				
				<div class="form-group">
					<label class="col-md-3 control-label">Tempat Lahir</label>
					<div class="col-md-9">
						<input type="text" id="lahirtmp" name="lahirtmp" class="form-control" autocomplete="on">
					</div>
				</div>			
			</div> <!-- end tab-biodata -->

			<div class="tab-pane fade in" id="tab_alamat">
				<div class="form-group">
					<label class="col-md-3 control-label">Rumah</label>
					<div class="col-md-9">
						<textarea id="alamat" name="alamat" class="form-control input-sm" style="resize:none; overflow-y:scroll" rows="2"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Telp</label>
					<div class="col-md-9">
						<input type="text" id="telp" name="telp" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Handphone</label>
					<div class="col-md-9">
						<input type="text" id="ponsel" name="ponsel" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">E-Mail</label>
					<div class="col-md-9">
						<input type="text" id="email" name="email" class="form-control" autocomplete="on">
					</div>
				</div>
			</div> <!-- end tab-alamat -->
	
			<div class="tab-pane fade in" id="tab_kartu">
				<div class="form-group">
					<label class="col-md-3 control-label">Karpeg</label>
					<div class="col-md-9">
						<input type="text" id="karpeg" name="karpeg" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">NPWP</label>
					<div class="col-md-9">
						<input type="text" id="npwp" name="npwp" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
			</div>  <!-- end tab-kartu -->
		
			<div class="tab-pane fade in" id="tab_foto">
				Fitur menyusul.
				<!-- 
				<div class="form-group">
					<label class="col-md-3 control-label">Avatar</label>
					<div class="col-md-9">		
						<div class="fileinput fileinput-new" data-provides="fileinput">			
							<div class="input-group">
								<div class="form-control uneditable-input span3" data-trigger="fileinput">
									<i class="fa fa-file fileinput-exists"></i>&nbsp;
									<span class="fileinput-filename"></span>
								</div>
								<span class="input-group-addon btn default btn-file">
								<span class="fileinput-new">Pilih...</span>
								<span class="fileinput-exists">Ganti</span>
								<input type="file" id="imavatar" name="imavatar"></span>
								<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger">Catatan!</span> Bisa nanti saja (Ukuran avatar 29 x 29 pixel!)
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Foto Pegawai</label>
					<div class="col-md-9">				
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
							<div>
								<span class="btn btn-default btn-file">
									<span class="fileinput-new">Pilih... </span>
									<span class="fileinput-exists">Ganti</span>
									<input type="file" id="imfoto" name="imfoto">
								</span>
								<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">Catatan!</span> Bisa nanti saja (Ukuran Foto ... pixel!)
						</div>			
					</div>
				</div>
				-->					
			</div> <!-- end tab-foto -->

		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';
	
	echo $isi;
}

else if(isset($_GET['pegawai']) && $_GET['pegawai'] == 'edit') {
	
	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `um_pegawai` WHERE id = :uid");
	$res->bindParam(":uid", $uid);
	$res->execute();		
	$r = $res->fetch(PDO::FETCH_ASSOC);
	
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	
	<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Biodata Diri</a></li>
			<li><a href="#tab_alamat" data-toggle="tab">Alamat</a></li>
			<li><a href="#tab_kartu" data-toggle="tab">Kartu</a></li>
			<li><a href="#tab_foto" data-toggle="tab">Foto</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
				<div class="form-group">
					<label class="col-md-3 control-label">NIP Baru</label>
					<div class="col-md-9">
						<input type="text" id="nip" name="nip" value="'.$r['nip'].'" class="form-control input-medium" autocomplete="on" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Nama</label>
					<div class="col-md-9">
						<input type="text" id="nama" name="nama" value="'.$r['nama'].'" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Gelar Depan</label>
					<div class="col-md-9">
						<input type="text" id="gelardp" name="gelardp" value="'.$r['gelardp'].'" class="form-control input-small" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Gelar Belakang</label>
					<div class="col-md-9">
						<input type="text" id="gelarbk" name="gelarbk" value="'.$r['gelarbk'].'" class="form-control input-small" autocomplete="on">
					</div>
				</div>
				<!-- Hidden
				<div class="form-group">
					<label class="col-md-3 control-label">Jenis Kelamin</label>
					<div class="col-md-9">
						<div class="radio-list">';
						$pil = array("L"=>"Laki-laki","P"=>"Perempuan");
						foreach($pil as $key => $val) {
							$isi.='<label class="radio-inline">
							<input type="radio" name="jenkel" id="jenkel" value="'.$key.'"'.(($r['jenkel'] == $key) ? ' checked="checked"' : '').'> '.$val.'
							</label>';
						}
						$isi.='
						</div>
					</div>
				</div>
				-->
				<div class="form-group">
					<label class="col-md-3 control-label">Agama</span></label>
					<div class="col-md-9">
						<select id="agama" name="agama" class="form-control input-medium">
						<option value="">Pilih...</option>';
						$pil = array(
							"I"=>"Islam",
							"P"=>"Protestan",
							"K"=>"Katholik",
							"H"=>"Hindu",
							"B"=>"Budha",
							"C"=>"Kong Hu Cu"
						);
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['agama'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Golongan Darah</label>
					<div class="col-md-9">
						<select id="goldarah" name="goldarah" class="form-control input-small">
						<option value="">Pilih...</option>';
						$pil = array("A"=>"A","B"=>"B","AB"=>"AB","O"=>"O");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['goldarah'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label class="col-md-3 control-label">Sts. Pernikahan</span></label>
					<div class="col-md-9">
						<select id="status" name="status" class="form-control input-medium">
						<option value="">Pilih...</option>';
						$pil = array("K"=>"Kawin","B"=>"Belum Kawin","D"=>"Duda","J"=>"Janda");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['status'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Status Pegawai</label>
					<div class="col-md-9">
						<select id="aktif" name="aktif" class="form-control input-small">
						<option value="">Pilih...</option>';
						$pil = array("Y"=>"Aktif","P"=>"Pindah Wilayah Kerja","R"=>"Purna Bhakti","T"=>"Tidak Aktif");
						foreach($pil as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['aktif'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>				
				<div class="form-group">
					<label class="col-md-3 control-label">Tempat Lahir</label>
					<div class="col-md-9">
						<input type="text" id="lahirtmp" name="lahirtmp" value="'.$r['lahirtmp'].'" class="form-control" autocomplete="on">
					</div>
				</div>			
			</div> <!-- end tab-biodata -->

			<div class="tab-pane fade in" id="tab_alamat">
				<div class="form-group">
					<label class="col-md-3 control-label">Rumah</label>
					<div class="col-md-9">
						<textarea id="alamat" name="alamat" class="form-control input-sm" style="resize:none; overflow-y:scroll" rows="2">'.$r['alamat'].'</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Telp</label>
					<div class="col-md-9">
						<input type="text" id="telp" name="telp" value="'.$r['telp'].'" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Handphone</label>
					<div class="col-md-9">
						<input type="text" id="ponsel" name="ponsel" value="'.$r['ponsel'].'" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">E-Mail</label>
					<div class="col-md-9">
						<input type="text" id="email" name="email" value="'.$r['email'].'" class="form-control" autocomplete="on">
					</div>
				</div>
			</div> <!-- end tab-alamat -->
	
			<div class="tab-pane fade in" id="tab_kartu">
				<div class="form-group">
					<label class="col-md-3 control-label">Karpeg</label>
					<div class="col-md-9">
						<input type="text" id="karpeg" name="karpeg" value="'.$r['karpeg'].'" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">NPWP</label>
					<div class="col-md-9">
						<input type="text" id="npwp" name="npwp" value="'.$r['npwp'].'" class="form-control input-medium" autocomplete="on">
					</div>
				</div>
			</div>  <!-- end tab-kartu -->
		
			<div class="tab-pane fade in" id="tab_foto">
				Fitur menyusul.
				<!-- 
				<div class="form-group">
					<label class="col-md-3 control-label">Avatar</label>
					<div class="col-md-9">		
						<div class="fileinput fileinput-new" data-provides="fileinput">			
							<div class="input-group">
								<div class="form-control uneditable-input span3" data-trigger="fileinput">
									<i class="fa fa-file fileinput-exists"></i>&nbsp;
									<span class="fileinput-filename"></span>
								</div>
								<span class="input-group-addon btn default btn-file">
								<span class="fileinput-new">Pilih...</span>
								<span class="fileinput-exists">Ganti</span>
								<input type="file" id="imavatar" name="imavatar" value="'.$r['imavatar'].'"></span>
								<a href="#" class="input-group-addon btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
							</div>
							<div class="clearfix margin-top-10">
								<span class="label label-danger">Catatan!</span> Bisa nanti saja (Ukuran avatar 29 x 29 pixel!)
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Foto Pegawai</label>
					<div class="col-md-9">				
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
							<div>
								<span class="btn btn-default btn-file">
									<span class="fileinput-new">Pilih... </span>
									<span class="fileinput-exists">Ganti</span>
									<input type="file" id="imfoto" name="imfoto" value="'.$r['imfoto'].'">
								</span>
								<a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Hapus</a>
							</div>
						</div>
						<div class="clearfix margin-top-10">
							<span class="label label-danger">Catatan!</span> Bisa nanti saja (Ukuran Foto ... pixel!)
						</div>			
					</div>
				</div>
				-->					
			</div> <!-- end tab-foto -->

		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';
	$db = null; // Tutup koneksi
	echo $isi;
}

else if(isset($_GET['pegawai']) && $_GET['pegawai'] == 'hapus') {
	header("Content-type: application/json; charset=UTF-8");
	if(isset($_POST['uid'])) {
		try {		
			$res = $db->prepare("DELETE FROM `um_pegawai` WHERE `id` = :uid");
			$res->bindParam(':uid', $_POST['uid']);
			$res->execute();
			$response = array( 'status'=>'sukses','pesan'=>'' );
		} catch(PDOException $e) {
			$response = array( 'status'=>'gagal','pesan'=>'<h4>Perhatian!</h4><p>'.$sql.'<br><br>'.$e->getMessage().'</p>' );
		}		
		
		// Tampilkan Hasil Eksekusi
		echo json_encode($response);
		
		// Tutup koneksis
		$db = null;
	}
}

else if(isset($_GET['pegawai']) && $_GET['pegawai'] == 'tabeldata') {
	
	header("Content-type:application/json");
	
	// FIX untuk satu tabel
	/*	
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/
	
	// Kolom yang bisa di Sortir
	$columns = array(
		2=>'nip',
		3=>'nama'
	);
	
	// Untuk 2 Tabel
	/*
	$sql = "SELECT 
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/

	$sql = "SELECT 
	  i.id,
	  i.imavatar,
	  i.nip,
	  i.nama,
	  CONCAT(IF(ISNULL(i.gelardp), '', CONCAT(i.gelardp, '. ')), i.nama, IF(ISNULL(i.gelarbk), '', CONCAT(', ', i.gelarbk))) AS namagelar,
	  i.lahirtmp,
	  CONCAT(IF(ISNULL(i.lahirtmp), '', CONCAT(i.lahirtmp, ', ')), IF(ISNULL(i.lahirtgl), '', DATE_FORMAT(i.lahirtgl, '%d-%m-%Y'))) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif,
	  i.ponsel
	FROM
	  um_pegawai i
	WHERE
	  i.nip IS NOT NULL";

	// Custom Filter
	if (isset($_REQUEST['status']) && $_REQUEST['status'] != '') $sql.=" AND aktif='".$_REQUEST['status']."'";
	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" AND i.nip LIKE '%".$_REQUEST['search']['value']."%'";
		$sql.=" OR i.nama LIKE '%".$_REQUEST['search']['value']."%'";
		$sql.=" OR i.lahirtmp LIKE '%".$_REQUEST['search']['value']."%'";
	}
	
	$res = $db->prepare($sql);
	$res->execute();
	
	$totalData = $res->rowCount();
	$totalFiltered = $totalData;
		
	if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];

	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
		//$no++;
		$nestedData = array();
		// Nomor Urut
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		// Isi Tabel
		$nestedData[] = $row['imavatar'];
		$nestedData[] = Koleksi::spasiNip($row['nip']);
		$nestedData[] = $row['namagelar'].'<br>'.$row['kelahiran'];
		// $nestedData[] = $row['nip'];
		$nestedData[] = '<center>'.$row['jenkel'].'</center>';
		$nestedData[] = $row['ponsel'];
		/*
		$arr = array("Y"=>"Aktif", "T"=>"Tidak Aktif", "P"=>"Pindah");
		$aktif = isset($arr[$row[6]]) ? $arr[$row[6]] : null;
		*/
		if($row['aktif'] == "Y") {
			$nestedData[] = '<center><span class="label label-sm label-primary"> Aktif </span></center>';
		} else if ($row['aktif'] == "T") {
			$nestedData[] = '<center><span class="label label-sm label-default"> Tidak Aktif </span></center>';
		} else if ($row['aktif'] == "P") {
			$nestedData[] = '<center><span class="label label-sm label-warning"> PWK</span> </center>';
		} else if ($row['aktif'] == "R") {
			$nestedData[] = '<center><span class="label label-sm label-danger"> Purna Bhakti</span> </center>';
		} else {
			$nestedData[] = '';
		}
		
		// Opsi
		// $otoritas = 
		
		if(isset($_SESSION['level']))
		{
			if($_SESSION['level'] == 'admin' || $_SESSION['nip'] == $row['nip'])
			{
				$nestedData[] = '<center><div style="width:85px; border:0px solid #000">
				<div class="task-config">
					<div class="task-config-btn btn-group">
						<a href="#" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							Data <i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li><a href="#" id="edit" data-id="'.$row['id'].'" data-target="#form-modal" data-toggle="modal"><i class="fa fa-edit"></i> Ubah</a></li>';
							if ($_SESSION['level'] == 'admin') {
							$nestedData[].='
							<li class="divider"></li>
							<li><a href="#" id="hapus" data-id="'.$row['id'].'"><i class="fa fa-times"></i> Hapus</a></li>';
							}
							$nestedData[].='
						</ul>
					</div>
				</div>		
				</div></center>';			
			} else {
				$nestedData[] = '';
			}
		} 
		
		$data[] = $nestedData;
		$no++;
	}	
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {	
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">	
		
		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Pegawai Inspektorat Provinsi Kalimantan Utara
					</div>';
					if(isset($_SESSION['level']) && $_SESSION['level'] == 'admin') {
					$isi.='
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<a class="btn btn-sm btn-success dropdown-toggle" href="#" data-toggle="dropdown" style="margin-top:-3px">
								<i class="fa fa-cog"></i>
								<span class="hidden-480">Manajemen Data</span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
					';
					}
				$isi.='
				</div> <!-- portlet-title -->

				<div class="portlet-body">
					<div class="table-container">

						<div class="table-actions-wrapper">
							<span>
							</span>
							
							<form id="form-filter" class="form-horizontal">						
								<select name="status" id="status" class="form-control table-group-action-input form-filter input-inline input-small input-sm">
									<option value="">Filter...</option>
									<option value="Y">Aktif</option>
									<option value="P">Pindah</option>
									<option value="R">Purna</option>
									<option value="T">Tidak Aktif</option>
								</select>
								<button class="btn btn-sm btn-warning table-group-action-submit tooltips" data-placement="top" data-original-title="Filter"><i class="fa fa-filter"></i></button>
								<button class="btn btn-sm btn-danger table-group-action-reset tooltips" data-placement="top" data-original-title="Reset"><i class="fa fa-refresh"></i></button>
							</form>
							
						</div> <!-- End table-actions-wrapper -->
						
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th width="1%"><center>No</center></th>
						  <th width="10%"><center>Foto</center></th>
						  <th width="16%">NIP</th>
						  <th width="24%">Nama</th>
						  <th width="20%" class="cellcenter">Jenis Kelamin</th>
						  <th width="10%">Kontak</th>
						  <th width="10%" class="cellcenter">Status</th>
						  <th width="9%"></th>
						</tr>					
						</thead>
						<tbody></tbody>
						</table>
					
					</div> <!-- table-container -->
				</div> <!-- portlet-body -->
				
			</div>	
		</div>
		
		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="uid" name="uid" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>
						
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->
		
	</div>
	';
	
	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>