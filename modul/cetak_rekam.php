<?php
$nip=$_POST['nip'];
require 'configcetak.php';
$conn = new mysqli($servername, $username, $password, $dbname);


header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Rekam Jejak ".date('d-m-Y H:i:s').".xls");
echo "<br>
    <br>
    <br>
    <table>
    <tr>
    <th colspan=\"1\"></th> 
    <th colspan=\"4\">INSPEKTORAT DAERAH PROVINSI KALIMANTAN UTARA</th> 
    </tr>
    <tr>
    <th colspan=\"1\"></th> 
    <th colspan=\"4\"><b>CETAK REKAM JEJAK<b></th> 
    </tr>
    </table>
    <br>";


if(!empty($_POST['nip'])) {
    $tabel=1;
    $rekam=array();
    $nip=$_POST['nip'];
    $no=1;
    echo "NIP : ".$nip;
    $sql="SELECT
                `was_data`.`was_nolhp`
                , `was_data`.`was_obyek`
                , `was_tl_data`.`tl_uraian`
                , `was_tlrinci`.`rinci_nilai`
                , `was_tlrinci`.`rinci_bayar`
                , `was_tlrinci`.`rinci_nip`
                , `was_tlrinci`.`rinci_nama`
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                INNER JOIN `was_rekomendasi` 
                    ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                INNER JOIN `was_tlrinci` 
                    ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
            WHERE (`was_tlrinci`.`rinci_nip` = '$nip')";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $nested= array();
        $nested []=$no;
        $nested []=$row['was_nolhp'];
        $nested []=$row['was_obyek'];
        $nested []=$row['tl_uraian'];
        $nested []=$row['rinci_nilai'];
        $nested []=$row['rinci_bayar'];
        $sisa=$row['rinci_nilai']-$row['rinci_bayar'];
        $nested []=$sisa;
        if ($sisa>0){
            $nested []="Tengah Berjalan";
        }
        else {
            $nested []="Telah Selesai";
        }
        $nested []=$row['rinci_nip'];
        $nested []=$row['rinci_nama'];
        $no++;
        $rekam []=$nested;
    }
    echo "<br>Nama : ".$row['rinci_nama'];
}
else {
    $tabel=1;
    $rekam=array();
    $nama=$_POST['nama'];
    echo "Nama : ".$nama;
    $no=1;
    $sql="SELECT
                `was_data`.`was_nolhp`
                , `was_data`.`was_obyek`
                , `was_tl_data`.`tl_uraian`
                , `was_tlrinci`.`rinci_nilai`
                , `was_tlrinci`.`rinci_bayar`
                , `was_tlrinci`.`rinci_nip`
                , `was_tlrinci`.`rinci_nama`
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                INNER JOIN `was_rekomendasi` 
                    ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                INNER JOIN `was_tlrinci` 
                    ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
            WHERE (`was_tlrinci`.`rinci_nama` like '%$nama%' and was_tlrinci.rinci_nip = '0')";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $nested= array();
        $nested []=$no;
        $nested []=$row['was_nolhp'];
        $nested []=$row['was_obyek'];
        $nested []=$row['tl_uraian'];
        $nested []=$row['rinci_nilai'];
        $nested []=$row['rinci_bayar'];
        $sisa=$row['rinci_nilai']-$row['rinci_bayar'];
        $nested []=$sisa;
        if ($sisa>0){
            $nested []="Tengah Berjalan";
        }
        else {
            $nested []="Telah Selesai";
        }
        $nested []=$row['rinci_nip'];
        $nested []=$row['rinci_nama'];
        $no++;
        $rekam []=$nested;
    }

}
$x=0;
$y=0;
$z=0;
echo "<br>
    <table border=1>
    <thead>
    <tr>
      <th><center>No</center></th>
      <th>No LHP</th>
      <th>OPD</th>
      <th>Deskripsi Temuan</th>
      <th>Nilai Temuan</th>
      <th>Nilai Terbayar</th>
      <th>Nilai Sisa</th>
      <th>Status</th>
    </tr>
    <tr>
    <th>1</th>
    <th>2</th>
    <th>3</th>
    <th>4</th>
    <th>5</th>
    <th>6</th>
    <th>7</th>
    <th>8</th>
    </tr>
    </thead>
    <tbody>
    ";
foreach($rekam as $value) {
    echo '<tr>
            <td>'.$value[0].'</td>
            <td>'.$value[1].'</td>
            <td>'.$value[2].'</td>
            <td>'.$value[3].'</td>
            <td> Rp. '.number_format($value[4],2,",",".").' </td>
            <td> Rp. '.number_format($value[5],2,",",".").' </td>
            <td> Rp. '.number_format($value[6],2,",",".").' </td>
            <td>'.$value[7].'</td>';
    //if(empty($_POST['nip'])) {
            echo '<td>'.$value[9].'</td>';    
    //}
    echo '</tr>';
    $x=$x+$value[4];
    $y=$y+$value[5];
    $z=$z+$value[6];
    }
    if ($z>0){
        $status="Tengah Berjalan";
    }
    else {
        $status="Telah Selesai";
    }
echo "
    <tr>
    <th colspan=4>Total</th>
    <th>".number_format($x,2,",",".")."</th>
    <th>".number_format($y,2,",",".")."</th>
    <th>".number_format($z,2,",",".")."</th>
    <th>".$status."</th>
    </tr>
   </tbody>
    </table>";

?>
