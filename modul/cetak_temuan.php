<?php
$tahun=$_POST['tahun'];
$obyek=$_POST['obyek'];
$kel=$_POST['kel'];
require 'configcetak.php';
//$password = "73Q8sr8Gau4JifEW";
$conn = new mysqli($servername, $username, $password, $dbname);


header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Temuan ".date('d-m-Y H:i:s').".xls");
echo "<br>";
echo "<br>";
echo "<br>";
echo "<table>";
echo "<tr>";
echo "<th colspan=\"1\"></th> ";
echo "<th colspan=\"4\">INSPEKTORAT DAERAH PROVINSI KALIMANTAN UTARA</th> ";
echo "</tr>";
echo "<tr>";
echo "<th colspan=\"1\"></th> ";
echo "<th colspan=\"4\"><b>LAPORAN TEMUAN PEMERIKSAAN <b></th> ";
echo "</tr>";
echo "</table>";
 echo "<br>";
if ($obyek!='0'){
     echo "OPD :".$obyek;
    echo "<br>";
}
if ($tahun!='0'){
     echo "Tahun :".$tahun;
    echo "<br>";
}


$sql="SELECT
     `was_tl_data`.`tl_kelompok`
FROM
    `was_tl_data`
    INNER JOIN `was_data` 
        ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)";
if ($obyek!='0'|| $tahun!='0' || $kel!='0'){
$sql.= "WHERE (";
    if ($obyek!='0'){
     $sql .= " `was_data`.`was_obyek` LIKE '".$obyek."' AND ";
    }
    if ($tahun!='0'){
     $sql .= " `was_data`.`was_tahun` = '".$tahun."'AND ";
    }
    if ($kel!='0'){
     $sql .= " `was_tl_data`.`tl_kelompok` = '".$kel."' AND ";
    }
$sql = substr($sql,0,-4);
$sql.=")";
}
else {
    
}

$sql.="GROUP BY `was_tl_data`.`tl_kelompok`
ORDER BY `was_data`.`was_tahun` ASC, `was_tl_data`.`tl_kelompok` ASC;";
$result = $conn->query($sql);
echo "<br>";
echo "<table border=1>";
echo "<tr>";
echo "<th>No</th>";
echo "<th>Sub Kelompok Temuan</th>";
echo "<th>Kode</th>";
echo "<th>Jumlah Kejadian</th>";
echo "<th>Presentase</th>";
echo "<th>Nilai</th>";
echo "</tr>";
echo "<tr>";
echo "<th>1</th>";
echo "<th>2</th>";
echo "<th>3</th>";
echo "<th>4</th>";
echo "<th>5</th>";
echo "<th>6</th>";
echo "</tr>";
$totaltemuan=0;
$totalnilai=0;
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $no=1;
        $nilai=0;
        $namakel="SELECT
                    `kode_deskripsi`
                FROM
                    `was_tl_kode`
                WHERE (`kode_kel` =".$row["tl_kelompok"]."
                    AND `kode_sub_kel` =0);";
        $namakel = $conn->query($namakel);
        $namakel = $namakel->fetch_assoc();
        echo "<tr>";
        echo "<th colspan=6 style=\"text-align:left\" ><b>".$namakel["kode_deskripsi"]."<b></th>";
        echo "</tr>";
        $sql="SELECT
                `was_data`.`was_id`
                , `was_data`.`was_obyek`
                , `was_data`.`was_tahun`
                , `was_tl_data`.`tl_kelompok`
                , `was_tl_data`.`tl_subkel`
                , COUNT(`was_tl_data`.`tl_id`) AS `jumlah`
                , SUM(`was_rekomendasi`.`rek_nilai`)  AS `nilai`
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                INNER JOIN `was_rekomendasi` 
                    ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)";
        if ($obyek!='0'|| $tahun!='0'){
            $sql.= "WHERE (";
                if ($obyek!='0'){
                 $sql .= " `was_data`.`was_obyek` LIKE '".$obyek."' AND ";
                }
                if ($tahun!='0'){
                 $sql .= " `was_data`.`was_tahun` = '".$tahun."'AND ";
                }
            $sql = substr($sql,0,-4);
            $sql.="AND `was_tl_data`.`tl_kelompok` = '".$row["tl_kelompok"]."')";
        }
        else {
            $sql.="WHERE (`was_tl_data`.`tl_kelompok` = '".$row["tl_kelompok"]."')";
        }
        $sql.="GROUP BY `was_tl_data`.`tl_kelompok`, `was_tl_data`.`tl_subkel`
        ORDER BY `was_tl_data`.`tl_subkel` ASC;";
        $sqlsum="Select SUM(jumlah) AS total from (".substr($sql,0,-1)." ) total";
        $result2 = $conn->query($sqlsum);
        $row2 = $result2->fetch_assoc();
        $total=$row2["total"];
        $result3 = $conn->query($sql);
        while($row3 = $result3->fetch_assoc()) {
            echo "<tr>";
            echo "<td>".$no."</td>";
            $namakel="SELECT
                    `kode_deskripsi`
                FROM
                    `was_tl_kode`
                WHERE (`kode_kel` =".$row3["tl_kelompok"]."
                    AND `kode_sub_kel` =".$row3["tl_subkel"].");";
            $namakel = $conn->query($namakel);
            $namakel = $namakel->fetch_assoc();
            echo "<td>".$namakel["kode_deskripsi"]."</td>";
            echo "<td>".$row3["tl_subkel"]."</td>";
            echo "<td>".$row3["jumlah"]."</td>";
            $persen=$row3["jumlah"]/$row2["total"]*100;
            echo "<td>".number_format($persen,2,",",".")."</td>";
            echo "<td>Rp. ".number_format($row3["nilai"],2,",",".")."</td>";
            $nilai=$nilai+$row3["nilai"];
            echo "</tr>";
            $no++;
        }
        echo "<tr>";
        echo "<th colspan=3><b>Sub Total Kejadian<b></th>";
        echo "<th style=\"text-align:right\" >".$row2["total"]."</th>";
        $totaltemuan=$totaltemuan+$row2["total"];
        echo "<th style=\"text-align:right\">100%</th>";
        echo "<th style=\"text-align:left\"> Rp. ".number_format($nilai,2,",",".")."</th>";
        $totalnilai=$totalnilai+$nilai;
        echo "</tr>";
    }
} 
echo "<tr>";
        echo "<th colspan=3><b>Sub Total Kejadian<b></th>";
        echo "<th style=\"text-align:right\" >".$totaltemuan."</th>";
        echo "<th style=\"text-align:right\">100%</th>";
        echo "<th style=\"text-align:left\"> Rp. ".number_format($totalnilai,2,",",".")."</th>";
        echo "</tr>";
echo "<table>";

?>
