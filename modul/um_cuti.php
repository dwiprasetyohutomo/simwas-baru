<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=um_cuti';
$theme		= 'themes/conquer';
$tabelData	= 'um_cuti';
$pageTitle	= 'Umum Kepegawaian <small>Daftar Pegawai</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i> 
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Umum Kepegawaian</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Daftar Pegawai</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-um-cuti.js"></script>
';
$initA = '
// var modul = "modul.php?ke=um_cuti";
var modul = window.location.href;
var pageTitle = "Form Data Pegawai";
	
// CRUD
var menuBaru 	= modul+"&cuti=baru";
var menuEdit	= modul+"&cuti=edit";
	
// Table Data
var srcUrl 	= modul+"&cuti=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
cuti.init();
';
$initC = '';
// MAIN CODE

//DATA KD JENIS IN ARRAY
$kdjenis = array();
$query = "select * from um_cuti_jenis";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdjenis += [$jenis_id => $jenis_ket];
}
//DATA PEGAWAI IN ARRAY
$peg = array();
$query = "select nip, nama from um_pegawai";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $peg += [$nip => $nama];
}
//DATA KD STATUS IN ARRAY
$kdstatus = array();
$query = "select * from um_cuti_status";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdstatus += [$status_id => $status_ket];
}
//DATA ALASAN IN ARRAY
$kdalasan = array();
$query = "select * from um_cuti_alasan";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdalasan += [$alasan_id => $alasan_ket];
}
if (isset($_POST['submit'])) {
	
	/*
	$avname		= $_FILES['imavatar']['name'];
	$avsize		= $_FILES['imavatar']['size'];
	$avtype		= $_FILES['imavatar']['type'];
	$avtmp 		= $_FILES['imavatar']['tmp_name'];

	$ftname		= $_FILES['imfoto']['name'];
	$ftsize		= $_FILES['imfoto']['size'];
	$fttype		= $_FILES['imfoto']['type'];
	$ftmp 		= $_FILES['imfoto']['tmp_name'];
	
	$ekstensi	= array('jpg','jpeg');
	
	$lok_avfile	= "img/profil/".$avname;
	$lok_ftfile	= "img/profil/".$avname;
	
	$file_av	= explode(".", $avname);
	$ext_av		= array_pop($file_av);
	
	$file_ft	= explode(".", $ftname);
	$ext_ft		= array_pop($file_ft);	
	*/
		
	try {
		// Query Simpan
		if ($_POST['aksi'] == "cuti_simpan") {
			$sql = "INSERT INTO `um_cuti`
			(`cuti_nip`,
			`cuti_jenis`,
			`cuti_dokumen`,
			`cuti_jumlah`,
			`cuti_mulai`,
			`cuti_hingga`,
			`cuti_status`,
			`cuti_alasan`)
			VALUES
			(:nip,
            :jenis,
            :dokumen,
            :jumlah,
            :mulai,
            :hingga,
            :status,
            :alasan            
            )";
		}
		
		// Query Update
		if ($_POST['aksi'] == "cuti_update") {
			$sql = "UPDATE `um_cuti` SET 
			`cuti_jenis` = :jenis,
			`cuti_alasan` = :alasan,
			`cuti_dokumen` = :dokumen,
			`cuti_jumlah` = :jumlah,
			`cuti_mulai` = :mulai,
			`cuti_hingga` = :hingga,
			`cuti_status` = '1'
			WHERE `cuti_id` = :uid";
		}
		
		// Persiapkan Query
		$res = $db->prepare($sql);
	
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		//$nip = preg_replace('/\D/','', $_POST['nip']);
		$nip = $_POST['nip'];
		
		// Pasangkan dan Eksekusi
		if ($_POST['aksi'] == "cuti_simpan")			
			$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		
		$res->bindParam(":jenis", Koleksi::EmptyPostVal($_POST['jenis']));
		$res->bindParam(":dokumen", Koleksi::EmptyPostVal($_POST['dokumen']));
		$res->bindParam(":jumlah", Koleksi::EmptyPostVal($_POST['jumlah']));
		$res->bindParam(":mulai", Koleksi::EmptyPostVal($_POST['mulai']));
		$res->bindParam(":hingga", Koleksi::EmptyPostVal($_POST['hingga']));
		$res->bindParam(":status", Koleksi::EmptyPostVal($_POST['status']));
        if (!empty($_POST['alasan1'])){
            $res->bindParam(":alasan", Koleksi::EmptyPostVal($_POST['alasan1']));
        } else {
            $res->bindParam(":alasan", Koleksi::EmptyPostVal($_POST['alasan2']));
        }
		
		// Diperlukan dalam proses update
		if ($_POST['aksi'] == "cuti_update")
			$res->bindParam(":uid", $_POST['cuti_id'], PDO::PARAM_INT);
		// Eksekusi
		$res->execute();
		
		// Tampilkan Informasi Sukses
		if ($_POST['aksi'] == "cuti_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "cuti_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}
	
	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}
		
		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;
		
		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}	
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['cuti']) && $_GET['cuti'] == 'baru') {
	
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	
		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Cuti</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
				<div class="form-group">
					<label class="col-md-3 control-label">Nama</span></label>
					<div class="col-md-9">
						<select id="nip" name="nip" class="form-control input-medium">
						<option value="">Pilih...</option>';
						foreach($peg as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jenis Cuti</span></label>
					<div class="col-md-9">
						<select id="jenis" name="jenis" class="form-control input-medium">
						<option value="">Pilih...</option>';
						foreach($kdjenis as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>		
                <div class="form-group">
					<label class="col-md-3 control-label">Alasan</label>
					<div class="col-md-9">
						<input type="text" id="alasan1" name="alasan1" value="" class="form-control" autocomplete="on" style="display: none">
					</div>
                    <div class="col-md-9">
						<select id="alasan2" name="alasan2" class="form-control input-medium" style="display: none">
						<option value="">Pilih...</option>';
						foreach($kdalasan as $key => $val) {
							$isi.='<option>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Dokumen </label>
					<div class="col-md-9">
						<input type="text" id="dokumen" name="dokumen" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jumlah </label>
					<div class="col-md-9">
						<input type="text" id="jumlah" name="jumlah" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Mulai</label>
					<div class="col-md-9">
						<input type="date" id="mulai" name="mulai" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Berakhir</label>
					<div class="col-md-9">
						<input type="date" id="hingga" name="hingga" value="" class="form-control" autocomplete="on">
					</div>
				</div>
			</div> <!-- end tab-biodata -->	
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';
	
	echo $isi;
}

else if(isset($_GET['cuti']) && $_GET['cuti'] == 'edit') {
	
	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `um_cuti` WHERE cuti_id = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();		
	$r = $res->fetch(PDO::FETCH_ASSOC);
	
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	
	<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Cuti</a></li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <input type="hidden" id="cuti_id" name="cuti_id" value="'.$r['cuti_id'].'">
				<div class="form-group">
					<label class="col-md-3 control-label">NIP Baru</label>
					<div class="col-md-9">
						<input type="text" id="nip" name="nip" value="'.$r['cuti_nip'].'" class="form-control input-medium" autocomplete="on" readonly="readonly">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Jenis Cuti</span></label>
					<div class="col-md-9">
						<select id="jenis" name="jenis" class="form-control input-medium">
						<option value="">Pilih...</option>';
						foreach($kdjenis as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['cuti_jenis'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>		
				<div class="form-group">
					<label class="col-md-3 control-label">Alasan</label>
					<div class="col-md-9">
				        <textarea class="form-control" rows="3" id="alasan" name="alasan" >'.$r['cuti_alasan'].'</textarea>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Dokumen </label>
					<div class="col-md-9">
						<input type="text" id="dokumen" name="dokumen" value="'.$r['cuti_dokumen'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jumlah </label>
					<div class="col-md-9">
						<input type="text" id="jumlah" name="jumlah" value="'.$r['cuti_jumlah'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Mulai</label>
					<div class="col-md-9">
						<input type="date" id="mulai" name="mulai" value="'.$r['cuti_mulai'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Berakhir</label>
					<div class="col-md-9">
						<input type="date" id="hingga" name="hingga" value="'.$r['cuti_hingga'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Status Cuti</span></label>
					<div class="col-md-9">
						<select id="status" name="status" class="form-control input-medium">
						<option value="">Pilih...</option>';
						foreach($kdstatus as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['cuti_status'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
			</div> <!-- end tab-biodata -->	
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';
	$db = null; // Tutup koneksi
	echo $isi;
}
else if(isset($_GET['cuti']) && $_GET['cuti'] == 'tabeldata') {
	
	header("Content-type:application/json");
	
	// FIX untuk satu tabel
	/*	
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/
	
	// Kolom yang bisa di Sortir
	$columns = array(
		2=>'cuti_nip',
		3=>'cuti_jenis',
		4=>'cuti_mulai'
	);
	
	// Untuk 2 Tabel
	/*
	$sql = "SELECT 
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/

$sql = "SELECT
        `um_cuti`.*
        , `um_pegawai`.`nama`
    FROM
        `1n5p3k7o12a7`.`um_cuti`
    LEFT JOIN `1n5p3k7o12a7`.`um_pegawai` 
        ON (`um_cuti`.`cuti_nip` = `um_pegawai`.`nip`)";

	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" AND cuti_nip LIKE '%".$_REQUEST['search']['value']."%'";
		$sql.=" OR cuti_jenis LIKE '%".$_REQUEST['search']['value']."%'";
		$sql.=" OR cuti_mulai LIKE '%".$_REQUEST['search']['value']."%'";
	}
	
	$res = $db->prepare($sql);
	$res->execute();
	
	$totalData = $res->rowCount();
	$totalFiltered = $totalData;
		
	if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];

	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
		//$no++;
		$nestedData = array();
		// Nomor Urut
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		// Isi Tabel
		$nestedData[] = Koleksi::spasiNip($row['cuti_nip']);
		$nestedData[] = $row['nama'];
		$nestedData[] = $kdjenis[$row['cuti_jenis']];
		$nestedData[] = $row['cuti_alasan'];
		$nestedData[] = $row['cuti_jumlah'];
		$nestedData[] = $row['cuti_mulai'];
		$nestedData[] = $row['cuti_hingga'];

		/*
		$arr = array("Y"=>"Aktif", "T"=>"Tidak Aktif", "P"=>"Pindah");
		$aktif = isset($arr[$row[6]]) ? $arr[$row[6]] : null;
		*/
		if($row['cuti_status'] == "1") {
			$nestedData[] = '<center><span class="label label-sm label-warning"> Dalam Proses </span></center>';
		} 
        else  {
			$nestedData[] = '<center><span class="label label-sm label-success"> Disetujui </span></center>';
		}
		// Opsi
		// $otoritas = 
		

				$nestedData[] = '
							<a href="#" id="edit" data-id="'.$row['cuti_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Rubah </span></center>
							';			
		
		$data[] = $nestedData;
		$no++;
	}	
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}
else {	
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">	
		
		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Pegawai Inspektorat Provinsi Kalimantan Utara
					</div>
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<a class="btn btn-sm btn-success dropdown-toggle" href="#" data-toggle="dropdown" style="margin-top:-3px">
								<i class="fa fa-cog"></i>
								<span class="hidden-480">Manajemen Data</span>
								<i class="fa fa-angle-down"></i>
							</a>
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
				</div> <!-- portlet-title -->

				<div class="portlet-body">
					<div class="table-container">						
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th><center>No</center></th>
						  <th>NIP</th>
						  <th>Nama</th>
						  <th>Jenis</th>
						  <th>Alasan</th>
						  <th>Lamanya</th>
						  <th>Mulai</th>
						  <th>Hingga</th>
						  <th></th>
						  <th></th>
						</tr>					
						</thead>
						<tbody></tbody>
						</table>
					
					</div> <!-- table-container -->
				</div> <!-- portlet-body -->
				
			</div>	
		</div>
		
		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>
						
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->
		
	</div>
	';
	
	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>