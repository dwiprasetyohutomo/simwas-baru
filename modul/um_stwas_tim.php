<?php 
error_reporting(9);
//require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

// Buka Koneksi
$tabelData	= 'um_stwas_tim';
$modul		= 'modul.php?ke=um_stwas_tim';
$pageTitle	= 'Surat Tugas (Pengawasan)';

// MAIN CODE
if (isset($_POST['aksi'])) {
	$sql = '';
	try {
		if ($_POST['aksi'] == "tim_simpan") {
			$sql = "INSERT INTO 
			  `um_stwas_tim`(
			  `idst`,`kode`,`tahun`,`pos`,`nip`,`gol`,`jab`,`sert`,`pp_peran`,`pp_jmlhari`,`pp_jmljam`,`pp_jamreal`,`pp_sak`,`pp_jak`
			  ) VALUE (
			  :idst, :kode, :tahun, :pos, :nip, :gol, :jab, :sert, :pp_peran, :pp_jmlhari, :pp_jmljam, :pp_jamreal, :pp_sak, :pp_jak )";
		}
		if ($_POST['aksi'] == "tim_update") {
			$sql = "UPDATE 
			  `um_stwas_tim`  
			SET
			  `idst` = :idst,
			  `kode` = :kode,
			  `tahun` = :tahun,
			  `pos` = :pos,
			  `nip` = :nip,
			  `gol` = :gol,
			  `jab` = :jab,
			  `sert` = :sert,
			  `pp_peran` = :pp_peran,
			  `pp_jmlhari` = :pp_jmlhari,
			  `pp_jmljam` = :pp_jmljam,
			  `pp_jamreal` = :pp_jamreal,
			  `pp_sak` = :pp_sak,
			  `pp_jak` = :pp_jak
			WHERE 
			  `id` = :uid";
		}
		
		$res = $db->prepare($sql);
	
		$idst		= Koleksi::EmptyPostVal($_POST['idst']);
		$kode 		= Koleksi::EmptyPostVal($_POST['kode']);
		$tahun 		= Koleksi::EmptyPostVal($_POST['tahun']);
		$pos 		= Koleksi::EmptyPostVal($_POST['pos']);
		$nip 		= Koleksi::EmptyPostVal($_POST['nip']);
		$gol 		= Koleksi::EmptyPostVal($_POST['gol']);
		$jab 		= Koleksi::EmptyPostVal($_POST['jab']);
		$sert		= Koleksi::EmptyPostVal($_POST['sert']);
		$pp_peran 	= Koleksi::EmptyPostVal($_POST['pp_peran']);
		$pp_jmlhari	= Koleksi::EmptyPostVal($_POST['pp_jmlhari']);
		$pp_jmljam 	= Koleksi::EmptyPostVal($_POST['pp_jmljam']);
		/*
		===========================================================
		Jam Realisasi di dapat dari:
		1. Cek ada Surat Tugas Lembur
		2. Cek ada Surat Tugas Tumpuk
		3. Jam Realisasi = (Jam ST + Jam ST Lembur) - Jam ST Tumpuk
		=========================================================== */
		$pp_jamreal = Koleksi::EmptyPostVal($_POST['pp_jamreal']); // Sementara
		$pp_sak 	= Koleksi::EmptyPostVal($_POST['pp_sak']);
		$pp_jak		= Koleksi::EmptyPostVal($_POST['pp_jak']);

		// Pasangkan dan Eksekusi			
		$res->bindParam(":idst", $idst, PDO::PARAM_STR);
		$res->bindParam(":kode", $kode, PDO::PARAM_STR);
		$res->bindParam(":tahun", $tahun, PDO::PARAM_STR);
		$res->bindParam(":pos", $pos, PDO::PARAM_STR);
		$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		$res->bindParam(":gol", $gol, PDO::PARAM_STR);
		$res->bindParam(":jab", $jab, PDO::PARAM_STR);
		$res->bindParam(":sert", $sert, PDO::PARAM_STR);
		$res->bindParam(":pp_peran", $pp_peran, PDO::PARAM_STR);
		$res->bindParam(":pp_jmlhari", $pp_jmlhari, PDO::PARAM_STR);
		$res->bindParam(":pp_jmljam", $pp_jmljam, PDO::PARAM_STR);
		$res->bindParam(":pp_jamreal", $pp_jamreal, PDO::PARAM_STR);
		$res->bindParam(":pp_sak", $pp_sak, PDO::PARAM_STR);
		$res->bindParam(":pp_jak", $pp_jak, PDO::PARAM_STR);

		if($_POST['aksi'] == "tim_update")
			$res->bindParam(":uid", $_POST['uid'], PDO::PARAM_INT);

		$res->execute();		
			if ($_POST['aksi'] == "tim_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Proses menyimpan data berhasil tanpa kendala.' );
			if ($_POST['aksi'] == "tim_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa kendala.' );	
	}
	catch(PDOException $e) {
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}
		$response = array(
			'status'=>'gagal',
			'pesan'=>'<span class="label label-danger">Error:</span>&nbsp;'.$e->getMessage().'<hr>'.$sql.'<hr>'.$err
		);
	}
	echo json_encode($response); // Tampilkan hasil Eksekusi
	$db = null; // Tutup koneksi
}

/*
else if(isset($_GET['form']) && $_GET['form'] == 'baru') {
	$isi = '
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				Ada isian Form yang belum di isi. Mohon di periksa kembali.
			</div>
			<input type="text" id="stid" name="stid" value="'.$_POST['stid'].'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
			<input type="text" id="tahun" name="tahun" value="'.$_POST['stthn'].'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
			<input type="text" id="lama" name="lama" value="'.$_POST['stlama'].'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
		</div>
	</div>
	
	<div class="row">
	
		<div class="col-md-3">			
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-9">
					<div id="spinpos">
						<div class="input-group input-xsmall">';
							$sql = "SELECT count(um_stwas_tim.id)+1 AS urutan
							FROM um_stwas_tim
							WHERE um_stwas_tim.stid = '".$_POST['stid']."'";
							$res = $db->prepare($sql);
							$res->execute();
							$r = $res->fetch(PDO::FETCH_ASSOC);
							$isi.='<input type="text" id="pos" name="pos" value="'.$r['urutan'].'" class="form-control spinner-input" maxlength="3">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn spinner-up btn-xs btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn spinner-down btn-xs btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
		
		<div class="col-md-9">			
			<div class="form-group">
				<label class="col-md-2 control-label">Nama</label>
				<div class="col-md-10">
					<select id="timnip" name="timnip" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$sql = "SELECT
					  i.nip,
					  CONCAT(IF(ISNULL(i.gelardp), '', CONCAT(i.gelardp, '. ')), i.nama, IF(ISNULL(i.gelarbk), '', CONCAT(', ', i.gelarbk))) AS nama,
					FROM
					  um_pegawai i
					WHERE
					  i.aktif = 'Y'";
					$res = $db->prepare($sql);
					$res->execute();
					while($n = $res->fetch(PDO::FETCH_ASSOC)) {
						$isi.='<option value="'.$n['nip'].'">'.$n['nama'].'</option>';
					}
					$isi.='</select>
					<input type="text" id="timnama" name="timnama" placeholder="" class="form-control margin-top-15" readonly>
				</div>
			</div>			
		</div>
		
	</div> <!-- END ROW PERTAMA -->
	
	<div class="row"> <!-- ROW KEDUA -->
		<div class="col-md-2"></div>
		<div class="col-md-10">
			<div class="form-group">
				<label class="col-md-3 control-label">Pangkat/Gol.</label>
				<div class="col-md-9">
					<select id="timgol" name="timgol" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$res = $db->prepare("SELECT kdgol, pangolru2 FROM kdgolru");
					$res->execute();
					while($g = $res->fetch(PDO::FETCH_ASSOC)) {
						$isi.='<option value="'.$g['kdgol'].'">'.$g['pangolru2'].'</option>';				
					}
					$isi.='</select>
				</div>
			</div>
		</div>
	</div> <!-- END ROW KEDUA -->
	
	<div class="row"> <!-- ROW KETIGA -->
		<div class="col-md-2"></div>
		<div class="col-md-10">
			<div class="form-group">
				<label class="col-md-3 control-label">Jabatan</label>
				<div class="col-md-9">
					
					<div>
						<select id="timperan" name="timperan" class="form-control input-medium" data-placeholder="Pilih...">
						<option value=""></option>';
						$sbg = array (
							"dalnis"=>"Pengendali Teknis",
							"ketua"=>"Ketua Tim",
							"anggota"=>"Anggota"
						);
						foreach($sbg as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='</select>
						<input type="text" id="timjabnow" name="timjabnow" placeholder="" class="form-control margin-top-15" readonly>
					</div>
					<div class="margin-top-15">
						<select id="timjab" name="timjab" class="form-control" data-placeholder="Pilih...">
						<option value=""></option>';
						$jab = array (
							"Sekretaris diperankan",
							"Irban diperankan",
							"Kasubbag diperankan",
							"Pengawas Pemerintah Madya",
							"Pengawas Pemerintah Muda",
							"Pengawas Pemerintah Pertama",
							"Auditor Utama",
							"Auditor Madya",
							"Auditor Muda",
							"Auditor Pertama",
							"Auditor Penyelia",
							"Auditor Pelaksana Lanjutan",
							"Auditor Pelaksana",
							"Penunjang"
						);
						foreach($jab as $keyval) {
							$isi.='<option value="'.$keyval.'">'.$keyval.'</option>';
						}					
						$isi.='</select>
					</div>
					
					<div class="margin-top-15 hidden" id="sert">
						<select id="sertifikasi" name="sertifikasi" class="form-control" data-placeholder="Pilih...">
						<option value=""></option>';
						$jab = array (
							"( Bersertifikat Dalnis dan Lulus )",
							"( Bersertifikat P2UPD dan Lulus )",
							"( Bersertifikat P2UPD )",
							"( Bersertifikat Ahli dan Lulus )",
							"( Bersertifikat Terampil dan Lulus )",
							"( Bersertifikat Ahli )",
							"( Bersertifikat Terampil )",
						);
						foreach($jab as $keyval) {
							$isi.='<option value="'.$keyval.'">'.$keyval.'</option>';
						}					
						$isi.='</select>
					</div>
					
					<div class="margin-top-15 hidden" id="kp">
						<div class="input-group input-xsmall" id="tblpoin" data-target="#modal-tblpoin" data-toggle="" style="float:left">
							<input type="text" id="kode" name="kode" class="form-control" readonly>
							<span class="input-group-btn">
								<button class="btn btn-info" type="button"><i class="fa fa-sitemap"></i></button>
							</span>
						</div>
						<input type="text" id="satuankp" name="satuankp" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
						<input type="text" id="kreditpoin" name="kreditpoin" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					</div>
					<div style="clear:both"></div>
					<span id="kdkeginfo" class="help-block"></span>
					
				</div> <!-- col-md-9 -->
			</div> <!-- form-group -->
		</div>
	</div> <!-- END ROW KETIGA -->
	';
	
	echo $isi;
}
*/

else if(isset($_GET['form']) && $_GET['form'] == 'baru') {
	
	// Ambil data POSTING dari jquery handleTimCRUD section #timbaru
	$idst 		= $_POST['idst'];
	$thn 		= $_POST['thn'];
	$pp_jmlhari	= $_POST['hari'];
	$pp_jmljam 	= $_POST['jam'];
	$pp_jamreal	= $pp_jmljam;
	
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Terdapat isian Form yang belum di isi. Mohon di periksa kembali.
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				Ada isian Form yang belum di isi. Mohon di periksa kembali.
			</div>
			<div class="form-group hidden">
				<label class="col-md-3 control-label">Optional</label>
				<div class="col-md-9">
					<input type="text" id="idst" name="idst" value="'.$idst.'" class="form-control input-xsmall" readonly>
					<input type="text" id="tahun" name="tahun" value="'.$thn.'" class="form-control input-xsmall margin-top-15" readonly>
					<input type="text" id="pp_jmlhari" name="pp_jmlhari" value="'.$pp_jmlhari.'" class="form-control input-xsmall margin-top-15" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Nomor Urut</label>
				<div class="col-md-9">
					<div id="spinpos">
						<div class="input-group input-xsmall">';
							// Penomoran naik otomatis
							$res = $db->prepare("SELECT count(idst)+1 AS pos FROM um_stwas_tim WHERE idst = '".$idst."'");
							$res->execute();
							$r = $res->fetch(PDO::FETCH_ASSOC);
							$isi.='
							<input type="text" id="pos" name="pos" value="'.$r['pos'].'" class="form-control spinner-input" maxlength="3">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn spinner-up btn-xs btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn spinner-down btn-xs btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Nama</label>
				<div class="col-md-9">
					<select id="nip" name="nip" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					// Query NamaGelar dari "Views"
					$res = $db->prepare("SELECT nip, nama FROM namagelar");
					$res->execute();
					while($n = $res->fetch(PDO::FETCH_ASSOC)) {
						$isi.='<option value="'.$n['nip'].'">'.$n['nama'].'</option>';
					}
					$isi.='</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Pangkat/Gol.</label>
				<div class="col-md-9">
					<select id="gol" name="gol" class="form-control spinner" data-placeholder="Pilih...">
					<option value=""></option>';
					$res = $db->prepare("SELECT kdgol, pangolru2 FROM kd_golru");
					$res->execute();
					while($g = $res->fetch(PDO::FETCH_ASSOC)) {
						$isi.='<option value="'.$g['kdgol'].'">'.$g['pangolru2'].'</option>';				
					}
					$isi.='</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Jabatan</label>
				<div class="col-md-9">
					<select id="jab" name="jab" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';			
					$jab = array (
						"2"=>"Sekretaris diperankan",
						"5"=>"Irban I diperankan",
						"6"=>"Irban II diperankan",
						"7"=>"Irban III diperankan",
						"8"=>"Irban IV diperankan",
						"3"=>"Kasubbag Umum diperankan",
						"4"=>"Kasubbag Perencanaan diperankan",
						"9"=>"Pengawas Pemerintah Madya",
						"10"=>"Pengawas Pemerintah Muda",
						"11"=>"Pengawas Pemerintah Pertama",
						"12"=>"Auditor Utama",
						"13"=>"Auditor Madya",
						"14"=>"Auditor Muda",
						"15"=>"Auditor Pertama",
						"16"=>"Auditor Penyelia",
						"17"=>"Auditor Pelaksana Lanjutan",
						"18"=>"Auditor Pelaksana",
						"24"=>"Penunjang"
					);
					foreach($jab as $key => $val) {
						$isi.='<option value="'.$key.'">'.$val.'</option>';
					}					
					$isi.='</select>
				</div>
				<div id="sql"></div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Peran Sebagai</label>
				<div class="col-md-9">
					<select id="pp_peran" name="pp_peran" class="form-control input-medium" data-placeholder="Pilih...">
					<option value=""></option>';
					$sbg = array ( "PT"=>"Pengendali Teknis", "KT"=>"Ketua Tim", "AT"=>"Anggota" );
					foreach($sbg as $key => $val) {
						$isi.='<option value="'.$key.'">'.$val.'</option>';
					}
					$isi.='</select>				
				</div>
			</div>
			<div id="epoin" class="form-group hidden">
				<label class="col-md-3 control-label">Kredit Poin</label>
				<div class="col-md-9">
					<div class="input-group input-small" id="tblpoin" data-target="#modal-tblpoin" data-toggle="modal" style="float:left">
						<input type="text" id="kode" name="kode" class="form-control" readonly>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"><i class="fa fa-sitemap"></i></button>
						</span>
					</div>&nbsp;
					&nbsp;<span class="label label-default">Satuan</span>&nbsp;&nbsp;<input type="text" id="pp_sak" name="pp_sak" placeholder="sak" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					&nbsp;<span class="label label-default">Jam</span>&nbsp;&nbsp;<input type="text" id="pp_jmljam" name="pp_jmljam" value="'.$pp_jmljam.'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					<div style="float:left; margin: 22px 5px 0 0"><span class="label label-warning">Realisasi</span></div>
					<div id="spinjamreal" style="float:left; margin-top:15px">
						<div class="input-group input-xsmall">							
							<input type="text" id="pp_jamreal" name="pp_jamreal" value="'.$pp_jamreal.'" class="form-control spinner-input">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn spinner-up btn-xs btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn spinner-down btn-xs btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
					<div style="float:left; margin: 22px 5px 0 10px"><span class="label label-danger">Hasil</span></div>
					<div class="margin-top-15" style="float:left">
						<input type="text" id="pp_jak" name="pp_jak" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					</div>
				</div>
			</div>
			<div id="esert" class="form-group">
				<label class="col-md-3 control-label">Sertifikasi</label>
				<div class="col-md-9">
					<select id="sert" name="sert" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$res = $db->prepare("SELECT id, sert FROM kd_stsert");
					$res->execute();
					while($p = $res->fetch(PDO::FETCH_ASSOC)) {
						$isi.='<option value="'.$p['id'].'">'.$p['sert'].'</option>';				
					}
					$isi.='</select>';
					/*
					$sert = array (
						'Bersertifikat Dalnis dan Lulus',
						'Bersertifikat Ketua Tim dan Lulus',
						'Bersertifikat P2UPD dan Lulus',
						'Bersertifikat P2UPD',
						'Bersertifikat Ahli dan Lulus',
						'Bersertifikat Terampil dan Lulus',
						'Bersertifikat Ahli',
						'Bersertifikat Terampil',
					);
					foreach($sert as $keyval) {
						$isi.='<option value="'.$keyval.'">'.$keyval.'</option>';
					}					
					$isi.='</select>';
					*/
					$isi.='
				</div>
			</div>		
			
		</div>
	</div>
	
	<div id="kpoin" class="row hidden">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Tabel Kegiatan dan Perhitungan Kredit Poin Fungsional Auditor</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-9">

						</div>
					</div>				
					<span id="kdkeginfo" class="help-block" style="text-align:justify"></span>
				</div> <!-- // panel-body -->
			</div> <!-- // panel -->			
		</div> <!-- // col-md-12 -->
	</div> <!-- // row -->
	';
	
	echo $isi;
}

else if(isset($_GET['form']) && $_GET['form'] == 'edit') {
		
	// Jika ada Posting ID Surat Tugas dan Nomor Surat Tugas tidak kosong
	if(isset($_POST['id']) && $_POST['id'] != '') {
		$id = $_POST['id'];
	} else {
		exit();
	}

	// Data Personal Tim
	$res = $db->prepare("SELECT * FROM um_stwas_tim WHERE id = '".$id."'");
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	
	$idst 		= $_POST['idst'];
	$kode		= $r['kode'];
	$thn 		= $_POST['thn'];
	$pos		= $r['pos'];
	$nip		= $r['nip'];
	$gol		= $r['gol'];
	$jab		= $r['jab'];
	$sert		= $r['sert'];
	$pp_peran 	= $r['pp_peran'];
	$pp_jmlhari	= $_POST['hari'];
	$pp_jmljam 	= $_POST['jam'];
	$pp_jamreal = $r['pp_jamreal'];
	$pp_sak		= $r['pp_sak'];
	$pp_jak		= $r['pp_jak'];
	
	// Data Surat Tugas
	/*
	$resA = $db->prepare("SELECT tahun, lama FROM um_stwas WHERE id = :stid");
	$resA->bindParam(":stid", $stid);
	$resA->execute();
	$rA = $resA->fetch(PDO::FETCH_ASSOC);
	
	$thn = $rA['tahun'];
	$jam = $rA['lama'] * 6.5;
	
	$stid 	= $_POST['stid'];
	$stthn 	= $_POST['stthn'];
	$stlama	= $_POST['stlama'];
	$jam 	= $_POST['stjam'];
	*/
	
	$isi = '
	
	<div class="row">
		
		<div class="col-md-12">
		
			<div class="alert alert-danger display-hide">
				<button class="close" data-close="alert"></button>
				Ada isian Form yang belum di isi. Mohon di periksa kembali.
			</div>
			<div class="form-group hidden">
				<label class="col-md-3 control-label">Optional</label>
				<div class="col-md-9">
					<input type="text" id="idst" name="idst" value="'.$idst.'" class="form-control input-xsmall" readonly>
					<input type="text" id="tahun" name="tahun" value="'.$thn.'" class="form-control input-xsmall margin-top-15" readonly>
					<input type="text" id="pp_jmlhari" name="pp_jmlhari" value="'.$pp_jmlhari.'" class="form-control input-xsmall margin-top-15" readonly>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Urutan</label>
				<div class="col-md-9">
					<div id="spinpos">
						<div class="input-group input-xsmall">
							<input type="text" id="pos" name="pos" value="'.$pos.'" class="form-control spinner-input" maxlength="3">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn spinner-up btn-xs btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn spinner-down btn-xs btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
				</div>
			</div>			
			<div class="form-group">
				<label class="col-md-3 control-label">Nama</label>
				<div class="col-md-9">
					<select id="nip" name="nip" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$res = $db->prepare("SELECT nama, nip FROM namagelar");
					$res->execute();
					$jum = $res->rowCount();
					while($n = $res->fetch(PDO::FETCH_ASSOC)) {
						$p = (($nip == $n['nip']) ? ' selected="selected"' : '');
						$isi.='<option value="'.$n['nip'].'"'.$p.'>'.$n['nama'].'</option>';
					}
					$isi.='</select>
				</div>
			</div>	
			<div class="form-group">
				<label class="col-md-3 control-label">Pangkat/Gol.</label>
				<div class="col-md-9">
					<select id="gol" name="gol" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$res = $db->prepare("SELECT kdgol, pangolru2 FROM kd_golru");
					$res->execute();
					while($g = $res->fetch(PDO::FETCH_ASSOC)) {
						$p = (($gol == $g['kdgol']) ? ' selected="selected"' : '');
						$isi.='<option value="'.$g['kdgol'].'"'.$p.'>'.$g['pangolru2'].'</option>';
					}
					$isi.='</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Jabatan</label>
				<div class="col-md-9">
					<select id="jab" name="jab" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$arrJab = array (
						"2"=>"Sekretaris diperankan",
						"5"=>"Irban I diperankan",
						"6"=>"Irban II diperankan",
						"7"=>"Irban III diperankan",
						"8"=>"Irban IV diperankan",
						"3"=>"Kasubbag Umum diperankan",
						"4"=>"Kasubbag Perencanaan diperankan",
						"9"=>"Pengawas Pemerintah Madya",
						"10"=>"Pengawas Pemerintah Muda",
						"11"=>"Pengawas Pemerintah Pertama",
						"12"=>"Auditor Utama",
						"13"=>"Auditor Madya",
						"14"=>"Auditor Muda",
						"15"=>"Auditor Pertama",
						"16"=>"Auditor Penyelia",
						"17"=>"Auditor Pelaksana Lanjutan",
						"18"=>"Auditor Pelaksana",
						"24"=>"Penunjang"
					);
					foreach($arrJab as $key => $val) {
						$p = (( $key == $jab ) ? ' selected="selected"' : '');
						$isi.='<option value="'.$key.'"'.$p.'>'.$val.'</option>';
					}
					$isi.='</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label">Peran Sebagai</label>
				<div class="col-md-9">					
					<select id="pp_peran" name="pp_peran" class="form-control input-medium" data-placeholder="Pilih...">
					<option value=""></option>';
					$sbg = array ( "PT"=>"Pengendali Teknis", "KT"=>"Ketua Tim", "AT"=>"Anggota" );
					foreach($sbg as $key => $val) {
						$p = (( $pp_peran == $key) ? ' selected="selected"' : '' );
						$isi.='<option value="'.$key.'"'.$p.'>'.$val.'</option>';
					}
					$isi.='</select>					
				</div>
			</div>
			<div id="epoin" class="form-group hidden">
				<label class="col-md-3 control-label">Kredit Poin</label>
				<div class="col-md-9">
					<div class="input-group input-small" id="tblpoin" data-target="#modal-tblpoin" data-toggle="modal" style="float:left">
						<input type="text" id="kode" name="kode" value="'.$kode.'"class="form-control" readonly>
						<span class="input-group-btn">
							<button class="btn btn-info" type="button"><i class="fa fa-sitemap"></i></button>
						</span>
					</div>&nbsp;
					&nbsp;<span class="label label-default">Satuan</span>&nbsp;&nbsp;<input type="text" id="pp_sak" name="pp_sak" value="'.$pp_sak.'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					&nbsp;<span class="label label-default">Jam</span>&nbsp;&nbsp;<input type="text" id="pp_jmljam" name="pp_jmljam" value="'.$pp_jmljam.'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					<div style="float:left; margin: 22px 5px 0 0"><span class="label label-warning">Realisasi</span></div>
					<div id="spinjamreal" style="float:left; margin-top:15px">
						<div class="input-group input-xsmall">							
							<input type="text" id="pp_jamreal" name="pp_jamreal" value="'.$pp_jamreal.'" class="form-control spinner-input">
							<div class="spinner-buttons input-group-btn btn-group-vertical">
								<button type="button" class="btn spinner-up btn-xs btn-info"><i class="fa fa-angle-up"></i></button>
								<button type="button" class="btn spinner-down btn-xs btn-info"><i class="fa fa-angle-down"></i></button>
							</div>
						</div>
					</div>
					<div style="float:left; margin: 22px 5px 0 10px"><span class="label label-danger">Hasil</span></div>
					<div class="margin-top-15" style="float:left">
						<input type="text" id="pp_jak" name="pp_jak" value="'.$pp_jak.'" placeholder="" class="form-control placeholder-no-fix input-xsmall inline" readonly>
					</div>
				</div>
			</div>
			<div id="esert" class="form-group">
				<label class="col-md-3 control-label">Sertifikasi</label>
				<div class="col-md-9">
					<select id="sert" name="sert" class="form-control" data-placeholder="Pilih...">
					<option value=""></option>';
					$res = $db->prepare("SELECT id, sert FROM kd_stsert");
					$res->execute();
					while($r = $res->fetch(PDO::FETCH_ASSOC)) {
						$pil = ($r['id'] == $sert) ? ' selected="selected"' : '';
						$isi.='<option value="'.$r['id'].'"'.$pil.'>'.$r['sert'].'</option>';				
					}
					$isi.='</select>
				</div>
			</div>
		</div>
	</div>
	';

	echo $isi;
}

else if(isset($_GET['form']) && $_GET['form'] == 'hapus') { // SUKSES
	header("Content-type: application/json; charset=UTF-8");
	if(isset($_POST['timid'])) {
		try {		
			$res = $db->prepare("DELETE FROM $tabelData WHERE `id`=:timid");
			$res->bindParam(':timid', $_POST['timid']);
			$res->execute();
			$response = array( 'status'=>'sukses','pesan'=>'' );
		} catch(PDOException $e) {
			$response = array( 'status'=>'gagal','pesan'=>'<h4>Perhatian!</h4><p>'.$sql.'<br><br>'.$e->getMessage().'</p>' );
		}
		echo json_encode($response);
		$db = null;
	}
}

else if(isset($_GET['lihat']) && $_GET['lihat'] == 'isiotomatis') { // Otomatis Golongan dan Jabatan
	
	if(isset($_POST['nip'])) {
		// Mencari Golongan Akhir dari NIP
		/*
		$sql = "SELECT
		  t.golru,
		  j.jabatan
		FROM
		  um_pegawai i
		  INNER JOIN um_pangkat t ON (t.nip = i.nip)  
		  INNER JOIN(SELECT nip, MAX(golru) AS mg FROM um_pangkat GROUP BY nip) AS t2 ON t2.nip = t.nip AND t2.mg = t.golru
		  INNER JOIN um_jabatan j ON (j.nip = i.nip)
		  INNER JOIN(SELECT nip, MAX(tmtjab) AS mj FROM um_jabatan GROUP BY nip) AS j2 ON j2.nip = j.nip AND j2.mj = j.tmtjab
		WHERE
		  i.nip = '".$_POST['nip']."'";
		*/
		
		// Query
		$res = $db->prepare("SELECT golru FROM um_pegawai WHERE nip = '".$_POST['nip']."'");
		$res->execute();
		$r = $res->fetch(PDO::FETCH_ASSOC);
		// Tampilkan
		$data = array (
			'gol' => $r['golru']
		);
		echo json_encode($data);
	}
}

else if(isset($_GET['lihat']) && $_GET['lihat'] == 'autoisi') {
	$data = '';
	if(isset($_POST['nip'])) {
		$res = $db->prepare("SELECT golru, jabatan FROM um_pegawai WHERE nip = '".$_POST['nip']."'");
		$res->execute();
		$r = $res->fetch(PDO::FETCH_ASSOC);		
		$data = array( 'golru'=>$r['golru'], 'jabatan'=>$r['jabatan'] );
	}
	echo json_encode($data);
}

else if(isset($_GET['lihat']) && $_GET['lihat'] == 'poinwas') { // Tabel Kegiatan dan Poin Auditor

	if(isset($_POST['tbl'])) {
		
		// $res = $db->prepare("SELECT LOWER(REPLACE(kd_jabatan.ket, ' ', '_')) AS jab FROM kd_jabatan WHERE id = '".$_POST['jab']."'");
		$res = $db->prepare("SELECT jab AS jab FROM kd_jabatan WHERE id = '".$_POST['jab']."'");
		$res->execute();
		$r = $res->fetch(PDO::FETCH_ASSOC);
		$jab = strtolower(str_replace(' ','_',$r['jab'])); // auditor_.....
		
		try {
			$sql = "SELECT kode, kegiatan, ".$jab." AS kp FROM kd_poin_".$_POST['tbl']." WHERE unsur = 'II' AND ".$jab." IS NOT NULL";
			$res = $db->prepare($sql);
			$res->execute();
			$isi = '';
			// $isi.='<tr><th colspan="3">'.ucwords(str_replace('_', ' ', $jab)).'</th></tr>';
			while($r = $res->fetch(PDO::FETCH_ASSOC)) {
				$isi.= '
				<tr class="pilih" data-kode="'.$r['kode'].'" data-kegiatan="'.$r['kegiatan'].'" data-poin="'.$r['kp'].'" style="cursor:pointer">
				<td class="cellright">'.$r['kode'].'</td>
				<td align="justify">'.$r['kegiatan'].'</td>
				<td>'.$r['kp'].'</td>
				</tr>';
			}
			echo $isi;
		}
		catch(PDOException $e) {			
			echo '<tr><td colspan="3">'.$e->getMessage().'<br/>'.$sql.'</td></tr>';
		}
	}
}

else if(isset($_GET['lihat']) && $_GET['lihat'] == 'personiltim') { // Daftar Tim Pemeriksa
	
	/*
	$sql = "SELECT
	  t.id,
	  t.idst,
	  t.tahun,
	  t.pos,
	  t.timnip,
	  CONCAT(MID(t.timnip, 1, 4), '-', MID(t.timnip, 5, 2), '-', MID(t.timnip, 7, 2)) AS bdate,
	  CONCAT(MID(t.timnip, 9, 4), '-', MID(t.timnip, 13, 2)) AS tmt,
	  t.timnama,
	  t.timgol,
	  g.pangolru1,
	  CASE
		WHEN t.timperan = 'dalnis' THEN 'Dalnis'
		WHEN t.timperan = 'ketua' THEN 'Ketua Tim'
	  ELSE 'Anggota'
	  END AS timperan,
	  t.timjab,
	  t.sertifikasi
	FROM
	  um_stwas_tim t
	  INNER JOIN kdgolru g ON (t.timgol = g.kdgol)
	WHERE
	  t.idst = '".$_POST['stid']."'
	ORDER BY
	  t.pos";
	/*
	ORDER BY
	  - FIELD(t.timperan, 'dalnis', 'ketua', 'anggota') DESC,
	  t.timgol DESC,
	  - bdate DESC
	*/
	
	$sql = "
	SELECT 
	  um_stwas_tim.id,
	  um_stwas_tim.idst,
	  um_stwas_tim.kode,
	  um_stwas_tim.tahun,
	  um_stwas_tim.pos,
	  um_stwas_tim.nip,
	  CONCAT(IF(ISNULL(um_pegawai.gelardp), '', CONCAT(um_pegawai.gelardp, '. ')), um_pegawai.nama, IF(ISNULL(um_pegawai.gelarbk), '', CONCAT(', ', um_pegawai.gelarbk))) AS nama,
	  um_stwas_tim.gol,
	  kd_golru.pangolru1 AS pangkat,
	  CASE kd_jabatan.id
		WHEN '2' THEN 'Sekretaris diperankan'
		WHEN '3' THEN 'Kasubbag Umum diperankan'
		WHEN '4' THEN 'Kasubbag Perencanaan diperankan'
		WHEN '5' THEN 'Irban I diperankan'
		WHEN '6' THEN 'Irban II diperankan'
		WHEN '7' THEN 'Irban III diperankan'
		WHEN '8' THEN 'Irban IV diperankan'
	  ELSE kd_jabatan.jab
	  END AS jab,
	  kd_stsert.sert,
	  CASE um_stwas_tim.pp_peran
	  	 WHEN 'PT' THEN 'Dalnis'
		 WHEN 'KT' THEN 'Ketua Tim'
		 WHEN 'AT' THEN 'Anggota'
	  END AS pp_peran	  
	FROM
	  um_stwas_tim
	  INNER JOIN kd_golru ON (um_stwas_tim.gol = kd_golru.kdgol)
	  INNER JOIN um_pegawai ON (um_stwas_tim.nip = um_pegawai.nip)
	  INNER JOIN kd_jabatan ON (um_stwas_tim.jab = kd_jabatan.id)
	  LEFT OUTER JOIN kd_stsert ON (um_stwas_tim.sert = kd_stsert.id)
	WHERE
	  um_stwas_tim.idst = '".$_POST['idst']."'
	ORDER BY
	  um_stwas_tim.pos";
	
	$res = $db->prepare($sql);
	$res->execute();	
	
	$isi = '';
	if($res->rowCount() > 0) {
		$no = 1;
		while ($r = $res->fetch(PDO::FETCH_ASSOC)) {
			$isi.='
			<tr>
			<td class="cellright" style="padding: 5px; vertical-align:top; ">'.$no.'</th>
			<td style="padding: 5px; vertical-align:top; border-bottom:1px solid #eee">Nama<br>NIP<br>Pangkat / Golongan<br>Jabatan</td>
			<td style="padding: 5px; vertical-align:top; border-bottom:1px solid #eee">'.$r['nama'].'<br>
			'.Koleksi::spasiNip($r['nip']).'<br>
			'.$r['pangkat'].'<br>
			'.ucfirst($r['pp_peran']);
			if (!empty($r['jab'])) $isi.=' / '.$r['jab'];
			if (!empty($r['sert'])) $isi.='<br>( '.$r['sert'].' )';
			$isi.='</td>
			<td style="padding: 5px; vertical-align:top"><center>
			<div class="task-config">
				<div class="task-config-btn btn-group">
					<!-- <a href="#" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> -->
					<a href="#" class="btn btn-xs btn-success dropdown-toggle" data-toggle="dropdown" data-close-others="true">
						Data <i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu pull-right" role="menu">
					<li><a href="#" id="timedit" data-id="'.$r['id'].'" data-idst="'.$r['idst'].'" data-thn="'.$_POST['thn'].'" data-hari="'.$_POST['hari'].'" data-target="#tim-modal" data-toggle="modal"><i class="fa fa-edit"></i> Ubah</a></li>
					<li class="divider"></li>
					<li><a href="#" id="timhapus" data-id="'.$r['id'].'"><i class="fa fa-times"></i> Hapus</a></li>
					</ul>
				</div>
			</div>
			</center></td>
			</tr>';
			$no++;
		}		
	} else {
		$isi.= '<center><span class="label label-sm label-danger"><strong>Perhatian!</strong></span><br>
		Sementara belum ada data Tim Personil untuk Surat Tugas ini.<br>
		Silahkan klik tombol &quot;+Tim Personil&quot; diatas untuk menambah</center>';
	}
	echo $isi;
}

else {
	
	$res = $db->prepare("SELECT tahun, nomor, tglsrt, kegiatan FROM um_stwas WHERE id = '".$_POST['id']."'");
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	
	$isi = '
	<div class="alert alert-info">
		<center>
		<strong>SURAT TUGAS '.$r['tahun'].'</strong><br>
		Nomor : 700/'.$r['nomor'].'Inspektorat, tanggal : '.Koleksi::tanggalIndo($r['tglsrt']).'<br>'.$r['kegiatan'].'
		</center>
	</div>

	<table id="tableTim" width="100%" class="">
	<tbody>
	</tbody>
	</table>
	<br />';
	
	echo $isi;
}
?>