<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=was_tl';
$theme		= 'themes/conquer';
$tabelData	= 'was_tl';
$pageTitle	= 'Umum <small>Surat Tugas</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i>
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Umum</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Surat Tugas</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-was-tl.js"></script>
';
$initA = '
// var modul = "modul.php?ke=was_tl";
var modul = window.location.href;
var pageTitle = "Surat Tugas";

// CRUD
var menuBaru 	= modul+"&temuan=baru";
var menuSTBaru 	= modul+"&was=stbaru";
var menuDetailST 	= modul+"&was=detailst";
var menuEdit	= modul+"&was=edit";

// Table Data
var srcUrl 	= modul+"&was=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
umst.init();
';
$initC = '';
// MAIN CODE
$lhp=$_GET["id"];



//kode status
$kd_status= array();
$query = "select * from was_tl_status";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kd_status += [$status_id => $status_ket];
}
$blank="";
if (isset($_POST['submit'])) {
	try {
		// Query Simpan
		if ($_POST['aksi'] == "tl_simpan") {
			$input = "INSERT INTO was_tl_data
			( tl_lhp
            , tl_no
            , tl_uraian
            , tl_jenis
            , tl_kelompok
            , tl_subkel
			)
			VALUES
			( :lhp
			, :no
			, :uraian
			, :jenis
			, :kelompok
			, :subkel
            )";
            $res = $db->prepare($input);
            $res->bindParam(":lhp",$lhp);
		}
		// Query Update
		if ($_POST['aksi'] == "tl_update") {
			$input = "UPDATE `was_tl_data` SET
			`tl_no` = :no,
			`tl_uraian` = :uraian,
			`tl_jenis` = :jenis,
			`tl_kelompok` = :kelompok,
			`tl_subkel` = :subkel
			WHERE `tl_id` = :tl_id";
            $res = $db->prepare($input);
            $res->bindParam(":tl_id", Koleksi::EmptyPostVal($_POST['tl_id']));
		}
        
        // Query Simpan
		if ($_POST['aksi'] == "rek_baru") {
			$input = "INSERT INTO was_rekomendasi
			( rek_temuan
            , rek_rekomendasi
            , rek_kode
            , rek_nilai
            , rek_status
            , rek_tindaklanjut

			)
			VALUES
			( :temuan
			, :rekomendasi
			, :kode
			, :nilai
			, :status
			, :tl
            )";
            $res = $db->prepare($input);
            $res->bindParam(":temuan", Koleksi::EmptyPostVal($_POST['temuan']));
		}
		// Query Update
		if ($_POST['aksi'] == "rek_update") {
			$input = "UPDATE was_rekomendasi SET
			`rek_rekomendasi` = :rekomendasi,
			`rek_kode` = :kode,
			`rek_nilai` = :nilai,
			`rek_status` = :status,
			`rek_tindaklanjut` = :tl
			WHERE `rek_id` = :id";
            $res = $db->prepare($input);
            $id=$_POST['rek_id'];
            $res->bindParam(":id",$id);
		}


		// Persiapkan Query
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		//$nip = preg_replace('/\D/','', $_POST['nip']);
		//$nip = $_POST['nip'];

		// Pasangkan dan Eksekusi
		//if ($_POST['aksi'] == "cuti_simpan")
			//$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		if ($_POST['aksi'] == "tl_simpan" || $_POST['aksi'] == "tl_update"){
            $res->bindParam(":no", Koleksi::EmptyPostVal($_POST['no']));
            $res->bindParam(":uraian", Koleksi::EmptyPostVal($_POST['uraian']));
            $res->bindParam(":jenis", Koleksi::EmptyPostVal($_POST['jenis']));
            $res->bindParam(":kelompok", Koleksi::EmptyPostVal($_POST['kelompok']));
            $res->bindParam(":subkel", Koleksi::EmptyPostVal($_POST['subkel']));         
            }
        if ($_POST['aksi'] == "rek_baru" || $_POST['aksi'] == "rek_update"){
            $res->bindParam(":rekomendasi", Koleksi::EmptyPostVal($_POST['rekomendasi']));        
            $res->bindParam(":kode", Koleksi::EmptyPostVal($_POST['kode']));        
            $res->bindParam(":nilai", Koleksi::EmptyPostVal($_POST['nilai']));        
            $res->bindParam(":status", Koleksi::EmptyPostVal($_POST['status']));        
            $res->bindParam(":tl", Koleksi::EmptyPostVal($_POST['tl']));        
            }
           
        $res->execute();  
		// Diperlukan dalam proses update
		// Eksekusi
		// Tampilkan Informasi Sukses
		if ($_POST['aksi'] == "tl_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "rek_baru") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "tl_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
        if ($_POST['aksi'] == "rek_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}

	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}

		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;

		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['temuan']) && $_GET['temuan'] == 'baru') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Tambah Temuan</a></li>
		</ul>

		<div class="tab-content">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Nomor Temuan</b></label>
                            <div class="col-sm-9">
                                <input type="text" id="no" name="no" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Uraian Temuan</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="uraian" name="uraian" class="form-control">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kode Temuan</label>
                            
                            <div class="col-sm-3">
                                <input type="text" id="kelompok" name="kelompok" placeholder="Kelompok" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="subkel" name="subkel" placeholder="Sub Kelompok" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="jenis" name="jenis" placeholder="Jenis" class="form-control">
                            </div>
                        </div> 
               
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['temuan']) && $_GET['temuan'] == 'addrek') {
    $id=$_POST['uid'];
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Tambah Temuan</a></li>
		</ul>

		<div class="tab-content">
                       
                        <input type="hidden" id="temuan" name="temuan" value="'.$id.'">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rekomendasi</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="rekomendasi" name="rekomendasi" class="form-control">
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kode Rekomendasi</label>
                            <div class="col-sm-3">
                                <input type="text" id="kode" name="kode" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nilai</label>
                            <div class="col-sm-9">
                                <input type="text" id="nilai" name="nilai" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9">
                                <select id="status" name="status" class="form-control input-medium">';
								foreach($kd_status as $key => $val) {
									$isi.='<option value="'.$key.'">'.$val.'</option>';
								}
								$isi.='
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tindak Lanjut</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="tl" name="tl" class="form-control">
                                </textarea>
                            </div>
                        </div>
               
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['temuan']) && $_GET['temuan'] == 'edittl') {
    if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `was_tl_data` WHERE tl_id = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Edit Temuan</a></li>
		</ul>

		<div class="tab-content">
                    <input type="hidden" id="tl_id" name="tl_id" value="'.$r['tl_id'].'">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><b>Nomor Temuan</b></label>
                            <div class="col-sm-9">
                                <input type="text" id="no" name="no" value="'.$r['tl_no'].'" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Uraian Temuan</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="uraian" name="uraian" class="form-control"> '.$r['tl_uraian'].'
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kode Temuan</label>
                            
                            <div class="col-sm-3">
                                <input type="text" id="kelompok" name="kelompok" placeholder="Kelompok" value="'.$r['tl_kelompok'].'" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="subkel" name="subkel" placeholder="Sub Kelompok" value="'.$r['tl_subkel'].'" class="form-control">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" id="jenis" name="jenis" placeholder="Jenis" value="'.$r['tl_jenis'].'" class="form-control">
                            </div>
                        </div> 
               
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['temuan']) && $_GET['temuan'] == 'editrek') {
    if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `was_rekomendasi` WHERE rek_id = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Tambah Temuan</a></li>
		</ul>

		<div class="tab-content">
                       
                        <input type="hidden" id="rek_id" name="rek_id" value="'.$uid.'">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Rekomendasi</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="rekomendasi" name="rekomendasi" class="form-control">'.$r['rek_rekomendasi'].'
                                </textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Kode Rekomendasi</label>
                            <div class="col-sm-3">
                                <input type="text" id="kode" name="kode" value="'.$r['rek_kode'].'" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nilai</label>
                            <div class="col-sm-9">
                                <input type="text" id="nilai" name="nilai" value="'.$r['rek_nilai'].'" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9">
                                <select id="status" name="status" class="form-control input-medium">';
								foreach($kd_status as $key => $val) {
                                    $isi.='<option value="'.$key.'"'.(($r['rek_status'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
									//$isi.='<option value="'.$key.'">'.$val.'</option>';
								}
								$isi.='
								</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Tindak Lanjut</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="tl" name="tl" class="form-control">'.$r['rek_tindaklanjut'].'
                                </textarea>
                            </div>
                        </div>
               
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'tabeldata') {

	header("Content-type:application/json");



	// Kolom yang bisa di Sortir
	$columns = array(
		1=>'was_nolhp',
		2=>'tl_no'
	);



$sql = "SELECT
    `was_tl_data`.*
    , `was_rekomendasi`.*
    , `was_data`.`was_nolhp`
FROM
    `was_rekomendasi`
    RIGHT JOIN `was_tl_data` 
        ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
    RIGHT JOIN `was_data` 
        ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
WHERE (`was_tl_data`.`tl_lhp` ='$lhp'";


	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		//$sql.=" WHERE was_data.was_nolhp LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" AND was_tl_data.tl_uraian LIKE '%".$_REQUEST['search']['value']."%' ";
	}
    $sql .=")
    GROUP BY `was_tl_data`.`tl_id`, `was_rekomendasi`.`rek_id`;";
	$res = $db->prepare($sql);
	$res->execute();
    
	$totalData = $res->rowCount();
	$totalFiltered = $totalData;

	if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];
	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
                //belum selesai di kerjakan
        $rek=$row['rek_id'];
		$nestedData = array();
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		$nestedData[] = $row['was_nolhp'];
		$nestedData[] = $row['tl_no'];
		$nestedData[] = $row['tl_uraian'];
        $nestedData[] = $row['tl_kelompok'];
		$nestedData[] = $row['tl_subkel'];
        $nestedData[] = $row['tl_jenis'];
        $nestedData[] = '
                            <tb><a href="#" id="edittl" data-id="'.$row['tl_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Edit Temuan </span></center> 
                            <br>
                            </tb>
                            <tb>
                            <a href="#" id="addrek" data-id="'.$row['tl_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Tambah Rekomendasi</span></center> </tb>
							'; 
		$nestedData[] = $row['rek_rekomendasi'];
		$nestedData[] = $row['rek_kode'];
		$nestedData[] = number_format($row['rek_nilai'],2,",",".");
        $nestedData[] = $kd_status[$row['rek_status']];
		$nestedData[] = $row['rek_tindaklanjut'];
		$nestedData[] = '
                            <a href="#" id="editrek" data-id="'.$row['rek_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Update Rekomendasi</span></center> 
                            <br>
                            <a href="modul.php?ke=was_rinci&id='.$row['rek_id'].'"><center><span class="label label-sm label-primary"> Rincian Rekomendasi </span></center></a>
                            <br>';                
		$data[] = $nestedData;
		$no++;
	}
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
} 

else {
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">

		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Temuan LHP
					</div>
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
				</div> <!-- portlet-title -->
                    
				<div class="portlet-body">
					<div class="table-container">
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th><center>No</center></th>
						  <th>No LHP</th>
						  <th>No Temuan</th>
						  <th>Uraian</th>
						  <th>KD Kelompok</th>
						  <th>KD Sub Kelompok</th>
                          <th>KD Jenis</th>
						  <th>Aksi Temuan</th>
                          <th>Rekomendasi</th>
                          <th>KD Rekomendasi</th>
                          <th>Nilai</th>
                          <th>Status</th>
						  <th>Tindak Lanjut</th>
						  <th>Aksi Rekomendasi</th>
						</tr>
						</thead>
						<tbody></tbody>
						</table>

					</div> <!-- table-container -->
				</div> <!-- portlet-body -->

			</div>
		</div>

		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->

	</div>
	';

	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>
