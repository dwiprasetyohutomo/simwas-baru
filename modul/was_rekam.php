<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();

if(!isset($_SESSION['nip']) && empty($_SESSION['nip'])) {
	header('location: login.php');
	exit();
}

$db = koleksi::db_pdo($conn);
$blank="";
$tabel=0;
if(!empty($_GET['nip'])) {
    $tabel=1;
    $rekam=array();
    $nip=$$_GET['nip'];
    $no=1;
    $sql="SELECT
                `was_data`.`was_nolhp`
                , `was_data`.`was_obyek`
                , `was_tl_data`.`tl_uraian`
                , `was_tlrinci`.`rinci_nilai`
                , `was_tlrinci`.`rinci_bayar`
                , `was_tlrinci`.`rinci_nip`
                , `was_tlrinci`.`rinci_nama`
            FROM
                `was_tl_data`
                INNER JOIN `was_data` 
                    ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
                INNER JOIN `was_rekomendasi` 
                    ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
                INNER JOIN `was_tlrinci` 
                    ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
            WHERE (`was_tlrinci`.`rinci_nip` = '$nip')";
    $res = $db->prepare($sql);
    $res->execute();
    while($row = $res->fetch(PDO::FETCH_BOTH)) {
        $nested= array();
        $nested []=$no;
        $nested []=$row['was_nolhp'];
        $nested []=$row['was_obyek'];
        $nested []=$row['tl_uraian'];
        $nested []=$row['rincian_nilai'];
        $nested []=$row['rincian_bayar'];
        $sisa=$row['rincian_nilai']-$row['rincian_bayar'];
        $nested []=$sisa;
        if ($sisa>0){
            $nested []="Tengah Berjalan";
        }
        else {
            $nested []="Telah Selesai";
        }
        $nested []=$row['rincian_nip'];
        $nested []=$row['rincian_nama'];
        $no++;
        $rekam []=$nested;
    }        
}
else {
    
}


$isi ='
<!--
<div class="note note-success">
    <p></p>
</div>
-->

<div class="row">

    <div class="col-md-12 col-sm-12">
        <div class="portlet tasks-widget">

            <div class="portlet-title">
                <div class="caption">
                    Pencarian Rekam Jejak
                </div>
                <div class="actions">

                </div> <!-- end actions -->
            </div> <!-- portlet-title -->

            <div class="portlet-body">
                <div class="container">
                    <form action="modul/cetak_rekam.php" name="form1" method="post" >
                    <input type="hidden" id="ke" name="ke" value="was_rekam">
                        <div class="form-group">
                            <label class="col-md-3 control-label">NIP</span></label>
                            <div class="col-md-9">
                                <input type="text" id="nip" name="nip" placeholder="NIP" class="form-control" autocomplete="on">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Nama</span></label>
                            <div class="col-md-9">
                                <input type="text" id="nama" name="nama" placeholder="NAMA" class="form-control" autocomplete="on">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"></span></label>
                            <div class="col-md-9">
                                 <input type="submit" name="cek" value="CEK">
                            </div>
                        </div>
                    </form> 
                </div>
            </div> <!-- portlet-body -->
        </div>
    </div>
    <div class="col-md-12 col-sm-12">
        <div class="portlet tasks-widget">

            <div class="portlet-title">
                <div class="caption">
                    Tabel Rekam Jejak
                </div>
                <div class="actions">

                </div> <!-- end actions -->
            </div> <!-- portlet-title -->

            <div class="portlet-body">
                <div class="table-container">
                    <table width="100%" class="table table-striped table-hover">
                    <thead>
                    <tr class="heading">
                      <th><center>No</center></th>
                      <th>No LHP</th>
                      <th>OPD</th>
                      <th>Deskripsi Temuan</th>
                      <th>Nilai Temuan</th>
                      <th>Nilai Terbayar</th>
                      <th>Nilai Sisa</th>
                      <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>';
                    $x=0;
                    $y=0;
                    $z=0;
                    foreach($rekam as $value) {
                        $isi .='<tr>
                                <td>'.$value[0].'</td>
                                <td>'.$value[1].'</td>
                                <td>'.$value[2].'</td>
                                <td>'.$value[3].'</td>
                                <td>'.number_format($value[4],2,",",".").'% </td>
                                <td>'.number_format($value[5],2,",",".").'% </td>
                                <td>'.number_format($value[6],2,",",".").'% </td>
                                <td>'.$value[7].'</td>
                                </tr>';
                                $x=$x+$value[4];
                                $y=$y+$value[5];
                                $z=$z+$value[6];
                        }
                    if ($z>0){
                        $status="Tengah Berjalan";
                    }
                    else {
                        $status="Telah Selesai";
                    }
                    $isi .='
                        <tr>
                        <th colsapan=4>Total</th>
                        <th></th>
                        <th>'.number_format($x,2,",",".").'</th>
                        <th>'.number_format($y,2,",",".").'</th>
                        <th>'.number_format($z,2,",",".").'</th>
                        <th>'.number_format($z,2,",",".").'</th>
                        <th>'.$status.'</th>
                        </tr>
                      </tbody>
                    </table>

                </div> <!-- table-container -->
            </div> <!-- portlet-body -->
        </div>
    </div>
    <!--

</div>
';

$tpl = new template;
$tpl->load('themes/conquer/index_full.html');
$tpl->set('theme',$tpl->theme);
$tpl->set('css-tambahan',$tpl->cssTambahan);
$tpl->set('page-title',$tpl->pageTitle);
$tpl->set('page-breadcrumb',$tpl->pageBreadcrumb);
$tpl->set('page-kontent',$isi);
$tpl->set('page-plugin-script',$tpl->pagePlugins);
$tpl->set('page-styles-script',$tpl->pageStyles);
$tpl->set('initA',$tpl->initA);
$tpl->set('initB',$tpl->initB);
$tpl->set('initC',$tpl->initC);
$tpl->publish();

?>
