<?php
$tahun=$_POST['tahun'];
$obyek=$_POST['obyek'];
require 'configcetak.php';
//$password = "73Q8sr8Gau4JifEW";
$conn = new mysqli($servername, $username, $password, $dbname);


header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Laporan Rekomendasi ".date('d-m-Y H:i:s').".xls");
echo "<br>";
echo "<br>";
echo "<br>";
echo "<table>";
echo "<tr>";
echo "<th colspan=\"1\"></th> ";
echo "<th colspan=\"4\">INSPEKTORAT DAERAH PROVINSI KALIMANTAN UTARA</th> ";
echo "</tr>";
echo "<tr>";
echo "<th colspan=\"1\"></th> ";
echo "<th colspan=\"4\"><b>LAPORAN REKOMENDASI PEMERIKSAAN <b></th> ";
echo "</tr>";
echo "</table>";
 echo "<br>";
if ($obyek!='0'){
     echo "OPD :".$obyek;
    echo "<br>";
}
if ($tahun!='0'){
     echo "Tahun :".$tahun;
    echo "<br>";
}
echo "<br>";
echo "<table border=1>";
echo "<tr>";
echo "<th>No</th>";
echo "<th>Rekomendasi</th>";
echo "<th>Kode Rekomendasi</th>";
echo "<th>Jumlah Kejadian</th>";
echo "<th>Presentase</th>";
echo "<th>Nilai</th>";
echo "</tr>";
echo "<tr>";
echo "<th>1</th>";
echo "<th>2</th>";
echo "<th>3</th>";
echo "<th>4</th>";
echo "<th>5</th>";
echo "<th>6</th>";
echo "</tr>";
$no=1;
$nilai=0;
$sql="SELECT
    `was_rekomendasi`.`rek_kode`
    , `was_tl_rekomendasi`.`rekomendasi_deskripsi`
    , SUM(`was_rekomendasi`.`rek_nilai`) AS `nilai`
    , COUNT(`was_rekomendasi`.`rek_id`) AS `jumlah`
    , `was_data`.`was_tahun`
    FROM
        `was_tl_data`
        INNER JOIN `was_data` 
            ON (`was_tl_data`.`tl_lhp` = `was_data`.`was_id`)
        INNER JOIN `was_rekomendasi` 
            ON (`was_rekomendasi`.`rek_temuan` = `was_tl_data`.`tl_id`)
        INNER JOIN `was_tl_rekomendasi` 
            ON (`was_rekomendasi`.`rek_kode` = `was_tl_rekomendasi`.`rekomendasi_jenis`)";
if ($obyek!='0'|| $tahun!='0'){
    $sql.= "WHERE (";
        if ($obyek!='0'){
         $sql .= " `was_data`.`was_obyek` LIKE '".$obyek."' AND ";
        }
        if ($tahun!='0'){
         $sql .= " `was_data`.`was_tahun` = '".$tahun."'AND ";
        }
    $sql = substr($sql,0,-4);
    $sql.=")";
}
else {
    
}
$sql.="GROUP BY `was_rekomendasi`.`rek_kode`
ORDER BY `was_rekomendasi`.`rek_kode` ASC;";
$sqlsum="Select SUM(jumlah) AS total from (".substr($sql,0,-1)." ) total";
$result2 = $conn->query($sqlsum);
$row2 = $result2->fetch_assoc();
$total=$row2["total"];
$result3 = $conn->query($sql);
while($row3 = $result3->fetch_assoc()) {
    echo "<tr>";
    echo "<td>".$no."</td>";
    echo "<td>".$row3["rekomendasi_deskripsi"]."</td>";
    echo "<td>".$row3["rek_kode"]."</td>";
    echo "<td>".$row3["jumlah"]."</td>";
    $persen=$row3["jumlah"]/$row2["total"]*100;
    echo "<td>".number_format($persen,2,",",".")."</td>";
    echo "<td>Rp. ".number_format($row3["nilai"],2,",",".")."</td>";
    $nilai=$nilai+$row3["nilai"];
    echo "</tr>";
    $no++;
}
        echo "<th colspan=3><b>Total Kejadian<b></th>";
        echo "<th style=\"text-align:right\" >".$row2["total"]."</th>";
        echo "<th style=\"text-align:right\">100%</th>";
        echo "<th style=\"text-align:left\"> Rp. ".number_format($nilai,2,",",".")."</th>";
        echo "</tr>";
echo "<table>";

?>
