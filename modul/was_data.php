<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=was_data';
$theme		= 'themes/conquer';
$tabelData	= 'was_data';
$pageTitle	= 'Pengawasan <small>Daftar Pengawasan</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i>
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Pengawasan</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Daftar Kegiatan Pengawasan</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-was-data.js"></script>
';
$initA = '
// var modul = "modul.php?ke=was_data";
var modul = window.location.href;
var pageTitle = "Form Data Pengawasan";

// CRUD
var menuBaru 	= modul+"&was=baru";
var menuSTBaru 	= modul+"&was=stbaru";
var menuDetailST 	= modul+"&was=detailst";
var menuEdit	= modul+"&was=edit";

// Table Data
var srcUrl 	= modul+"&was=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
wasdata.init();
';
$initC = '';
// MAIN CODE

//DATA KD JENIS IN ARRAY
$kdstatus = array();
$query = "select * from was_kd_status";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdstatus += [$status_id => $status_ket];
}
//
//DATA KD GOLRU IN ARRAY
$kdgolru = array();
$query = "select * from kd_golru";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdgolru += [$kdgol => $ket3];
}
//
//DATA KD JENIS IN ARRAY
$kdjenis = array();
$query = "select * from was_kd_jenis";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdjenis += [$jenis_id => $jenis_ket];
}
//
//DATA KD PEMERIKSa IN ARRAY
$kdpemeriksa = array();
$query = "select * from was_pemeriksa";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdpemeriksa += [$pemeriksa_id => $pemeriksa_nama];
}
//
// kode pegawai
$peg = array();
$query = "select nip, nama from um_pegawai";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $peg += [$nip => $nama];
}
// kode posisi st
$kdposisi = array();
$query = "select * from was_st_posisi";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdposisi += [$posisi_id => $posisi_ket];
}
//kode opd
$opd = array();
$query = "select * from opd";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $opd += [$opd_id => $opd_nama];
}

$blank="";
if (isset($_POST['submit'])) {
	try {
		// Query Simpan
		if ($_POST['aksi'] == "was_simpan") {
			$sql = "INSERT INTO `was_data`
			(`was_tahun`,
			 `was_obyek`,
			 `was_pemeriksa`,
			 `was_jenis`,
			 `was_p2hp`,
			 `was_tgl_p2hp`,
			 `was_lhp`,
			 `was_nolhp`,
			 `was_tgl_lhp`,
			 `was_status`
			)
			VALUES
			(:tahun,
            :obyek,
            :pemeriksa,
            :jenis,
            :p2hp,
            :tgl_p2hp,
            :lhp,
            :nolhp,
            :tgl_lhp,
            '1')";

		}
		// Query Update
		if ($_POST['aksi'] == "was_update") {
			$sql = "UPDATE `was_data` SET
			`was_tahun` = :tahun,
			`was_obyek` = :obyek,
			`was_pemeriksa` = :pemeriksa,
      `was_jenis` = :jenis,
			`was_p2hp` = :p2hp,
			`was_tgl_p2hp` = :tgl_p2hp,
			`was_lhp` = :lhp,
			`was_nolhp` = :nolhp,
			`was_tgl_lhp` = :tgl_lhp,
			`was_status` = :status
			WHERE `was_id` = :was_id";
		}

		// Persiapkan Query
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		//$nip = preg_replace('/\D/','', $_POST['nip']);
		//$nip = $_POST['nip'];

		// Pasangkan dan Eksekusi
		//if ($_POST['aksi'] == "cuti_simpan")
			//$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		if ($_POST['aksi'] == "was_simpan" || $_POST['aksi'] == "was_update"){
            $res = $db->prepare($sql);
            $res->bindParam(":jenis", Koleksi::EmptyPostVal($_POST['jenis']));
            $res->bindParam(":pemeriksa", Koleksi::EmptyPostVal($_POST['pemeriksa']));
            $res->bindParam(":tahun", Koleksi::EmptyPostVal($_POST['tahun']));
            $res->bindParam(":tgl_p2hp", Koleksi::EmptyPostVal($_POST['tglp2hp']));
            $res->bindParam(":tgl_lhp", Koleksi::EmptyPostVal($_POST['tgllhp']));
            $nolhp=$_POST['nolhp'];
                $res->bindParam(":nolhp", Koleksi::EmptyPostVal($_POST['nolhp']));
            if ($_POST['aksi'] == "was_simpan") {
                if (empty($_FILES["p2hp"]["name"])) {
                    $res->bindParam(":p2hp",$blank);
                }
                if (empty($_FILES["lhp"]["name"])) {
                    $res->bindParam(":lhp",$blank);
                }

                if (!empty($_POST['obyek1'])){
                    $obyek=$_POST['obyek1'];
                    $res->bindParam(":obyek", Koleksi::EmptyPostVal($_POST['obyek1']));
                } else {
                    $obyek=$_POST['obyek2'];
                    $res->bindParam(":obyek", Koleksi::EmptyPostVal($_POST['obyek2']));
                }
            }
            if ($_POST['aksi'] == "was_update") {
                $res->bindParam(":was_id",Koleksi::EmptyPostVal($_POST['was_id']));
                $obyek=$_POST['obyek'];
                $res->bindParam(":obyek",Koleksi::EmptyPostVal($_POST['obyek']));
                $res->bindParam(":status",Koleksi::EmptyPostVal($_POST['status']));
                if (empty($_FILES["p2hp"]["name"])) {
                    $res->bindParam(":p2hp",Koleksi::EmptyPostVal($_POST['p2hpawal']));
                }
                else {
                    unlink($_POST['p2hpawal']);
                }
                if (empty($_FILES["lhp"]["name"])) {
                    $res->bindParam(":lhp",Koleksi::EmptyPostVal($_POST['lhpawal']));
                }
                else {
                    unlink($_POST['p2hpawal']);
                }
            }
            //=================== ini upload file p2hp ====================
            if (empty($_FILES["p2hp"]["name"])) {
            } else {
                $temp = explode(".", $_FILES['p2hp']['name']);
                $nama = $_POST['tahun']."-P2HP-".$_POST['jenis']."-".$obyek;
                $target_dir = "upload/was/";
                $extension = end($temp);
                $target_file = $target_dir.$nama.".".$extension;
                move_uploaded_file($_FILES["p2hp"]["tmp_name"], $target_file);
                $p2hp=$target_file;
                $res->bindParam(":p2hp",$p2hp);
            }
            //=============================================================
            //=================== ini upload file lhp ====================
            if (empty($_FILES["lhp"]["name"])) {
            } else {
                $temp = explode(".", $_FILES['lhp']['name']);
                $nolhp = explode("/",$nolhp);
                $nolhp = implode ("_", $nolhp);
                $nama = $_POST['tahun']."-LHP-".$_POST['jenis']."-".$nolhp;
                $target_dir = "upload/was/";
                $extension = end($temp);
                $target_file = $target_dir.$nama.".".$extension;
                move_uploaded_file($_FILES["lhp"]["tmp_name"], $target_file);
                $lhp=$target_file;
                $res->bindParam(":lhp",$lhp);
            }
            //=============================================================
            $res->execute();
        }

        if($_POST['aksi'] == "was_stsimpan"){
            $key=1;
            do {
                $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 10; $i++) {
                    $randstring .= $char[rand(0, strlen($char)-1)];
                }
                $query2 = "select count(*) from was_st where st_key='$randstring'";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute();
                $key= $stmt2->fetchColumn();
            } while ($key > 0);

            $sql = "INSERT INTO `was_st`
			(`st_key`,
			 `st_was`,
			 `st_kode`,
			 `st_lama`,
			 `st_mulai`,
			 `st_akhir`,
			 `st_asal`,
			 `st_tujuan`,
			 `st_untuk`,
			 `st_tanggal`,
			 `st_upload`
			)
			VALUES
			(:key,
             :was,
             :kode,
             :lama,
             :mulai,
             :akhir,
             :asal,
             :tujuan,
             :untuk,
             :tanggal,
             :upload
            )";
            $res = $db->prepare($sql);
            $res->bindParam(":key",$randstring);
            $res->bindParam(":was",Koleksi::EmptyPostVal($_POST['was_id']));
            $res->bindParam(":kode",Koleksi::EmptyPostVal($_POST['kodest']));
            $kode=$_POST['kodest'];
            $res->bindParam(":lama",Koleksi::EmptyPostVal($_POST['lama']));
            $res->bindParam(":mulai",Koleksi::EmptyPostVal($_POST['awal']));
            $res->bindParam(":akhir",Koleksi::EmptyPostVal($_POST['akhir']));
            $res->bindParam(":asal",Koleksi::EmptyPostVal($_POST['asal']));
            $res->bindParam(":tujuan",Koleksi::EmptyPostVal($_POST['tujuan']));
            $res->bindParam(":untuk",Koleksi::EmptyPostVal($_POST['keperluan']));
            $tahun = (int) $_POST['tanggal'];
            $res->bindParam(":tanggal",Koleksi::EmptyPostVal($_POST['tanggal']));
            if (empty($_FILES["filest"]["name"])) {
                $res->bindParam(":upload",$blank);
            } else {
                $temp = explode(".", $_FILES['filest']['name']);
                $kode = explode("/",$kode);
                $kode = implode ("_", $kode);
                $nama = $tahun."-ST-".$kode;
                $target_dir = "upload/st/was/";
                $extension = end($temp);
                $target_file = $target_dir.$nama.".".$extension;
                move_uploaded_file($_FILES["filest"]["tmp_name"], $target_file);
                $filest=$target_file;
                $res->bindParam(":upload",$filest);
            }
            $res->execute();
            for ($x = 0; $x <= 25; $x++) {
                if (!empty($_POST['nip'.$x]) AND !empty($_POST['posisi'.$x])){
                    $sql1 = "INSERT INTO `was_st_anggota`
                    (`st_key`,
                    `st_pangkat`,
                    `st_nip`,
                    `st_posisi`
                    )
                    VALUES
                    (:key,
                    :pangkat,
                    :nip,
                    :posisi
                    )";
                    $res1 = $db->prepare($sql1);
                    $res1->bindParam(":key",$randstring);
                    $res1->bindParam(":nip",Koleksi::EmptyPostVal($_POST['nip'.$x]));
                    $res1->bindParam(":posisi",Koleksi::EmptyPostVal($_POST['posisi'.$x]));
                    $res1->bindParam(":pangkat",Koleksi::EmptyPostVal($_POST['gol'.$x]));
                    $res1->execute();
                }
            }
        }
		// Diperlukan dalam proses update
		// Eksekusi
		// Tampilkan Informasi Sukses
		if ($_POST['aksi'] == "was_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "was_stsimpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "was_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}

	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}

		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;

		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['was']) && $_GET['was'] == 'baru') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Pengawasan</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Tahun </label>
					<div class="col-md-9">
						<input type="text" id="tahun" name="tahun" value="" class="form-control" autocomplete="on">
					</div>
				</div>
				<div class="form-group">
						<label class="col-md-3 control-label">Pemeriksa</span></label>
						<div class="col-md-9">
							<select id="pemeriksa" name="pemeriksa" class="form-control input-medium">
							<option value="">Pilih...</option>';
							foreach($kdpemeriksa as $key => $val) {
								$isi.='<option value="'.$key.'">'.$val.'</option>';
							}
							$isi.='
							</select>
						</div>
					</div>
			<div class="form-group">
					<label class="col-md-3 control-label">Jenis Pemeriksaan</span></label>
					<div class="col-md-9">
						<select id="jenis" name="jenis" class="form-control input-medium">
						<option value="">Pilih...</option>';
						foreach($kdjenis as $key => $val) {
							$isi.='<option value="'.$key.'">'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Obyek</label>
					<div class="col-md-9">
						<input type="text" id="obyek1" name="obyek1" value="" class="form-control" autocomplete="on" style="display: none">
					</div>
                    <div class="col-md-9">
						<select id="obyek2" name="obyek2" class="form-control input-medium" style="display: none">
						<option value="">Pilih...</option>';
						foreach($opd as $key => $val) {
							$isi.='<option>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">P2HP </label>
					<div class="col-md-4">
						<input id="p2hp" name="p2hp" class="input-file" type="file">
					</div>
                    <div class="col-md-5">
						<input id="tglp2hp" name="tglp2hp"  class="form-control" type="date" >
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">LHP </label>
					<div class="col-md-9">
                        <input type="text" id="nolhp" name="nolhp" value="" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-md-4">
						<input id="lhp" name="lhp" class="input-file" type="file">
					</div>
                    <div class="col-md-5">
						<input id="tgllhp" name="tgllhp"  class="form-control" type="date" >
					</div>
				</div>
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'stbaru') {
    $id=$_POST['jenis'];
    $was=$_POST['uid'];
	$query2 = "select * from was_kd_jenis where jenis_id='$id'";
    $stmt2 = $db->prepare($query2);
    $stmt2->execute();
    $row2= $stmt2->fetch(PDO::FETCH_ASSOC);
    $kodest=$row2['jenis_kodest'];
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Surat Tugas</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in form_st" id="tab_biodata">
            <input type="hidden" id="was_id" name="was_id" value="'.$was.'">
                <div class="form-group">
					<label class="col-md-3 control-label">Nomor ST </label>
					<div class="col-md-9">
						<input type="text" id="kodest" name="kodest" value="'.$kodest.'" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Lama ST </label>
					<div class="col-md-9">
						<input type="text" id="lama" name="lama" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Kegiatan</label>
					<div class="col-md-4">
						<input id="awal" name="awal"  class="form-control" type="date" >
					</div>
                    <div class="col-md-5">
						<input id="akhir" name="akhir"  class="form-control" type="date" >
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Asal </label>
					<div class="col-md-9">
						<input type="text" id="asal" name="asal" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tujuan </label>
					<div class="col-md-9">
						<input type="text" id="tujuan" name="tujuan" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Keperluan </label>
					<div class="col-md-9">
						<input type="text" id="keperluan" name="keperluan" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal ST</label>
					<div class="col-md-9">
						<input id="tanggal" name="tanggal"  class="form-control" type="date" >
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan ST</label>
					<div class="col-md-4">
                        <input type="text" id="tahun" name="tahun" class="form-control">
					</div>
                    <div class="col-md-5">
						<input id="filest" name="filest" class="input-file" type="file">
					</div>
				</div>
                <div id="anggota0">
                    <div class="form-group">
				    	<label class="col-md-3 control-label">Posisi</span></label>
				    	<div class="col-md-9">
				    		<select id="posisi0" name="posisi0" class="form-control input-medium">
				    		<option value="">Pilih...</option>';
				    		foreach($kdposisi as $key => $val) {
				    			$isi.='<option value="'.$key.'">'.$val.'</option>';
				    		}
				    		$isi.='
				    		</select>
				    	</div>
				    </div>
                    <div class="form-group">
				    	<label class="col-md-3 control-label">Nama Pegawai</label>
                        <div class="col-md-9">
				    		<select id="nip0" name="nip0" class="form-control input-medium">
				    		<option value="">Pilih...</option>';
				    		foreach($peg as $key => $val) {
				    			$isi.='<option value="'.$key.'">'.$val.'</option>';
				    		}
				    		$isi.='
				    		</select>
				    	</div>
				    </div>
                    <div class="form-group">
				    	<label class="col-md-3 control-label">Golongan Ruang</label>
                        <div class="col-md-9">
				    		<select id="gol0" name="gol0" class="form-control input-medium">
				    		<option value="">Pilih...</option>';
				    		foreach($kdgolru as $key => $val) {
				    			$isi.='<option value="'.$key.'">'.$val.'</option>';
				    		}
				    		$isi.='
				    		</select>
				    	</div>
				    </div>
                    <divclass="row" id="tombol0">
                        <center>
                            <button type="button" class="btn btn-primary" id="add" kolom="0">Tambah</button>
                        </center>
                    </div>
                </div>';
                for ($x = 1; $x <= 25; $x++) {
                    $isi.='<div id="anggota'.$x.'" style="display: none" >
                                <div class="form-group">
				                	<label class="col-md-3 control-label">Posisi</span></label>
				                	<div class="col-md-9">
				                		<select id="posisi'.$x.'" name="posisi'.$x.'" class="form-control input-medium">
				                		<option value="">Pilih...</option>';
				                		foreach($kdposisi as $key => $val) {
				                			$isi.='<option value="'.$key.'">'.$val.'</option>';
				                		}
				                		$isi.='
				                		</select>
				                	</div>
				                </div>
                                <div class="form-group">
				                	<label class="col-md-3 control-label">Nama Pegawai</label>
                                    <div class="col-md-9">
				                		<select id="nip'.$x.'" name="nip'.$x.'" class="form-control input-medium">
				                		<option value="">Pilih...</option>';
				                		foreach($peg as $key => $val) {
				                			$isi.='<option value="'.$key.'">'.$val.'</option>';
				                		}
				                		$isi.='
				                		</select>
				                	</div>
				                </div>
                                <div class="form-group">
				    	           <label class="col-md-3 control-label">Golongan Ruang</label>
                                    <div class="col-md-9">
				    		          <select id="gol'.$x.'" name="gol'.$x.'" class="form-control input-medium">
				    		          <option value="">Pilih...</option>';
				    		          foreach($kdgolru as $key => $val) {
				    			         $isi.='<option value="'.$key.'">'.$val.'</option>';
				    		          }
				    		          $isi.='
				    		          </select>
				    	           </div>
				                </div>
                                <div class="row" id="tombol'.$x.'">
                                    <center>
                                        <button type="button" class="btn btn-danger" id="remove" kolom="'.$x.'">Hapus</button>
                                        <button type="button" class="btn btn-primary" id="add" kolom="'.$x.'">Tambah</button>
                                    </center>
                                </div>
                            </div>';}
                $isi.='
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'detailst') {
    $key=$_POST['uid'];
	$query2 = "select * from was_st where st_key='$key'";
    $stmt2 = $db->prepare($query2);
    $stmt2->execute();
    $row2= $stmt2->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Detail Surat Tugas <b>'.$row2['st_kode'].'</b> </a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in form_st" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Nomor ST </label>
					<div class="col-md-9">
						<input type="text" id="kodest" name="kodest" readonly value="'.$row2['st_kode'].'" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Lama ST </label>
					<div class="col-md-9">
						<input type="text" id="lama" name="lama" readonly value="'.$row2['st_lama'].'" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Kegiatan</label>
					<div class="col-md-4">
						<input id="awal" name="awal"  class="form-control" type="date" readonly value="'.$row2['st_mulai'].'">
					</div>
                    <div class="col-md-5">
						<input id="akhir" name="akhir"  class="form-control" type="date" readonly value="'.$row2['st_akhir'].'">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Asal </label>
					<div class="col-md-9">
						<input type="text" id="asal" name="asal" class="form-control" readonly value="'.$row2['st_asal'].'">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tujuan </label>
					<div class="col-md-9">
						<input type="text" id="tujuan" name="tujuan" class="form-control" readonly value="'.$row2['st_tujuan'].'">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Keperluan </label>
					<div class="col-md-9">
						<input type="text" id="keperluan" name="keperluan" class="form-control" readonly value="'.$row2['st_untuk'].'">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal ST</label>
					<div class="col-md-9">
						<input id="tanggal" name="tanggal"  class="form-control" type="date" readonly value="'.$row2['st_tanggal'].'">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan ST</label>
                    <div class="col-md-9">';
                        if (file_exists($row2['st_upload'])) {
                            $isi.='
                                <embed src="'.$row2['st_upload'].'"  height="500px" />
                            ';
                        }
                        $isi.='
					</div>
				</div>';
                $query3 = "select * from was_st_anggota where st_key='$key'";
                $stmt3 = $db->prepare($query3);
                $stmt3->execute();
                while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){
                    $isi.='<div >
                                <div class="form-group">
				                	<label class="col-md-3 control-label">Posisi</span></label>
				                	<div class="col-md-9">
                                        <input type="text" id="asal" name="asal" class="form-control" readonly value="'.$kdposisi[$row3['st_posisi']].'">
				                	</div>
				                </div>
                                <div class="form-group">
				                	<label class="col-md-3 control-label">Nama Pegawai</label>
                                    <div class="col-md-9">
                                        <input type="text" id="asal" name="asal" class="form-control" readonly value="'.$peg[$row3['st_nip']].'">
				                	</div>
				                </div>
                                <div class="form-group">
				    	           <label class="col-md-3 control-label">Golongan Ruang</label>
                                    <div class="col-md-9">
                                        <input type="text" id="asal" name="asal" class="form-control" readonly value="'.$kdgolru[$row3['st_pangkat']].'">
				    	           </div>
				                </div>
                            </div>';}
                $isi.='
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'edit') {

	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `was_data` WHERE was_id = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Pengawasan</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <input type="hidden" id="was_id" name="was_id" value="'.$r['was_id'].'">
                <input type="hidden" id="p2hpawal" name="p2hpawal" value="'.$r['was_p2hp'].'">
                <input type="hidden" id="lhpawal" name="lhpawal" value="'.$r['was_lhp'].'">
                <div class="form-group">
					<label class="col-md-3 control-label">Tahun </label>
					<div class="col-md-9">
						<input type="text" id="tahun" name="tahun" value="'.$r['was_tahun'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jenis Pemeriksaan</span></label>
					<div class="col-md-9">
						<select id="pemeriksa" name="pemeriksa" class="form-control input-medium">
                        <option value="">Pilih...</option>';
						foreach($kdpemeriksa as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['was_pemeriksa'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Jenis Pemeriksaan</span></label>
					<div class="col-md-9">
						<select id="jenis" name="jenis" class="form-control input-medium">
                        <option value="">Pilih...</option>';
						foreach($kdjenis as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['was_jenis'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Obyek</label>
					<div class="col-md-9">
						<input type="text" id="obyek" name="obyek" value="'.$r['was_obyek'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">P2HP </label>
					<div class="col-md-4">
						<input id="p2hp" name="p2hp" class="input-file" type="file">
					</div>
                    <div class="col-md-5">
						<input id="tglp2hp" name="tglp2hp"  value="'.$r['was_tgl_p2hp'].'" class="form-control" type="date" >
					</div>
				</div>';
                if (file_exists($r['was_p2hp'])) {
                $isi.='
                <div class="form-group">
                    <label class="col-md-3 control-label" for="nama">P2HP Terupload</label>
                    <div class="col-md-9">
                        <embed src="'.$r['was_p2hp'].'"  height="500px" />
                    </div>
                </div>';
                }
                $isi.='
                <div class="form-group">
					<label class="col-md-3 control-label">LHP </label>
					<div class="col-md-9">
                        <input type="text" id="nolhp" name="nolhp" value="'.$r['was_nolhp'].'" class="form-control">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label"></label>
					<div class="col-md-4">
						<input id="lhp" name="lhp" class="input-file" type="file">
					</div>
                    <div class="col-md-5">
						<input id="tgllhp" name="tgllhp" value="'.$r['was_tgl_lhp'].'" class="form-control" type="date" >
					</div>
				</div>';
                if (file_exists($r['was_lhp'])) {
                $isi.='
                <div class="form-group">
                    <label class="col-md-3 control-label" for="nama">LHP Terupload</label>
                    <div class="col-md-9">
                        <embed src="'.$r['was_lhp'].'"  height="500px" />
                    </div>
                </div>';
                }
                $isi.='
                 <div class="form-group">
					<label class="col-md-3 control-label">Status Pemeriksaaan</span></label>
					<div class="col-md-9">
						<select id="status" name="status" class="form-control input-medium">
                        <option value="">Pilih...</option>';
						foreach($kdstatus as $key => $val) {
							$isi.='<option value="'.$key.'"'.(($r['was_status'] == $key) ? ' selected=selected' : '').'>'.$val.'</option>';
						}
						$isi.='
						</select>
					</div>
				</div>
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'tabeldata') {

	header("Content-type:application/json");

	// FIX untuk satu tabel
	/*
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/

	// Kolom yang bisa di Sortir
	$columns = array(
		1=>'was_tahun',
		2=>'was_pemeriksa',
		3=>'was_jenis',
		4=>'was_obyek',
		10=>'was_status'
	);

	// Untuk 2 Tabel
	/*
	$sql = "SELECT
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/

$sql = "SELECT
        *
        FROM
        `was_data` ";

	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" WHERE `was_obyek` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `was_nolhp` LIKE '%".$_REQUEST['search']['value']."%' ";
	}
	$res = $db->prepare($sql);
	$res->execute();

	$totalData = $res->rowCount();
	$totalFiltered = $totalData;

	if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];

	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
        $listst='<table style="border:none">';
        //belum selesai di kerjakan
            $was=$row['was_id'];
            $sql2 = "SELECT * FROM was_st WHERE st_was='$was'";
            $datast = $db->prepare($sql2);
            $datast->execute();
            while($rowst = $datast->fetch(PDO::FETCH_BOTH)) {
                $listst .='<tr> <td>  <a href="#" id="detailst" data-id="'.$rowst['st_key'].'" data-target="#form-modal" data-toggle="modal">'.$rowst['st_kode'].'</a> </td> </tr>';
            }
        //======================================================
        $listst.='</table>';
		$nestedData = array();
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		$nestedData[] = $row['was_tahun'];
		$nestedData[] = $kdpemeriksa[$row['was_pemeriksa']];
		$nestedData[] = $kdjenis[$row['was_jenis']];
        $nestedData[] = $row['was_obyek'];
        $nestedData[] = $listst;
        $nestedData[] = '<a href="#" id="stbaru" data-id="'.$row['was_id'].'" jenis-id="'.$row['was_jenis'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Tambah ST </span></center></a>';
        $nestedData[] = '<a href="'.$row['was_p2hp'].'" target="_blank">'.$row['was_tgl_p2hp'].'</a>';
        $nestedData[] = '<a href="'.$row['was_lhp'].'" target="_blank">'.$row['was_nolhp'].' Tanggal: '.$row['was_tgl_lhp'].'</a>';
        $nestedData[] = $row['was_biaya'];
		/*
		$arr = array("Y"=>"Aktif", "T"=>"Tidak Aktif", "P"=>"Pindah");
		$aktif = isset($arr[$row[6]]) ? $arr[$row[6]] : null;
		*/
		if($row['was_status'] == "2") {
			$nestedData[] = '<center><span class="label label-sm label-success"> Selesai </span></center>';
		}
        else {
			$nestedData[] = '<center><span class="label label-sm label-warning">'.$kdstatus[$row['was_status']].'</span></center>';
		}
		// Opsi
		// $otoritas =


				$nestedData[] = '
							<a href="#" id="edit" data-id="'.$row['was_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Update </span></center></a>
                            <br>
                            <a href="modul.php?ke=was_tl&id='.$row['was_id'].'"><center><span class="label label-sm label-primary"> Temuan </span></center></a>
                            
							';

		$data[] = $nestedData;
		$no++;
	}
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">

		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Kegiatan Pengawasan Inspektorat Provinsi Kalimantan Utara
					</div>
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
				</div> <!-- portlet-title -->

				<div class="portlet-body">
					<div class="table-container">
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th><center>No</center></th>
						  <th>Tahun</th>
						  <th>Pemeriksa</th>
						  <th>Jenis</th>
						  <th>Obyek</th>
						  <th colspan="2">Surat Tugas</th>
						  <th>P2HP</th>
						  <th>LHP</th>
						  <th>Biaya</th>
						  <th>Status</th>
						  <th>Aksi</th>
						</tr>
						</thead>
						<tbody></tbody>
						</table>

					</div> <!-- table-container -->
				</div> <!-- portlet-body -->

			</div>
		</div>

		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->

	</div>
	';

	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>
