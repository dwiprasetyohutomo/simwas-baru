<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=was_spi';
$theme		= 'themes/conquer';
$tabelData	= 'was_rinci';
$pageTitle	= 'Pengawasan <small>Rincian Rekomendasi</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i>
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Pengawasan</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Bukti SPIi</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-was-spi.js"></script>
';
$initA = '
// var modul = "modul.php?ke=was_rinci";
var modul = window.location.href;
var pageTitle = "Rincian Rekomendasi";

// CRUD
var menuBaru 	= modul+"&rinci=baru";
var menuEdit	= modul+"&rinci=edit";

// Table Data
var srcUrl 	= modul+"&was=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
umst.init();
';
$initC = '';
// MAIN CODE

//DATA Obyek
$kdobyek = array();
$query = "SELECT
    `was_obyek`
FROM
    `was_data`
GROUP BY `was_obyek`";
$stmt1 = $db->prepare($query);
$stmt1->execute();
 while ($row1= $stmt1->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row1);
     $kdobyek += [$was_obyek => $was_obyek];
}

//DATA Tahun
$kdtahun = array();
$query = "SELECT
    `was_tahun`
FROM
    `was_data`
GROUP BY `was_tahun`";
$stmt1 = $db->prepare($query);
$stmt1->execute();
 while ($row1= $stmt1->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row1);
     $kdtahun += [$was_tahun => $was_tahun];
}

//DATA Kelompok
$kdkel = array();
$query = "SELECT
    `kode_kel`
    , `kode_deskripsi`
FROM
   `was_tl_kode`
WHERE (`kode_sub_kel` =0)
GROUP BY `kode_kel`";
$stmt1 = $db->prepare($query);
$stmt1->execute();
 while ($row1= $stmt1->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row1);
     $kdkel += [$kode_kel => $kode_deskripsi];
}


$blank="";
if (isset($_POST['submit'])) {
	try {
		// Query Simpan
		if ($_POST['aksi'] == "spi_simpan") {
			$sql = "INSERT INTO `was_buktispi`
			(`spi_kode`,
			`spi_nosurat`,
			`spi_tgl`,
			`spi_file`,
			`spi_ket`
			)
			VALUES
			(:kode,
			:nosurat,
			:tgl,
			:file,
			:ket
            )";
            $res = $db->prepare($sql);
            $key=1;
            do {
                $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $randstring = '';
                for ($i = 0; $i < 10; $i++) {
                    $randstring .= $char[rand(0, strlen($char)-1)];
                }
                $query2 = "select count(*) from was_buktispi where spi_kode='$randstring'";
                $stmt2 = $db->prepare($query2);
                $stmt2->execute();
                $key= $stmt2->fetchColumn();
            } while ($key > 0);
            $res->bindParam(":kode",$randstring);
            $kode=$randstring;

		}
		// Query Update
		if ($_POST['aksi'] == "spi_update") {
			$sql = "UPDATE `was_buktispi` SET
			`spi_nosurat` = :nosurat,
			`spi_tgl` = :tgl,
			`spi_file` = :file,
			`spi_ket` = :ket
			WHERE `spi_kode` = :kode";
            $res = $db->prepare($sql);
            $res->bindParam(":kode", Koleksi::EmptyPostVal($_POST['kode']));
            $kode=$_POST['kode'];
		}

		// Persiapkan Query
		// Posisikan $nip disini jangan dibawah pegawai_simpan bisa ERROR!!!
		//$nip = preg_replace('/\D/','', $_POST['nip']);
		//$nip = $_POST['nip'];

		// Pasangkan dan Eksekusi
		//if ($_POST['aksi'] == "cuti_simpan")
			//$res->bindParam(":nip", $nip, PDO::PARAM_STR);
		if ($_POST['aksi'] == "spi_simpan" || $_POST['aksi'] == "spi_update"){
            $res->bindParam(":nosurat", Koleksi::EmptyPostVal($_POST['nosurat']));
            $res->bindParam(":tgl", Koleksi::EmptyPostVal($_POST['tanggal']));
            $res->bindParam(":ket", Koleksi::EmptyPostVal($_POST['ket']));
            if ($_POST['aksi'] == "spi_simpan") {
                if (empty($_FILES["spi"]["name"])) {
                    $res->bindParam(":file",$blank);
                }
            }
            if ($_POST['aksi'] == "spi_update") {
                if (empty($_FILES["spi"]["name"])) {
                    $res->bindParam(":file", Koleksi::EmptyPostVal($_POST['filespi']));
                }
                else {
                    unlink($_POST['filespi']);
                }
            }
            //=================== ini upload file sts ====================
            if (empty($_FILES["spi"]["name"])) {
            } else {
                $temp = explode(".", $_FILES['spi']['name']);
                $nama = $kode;
                $target_dir = "upload/spi/";
                $extension = end($temp);
                $target_file = $target_dir.$nama.".".$extension;
                move_uploaded_file($_FILES["spi"]["tmp_name"], $target_file);
                $file=$target_file;
                $res->bindParam(":file",$file);
            }
            //=============================================================

            $res->execute();
        }

		// Diperlukan dalam proses update
		// Eksekusi
		// Tampilkan Informasi Sukses
		if ($_POST['aksi'] == "spi_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "spi_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}

	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}

		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;

		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['spi']) && $_GET['spi'] == 'baru') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Bukti Penyelesaian SPI</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Surat </label>
					<div class="col-md-9">
						<input type="date" id="tanggal" name="tanggal" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">No Surat</label>
					<div class="col-md-9">
						<input type="text" id="nosurat" name="nosurat" value="" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan Bukti</label>
					<div class="col-md-9">
						<input id="spi" name="spi" class="input-file" type="file">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="ket" name="ket" class="form-control">
                        </textarea>
                    </div>
                </div> 
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['spi']) && $_GET['spi'] == 'edit') {

	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `was_buktispi` WHERE spi_kode = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Pengawasan</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <input type="hidden" id="kode" name="kode" value="'.$r['spi_kode'].'">
                <input type="hidden" id="filespi" name="filespi" value="'.$r['spi_file'].'">
                <div class="tab-pane active fade in" id="tab_biodata">
                <div class="form-group">
					<label class="col-md-3 control-label">Tanggal Surat </label>
					<div class="col-md-9">
						<input type="date" id="tanggal" name="tanggal" value="'.$r['spi_tgl'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Nomor Surat</label>
					<div class="col-md-9">
						<input type="text" id="nosurat" name="nosurat" value="'.$r['spi_nosurat'].'" class="form-control" autocomplete="on">
					</div>
				</div>
                <div class="form-group">
					<label class="col-md-3 control-label">Scan Bukti</label>
					<div class="col-md-9">
						<input id="spi" name="spi" class="input-file" type="file">
					</div>
				</div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Keterangan</label>
                    <div class="col-sm-9">
                        <textarea type="text" id="ket" name="ket" class="form-control">
                        '.$r['spi_ket'].'
                        </textarea>
                    </div>
                </div>          
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'tabeldata') {

	header("Content-type:application/json");

	// FIX untuk satu tabel
	/*
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/

	// Kolom yang bisa di Sortir
	/*$columns = array(
		0=>'rinci_nip',
		1=>'rinci_nama',
		2=>'rinci_nilai'
	);
    */

	// Untuk 2 Tabel
	/*
	$sql = "SELECT
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/
$sql = "SELECT
*
FROM
  was_buktispi
    ";

	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		$sql.=" WHERE `spi_nosurat` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `spi_tgl` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `spi_ket` LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR `spi_kode` LIKE '%".$_REQUEST['search']['value']."%' ";
	}
	$res = $db->prepare($sql);
	$res->execute();

	$totalData = $res->rowCount();
	$totalFiltered = $totalData;

	//if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];
    
	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
		$nestedData = array();
        
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		$nestedData[] = $row['spi_kode'];
		$nestedData[] = $row['spi_tgl'];
		$nestedData[] = $row['spi_nosurat'];
		$nestedData[] = '<a href="'.$row['spi_file'].'" target="_blank">'.$row['spi_file'].'</a>';
		$nestedData[] = $row['spi_ket'];
		$nestedData[] = '
							<a href="#" id="edit" data-id="'.$row['spi_kode'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Update Bukti </span></center> 
                            <br>
							';                
		$data[] = $nestedData;
		$no++;
	}
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->

	<div class="row">

		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Seleksi Laporan Temuan
					</div>
					<div class="actions">

					</div> <!-- end actions -->
				</div> <!-- portlet-title -->
                    
				<div class="portlet-body">
					<div class="container">
                        <form action="modul/cetak_rekap.php" name="form1" method="post" >
                            <div class="form-group">
                                <label class="col-md-3 control-label">Obyek Pemeriksaan</span></label>
                                <div class="col-md-9">
                                    <select id="obyek" name="obyek" class="form-control input-medium">
                                    <option value="0">Semua</option>';
                                    foreach($kdobyek as $key => $val) {
                                        $isi.='<option value="'.$key.'">'.$val.'</option>';
                                    }
                                    $isi.='
                                    </select>
                                    </br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tahun Pemeriksaan</span></label>
                                <div class="col-md-9">
                                    <select id="tahun" name="tahun" class="form-control input-medium">
                                    <option value="0">Semua</option>';
                                    foreach($kdtahun as $key => $val) {
                                        $isi.='<option value="'.$key.'">'.$val.'</option>';
                                    }
                                    $isi.='
                                    </select>
                                    </br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"></span></label>
                                <div class="col-md-9">
                                     <input type="submit" name="cetak" value="CETAK">
                                </div>
                            </div>
                           
                        </form> 
					</div>
				</div> <!-- portlet-body -->
			</div>
		</div>
        <div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Tabel Rekapitulasi
					</div>
					<div class="actions">

					</div> <!-- end actions -->
				</div> <!-- portlet-title -->
                    
				<div class="portlet-body">
					<div class="container">
                    <br>
                     SEHARUSNYA TABEL tapi masih ngebug
					</div>
				</div> <!-- portlet-body -->
			</div>
		</div>

		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->

	</div>
	';

	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>
