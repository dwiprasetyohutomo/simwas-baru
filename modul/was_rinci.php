<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['sesid']) && empty($_SESSION['sesid'])) { header('location: login.php'); exit(); }

// Buka koneksi ke Database
$db = koleksi::db_pdo($conn);

$modul		= 'modul.php?ke=was_rinci';
$theme		= 'themes/conquer';
$tabelData	= 'was_rinci';
$pageTitle	= 'Pengawasan <small>Rincian Rekomendasi</small>';



$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i>
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Pengawasan</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<span>Rincian Rekomendasi</span>
</li>
';

$pageStyles = '
<script type="text/javascript" src="'.$theme.'/assets/scripts/form-was-rinci.js"></script>
';
$initA = '
// var modul = "modul.php?ke=was_rinci";
var modul = window.location.href;
var pageTitle = "Rincian Rekomendasi";

// CRUD
var menuBaru 	= modul+"&rinci=baru";
var menuEdit	= modul+"&rinci=edit";

// Table Data
var srcUrl 	= modul+"&was=tabeldata";
var destSel	= $("#tableAjax");
';
$initB = '
umst.init();
';
$initC = '';
// MAIN CODE
$rekid=$_GET["id"];

//DATA KD JENIS ST
$kdjenis = array();
$query = "select * from um_stjenis";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdjenis += [$jenis_id => $jenis_nama];
}
//
//DATA KD JENIS ST
$kddasar= array();
$query = "select * from um_stdasar";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kddasar += [$dasar_id => $dasar_text];
}
//
//DATA KD GOLRU IN ARRAY
$kdgolru = array();
$query = "select * from kd_golru";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdgolru += [$kdgol => $ket3];
}
//
//DATA KD PEMERIKSa IN ARRAY
$kdpemeriksa = array();
$query = "select * from was_pemeriksa";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $kdpemeriksa += [$pemeriksa_id => $pemeriksa_nama];
}
//
// kode pegawai
$peg = array();
$query = "select nip, nama from um_pegawai";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $peg += [$nip => $nama];
}
// no lhp
$nolhp = array();
$query = "SELECT
            `was_nolhp`
        FROM
            `was_data`
           WHERE was_nolhp IS NOT NULL
        ORDER BY `was_tahun` DESC";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $nolhp += [$was_nolhp => $was_nolhp];
}
//kode opd
$opd = array();
$query = "select * from opd";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $opd += [$opd_id => $opd_nama];
}
//kode opd
$tl_status = array();
$query = "select * from was_tl_status";
$stmt3 = $db->prepare($query);
$stmt3->execute();
 while ($row3= $stmt3->fetch(PDO::FETCH_ASSOC)){//yes, I know mysql is deprecated
     extract($row3);
     $tl_status += [$status_id => $status_ket];
}
$blank="";
if (isset($_POST['submit'])) {
	try {
		// Query Simpan
		if ($_POST['aksi'] == "rinci_simpan") {
			$input = "INSERT INTO was_tlrinci
			( rinci_rek
            , rinci_nip
            , rinci_nama
            , rinci_nilai
            , rinci_bayar
            , rinci_ket
			)
			VALUES
			( :rekid
            , :nip
            , :nama
            , :nilai
            , :bayar
            , :ket
            )";
            $res = $db->prepare($input);
            $res->bindParam(":rekid",$rekid);
		}
		// Query Update
		if ($_POST['aksi'] == "rinci_update") {
			$input = "UPDATE `was_tlrinci` SET
			`rinci_nip` = :nip,
			`rinci_nama` = :nama,
			`rinci_nilai` = :nilai,
			`rinci_bayar` = :bayar,
			`rinci_ket` = :ket
			WHERE `rinci_id` = :id";
            $res = $db->prepare($input);
            $res->bindParam(":id",Koleksi::EmptyPostVal($_POST['id']));
		}
        $res->bindParam(":nip",Koleksi::EmptyPostVal($_POST['nip']));
        $res->bindParam(":nama",Koleksi::EmptyPostVal($_POST['nama']));
        $res->bindParam(":nilai",Koleksi::EmptyPostVal($_POST['nilai']));
        $res->bindParam(":bayar",Koleksi::EmptyPostVal($_POST['bayar']));
        $res->bindParam(":ket",Koleksi::EmptyPostVal($_POST['ket']));
        $res->execute();

		if ($_POST['aksi'] == "rinci_simpan") $response = array( 'status'=>'sukses', 'pesan'=>'Data berhasil diproses tanpa ada kendala.' );
		if ($_POST['aksi'] == "rinci_update") $response = array( 'status'=>'sukses', 'pesan'=>'Proses memperbaharui data berhasil tanpa ada kendala.' );
	}

	catch(PDOException $e) {
		// Jika terdapat kesalahan
		// Kalo sudah selesai hapus $sql dan $err nya ........................
		$err = '';
		foreach($_POST as $k => $v) {
			$err.=$k.' = '.$v.'<br>';
		}

		if ( $e->getCode() == 23000 ) {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp; NIP sudah ada! Silahkan periksa kembali.';
		} else {
			$p = '<span class="label label-danger">Perhatian:</span>&nbsp;'.$e->getMessage();
		}
		$pesan = array();
		$pesan[] = $p;

		$response = array(
			'status'=>'gagal', 'pesan'=>$pesan
		);
	}
	// Tampilkan hasil Eksekusi
	echo json_encode($response);
	$db = null; // Tutup koneksi
}

else if(isset($_GET['rinci']) && $_GET['rinci'] == 'baru') {

	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Tambah Rincian Temuan</a></li>
		</ul>

		<div class="tab-content">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nip</label>
                            <div class="col-sm-9">
                                <input type="text" id="nip" name="nip" class="form-control">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" id="nama" name="nama" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nilai</label>
                            <div class="col-sm-9">
                                <input type="text" id="nilai" name="nilai" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Bayar</label>
                            <div class="col-sm-9">
                                <input type="text" id="bayar" name="bayar" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Keterangan</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="ket" name="ket" class="form-control">
                                </textarea>
                            </div>
                        </div>               
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['rinci']) && $_GET['rinci'] == 'edit') {

	if(isset($_POST['uid']) && $_POST['uid'] != '') $uid = $_POST['uid'];
	$res = $db->prepare("SELECT * FROM `was_tlrinci` WHERE rinci_id = '$uid'");
	$res->bindParam(":uid", $uid);
	$res->execute();
	$r = $res->fetch(PDO::FETCH_ASSOC);
	$isi = '
	<div class="alert alert-danger display-hide">
		<button class="close" data-close="alert"></button>
		Ada isian Form yang belum di isi. Mohon di periksa kembali.
	</div>

		<div class="tabbable tabbable-custom">
		<ul class="nav nav-tabs" id="tabpegawai">
			<li class="active"><a href="#tab_biodata" data-toggle="tab">Data Pengawasan</a></li>
		</ul>

		<div class="tab-content">
			<div class="tab-pane active fade in" id="tab_biodata">
                <input type="hidden" id="id" name="id" value="'.$r['rinci_id'].'">
                <div class="form-group">
                            <label class="col-sm-3 control-label">Nip</label>
                            <div class="col-sm-9">
                                <input type="text" id="nip" name="nip" value="'.$r['rinci_nip'].'" class="form-control">
                            </div>
                        </div> 
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" id="nama" name="nama" value="'.$r['rinci_nama'].'" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Nilai</label>
                            <div class="col-sm-9">
                                <input type="text" id="nilai" name="nilai" value="'.$r['rinci_nilai'].'" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Bayar</label>
                            <div class="col-sm-9">
                                <input type="text" id="bayar" name="bayar" value="'.$r['rinci_bayar'].'" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Keterangan</label>
                            <div class="col-sm-9">
                                <textarea type="text" id="ket" name="ket" class="form-control">'.$r['rinci_ket'].'
                                </textarea>
                            </div>
                        </div>           
			</div> <!-- end tab-biodata -->
		</div> <!-- end tab-content -->
	<!-- </div> --><!-- end tabbable -->
	';

	echo $isi;
}

else if(isset($_GET['was']) && $_GET['was'] == 'tabeldata') {

	header("Content-type:application/json");

	// FIX untuk satu tabel
	/*
	SELECT p.nip, p.golru, p.pangkat
	FROM (SELECT nip, MAX(golru) AS maxgolru FROM um_pangkat GROUP BY nip) AS x
	INNER JOIN um_pangkat AS p ON (p.nip = x.nip) AND p.golru = x.maxgolru
	*/

	// Kolom yang bisa di Sortir
	/*$columns = array(
		0=>'rinci_nip',
		1=>'rinci_nama',
		2=>'rinci_nilai'
	);
    */

	// Untuk 2 Tabel
	/*
	$sql = "SELECT
	  i.id,
	  i.imavatar,
	  i.nip,
	  CONCAT(IF(i.gelardp != '', CONCAT(i.gelardp, '. '), ''), i.nama, IF(i.gelarbk != '', CONCAT(', ', i.gelarbk), '')) AS nama,
	  CONCAT(IF(i.lahirtmp != '', CONCAT(i.lahirtmp,', '),''), IF(i.lahirtgl != '', DATE_FORMAT(i.lahirtgl,'%d-%m-%Y'), '')) AS kelahiran,
	  IF(i.jenkel = 'L', 'Laki-laki', 'Perempuan') AS jenkel,
	  i.aktif
	FROM
	  um_pegawai i
	WHERE
	  i.nip != ''";
	*/
$sql = "SELECT
*
FROM
  was_tlrinci
   where rinci_rek='$rekid'
    ";

	// Pencarian
	if (!empty($_REQUEST['search']['value'])) {
		//$sql.=" WHERE was_data.was_nolhp LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" WHERE nama LIKE '%".$_REQUEST['search']['value']."%' ";
		$sql.=" OR nip LIKE '%".$_REQUEST['search']['value']."%' ";
	}
	$res = $db->prepare($sql);
	$res->execute();

	$totalData = $res->rowCount();
	$totalFiltered = $totalData;

	//if (isset($_REQUEST['order'])) $sql.=" ORDER BY ".$columns[$_REQUEST['order'][0]['column']]." ".$_REQUEST['order'][0]['dir']." ";
	if (isset($_REQUEST['length']) && $_REQUEST['length'] <> -1) $sql.=" LIMIT ".$_REQUEST['start'].",".$_REQUEST['length'];
    
	$res = $db->prepare($sql);
	$res->execute();

	$data = array();
	if(!empty($_REQUEST['start'])) $no = $_REQUEST['start'] + 1; else $no = 1;
	while($row = $res->fetch(PDO::FETCH_BOTH)) {
		$nestedData = array();
        
		$nestedData[] = '<div style="text-align:right">'.$no.'.</div>';
		$nestedData[] = $row['rinci_nip'];
		$nestedData[] = $row['rinci_nama'];
		$nestedData[] = number_format($row['rinci_nilai'],2,",",".");
		$nestedData[] = number_format($row['rinci_bayar'],2,",",".");
        $sisa=$row['rinci_nilai']-$row['rinci_bayar'];
        $persen=$row['rinci_bayar']/$row['rinci_nilai']*100;
		$nestedData[] = number_format($sisa,2,",",".");
		$nestedData[] = number_format($persen,2,",",".") .' %';
		$nestedData[] = $row['rinci_ket'];
		$nestedData[] = '
							<a href="#" id="edit" data-id="'.$row['rinci_id'].'" data-target="#form-modal" data-toggle="modal"><center><span class="label label-sm label-primary"> Update Rincian </span></center> 
                            <br>
							';                
		$data[] = $nestedData;
		$no++;
	}
	if(!empty($_REQUEST['draw'])) $draw = $_REQUEST['draw']; else $draw = 0;
	echo json_encode( array( "draw"=>intval($draw), "recordsTotal"=>intval($totalData), "recordsFiltered"=>intval($totalFiltered), "data"=>$data ));
}

else {
	$isi ='
	<!--
	<div class="note note-success">
		<p></p>
	</div>
	-->
	<div class="row">

		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						Daftar Temuan LHP
					</div>
					<div class="actions">
						<a class="btn btn-sm btn-primary" href="#" id="baru" role="button" data-target="#form-modal" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span class="hidden-480">Data Baru</span>
						</a>
						<div class="btn-group">
							<ul class="dropdown-menu pull-right">
								<li><a href="#" id="refresh"><i class="fa fa-refresh"></i> Refresh</a></li>
								<li class="divider"></li>
								<li><a href="#" id="import"><i class="glyphicon glyphicon-import"></i> Import</a></li>
								<li><a href="'.$modul.'&lihat=export" id="export"><i class="glyphicon glyphicon-export"></i> Export</a></li>
								<li class="divider"></li>
								<li><a href="#" id="empty"><i class="fa fa-recycle"></i> Kosongkan</a></li>
							</ul>
						</div>	<!-- end btn-group -->
					</div> <!-- end actions -->
				</div> <!-- portlet-title -->
                <div class="portlet-body">
                    <table width="100%" class="table table-striped">
                        <thead>
                        <tr class="heading">
                          <th>Nilai LHP</th>
                          <th>Total Nilai Rinci</th>
                          <th>Total Bayar</th>
                          <th>Total Sisa</th>
                          <th>Persentase</th>
                        </tr>
                        </thead>';
                   $sql2 = "SELECT
                                `was_rekomendasi`.`rek_nilai`
                                , SUM(`was_tlrinci`.`rinci_nilai`) AS `nilai`
                                , SUM(`was_tlrinci`.`rinci_bayar`) AS `bayar`
                                , `was_rekomendasi`.`rek_id`
                            FROM
                                `was_tlrinci`
                                INNER JOIN `was_rekomendasi` 
                                    ON (`was_tlrinci`.`rinci_rek` = `was_rekomendasi`.`rek_id`)
                            WHERE `was_rekomendasi`.`rek_id` ='$rekid'";
                    $res2 = $db->prepare($sql2);
                    $res2->execute();
                    $row2 = $res2->fetch(PDO::FETCH_BOTH);
                    $nilailhp=$row2['rek_nilai'];
                    $totalnilai=$row2['nilai'];
                    $totalbayar=$row2['bayar'];
                    $totalsisa=$totalnilai-$totalbayar;
                    $persen=$totalbayar/$totalnilai*100;
              $isi .='<tbody>
                        <td>'.number_format($nilailhp,2,",",".").'</td>
                        <td>'.number_format($totalnilai,2,",",".").'</td>
                        <td>'.number_format($totalbayar,2,",",".").'</td>
                        <td>'.number_format($totalsisa,2,",",".").'</td>
                        <td>'.number_format($persen,2,",",".").' %</td>
                    </tbody>
                    </table>
                </div>
				<div class="portlet-body">
					<div class="table-container">
						<table id="tableAjax" width="100%" class="table table-striped table-hover">
						<thead>
						<tr class="heading">
						  <th><center>No</center></th>
						  <th>NIP</th>
						  <th>Nama</th>
						  <th>Nilai</th>
						  <th>Bayar</th>
						  <th>Sisa</th>
						  <th>Persen</th>
						  <th>Keterangan</th>
						  <th>Aksi</th>
						</tr>
						</thead>
						<tbody></tbody>
						</table>

					</div> <!-- table-container -->
				</div> <!-- portlet-body -->

			</div>
		</div>

		<!--
		========================
		Form Simpan / Perbaharui
		======================== -->
		<div id="form-modal" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="true" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close white" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Modal title</h4>
					</div>
					<form action="'.$modul.'" id="form-user" name="form-user" class="form-horizontal" method="post" enctype="multipart/form-data">
						<div class="modal-body form">
							<div class="modal-loading"><img src="img/loading/loading-page.gif"/></div>
							<div class="form-body">
							..... disini kode form kamu .....
							</div>
						</div>
						<div class="modal-footer">
							<input type="hidden" id="cuti_id" name="cuti_id" readonly="readonly">
							<input type="hidden" id="aksi" name="aksi">
							<button type="submit" id="submit" name="submit" class="btn btn-success">Submit</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						</div>
					</form>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.form-modal -->

	</div>
	';

	$tpl = new template;
	$tpl->load('themes/conquer/index_full.html');
	$tpl->set('theme',$theme);
	$tpl->set('css-tambahan','');
	$tpl->set('page-title',$pageTitle);
	$tpl->set('page-breadcrumb',$pageBreadcrumb);
	$tpl->set('page-kontent',$isi);
	$tpl->set('page-plugin-script','');
	$tpl->set('page-styles-script',$pageStyles);
	$tpl->set('initA',$initA);
	$tpl->set('initB',$initB);
	$tpl->set('initC',$initC);
	$tpl->publish();
}
?>
