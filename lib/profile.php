<?php
if(preg_match("#profile.php#", $_SERVER['PHP_SELF'])) {
	header("location: index.php");
	die;
}
$pro['propinsi']  = 'Kalimantan Utara';
$pro['kab']       = '';
$pro['kota']      = 'Tarakan';
$pro['instansi']  = 'Inspektorat Provinsi Kaltara';
$pro['kantor']    = '';
$pro['alamat']    = '';
$pro['visi']      = 'Terwujudnya Aparatur Inspektorat yang mampu melaksanakan tugas dan fungsi secara profesionalisme.';
// $pro['copyright'] = '&copy 2010 - '.date('Y');
$pro['copyr']     = '&copy 2010 - '.date('Y').' '.$pro['instansi'].' '.date('Y').'. <a href="#">Privacy Policy</a> | <a href="#">Terms of Service</a>';
$pro['telepon']   = '';
$pro['fax']       = '';
$pro['email']     = 'inspektoratktt@gmail.com';
$pro['twitter']   = '#';
$pro['facebook']  = 'http://www.facebook.com/cutesmonk';
$pro['pint']      = '#';
$pro['github']    = '#';
$pro['linkedin']  = '#';
$pro['vk']        = '#';
$pro['plus']      = '#';
?>