<?php
class template {
	
	private $html;
	
	// Administrator
	var $theme 			= 'themes/conquer/';
	var $cssTambahan	= '';
	var $pageTitle		= 'Halaman Administrator';
	var $pageBreadcrumb	= '<li><i class="fa fa-home"></i> <a href="index.php">Beranda</a>';
	var $isi			= '';
	var $pagePlugins	= '';
	var $pageStyles		= '';
	var $initA			= '';
	var $initB			= '';
	var $initC			= '';
	
	
	public function load($template) {
		if(!file_exists($template)) {
		echo "halaman template ($template) tidak ada, silahkan periksa keberadaan file.";
			return false;
		}
		$this->html = file_get_contents($template);
		return true;
	}
	
	public function set($var, $content) {
		$this->html = str_replace("#$var#", $content, $this->html);
	}
	
	public function publish() {
		/*eval("?>".$this->template."<?php");*/
		eval("?>".$this->html."");
	}
}

?>