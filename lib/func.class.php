<?php
error_reporting(9);
require_once "config.php";

$conn = array( "host"=>$dbHost, "user"=>$dbUser, "pass"=>$dbPass, "dbnm"=>$dbName );

$nama_bulan		= [0=>"","Januari","Pebruari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember"];
$nama_bulan_min	= [0=>"","Jan","Peb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des"];

class Koleksi {
	// Koneksi MySQLi

	static function mysqli_connect($conn) {
		$db = @new mysqli($conn['host'], $conn['user'], $conn['pass'], $conn['dbnm']);
		//$db->set_charset("utf8");		
		if($db->connect_error) {
			die('Database Error : <b>'.$db->connect_error.'</b>');
		} else {
			return $db;
		}	
	}
	
	// Koneksi PDO
	static function pdo_connect ( $sql_details ) {
		try {
			$db = @new PDO(
				"mysql:host={$sql_details['host']};dbname={$sql_details['dbnm']}", $sql_details['user'], $sql_details['pass'], array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION )
			);
		}
		catch (PDOException $e) {
			self::fatal (
				"Terdapat kesalahan pada saat koneksi ke database.".
				"Kesalahan yang dilaporkan server adalah: ".$e->getMessage()
			);
		}
		return $db;
	}
	
	// Periksa apakah variabel $conn adalah array, untuk fungsi-fungsi yang membutuhkan verifikasi
	static function db_mysqli($conn) {
		if(is_array($conn)) {	
			return self::mysqli_connect($conn);
		}
		return $conn;
	}
	
	static function db_pdo($conn) {
		if(is_array($conn)) {	
			return self::pdo_connect($conn);
		}
		return $conn;
	}
	
	// Anti SQL Injection dari Lokomedia
	static function anti_injection($data) {
		$filter  = stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));
		return $filter;
	}
	
	static function safeInput($data) {
		$filter  = stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES)));
		return $filter;
	}
	
	// Fungsi Login
	//static function login($username, $password, $remember, $conn) {
	static function login($username, $password, $conn) {
		
		//$db = self::db_mysqli($conn);
		$db = self::db_pdo($conn);
		
		/*
		$userf = self::anti_injection($uName);
		$passf = self::anti_injection($uPass);
		
		$injek_userf = $db->real_escape_string($userf);
		$injek_passf = $db->real_escape_string($passf);
		*/
		
		$nm = trim($username);		
		$ps = trim($password);

		/*
		if(!empty($remember)) {
			setcookie("login_user", $nm, time()+(10 * 365 * 24 * 60 * 60));
			setcookie("login_pass", $ps, time()+(10 * 365 * 24 * 60 * 60));
		}
		*/
					
		if (ctype_alnum($nm) or ctype_alnum($ps)) {
		
			try {
				$sql = "
				SELECT 
				  um_users.sesid,
				  um_users.nip,
				  CONCAT(IF(ISNULL(um_pegawai.gelardp), '', CONCAT(um_pegawai.gelardp, '. ')), um_pegawai.nama, IF(ISNULL(um_pegawai.gelarbk), '', CONCAT(', ', um_pegawai.gelarbk))) AS nama,
				  um_users.tipe,
				  um_users.user,
				  um_users.pass,
				  um_users.level,
				  um_users.otoritas
				FROM
				  um_users
				  INNER JOIN um_pegawai ON (um_users.nip = um_pegawai.nip)
				WHERE
				  um_users.user = :user AND
				  um_users.pass = :pass AND
				  um_users.aktif = '1'
				LIMIT 1
				";
				$res = $db->prepare($sql);
				$res->bindParam(":user", $nm);
				$res->bindValue(":pass", $ps);
				$res->execute();
				$row = $res->fetch(PDO::FETCH_ASSOC);
				
				if(!empty($row) && $res->rowCount() == 1) {
					$nip 	= $row['nip'];
					$user 	= $row['user'];
					$level	= $row['level'];
					$otority= $row['otoritas'];
					$sesid 	= $row['sesid'];
					$nama 	= $row['nama'];
						/*
					$res1 = $db->prepare("SELECT nama FROM `um_profil` WHERE `nip`=:nip");
					$res1->execute(array("nip" => $row['nip']));
					$row1 = $res1->fetch(PDO::FETCH_ASSOC);
					*/
					
					// kedepannya $_SESSION[''] jadikan dulu MD5
					// contoh  : $_SESSION['62dfdb071d8266dfe4aeee1e52f3f43e'] = $r['hakf'];
					
					session_start();
					$_SESSION['nip']	= $nip;
					$_SESSION['user']	= $user;
					$_SESSION['level']	= $level;
					$_SESSION['otoritas']= $otority;
					$_SESSION['sesid']	= $sesid;
					$_SESSION['nama']	= $nama;
				
					$sesid_lama = session_id();
					session_regenerate_id();
					$sesid_baru = session_id();
					
					$res2 = $db->prepare("UPDATE `um_users` SET `sesid` = :sesid_baru WHERE user=:user");
					$res2->execute(
						array(
							"sesid_baru" => $sesid_baru,
							"user" 		 => $nm
						)
					);
					date_default_timezone_set("Asia/Kuala_Lumpur");
					// $ip	 = self::getUserIP();
					$ip	 = $_SERVER['REMOTE_ADDR'];
					$log = date("Y-m-d H-i-s");
					$agn = $_SERVER['HTTP_USER_AGENT'];
						$sql3 = "INSERT INTO `um_users_log` (
					  `nip`,`ip`,`log`,`agn`
					) VALUE (
					  :nip, :ip, :log, :agn
					)";
					$res3 = $db->prepare($sql3);
					$res3->execute(
						array(
							"nip" => $nip,
							"ip" => $ip,
							"log" => $log,
							"agn" => $agn
						)
					);					
					$response = array( 'status'=>'sukses', 'pesan'=>'<strong>Menuju Administrator...</strong>' );
				}
				else {
					$response = array( 'status'=>'gagal', 'pesan'=>'<strong>Nama dan Password tidak cocok!</strong>' );
				}
				echo json_encode($response);					
			} catch (PDOException $e) {
				echo "Error : ".$e->getMessage();
			}			
		} else {
			echo 'Jika Anda memang pintar, carilah pekerjaan professional!';
		}
		/*		
		if (!ctype_alnum($injek_userf) OR !ctype_alnum($injek_passf)) {
			echo 'Jika Anda memang pintar, carilah pekerjaan professional!';
		} else {
			$res	= $db->query("SELECT * FROM pengguna WHERE user='$userf' AND pass='$passf' AND aktif='ya'") or die($db->error()); // query
			$row	= $res->fetch_array(); // data
			$ada	= $res->num_rows; // jumlah data

			if ($ada > 0) {
				// untuk ajax response di themes/conquer/assets/script/login.js
				$response = array( 'status'=>'sukses', 'pesan'=>'<strong>Menuju Administrator...</strong>' );
				echo json_encode($response);
				
				// Tambahan untuk mengambil nama
				$res1 = $db->query("SELECT nama FROM profil WHERE nip = '".$row['nip']."'") or die($db->error());
				$row1 = $res1->fetch_array();
				
				session_start();				
				// kedepannya $_SESSION[''] jadikan dulu MD5
				// contoh  : $_SESSION['62dfdb071d8266dfe4aeee1e52f3f43e'] = $r['hakf'];
				$_SESSION['user']	= $row['user'];
				$_SESSION['nip']	= $row['nip'];
				$_SESSION['nama']	= $row1['nama'];
				$_SESSION['level']	= $row['level'];
				$_SESSION['sesid']	= $row['sesid'];

				// Lakukan penelitian pemakaian "sesidf" untuk manajemen data
				// Update sesidf
				$id_lama = session_id();
				session_regenerate_id();
				$id_baru = session_id();
				$db->query("UPDATE pengguna SET sesid = '$id_baru' WHERE user='$userf'");
			}
			else {
				$response = array( 'status'=>'gagal', 'pesan'=>'<strong>Nama dan Password tidak cocok!</strong>' );
				echo json_encode($response);
			}
			$res->free();
			$db->close();
		}
		*/
	}

	static function sudahlogin() {
		session_start();
		if(isset($_SESSION['sesid']) && !empty($_SESSION['sesid'])) {
			return true;
		}
	}
	
	static function redirect($url) {
		header("Location: ".$url);
	}
	
	// Fungsi logoout
	static function logout(){
		session_start();
		session_destroy();
		header('location: index.php');
	}
	
	// Quick MySQLi
	static function getRows($tabel, $kondisi = array(), $conn) {
		$db = self::db_mysqli($conn);
		$sql = "SELECT ";
		$sql.= array_key_exists("select",$kondisi) ? $kondisi['select'] : '*';
		$sql.= " FROM ".$tabel;
		if(array_key_exists("where", $kondisi)) {
			$sql.= " WHERE ";
			$i = 0;
			foreach($kondisi['where'] as $key => $val) {
				$p = ($i > 0) ? " AND " : "";
				$sql.= $p.$key." = '".$val."'";
				$i++;
			}
		}
		if(array_key_exists("order",$kondisi)) $sql.= " ORDER BY ".$kondisi['order'];
		if(array_key_exists("start",$kondisi) && array_key_exists("limit",$kondisi))
		{
			$sql.=" LIMIT ".$kondisi['start'].",".$kondisi['limit'];
		}
		elseif(!array_key_exists("start",$kondisi) && array_key_exists("limit",$kondisi))
		{
			$sql.=" LIMIT ".$kondisi['limit'];
		}
		
		$result = $db->query($sql);
		
		if(array_key_exists("return",$kondisi) && $kondisi['return'] != 'all')
		{
			switch($kondisi['return']) {
				case 'count':
					$data = $result->num_rows;
					break;
				case 'single':
					$data = $result->fetch_assoc();
					break;
				default:
					$data = '';
			}
		}
		else
		{
			if($result->num_rows > 0)
			{
				while($row = $result->fetch_assoc())
				{
					$data[] = $row;
				}
			}
		}
		return !empty($data) ? $data : false;
	}

	// DataTable Simple and Complex
	static function sql_exec ( $db, $bindings, $sql=null ) {
		if ( $sql === null ) {
			$sql = $bindings;
		}
		
		$stmt = $db->prepare( $sql );
		
		if ( is_array( $bindings ) ) {
			for ( $i=0, $ien=count($bindings); $i<$ien ; $i++ ) {
				$binding = $bindings[$i];
				$stmt->bindvalue( $binding['key'], $binding['val'], $binding['type'] );
			}
		}
		
		try {
			$stmt->execute();
		}
		
		catch (PDOException $e) {
			self::fatal("Terjadi kesalahan pada SQL: ".$e->getMessage() );
		}
		
		return $stmt->fetchAll( PDO::FETCH_BOTH );
	}
	
	static function data_output( $columns, $data ) {

		$out = array();

		for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
			$row = array();
		
			for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++) {
				$column = $columns[$j];
				
				// Apa ada format?
				if ( isset( $column['formatter'] ) ) {
					$row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['db'] ], $data[$i] );
				}
				else {
					$row[ $column['dt'] ] = $data[$i][ $columns[$j]['db'] ];
				}
			}
			$out[] = $row;
		}
		return $out;
	}
	
	static function fatal ( $msg ) {
		echo json_encode(
			array(
				"error"=>$msg
			)
		);
		exit(0);
	}
	
	static function bind ( &$a, $val, $type ) {
		$key = ':binding_'.count( $a );
		
		$a[] = array(
				'key'	=> $key,
				'val'	=> $val,
				'type'	=> $type
		);
		
		return $key;
	}
	
	static function pluck ( $a, $prop )	{
		$out = array();
		for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
			$out[] = $a[$i][$prop];
		}
		return $out;
	}
	
	static function _flatten ( $a, $join = ' AND ' ) {
		if ( ! $a ) {
			return '';			
		}
		else if ( $a && is_array($a) ) {
			return implode( $join, $a );
		}
		return $a;
	}
	
	static function limit ( $request, $columns ) {
		$limit = '';
		
		if ( isset($request['start']) && $request['length'] != -1 ) {
			$limit = "LIMIT ".intval($request['start']).", ".intval($request['length']);
		}
		return $limit;
	}

	static function order ( $request, $columns ) {
		$order = '';
		
		if ( isset($request['order']) && count($request['order']) ) {
			$orderBy = array();
			$dtColumns = self::pluck ( $columns, 'dt' );
			
			for ($i=0, $ien=count($request['order']) ; $i<$ien ; $i++ ) {
				// konversi kolom index menjadi properti kolom data
				$columnIdx = intval($request['order'][$i]['column']);
				$requestColumn = $request['columns'][$columnIdx];
				
				$columnIdx = array_search($requestColumn['data'], $dtColumns );
				$column = $columns[ $columnIdx ];
				
				if ( $requestColumn['orderable'] == 'true' ) {
					$dir = $request['order'][$i]['dir'] === 'asc' ? 'ASC' : 'DESC';
					$orderBy[] = '`'.$column['db'].'` '.$dir;
				}
			}
			
			if ( count($orderBy) ) {
				$order = 'ORDER BY '.implode(', ', $orderBy);
			}
		}
		return $order;
	}
	
	static function filter ( $request, $columns, &$bindings ) {
		$globalSearch = array();
		$columnSearch = array();
		$dtColumns = self::pluck( $columns, 'dt' );
		
		if ( isset($request['search']) && $request['search']['value'] != '' ) {
			$str = $request['search']['value'];
			
			for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
				$requestColumn = $request['columns'][$i];
				$columnIdx = array_search( $requestColumn['data'], $dtColumns );
				$column = $columns[ $columnIdx ];
				
				if ( $requestColumn['searchable'] == 'true' ) {
					$binding = self::bind( $bindings, '%'.$str.'%', PDO::PARAM_STR );
					$globalSearch[] = "`".$column['db']."` LIKE ".$binding;
				}			
			}
		}
		
		if (  isset( $request['columns'] ) ) {
			for ( $i=0, $ien=count($request['columns']) ; $i<$ien ; $i++ ) {
				$requestColumn = $request['columns'][$i];
				$columnIdx = array_search( $requestColumn['data'], $dtColumns );
				$column = $columns[ $columnIdx ];
				
				$str = $requestColumn['search']['value'];
				
				if ( $requestColumn['searchable'] == 'true' && $str != '' ) {
					$binding = self::bind( $binding, '%'.$str.'%', PDO::PARAM_STR );
					$globalSearch[] = "`".$column['db']."` LIKE ".$binding;
				}
			}
		}
		
		$where = '';
		
		if ( count( $globalSearch ) ) {
			$where = '('.implode(' OR ', $globalSearch).')';
		}
		
		if ( count( $columnSearch ) ) {
			$where  = $where === '' ?
				implode(' AND ', $columnSearch) :
				$where .' AND '. implode(' AND ', $columnSearch);
		}
		
		if ( $where !== '' ) {
			$where = 'WHERE '.$where;
		}
		
		return $where;
	}

	// Datatable Simple
	static function dataTableSimple( $request, $conn, $table, $primaryKey, $columns ) {

		$bindings = array();		
		$db = self::db_pdo( $conn );

		// Bikin Query SQL
		$limit = self::limit( $request, $columns );
		$order = self::order( $request, $columns );
		$where = self::filter ( $request, $columns, $bindings );
		
		// Query utama untuk mendapatkan data
		$data = self::sql_exec( $db, $bindings, "SELECT `".implode("`, `", self::pluck($columns, 'db'))."` FROM `$table` $where $order $limit");
		$resFilterLength = self::sql_exec( $db, $bindings, "SELECT COUNT(`{$primaryKey}`) FROM `$table` $where");
		$recordsFiltered = $resFilterLength[0][0];
		
		// Ukuran keseluruhan data
		$resTotalLength = self::sql_exec ( $db, "SELECT COUNT(`{$primaryKey}`) FROM `$table`" );
		$recordsTotal = $resTotalLength[0][0];
		
		// Keluaran
		return array(
			"draw" 				=> isset ( $request['draw'] ) ? intval( $request['draw'] ) : 0,
			"recordsTotal" 		=> intval( $recordsTotal ),
			"recordsFiltered" 	=> intval ( $recordsFiltered ),
			"data"				=> self::data_output( $columns, $data )
		);
	}
	
	// Datatable Complex
	static function dataTableComplex( $request, $conn, $table, $primaryKey, $columns, $whereResult=null, $whereAll=null ) {
		
		$bindings = array();		
		$db = self::db_pdo( $conn );
		$localWhereResult = array();
		$localWhereAll = array();
		$whereAllSql = '';
		
		// Bikin Query SQL
		$limit = self::limit( $request, $columns );
		$order = self::order( $request, $columns );
		$where = self::filter ( $request, $columns, $bindings );
		
		$whereResult = self::_flatten( $whereResult );
		$whereAll = self::_flatten( $whereAll );
		
		if ( $whereResult ) {
			$where = $where ? $where .' OR '.$whereResult : 'WHERE '.$whereResult;
		}
		
		if ( $whereAll ) {
			$where = $where ? $where .' OR '.$whereAll : 'WHERE '.$whereAll;
			$whereAllSql = 'WHERE '.$whereAll;
		}
		
		// Query utama untuk mendapatkan data
		$data = self::sql_exec( $db, $bindings, "SELECT `".implode("`, `", self::pluck($columns, 'db'))."` FROM `$table` $where $order $limit");
		$recordsFiltered = $resFilterLength[0][0];
		
		// Ukuran keseluruhan data
		$resTotalLength = self::sql_exec ( $db, "SELECT COUNT(`{primaryKey}`) FROM `$table`" );
		$recordsTotal = $resTotalLength[0][0];

		// Keluaran
		return array(
			"draw" 				=> isset ( $request['draw'] ) ? intval( $request['draw'] ) : 0,
			"recordsTotal" 		=> intval( $recordsTotal ),
			"recordsFiltered" 	=> intval ( $recordFiltered ),
			"data"				=> self::data_output( $columns, $data )
		);	
	}
	
	// Export ke CSV
	static function tabletocsv($table, $conn) {
		
		$db = self::db_mysqli($conn);
	
		$csv = $table . "-" . date('d-m-Y-his') . '.csv';
		
		$res = $db->query("SELECT * FROM {$table}");		
		$kolom = array();
		$data = array();

		if($res->num_rows > 0) {
			//while($row = $res->fetch_field()) {
			//	$kolom[] = $row->name;
			//}
			while($row = $res->fetch_assoc()) {
				$data[] = $row;
			}
		}
		
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: text/x-csv");
		header("Content-type: text/csv; charset=utf-8");
		header("Content-type: application/csv");
		header("Content-Disposition: attachment; filename=$csv");
		
		$output = fopen('php://output', 'w');
		
		//fputcsv($output, $kolom);
 
		if (count($data) > 0) {
			foreach ($data as $row) {
				fputcsv($output, $row);
			}
		}
		
		fclose($output);		
	}
	
	//* used like this
	//$allowed = array('title','url','body','rating','term','type');
	//$data = $db->filterArray($_POST,$allowed);
	//* $data now contains allowed fields only
	//* and can be used to create INSERT or UPDATE query dynamically
	static function filterArray( $input, $allowed ) {
		foreach(array_keys($input) as $key ) {
			if ( !in_array($key,$allowed) ){
				unset($input[$key]);
			}
		}
		return $input;
	}
	
	// Contoh = koleksi::tanggalindo($date, 'l, j F Y | H:i', 'WIB');
	static function tanggalindo ($timestamp = '', $date_format = 'j F Y', $suffix = '') {
		if (trim ($timestamp) == '') {
				$timestamp = time ();
		}
		elseif (!ctype_digit ($timestamp)) {
			$timestamp = strtotime ($timestamp);
		}
		# remove S (st,nd,rd,th) there are no such things in indonesia :p
		$date_format = preg_replace ("/S/", "", $date_format);
		$pattern = array (
			'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
			'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
			'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
			'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
			'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
			'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
			'/April/','/June/','/July/','/August/','/September/','/October/',
			'/November/','/December/',
		);
		$replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
			'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
			'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
			'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
			'Oktober','November','Desember',
		);
		$date = date ($date_format, $timestamp);
		$date = preg_replace ($pattern, $replace, $date);
		$date = "{$date} {$suffix}";
		return $date;
	}
	
	// Jika tanggal yyyy-mm-dd menjadi dd-mm-yyyy demikian sebaliknya
	static function tanggalSQL ($tanggal = '') {
		if (!empty($tanggal)) {
			$pisah	= explode('-',$tanggal);
			$gabung	= $pisah[2].'-'.$pisah[1].'-'.$pisah[0];
			return $gabung;
		} else {
			return '';
		}		
	}
	
	static function tglAkhirBulan($thn, $bln) {
		$bulan[1]='31';
		$bulan[2]='28';
		$bulan[3]='31';
		$bulan[4]='30';
		$bulan[5]='31';
		$bulan[6]='30';
		$bulan[7]='31';
		$bulan[8]='31';
		$bulan[9]='30';
		$bulan[10]='31';
		$bulan[11]='30';
		$bulan[12]='31';
		
		if ($thn%4==0){
			$bulan[2]='29';
		}
		return $bulan[$bln];
	}

	static function spasiNip ($nip = '') {
		if(!empty($nip)) {
			$pisah = substr($nip,0,8).' '.substr($nip,8,6).' '.substr($nip,14,1).' '.substr($nip,15,3);
			return $pisah;
		} else {
			return '';
		}
	}
	
	static function extractTglDariNip($nip = '') {
		if(!empty($nip)) {
			$tgl = substr($nip,0,4).'-'.substr($nip,4,2).'-'.substr($nip,6,2);
			return $tgl;
		} else {
			return '';
		}
	}
	
	static function extractJenkelDariNip($nip = '') {
		// 197802052003121003
		if(!empty($nip)) {
			$jenkel = substr($nip,14,1);
			return $jenkel;
		} else {
			return '';
		}
	}	
	
	static function pisahTelp ($hp = '') { // Dirubah
		if(!empty($hp)) {
			// $pisah = substr($hp,0,4).' '.substr($hp,4,4).' '.substr($hp,8,4).' '.substr($hp,12,4);
			$pisah = substr($hp,0,4).' '.substr($hp,4,4).' '.substr($hp,8,4).' '.substr($hp,12);
			return $pisah;
		} else {
			return '';
		}
	}
	
	static function hapusSpasi ($kalimat = '') {
		if(!empty($kalimat)) {
			return str_replace(' ','',$kalimat);
		} else {
			return '';
		}
	}
	
	static function EmptyPostVal ($val) {
		// if(!empty($val)) {
		if($val != '') {
			return trim($val);
		} else {
			return null;
		}
	}

	static function EmptyTglVal ($val) {
		if($val != '' && $val != '00-00-0000') {
			return self::tanggalSQL($val);
		} else {
			return null;
		}
	}	
}
?>