<?php
error_reporting(9);
require_once "lib/template.class.php";
require_once "lib/func.class.php";

// Periksa kondisi login
session_start();

if(!isset($_SESSION['nip']) && empty($_SESSION['nip'])) {
	header('location: login.php');
	exit();
}

$db = koleksi::db_pdo($conn);

//QUERY KELOMPOK TEMUAN
$datakel=array();
$sql = "SELECT 
            `was_tl_data`.`tl_kelompok` AS kode
            , `was_tl_kode`.`kode_deskripsi` AS ket
            , COUNT(`was_tl_data`.`tl_kelompok`) AS jumlah
        FROM
            `was_tl_data`
            INNER JOIN `was_tl_kode` 
                ON (`was_tl_data`.`tl_kelompok` = `was_tl_kode`.`kode_kel`)
        WHERE (`was_tl_kode`.`kode_sub_kel` =0
            AND `was_tl_kode`.`kode_jenis` =0)
        GROUP BY `was_tl_data`.`tl_kelompok`";
$res = $db->prepare($sql);
$res->execute();
while($row = $res->fetch(PDO::FETCH_BOTH)) {
    $nested= array();
    $nested []=$row['kode'];
    $nested []=$row['ket'];
    $nested []=$row['jumlah'];
    $datakel []=$nested;
}
$datarek=array();
$sql = "SELECT
    COUNT(`was_rekomendasi`.`rek_kode`) as jumlah
    , `was_tl_rekomendasi`.`rekomendasi_jenis` as kode
    , `was_tl_rekomendasi`.`rekomendasi_deskripsi` as ket
FROM
    `was_rekomendasi`
    INNER JOIN `was_tl_rekomendasi` 
        ON (`was_rekomendasi`.`rek_kode` = `was_tl_rekomendasi`.`rekomendasi_jenis`)
GROUP BY `was_tl_rekomendasi`.`rekomendasi_jenis`";
$res = $db->prepare($sql);
$res->execute();
while($row = $res->fetch(PDO::FETCH_BOTH)) {
    $nested= array();
    $nested []=$row['kode'];
    $nested []=$row['ket'];
    $nested []=$row['jumlah'];
    $datarek []=$nested;
}
$datatl=array();
$sql = "SELECT
            COUNT(`was_rekomendasi`.`rek_kode`) as jumlah
            , `was_tl_status`.`status_id` as kode
            , `was_tl_status`.`status_ket` as ket
        FROM
            `was_rekomendasi`
            INNER JOIN `was_tl_status` 
                ON (`was_rekomendasi`.`rek_status` = `was_tl_status`.`status_id`)
        GROUP BY `was_tl_status`.`status_id`";
$res = $db->prepare($sql);
$res->execute();
while($row = $res->fetch(PDO::FETCH_BOTH)) {
    $nested= array();
    $nested []=$row['kode'];
    $nested []=$row['ket'];
    $nested []=$row['jumlah'];
    $datatl []=$nested;
}
$isi ='
<div class="col-md-12">
	
		<div class="col-md-12 col-sm-12">
			<div class="portlet tasks-widget">

				<div class="portlet-title">
					<div class="caption">
						DASHBOARD PEMERIKSAAN
					</div>
				</div> <!-- portlet-title --> 
				<div class="portlet-body ">
					<div class="container">
                        <div class="col-md-12 col-sm-12"> <!-- div tabel kelompok -->
                        <table width="100%"  class="table table-striped">
                            <thead>
                            <tr class="heading">
                            <th colspan=4>Rekapitulasi Kelompok Temuan</th>
                            </tr>
                            <trclass="heading">
                              <th><center>No</center></th>
                              <th>Kelompok</th>
                              <th>Jumlah</th>
                              <th>Persentase</th>
                            </tr>
                            </thead>
                            <tbody>';
                        $sum = 0;
                        foreach($datakel as $value) {
                            $sum += $value[2];
                        }
                            foreach($datakel as $value) {
                                $persen=$value[2]/$sum*100;
                            $isi .='<tr>
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[1].'</td>
                                    <td>'.$value[2].'</td>
                                    <td>'.number_format($persen,2,",",".").'% </td>
                                    </tr>';
                            }
                        $isi .='
                            <tr>
                            <th colspan=2>Total</th>
                            <th>'.$sum.'</th>
                            <th>100 %</th>
                            </tr>
                            </tbody>
						</table>
                        </div>
                        <div class="col-md-12 col-sm-12"> <!-- div tabel kelompok -->
                        <table width="100%" class="table table-striped">
                            <thead>
                            <tr class="heading">
                            <th colspan=4>Rekapitulasi Kelompok Rekomendasi</th>
                            </tr>
                            <tr class="heading">
                              <th><center>No</center></th>
                              <th>Kelompok</th>
                              <th>Jumlah</th>
                              <th>Persentase</th>
                            </tr>
                            </thead>
                            <tbody>';
                        $sum = 0;
                        foreach($datarek as $value) {
                            $sum += $value[2];
                        }
                            foreach($datarek as $value) {
                                $persen=$value[2]/$sum*100;
                            $isi .='<tr>
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[1].'</td>
                                    <td>'.$value[2].'</td>
                                    <td>'.number_format($persen,2,",",".").'% </td>
                                    </tr>';
                            }
                        $isi .='
                            <tr>
                            <th colspan=2>Total</th>
                            <th>'.$sum.'</th>
                            <th>100 %</th>
                            </tr>
                            </tbody>
						</table>
                        </div>
                        <div class="col-md-12 col-sm-12"> <!-- div tabel kelompok -->
                        <table width="100%" class="table table-striped">
                            <thead>
                            <tr class="heading">
                                <th colspan=4>Rekapitulasi Tindak Lanjut Temuan</th>
                            </tr>
                            <tr class="heading">
                              <th><center>No</center></th>
                              <th>Kelompok</th>
                              <th>Jumlah</th>
                              <th>Persentase</th>
                            </tr>
                            </thead>
                            <tbody>';
                        $sum = 0;
                        foreach($datatl as $value) {
                            $sum += $value[2];
                        }
                            foreach($datatl as $value) {
                                $persen=$value[2]/$sum*100;
                            $isi .='<tr>
                                    <td>'.$value[0].'</td>
                                    <td>'.$value[1].'</td>
                                    <td>'.$value[2].'</td>
                                    <td>'.number_format($persen,2,",",".").'% </td>
                                    </tr>';
                            }
                        $isi .='
                            <tr>
                            <th colspan=2>Total</th>
                            <th>'.$sum.'</th>
                            <th>100 %</th>
                            </tr>
                            </tbody>
						</table>
                        </div>
                    </div> <!-- table-container -->
            </div> <!-- portlet-body -->

			</div>
		</div>
    
</div>

';

$tpl = new template;
$tpl->load('themes/conquer/index_full.html');
$tpl->set('theme',$tpl->theme);
$tpl->set('css-tambahan',$tpl->cssTambahan);
$tpl->set('page-title',$tpl->pageTitle);
$tpl->set('page-breadcrumb',$tpl->pageBreadcrumb);
$tpl->set('page-kontent',$isi);
$tpl->set('page-plugin-script',$tpl->pagePlugins);
$tpl->set('page-styles-script',$tpl->pageStyles);
$tpl->set('initA',$tpl->initA);
$tpl->set('initB',$tpl->initB);
$tpl->set('initC',$tpl->initC);
$tpl->publish();
?>