<?php
error_reporting(9);
require_once "lib/template.class.php";

// Periksa kondisi login
session_start();

if(isset($_SESSION['nip']) && $_SESSION['nip'] != '') {
	header('location: index.php');
	exit();
}

$tpl = new template;
$tpl->load('themes/conquer/login.html');
// $tpl->set('theme','themes/conquer/');
$tpl->set('theme',$tpl->theme);
$tpl->publish();
?>