<?php
/*
========================================
Periksa dulu pada saat pemanggilan link
 - Apakah terdapat http://
 - File yang dipanggil tidak ada
 - op=index
jika ada alihkan ke index.php
========================================*/

// Jika yang dicari tidak ada arahkan ke 404.php
if (preg_match("/http:\/\//", $_GET['ke']) or !file_exists("modul/".$_GET['ke'].".php") or $_GET['ke'] == 'index') {
	header ("location: 404.php");
} else {
	include "modul/".$_GET['ke'].".php";	
}

// jalankan perintah pemanggilan

?>