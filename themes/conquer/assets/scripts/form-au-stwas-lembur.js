var SuratTugasWas = function () {
	
	// Modal Form
	// var modul 		= "modul.php?ke=um_stwas";
	var modul 		= window.location.href;
	var judul 		= "Form Surat Tugas (Pengawasan)";
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit 	= modul+'&form=edit';
	var menuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	var importData	= '';
	var exportData	= modul+'&tabel=export';
	var emptyData	= modul+'&tabel=kosongkan';
	
	// TabelData
	var tabeldata	= modul+'&lihat=tabeldata';
	var refID 		= $('#tableAjax');
	var tabel 		= $("#tableAjax");
	
	var grid 		= new Datatable();	
	
	// Daftar Surat Tugas yang dikemas dalam Datatable
	var handleRecords = function () {	
		grid.init({
			src: tabel,
			dataTable: {
				"dom": 	"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
						"<'tableAjax-scrollable't>" +
						"<'row'<'col-md-1 col-sm-12'><'col-md-11 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": tabeldata },
				"columnDefs": [{ "targets": [0,3,4,5,6] }],  // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[1, "desc"]] // Kolom pertama yang di Sortir
			}
		});
		
		// == CUSTOM FILTERING ==
		grid.getTableWrapper().on('click', '.table-group-action-submit', function(e) {
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				grid.setAjaxParam($(this).attr("name"), $(this).val());
			});
			grid.getDataTable().ajax.reload();
		});
		
		grid.getTableWrapper().on("click", ".table-group-action-reset", function(e) {
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				$(this).val("");
			});
			grid.clearAjaxParams();
			grid.getDataTable().ajax.reload();
		});		
	}
	
	// Manajemen Surat Tugas
	var handleCRUD = function () {
		
		$('#menubaru').on('click', function(e) {
			var aksival = 'lembur_simpan';
			formModal.tambahData( menuBaru, aksival, judul, false );
			e.preventDefault();
		});

		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');
			var data = { uid: uid }; // post uid untuk SQL
			var aksival = 'lembur_update';
			formModal.editData( menuEdit, uid, aksival, judul, data ); // suksess
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var uid = $(this).attr("data-id");
			var data = { uid: uid }
			// formModal.hapusData( menuHapus, uid, true, refID );
			formModal.hapusData( menuHapus, data, true, refID );
			e.preventDefault();
		});
	}
	
	// Validasi dan Proses Simpan / Update
	var handleValidate1 = function() {
		// Validation
		var form1 = $('#form-modal #form-user');
		var form1input = $('#form-user input');
		var error1 = $('#form-modal .alert-danger', form1);
		
		form1.validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			ignore: "",
			rules: {
				nomor: { required: true },
				jenis: { required: true },
				kegiatan: { required: true },
				tahun: { required: true },
				tglsrt: { required: true },
				anggaran: { required: true },
				tglbrkt: { required: true }
			},
			messages: {
				nomor: { required: "Nomor Surat Tugas contoh: 123" },
				jenis: { required: "Jenis Pemeriksaan wajib di isi" },
				kegiatan: { required: "Melakukan kegiatan apa ?" },
				tahun: { required: "Tahun Anggaran ?" },
				tglsrt: { required: "Tanggal Surat Tugas wajib di isi" },
				anggaran: { required: "Menggunakan anggaran apa ?" },
				tglbrkt: { required: "Tanggal berangkat dan kembali wajib di isi" }
			},
			invalidHandler: function (event, validator) {
				//$('.alert-danger', $('#form-user')).show();
				error1.show();
				App.scrollTo(error1, -200);
			},
			highlight: function (element) {
				$(element)
					.closest('.form-group').addClass('has-error')
			},
			success: function (label) {
				// DON'T CHANGE THIS CODE OR IT WILL RAISE AN ERROR
				label
					.closest(".form-group")
					.removeClass("has-error");
				label
					.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {				
				var data = new FormData($('#form-user')[0]);
				
				var theModal	= $('#form-modal');
				var theErr		= theModal.find('.alert-danger');
					theErr.hide();				
				var theForm		= $('#form-user');
				var theBody		= theForm.find('.form-body');
				var theSubmit	= theForm.find(':submit');
				// formModal.submitData( modul, data, true, refID ); // true = with dataTable
				theSubmit.html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
				$.ajax({
					url: modul,
					type: 'POST',
					timeout: 20000,
					data: data,
					dataType: 'json',
					processData: false,
					contentType: false,
					cache: false
				}).done(function(response, textStatus, jqXHR) {
					if (response.status == 'sukses') { // Sukses
						theSubmit.removeClass('disabled').text('Selesai...');
						theModal.modal('hide'); // Tutup Modal Form Simpan/Perbaharui					
						tabel.DataTable().ajax.reload();
						// Infokan Kondisi
						swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });					
						
						var idst = $('#idst').val();
						var jam = $('#jmljam').val();
						
						$.ajax({
							url: 'modul.php?ke=au_stwas.php&form=updatejam',
							type: 'POST',
							timeout: 20000,
							data: { idst: idst, jam: jam },
							processData: false,
							contentType: false							
						}).fail(function(jqXHR, textStatus, errorThrown) {
							alert(jqXHR.responseText);
						})
					}
					if (response.status == 'gagal') { // Gagal
						theForm.find('.alert')
							.removeClass('alert-success alert-danger alert-info')
							.addClass('alert-warning')
							.html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>')
							.show();
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.responseText);
				})
			}
		});
		
		$(document).on('keypress', '#form-user input', function(e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleModalShow = function () {	// Pada saat Form ditampilkan
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			$("#spinjml").spinner({ value: 20 });
			$("#tglskl").inputmask({ "mask": "99-99-9999" });
		});
		
		$('#form-modal').on('shown.bs.modal',function() {	
			$('#form-modal .form-body').scrollTop(0);
		});
		
		// Kode Kegiatan Pegawasan
		$("#modal-tblpoin").on("shown.bs.modal", function() {		
			$('#modal-tblpoin .modal-body').scrollTop(0);
		});

		$('#daftartim-modal').on('shown.bs.modal',function() {
			$('#daftartim-modal .form-body').scrollTop(0);
		});
		
		$('#tim-modal').on('shown.bs.modal',function() {			
			$('#tim-modal .form-body').scrollTop(0);
		});		
	}
		
	return {
		init: function () {
			handleRecords();
			handleCRUD();
			handleValidate1();
			handleModalShow();
		}
	};
}();