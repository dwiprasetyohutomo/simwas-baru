var pegawai = function () {
	
	var modul = 'modul.php?ke=pegawai';	
	var pageTitle = 'Data Pegawai';
	
	var handleRecords = function () {
		var grid = new Datatable();		
		var tabel = $("#tableAjax");
		grid.init({
			src: tabel,
			dataTable: {
				"pageLength": 10,
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'table-group-actions pull-right'>>r>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>",
				"ajax": { "url": modul+"&pegawai=tabeldata" },
				"columnDefs": [{"targets": [0,1,4,5,6,7]}], // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[3, "asc"]],  // Kolom yang di Sortir
			}
		});

		var $inputcari 	= $('div.dataTables_filter input');
		var $btncari 	= $('<span class="input-group-btn"><button class="btn btn-warning" type="button" style="margin-top:-1px"><i class="fa fa-search"></i></button></span>');
		$inputcari.contents().unwrap();
		$inputcari.attr('placeholder', 'Cari nip/nama/kelahiran...')
			//.removeClass('input-small')
			.wrap('<div class="input-group input-group-sm"></div>')
			.after($btncari);
		$inputcari.off();
		$inputcari.on("keypress", function (e) {
			if (e.which == 13) {
				tabel.DataTable().search($inputcari.val()).draw();
			}
		});
		$btncari.on("click", function() {
			tabel.DataTable().search($inputcari.val()).draw();
		});
	}
	
	var handleBaru = function() {

		$(document).on('click', '#baru', function(e) {
			
			$('#form-user')[0].reset();

			$('.modal-title').html('Form Tambah ' + pageTitle);
			$('.modal-loading').show();
			//$('.modal-dialog').addClass('modal-wide');
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			$('.form-body').html('');
		
			$('#uid').val('');
			$('#aksi').val('pegawai_simpan');
			$('#submit').val('pegawai_simpan').text('Simpan');
		
			$.ajax({
				//url: modul + '&isian=baru',
				url: modul+'&pegawai=baru',
				type: 'POST',
				timeout: 20000,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();
		});		
	}
	
	var handleEdit = function () {

		$(document).on('click', '#edit', function(e) {			
			
			$('#form-user')[0].reset();
			
			$('.modal-title').html('<i class="fa fa-edit"></i> ' + pageTitle);
			$('.modal-loading').show();
			//$('.modal-dialog').addClass('modal-wide');
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			//$(".form-body").wrap(\'<div class="scroller" style="height:70vh" data-always-visible="1" data-rail-visible="1" data-handle-color="#555" data-handle-size="10px"></div>\');
			$('.form-body').html('');
		
			var uid = $(this).attr('data-id');
			$('#uid').val(uid);
			$('#aksi').val('pegawai_update');
			$('#submit').val('pegawai_update').text('Update');

			$.ajax({
				//url: modul + '&isian=baru',
				url: modul+'&pegawai=edit',
				type: 'POST',
				timeout: 20000,
				data: 'uid='+uid,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeOut digunakan sebagai percobaan/testing saat online
				// setTimeout(function(){
				console.log(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
				// }, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});		
			e.preventDefault();
		});
	}
	
	var handleValidasi = function () {

		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				nip: 	{ required: true },
				nama: { required: true }
			},
			messages: {
				nip:	{ required: "Nomor harus di isi" },
				nama:	{ required: "Nama harus di isi" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				/*
				// With Serialize Work!
				var data = $('#form-user').serialize();
				$.ajax({
					url: modul,
					type: 'POST',
					data: data,
					dataType: 'JSON',
					success: function(data) {
						$('#form-modal').modal('hide');
						if(data.status == 'sukses') {
							alert(data.pesan);
						}
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert('Gagal');
					}
				});
				*/
				var data = new FormData($('#form-user')[0]);
				$('#submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
				$.ajax({
					url: modul,
					type: 'POST',
					data: data,
					dataType: 'json',
					contentType: false,
					processData: false,
					cache: false
				}).done(function(response, textStatus, jqXHR) {
					if (response.status == 'sukses') {
						$('#form-modal').modal('hide');
						refreshData();
						swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
					} else {
						$('#submit').text('Coba Lagi');
						$('.alert')
							.removeClass('alert-success alert-danger alert-info')
							.addClass('alert-warning')
							.html('<span>'+response.pesan+'</span>').show();
						$('.form-body').scrollTop(0);
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					$('#submit').hide();
					alert(jqXHR.responseText);
				});
			}
		});
		
		$('#form-user input').keypress(function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleHapus = function () {
		$(document).on('click', '#hapus', function(e) {
			var uid = $(this).attr('data-id');
			swalDelete(modul+'&pegawai=hapus', uid);
			e.preventDefault();
		});		
	}

	var handleModalShow = function () {
		
		$(document).on("ajaxComplete", function(e){
		//$(document).ajaxComplete(function( event, xhr, settings ){
			//alert(xhr.responseText);
			e.preventDefault();
			//event.preventDefault();
			
			//$("#tahun").inputmask({ "mask": "9999" });
			//$("#tgl_brkt").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			//$("#tgl_kmbl").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			$("#lahirtgl").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			$("#niplama").inputmask({ "mask": "999 999 999" });
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
			$('#nip').focus();
		});
	}
	
	var handleExport = function () {
		$(document).on('click','#export',function(e) {
			swal({
				title: "Export tabel ke format CSV?", text:"", type:"question", showCancelButton:true, confirmButtonText:"Ya", cancelButtonText:"Batal", cancelButtonColor:"#d33"
			}).then(function(result) {
				if(result.value) window.open("'.$modul.'&stgswas=export","_blank");
			});
			e.preventDefault();		
		});
	}
	
	var handleInspekturInfo = function () {
		$(document).on("change", "#insp", function () {
			var inspnama = $(this).find("option:selected").text();
			var inspnip = this.value;
			$("#insp_nama").val(inspnama)
			$("#insp_nip").val(inspnip);
			/*
			App.blockUI({ message: 'Mohon bersabar...', target: '.form-body', overlayColor: 'none', cenrerY: true, boxed: true });
			$.ajax({
				url: modul+'&personil=info',
				type: 'POST',
				timeout: 20000,
				dataType: 'json',
				//contentType: 'application/x-www-form-urlencoded',
				async: false,
				data: { nip : nip },
				success: function(data) {
					// Hilangkan setTimeout saat sudah online
					//setTimeout(function() {					
						App.unblockUI('.form-body'); // Tutup loading
						userData = data;
						$("#nama").val(userData.nama);
						$("#nip").val(userData.nip);
					//}, 500);
				}
			});
			*/
		});
	}

	var handleAnggaran = function () {
		$(document).on("change", "#anggaran", function() {
			var aggr = this.value;
			if ($(this).val() == "Lain") {
				$("#anggaran2").attr("style","margin-top:15px").val("").show().focus();
			} else {
				$("#anggaran2").hide();
			}			
		});
	}
	
	return {
		init: function () {
			//handlePickers();
			handleRecords();
			handleBaru();
			handleEdit();
			handleValidasi();
			handleHapus();
			handleModalShow();
			//handleInspekturInfo();
			//handleAnggaran();
		}
	};
}();
