var SuratTugasWas = function () {
	
	// Modal Form
	// var modul 		= "modul.php?ke=um_stwas";
	var modul 		= window.location.href;
	var judul 		= "Form Surat Tugas (Pengawasan)";
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit 	= modul+'&form=edit';
	var menuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	var importData	= '';
	var exportData	= modul+'&tabel=export';
	var emptyData	= modul+'&tabel=kosongkan';
	
	// TabelData
	var tabeldata	= modul+'&lihat=tabeldata';
	var refID 		= $('#tableAjax');
	var tabel = $("#tableAjax");
	
	var grid = new Datatable();
	
	// SURAT TUGAS
	
	// Daftar Surat Tugas yang dikemas dalam Datatable
	var handleRecords = function () {	
		grid.init({
			src: tabel,
			dataTable: {
				"dom": 	"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
						"<'tableAjax-scrollable't>" +
						"<'row'<'col-md-1 col-sm-12'><'col-md-11 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": tabeldata },
				"columnDefs": [{ "targets": [0,2,3,4,6] }],  // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[5, "desc"]] // Kolom pertama yang di Sortir
			}
		});
		
		// == CUSTOM FILTERING ==
		grid.getTableWrapper().on('click', '.table-group-action-submit', function(e) {
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				grid.setAjaxParam($(this).attr("name"), $(this).val());
			});
			grid.getDataTable().ajax.reload();
		});
		
		grid.getTableWrapper().on("click", ".table-group-action-reset", function(e) {
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				$(this).val("");
			});
			grid.clearAjaxParams();
			grid.getDataTable().ajax.reload();
		});		
	}
	
	// Manajemen Surat Tugas
	var handleCRUD = function () {
		
		$('#menubaru').on('click', function(e) {
			var aksival = 'stwas_simpan';
			formModal.tambahData( menuBaru, aksival, judul, false );
			e.preventDefault();
		});

		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');
			var data = { uid: uid }; // post uid untuk SQL
			var aksival = 'stwas_update';
			formModal.editData( menuEdit, uid, aksival, judul, data ); // suksess
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var uid = $(this).attr("data-id");
			var data = { uid: uid }
			// formModal.hapusData( menuHapus, uid, true, refID );
			formModal.hapusData( menuHapus, data, true, refID );
			e.preventDefault();
		});
	}
	
	// Validasi dan Proses Simpan / Update
	var handleValidate1 = function() {
		// Validation
		var form1 = $('#form-modal #form-user');
		var form1input = $('#form-user input');
		var error1 = $('#form-modal .alert-danger', form1);
		
		form1.validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			ignore: "",
			rules: {
				nomor: { required: true },
				jenis: { required: true },
				kegiatan: { required: true },
				tahun: { required: true },
				tglsrt: { required: true },
				anggaran: { required: true },
				tglbrkt: { required: true }
			},
			messages: {
				nomor: { required: "Nomor Surat Tugas contoh: 123" },
				jenis: { required: "Jenis Pemeriksaan wajib di isi" },
				kegiatan: { required: "Melakukan kegiatan apa ?" },
				tahun: { required: "Tahun Anggaran ?" },
				tglsrt: { required: "Tanggal Surat Tugas wajib di isi" },
				anggaran: { required: "Menggunakan anggaran apa ?" },
				tglbrkt: { required: "Tanggal berangkat dan kembali wajib di isi" }
			},
			invalidHandler: function (event, validator) {
				//$('.alert-danger', $('#form-user')).show();
				error1.show();
				App.scrollTo(error1, -200);
			},
			highlight: function (element) {
				$(element)
					.closest('.form-group').addClass('has-error')
			},
			success: function (label) {
				// DON'T CHANGE THIS CODE OR IT WILL RAISE AN ERROR
				label
					.closest(".form-group")
					.removeClass("has-error");
				label
					.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {				
				var data = new FormData($('#form-user')[0]);
				formModal.submitData( modul, data, true, refID ); // true = with dataTable
				// return false;
			}
		});
		
		$(document).on('keypress', '#form-user input', function(e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	// Opsi-opsi tambahan pada Table
	var handleTableMenu = function () {
		
		$('#menurefresh').on('click',function(e) {
			e.preventDefault();
			var tabel = $('#tableAjax');
			dataTableReload(tabel);
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&form=importform', 'Import data kepangkatan (.csv)', 'pangkat_import');
			e.preventDefault();
		});
		
		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&tabel=importupload',
				type: method,
				data: data,
				mimeType: 'multipart/form-data',
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(sourceUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = exportData;
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';
			var tabel = $("#tableAjax");
			
			modalForm.kosongkanData(emptyData, judul1, judul2, 'Hapus dah', 'Tidak Jadi', '', tabel, true);
		});		
	}
	
	// Laporan
	var handleModalReport = function () {
		$(document).on('click', '#view', function(e) {
			e.preventDefault();
			var uid = $(this).attr('data-id');
			reportModal.viewReport('modul.php?ke=um_stwas_rpt&uid='+uid, 'Hallo', true, true);			
		});
	}
	
	// Fungsi-fungsi mempermudah hidup
	var anggaran = {
		jenis: function (agr) {
			if(agr == "lain") {
				$('#anggaran2').val('').show().focus();
			}
			else {
				$('#anggaran2').hide();
			}
		}
	}

	var nip2nama = {
		inspnip: function() {
			var inspnama = $('#inspnip').find("option:selected").text();
			$("#inspnama").val(inspnama);	
		},
		timnip: function() {
			var timnama = $('#nip').find("option:selected").text();
			$("#nama").val(timnama);
		}
	}
	
	// Opsi-opsi pada Form
	var handleAnggaran = function () {
		
		// Anggaran Change = [ Rutin | Lain ]
		$(document).on("change", "#anggaran", function() {
			var agr = $(this).val();
			anggaran.jenis(agr);
		});
	}
	
	var handleInspektur = function () {	 // Edit Mode
		// Inspektur Change = [ Text | Value ]		
		$(document).on("change", "#inspnip", function () {
			nip2nama.inspnip();
		});
	}

	
	// ANGGOTA TIM
	
	// Fungsi-fungsi mempermudah hidup
	var timMenu = {
		
		daftarTim: function() {
			// Dapatkan data dan set ke variabel
			var idst 	= $('#timbaru').attr('data-idst');
			var thn 	= $('#timbaru').attr('data-thn');
			var hari	= $('#timbaru').attr('data-hari');
			
			// $('#tableTim tbody').html('<div align="center" class="loader2"><img src="img/loading/loading-orange.gif"><br><span>Loading...</span></div>');
			$('#tableTim tbody').html('<div align="center"><img src="img/loading/loading-barsm.gif"></div>'); // Loading
			// Kirim variabel tadi ke url dibawah
			$.ajax({
				url: 'modul.php?ke=um_stwas_tim&lihat=personiltim',
				type: 'POST',
				timeout: 20000,
				data: { idst: idst, thn: thn, hari: hari },
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				$('#tableTim tbody').html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			});
		},
		
		// Mengetahui Jabatan seseorang
		// Jika jabatannya Auditor maka tentukan jenis tabel yang dipakai untuk ahli / terampil
		// ## Sementara BELUM KEPAKE ##
		auditorLevel: function( jab ) {
			var tabel;
			var arr = [
				"Auditor Utama",
				"Auditor Madya",
				"Auditor Muda",
				"Auditor Pertama",
				"Auditor Penyelia",
				"Auditor Pelaksana Lanjutan",
				"Auditor Pelaksana"
			];
			
			// mencari jenis jabatan di arr jika ada maka...
			if (jQuery.inArray(jab, arr) != '-1') { // <<-- Kayanya nda perlu ini, karena jab sudah disebut di function(jab)
				if((jab == 'Auditor Utama') || (jab == 'Auditor Madya') || (jab == 'Auditor Muda') || (jab == 'Auditor Pertama')) {
					tabel = 'ahli';
				}
				else if((jab == 'Auditor Penyelia') || (jab == 'Auditor Pelaksana Lanjutan') || (jab == 'Auditor Pelaksana')) {
					tabel = 'terampil';
				}
				$.ajax({
					url: 'modul.php?ke=um_stwas_tim&lihat=poinwas',
					type: 'POST',
					data: { tabel: tabel, jab: jab },
					dataType: 'html',
					beforeSend: function() {
						$('#tblKegiatan tbody').html('');
						$('#tblpoin').attr('data-toggle','modal');
					},
					success: function(response) {
						$('#tblKegiatan tbody').html(response);
					},
					error: function(xhr, ajaxOption, thrownError) { alert(thrownError + '<br>' + xhr.responseText) }
				});
			}			
		},
		
		// Isian Form Otomatis untuk JABATAN berdasarkan NIP
		isiotomatis: function( timnip ) {
			
			// $('#sertifikasi').val('');
			// $('#kdkeg').val('');
			// $('#kdinfo').val('');
			// $('#jam').val('')
			// $('#satuankp').val('');
			// $('#kreditpoin').val('');
			
			// Mendapatkan Golongan dan Jabatan Akhir berdasarkan NIP
			$.ajax({
				url: 'modul.php?ke=um_stwas_tim&lihat=isiotomatis',
				type: 'POST',
				dataType: 'json',
				data: { nip: timnip }, // Posting nip ke url
				beforeSend: function(e) {
					if(e && e.overrideMimeType) { e.overrideMimeType("application/json;charset=UTF-8"); }
				},
				success: function(response) {
					console.log();
					
					var tgol = response.gol;
					
					$('#gol').val(tgol); // Otomatis input golongan akhir tim terpilih
					
					/*
					var arr = [
						"Pengawas Pemerintah Madya","Pengawas Pemerintah Muda","Pengawas Pemerintah Pertama",
						"Auditor Utama","Auditor Madya","Auditor Muda","Auditor Pertama",
						"Auditor Penyelia","Auditor Pelaksana Lanjutan","Auditor Pelaksana"
					];
					*/
					var arr = [
						4,5,6,7, 	// "Pengawas Pemerintah Madya", "Pengawas Pemerintah Muda", "Pengawas Pemerintah Pertama",
						8,9,10,11,	// "Auditor Utama", "Auditor Madya", "Auditor Muda", "Auditor Pertama",
						12,13,14	// "Auditor Penyelia", "Auditor Pelaksana Lanjutan", "Auditor Pelaksana"
					];
					
					// periksa jika terdapat jabatan array diatas
					if (jQuery.inArray(tjab, arr) != '-1') {
						// $('#sertifikasi').removeClass('hidden'); // Tampikan Kredit Poin Auditor
						
						// Pengecualian sertifikasi
						// Periksa Apakah P2UPD ? tidak tampil kredit poin : tampil kredit poin
						var jam = $('#lama').val();
						if ((response.jab == 4) || (response.jab == 5) || (response.jab == 6)) {
							$('#kpoin').addClass('hidden');
						} else {
							$('#kpoin').removeClass('hidden');
							var jam = $('#lama').val() * 6.5;
							$('#jam').val(jam);
						}
						
						if((response.jab == 8) || (response.jab == 9) || (response.jab == 10) || (response.jab == 11)) {
							tabel = 'ahli';
						}
						else if((response.jab == 12) || (response.jab == 13) || (response.jab == 14)) {
							tabel = 'terampil';
						}									
						var str = response.jab;
						
						// Ganti spasi " " dengan underscore "_" kemudian rubah ke huruf kecil semua
						var jafung = str.replace(/ /g,'_').toLowerCase();
						$.ajax({
							url: 'modul.php?ke=um_stwas_tim&lihat=poinwas',
							type: 'POST',
							data: { tabel: tabel, jafung: jafung },
							dataType: 'html',
							beforeSend: function() {
								$('#tblKegiatan tbody').html('');
								$('#tblpoin').attr('data-toggle','modal');
							},
							success: function(response) {
								$('#tblKegiatan tbody').html(response);
							},
							error: function(xhr, ajaxOption, thrownError) { alert(thrownError + '<br>' + xhr.responseText) }
						});
						
					} else {  // Jika bukan jabatan Auditor dan P2UPD
						// Form disediakan
						$('#sertifikasi').addClass('hidden');
						$('#kpoin').addClass('hidden');
						// Isian Form Otomatis di Kosongkan
						$('#kdkeg').val();
						$('#pp_jmljam').val('');
						$('#pp_sak').val('');
						$('#pp_jak').val('');
					}					
				},
				error: function(xhr, ajaxOption, thrownError) { alert(xhr.responseText) }
			});
		}
	}
	
	// Daftar Tim Pemeriksa
	var handleTimRecords = function () {
		// Bikin menu tim seperti di http://localhost/template/conquer2/table_managed.htm

		$(document).on('click', '#personil', function(e) {
			e.preventDefault();
			
			// Ambil data dari tombol personil surat tugas
			var id		= $(this).attr('data-id');
			var idst	= $(this).attr('data-idst');
			var thn		= $(this).attr('data-thn');
			var hari	= $(this).attr('data-hari');
			
			// Masukkan variabel data ke tombol tambah timpersonil
			$('#timbaru').attr('data-id', id);
			$('#timbaru').attr('data-idst', idst);
			$('#timbaru').attr('data-thn', thn);
			$('#timbaru').attr('data-hari', hari);
			
			$('.modal-loading').show(); // Munculkan Loading
			$('#daftartim-modal .form-body').html(''); // Kosongkan isian form
			
			$.ajax({
				type: 'POST',
				timeout: 20000,
				url: 'modul.php?ke=um_stwas_tim',
				data: { id: id, idst: idst, thn: thn, hari: hari }, // Kirim ke URL untuk info Surat Tugas
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeout(function() {
				$('.modal-loading').hide();
				$('#daftartim-modal .modal-loading').hide();
				$('#daftartim-modal .form-body').html(response);
				// }, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			});

		});
		
		// Pilih Nama pada Form Input Tim Pemeriksa
		$(document).on("change", "#nip", function () {
			var nip  = $(this).val();
			var nama = $(this).find('option:selected').text();
			$('#nama').val(nama);
			timMenu.isiotomatis(nip);
		});

		$(document).on("change", "#jab", function () {
			// var jab = $(this).val();
		});
	}
	
	// Manajemen Surat Tugas
	var handleTimCRUD = function() {
		
		$(document).on('click', '#timbaru', function(e) {
			e.preventDefault();
			
			// Ambil attribut dari tombol timbaru menjadi variabel
			var idst = $(this).attr('data-idst');
			var thn  = $(this).attr('data-thn');
			var hari = $(this).attr('data-hari');
			var jam  = (hari * 6.5);
			
			var theForm = $('#tim-modal-form');
			theForm.find('#uid').val('');
			theForm.find('#aksi').val('tim_simpan');
			theForm.find(':submit').text('Simpan');
						
			var theSpin	= $('#tim-modal-form .modal-loading');
			var theBody = $('#tim-modal-form .form-body');
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			
			$.ajax({
				url: 'modul.php?ke=um_stwas_tim&form=baru',
				type: 'POST',
				timeout: 20000,
				data: { idst: idst, thn: thn, hari: hari, jam: jam }, // Posting attribut ke url diatas
				dataType: 'html',		
			}).done(function(response, textStatus, jqXHR) {
				theSpin.hide();
				theBody.html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			});
			
			/*
			var stthn 	= $(this).attr('data-stthn');
			var stlama 	= $(this).attr('data-stlama');
			var data 	= { stid: stid, stthn: stthn, stlama: stlama };
			
			var url		= 'modul.php?ke=um_stwas_tim&form=baru';		
			var modalID = $('#tim-modal');
			var theForm = $('#tim-modal-form');
			
			simpleModal.tambahData( url, modalID, theForm, 'tim_simpan', data );
			*/
		});
		
		$(document).on('click', '#timedit', function(e) {
			e.preventDefault();
			
			var id		= $(this).attr('data-id');
			var idst 	= $(this).attr('data-idst');
			var thn  	= $(this).attr('data-thn');
			var hari	= $(this).attr('data-hari');
			var jam  	= hari * 6.5;
			
			// Setting Element di FORM
			var theForm = $('#tim-modal-form');
			theForm.find('#uid').val(id);
			theForm.find('#aksi').val('tim_update');
			theForm.find(':submit').text('Perbaharui');			
			
			// Set Scroll Visible
			$('#tim-modal-form .form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			
			$.ajax({
				url: 'modul.php?ke=um_stwas_tim&form=edit',
				type: 'POST',
				timeout: 20000,
				// data: { timid: timid }, // memposting tim-ID untuk SQL
				data: { id: id, idst: idst, thn: thn, hari: hari, jam: jam }, // Posting attribut ke url diatas
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				$('#tim-modal-form .modal-loading').hide();
				$('#tim-modal-form .form-body').html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			});
			e.preventDefault();
		});
		
		$(document).on('click', '#timhapus', function(e) { // SUKSES
			e.preventDefault();
			var timid  = $(this).attr('data-id');
			swal({
				title: "Yakin ingin menghapus data ini?",
				html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
				type: "question",
				showCancelButton: true,
				confirmButtonColor: "#AA0000",
				confirmButtonText: "Hapus",
				cancelButtonColor: "#F0AD4E",
				cancelButtonText: "Jangan"
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: 'modul.php?ke=um_stwas_tim&form=hapus',
						type: 'POST',
						dataType: "json",
						data: { timid: timid },
						success: function(response) {
							if(response.status == "sukses") {
								const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
								toast({ type: 'success', title: 'Data Anggota Tim telah terhapus' })
								timMenu.daftarTim();
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(xhr.responseText)
						}
					});
				}
			})
		});
		
		$(document).on('submit', '#tim-modal-form', function(e) { // SUKSES
			var data = new FormData($('#tim-modal-form')[0]);			
			$('#tim-modal-form').find(':submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			$.ajax({
				url: 'modul.php?ke=um_stwas_tim',
				type: 'POST',
				timeout: 20000,
				data: data,
				dataType: 'json',
				processData: false,
				contentType: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				if (response.status == 'sukses') { // Sukses
					$('#tim-modal').modal('hide'); // Tutup Modal Form Simpan/Perbaharui					
					timMenu.daftarTim(); // Load Daftar Tim					
					// Infokan Kondisi
					swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
				}
				if (response.status == 'gagal') { // Gagal
					$('.alert')
						.removeClass('alert-success alert-danger alert-info')
						.addClass('alert-warning')
						.html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>')
						.show();
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(xhr.responseText);
			});
			e.preventDefault();
		});
		
		$(document).on('keypress', '#tim-modal-form input', function(e) {
			if (e.which == 13) {
				$('#tim-modal-form').submit();
			}
		});
	}
	
	// Modal Tabel Kredit Poin dipilih
	var handleAuditorPoin = function () {
		$(document).on('click', '#tblKegiatan tr.pilih', function(e) {
			e.preventDefault();

			var kode = $(this).attr('data-kode');
			var poin = $(this).attr('data-poin');
			var keg	 = $(this).attr('data-kegiatan');
			var jam	 = $('#jam').val();
			
			$('#kdkeg').val(kode);
			$('#satuankp').val(poin);
			$('#kdkeginfo').text(keg);
			
			// Hitungan Poin
			var kp	= poin * jam;
			$('#kreditpoin').val(kp.toFixed(3));
			
			// Tutup Form KreditPoin
			$("#modal-tblpoin").modal('hide');
		});
	}

	// Pada saat Form ditampilkan
	var handleModalShow = function () {		
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			
			// Surat Tugas			
			$("#jenis").select2({ allowClear: true });
			
			$("#tahun").inputmask({ "mask": "9999" });
			$("#spinlama").spinner({ value: 20 });
			
			// $("#anggaran").select2({ allowClear: true });
			$("#tglbrkt").inputmask({ "mask": "99-99-9999" });
			$("#tglkmbl").inputmask({ "mask": "99-99-9999" });
			$("#tglsrt").inputmask({ "mask": "99-99-9999" });
			
			$("#inspnip").select2({ allowClear: true });
			$("#inspgol").select2({ allowClear: true });
			
			$("#anggaran").select2({ allowClear: true });
			anggaran.jenis($('#anggaran').val());
			nip2nama.inspnip();
			nip2nama.timnip();
						
			// Tim Pemeriksa
			$("#spinpos").spinner({ value: 1 });
			$("#nip").select2({ allowClear: true });
			$("#gol").select2({ allowClear: true });
			$("#pp_peran").select2({ allowClear: true });
			$("#jab").select2({ allowClear: true });
			$("#sert").select2({ allowClear: true });
		});
		
		$('#form-modal').on('shown.bs.modal',function() {	
			$('#form-modal .form-body').scrollTop(0);
		});
		
		// Kode Kegiatan Pegawasan
		$("#modal-tblpoin").on("shown.bs.modal", function() {		
			$('#modal-tblpoin .modal-body').scrollTop(0);
		});

		$('#daftartim-modal').on('shown.bs.modal',function() {
			timMenu.daftarTim();
			$('#daftartim-modal .form-body').scrollTop(0);
		});
		
		$('#tim-modal').on('shown.bs.modal',function() {			
			// timMenu.auditorLevel($('#timjab').val());
			// timMenu.isiotomatis($('#nip').val());
			$('#tim-modal .form-body').scrollTop(0);
		});		
	}
		
	return {
		init: function () {
			handleRecords();
			handleCRUD();
			handleValidate1();
			handleTableMenu();
			handleModalReport();
			handleAnggaran();
			handleInspektur();
			
			handleTimRecords();
			handleTimCRUD();
			handleAuditorPoin();
			handleModalShow();
		}
	};
}();