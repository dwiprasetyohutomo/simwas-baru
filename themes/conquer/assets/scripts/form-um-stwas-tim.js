var SuratTugasWasTim = function () {
	
	var tid = '';
	
	// Modal Form
	var timModul = 'modul.php?ke=um_stwas_tim';
	var timJudul = 'Personil';
	
	// CRUD
	var timMenuBaru	= modul+'&form=baru';
	var timMenuEdit 	= modul+'&form=edit';
	var timMenuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	
	// Tabel Data
	var timSourceUrl	 = modul + '&lihat=tabeltim&tid='+tid;
	var timDestSelector = $('#tableAjaxTim');

	var handleValidate = function () {
		
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				jenis: { required: true },
				nomor: { required: true },
				kegiatan: { required: true },
				tglsrt: { required: true }
			},
			messages: {
				// nosk: { required: "Anda belum mengisi Nomor SK" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function () {
				var nip = hapusSpasi($('.pegawainip').text());
				var data = new FormData($('#form-user')[0]);
				var tabel = $("#tableAjax");
				modalForm.submitData(modul, data, sourceUrl+'&nip='+nip, tabel, true);
			}
		});
		
		$('#form-user input').keypress(function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleTableMenu = function () {
		
		$('#menurefresh').on('click',function(e) {
			e.preventDefault();
			var tabel = $('#tableAjax');
			refreshData(tabel);
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&form=importform', 'Import data kepangkatan (.csv)', 'pangkat_import');
			e.preventDefault();
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = exportData;
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';
			var tabel = $("#tableAjax");
			
			modalForm.kosongkanData(emptyData, judul1, judul2, 'Hapus dah', 'Tidak Jadi', '', tabel, true);
		});		
	}

	var handleSubmisi = function () {

		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&tabel=importupload',
				type: method,
				data: data,
				mimeType: 'multipart/form-data',
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(sourceUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});
	}
	
	var handleTimModalShow = function () {
		$(document).on("ajaxComplete", function(e){
			/*
			var tableAjaxTim = $("#tableAjaxTim").DataTable({
				"language": {
					"emptyTable": "Tidak ada data pada tabel",
					"info": "_START_ s/d _END_ dari _TOTAL_ data",
					"infoEmpty": "Tidak ada data untuk ditampilkan",
					"lengthMenu": "_MENU_ data per-halaman",
					"loadingRecords": "<span class=\"glyphicon glyphicon-refresh glyphicon-refresh-animate\"></span> Sedang mengambil data...",
					"processing": "<span class=\"glyphicon glyphicon-refresh glyphicon-refresh-animate\"></span> Sedang memproses...",
					"search": "",
					"zeroRecords": "Tidak ditemukan data yang cocok",
					"paginate": {"first":"Awal","last":"Akhir","next":"Lanjut","previous":"Sebelum","page":"Halaman","pageOf": "dari"}
				},
				"lengthMenu": [[10, 20, -1],[10, 20, "Semua"]],
				"pageLength": 10
			});
			$('#tableAjaxTim_length select').removeClass('input-xsmall').addClass('input-small');
			*/
		});	
	}
	
	var handleTambahan = function () {
		
		$(document).on("change", "#anggaran", function() {
			var aggr = this.value;
			if ($(this).val() == "Lain") {
				$("#anggaran2").attr("style","margin-top:15px")
					.val('')
					.show()
					.focus();
			} else {
				$("#anggaran2").hide();
			}			
		});
		
		$(document).on("change", "#insp", function () {
			var inspnip = this.value;
			var inspnama = $(this).find("option:selected").text();
			$("#inspnama").val(inspnama)
			$("#inspnip").val(inspnip);
		});
		
	}
	
	return {
		init: function () {
			//handleTimRecords();
			handleTimPemeriksa();
			handleTimModalShow();
		}
	};
}();