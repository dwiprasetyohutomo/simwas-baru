var Kepangkatan = function () {
	
	// variabel Modal Form
	// var modul = $('#form-user').attr('action');
	// var modul = 'modul.php?ke=um_kepangkatan';
	var modul = window.location.href;
	var judul = 'Form Kepangkatan';
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit	= modul+'&form=edit';
	var menuHapus	= modul+'&form=hapus';
	
	// variabel Tabel Data
	// var loadUrl 	= modul+'&lihat=tabelpangkat';
	var loadUrl 	= modul+'&lihat=tabeldata';
	// var destSelector= $('#tblPangkat tbody');
	
	var tabel		= $("#tableAjax");
	var grid 		= new Datatable();
	
	var handleRecords = function () {

		// AJAX Biasa
		// tampilData(loadUrl, destSelector);
		
		// DATATABLE
		grid.init({
			src: tabel,
			dataTable: {
				"pageLength": 5,
				"language": { "lengthMenu": "<span class='seperator'>|</span><span class='hidden-480'>Tampil</span> _MENU_" },
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": loadUrl },
				// "ordering": false,
				"columnDefs": [{"targets": [0,1,3,4,5,7,8]}], // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[6, "desc"]],  // 1 kolom yang di sortir pertama kali
			}
		});
	}

	var handleCRUD = function() {

		$('#menubaru').on('click', function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var aksiVal = 'pangkat_simpan';		// mengisi value elemen #aksi
			formModal.tambahData ( menuBaru+'&nip='+nip, aksiVal, judul );
			// modalForm.tambahData( menuBaru+'&nip='+nip, 'pangkat_simpan', judul, true );
			e.preventDefault();
		});
		
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');	// ambil dan isi value element #uid
			var data = { uid: uid };			// posting data untuk SQL
			var aksiVal = 'pangkat_update';		// mengisi value elemen #aksi
			formModal.editData ( menuEdit, uid, aksiVal, judul, data );
			// modalForm.editData( menuEdit, uid, 'pangkat_update', judul);
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var uid = $(this).attr("data-id");
			var data = { uid: uid }
			formModal.hapusData ( menuHapus, data, true, tabel );
			// modalForm.hapusData( menuHapus, uid, loadUrl+'&nip='+nip, destSelector, false );
			e.preventDefault();
		});

	}
	
	var handlePegawai = function () {
		
		var tblPegawai = $('#tblPegawai').dataTable({
			"responsive": true,
			"language": { "search":"", "lengthMenu":"_MENU_ data", "zeroRecords":"Hasil pencarian tidak menemukan apa-apa", "info":"_START_ s/d _END_ dari _TOTAL_ data pns", "infoEmpty":"", "infoFiltered":"(Total _MAX_ data)" },
			"sort": false,
			"pageLength": 5,
			"lengthMenu": [[5, 10, 20, 50, -1],[5, 10, 20, 50, "Semua"]],
		})
		// .parent().removeClass('table-scrollable');
		$('#tblPegawai_filter .form-control').attr('placeholder','Pencarian').addClass('input-small input-sm');
		$('#tblPegawai_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm');
		$('#tblPegawai tr').css('cursor','pointer');
		
		$(document).on('click','#tblPegawai tr.pilih', function(e) {
			e.preventDefault();
			var datanip = $(this).attr('data-nip');
			var datanama = $(this).attr('data-nama');
			
			// Tutup Modal pegawai yang dipilih
			$("#modal-pegawai").modal('hide');
			
			// Tampilkan gambar, nama dan nip pegawai bersangkutan
			var tmp;
			$tmp='<span class="pegawaifoto">';
			if (datanip.substr(14,1) == 1) {
				$tmp+='<img src="img/placeholder_man.png" style="padding-bottom:10px"><br>';
			} else {
				$tmp+='<img src="img/placeholder_woman.png" style="padding-bottom:10px"><br>';
			}
			$tmp+='</span>';
			$tmp+='<span class="pegawainama">'+datanama+'</span><br>';
			$tmp+='NIP. <span class="pegawainip">'+spasiNip(datanip)+'</span>';
			$('.info-pegawai').html($tmp);
			
			// Tampilkan informasi pangkat pegawai bersangkutan
			// Ajax Biasa
			// tampilData(loadUrl+'&nip='+datanip, destSelector);
			grid.setAjaxParam('nip', datanip);
			grid.getDataTable().ajax.reload();
		});		
	}
	
	var handleValidate = function () {

		var form1 = $('#form-modal #form-user');
		var form1input = $('#form-modal #form-user input');
		var error1 = $('#form-modal .alert-danger', form1);
		
		form1.validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				tmt: { required: true },
				golru: { required: true }
			},
			messages: {
				tmt: { required: "TMT harap diisi" },
				golru: { required: "Silahkan pilih Golongan Ruang" }
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				// formSubmit();
				var data = new FormData(form1[0]);
				formModal.submitData ( modul, data, true, tabel );
			}
		});
		
		/*
		function formSubmit() {
			var nip = hapusSpasi($('.pegawainip').text());
			var data = new FormData(form1[0]);
			// modalForm.submitData(modul, data, loadUrl+'&nip='+nip, destSelector);
			modalForm.submitData(modul, data, loadUrl+'&nip='+nip, tabel, true);
		}
		*/
		// $('#form-user input').keypress(function (e) {
		/*
		$(document).on('keypress', form1input, function (e) {
			if (e.which == 13) {
				if (form1.validate().form()) {
					form1.submit();
                }
                return false;
            }
        });
		*/
	}
	
	var handleTableMenu = function () {

		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&pangkat=importupload',
				type: method,
				data: data,
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(loadUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});
		
		$('#menurefresh').on('click',function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			tampilData(loadUrl+'&nip='+nip, destSelector);
			e.preventDefault();
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&pangkat=import','pangkat_import','Import data kepangkatan (.csv)' );
			$.ajax({
				type: 'GET',
				url: modul+'&pangkat=importform',
				async: false,
				cache: false,
				dataType: 'html',
				processData: false,
				beforeSend: function() {
					$('#form-import')[0].reset();
					$('#import-modal .modal-title').html('Import data kepangkatan (.csv)');
					$('#import-modal .modal-loading').show();
					// $('#form-import').attr('action',modul+'&pangkat=pangkatupload');
					$('#import-modal .form-body').html('');
					$('#import-modal #import')
						.val('pangkat_import')
						.text('Import');
				},
				success: function(response) {
					$('#import-modal .form-body').html(response);
					$('#import-modal .modal-loading').hide();
				},
				error: function(xhr, ajaxOptions, thrownError) {
					$('#import-modal .form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + xhr.status + ': ' + xhr.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
					$('#import-modal .modal-loading').hide();
					$('#import-modal #import').attr('disabled','disabled');
				}
			});
			e.preventDefault();
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = modul+'&pangkat=export';
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';			
			modalForm.kosongkanData(modul + '&pangkat=kosong', judul1, judul2, 'Hapus dah', 'Tidak Jadi', loadUrl, destSelector);
		});		
	}
	
	var handleModalShow = function () {
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			
			// $("#tahun").inputmask({ "mask": "9999" });
			// $("#lahirtgl").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			// $("#masath").inputmask({ "mask": "99" });
			
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			$("#tmt").inputmask({ "mask": "99-99-9999" });
			// $("#ak").inputmask({ alias: "currency", prefix: "", groupSeparator: ".", digits:0 });  // Rupiah
			$('#spintahun').spinner({value:0, min: 0, max: 99});
			$('#spinbulan').spinner({value:0, min: 0, max: 99});
			$("#gapok").inputmask({ alias: "currency", prefix: "", groupSeparator: ".", digits:0 });  // Rupiah
			$("#tglsk").inputmask({ "mask": "99-99-9999" });
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
			// $('#nosk').focus();
		});		
	}
		
	return {
		init: function () {
			handleRecords();
			handleCRUD();
			handlePegawai();
			handleTableMenu();
			handleValidate();
			handleModalShow();
		}
	};
}();