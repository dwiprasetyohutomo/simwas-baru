var Laporan = function () {
	
	// Modal Form
	var modul = 'modul.php?ke=um_stwas';
	var judul = 'Form Surat Tugas (Pengawasan)';
		
	// Tabel Data
	
	
	var handleMenu = function () {
		
		/*
		$(document).on('click',function(e) {
			e.preventDefault();
			if(!$(e.target).is("#duk-modal")) {
				var url = $(this).attr("data-url");
				tampilData(url, $("#duk-modal modal-body"));
			}
		})
		*/
		$('.laporan').each(function() {
			var $this = $(this);
			$this.on('click', function() {
				var url = $this.attr("data-url");
				tampilData(url, $(".modal-body"));
			})
		})
	}
	
	var handleModalShow = function () {		
		
		// Kode Kegiatan Pegawasan
		$("#duk-modal").on("shown.bs.modal", function() {
			$('.modal-body').scrollTop(0);
		});
	}
	
	return {
		init: function () {
			handleMenu();
			handleModalShow();
		}
	};
}();