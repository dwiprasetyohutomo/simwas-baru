var Profil = function () {
	
	var modul = window.location.href;
	
	var handleTab = function() {
		
		// Tab Pilihan
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			e.preventDefault();	
			var url = $(this).attr("data-url");		
			var target = $(e.target).attr("href") // activated tab
			var pageContent = $('.page-content'); // untul loading message

			if (typeof url !== typeof undefined && url !== "") { // Jika terdapat data-url dan data-url tidak kosong
				$.ajax({
					type: "GET",
					url: url,
					dataType: "html",
					beforeSend: function() {
						App.blockUI({ // Loading Message blockUI
							message: "Memproses permintaan...",
							target: pageContent,
							overlayColor: 'none',
							cenrerY: true,
							boxed: true						
						});
					},				
					success: function(data){
						App.unblockUI(pageContent); // Loading Message blockUI
						$(target).html(data);
					},
					error: function(xhr){
						App.unblockUI(pageContent); // Loading Message blockUI
						alert("Mohon maaf, terjadi kesalahan dalam mengambil data");
						$(target).html(xhr.statusText + xhr.responseText);
					}
				});
			}
		});
	}
	
	var handleTabActiveFirst = function() {
		
		/*
		var loadingPage = $('.page-content');		
		App.blockUI({
			message: "Memproses permintaan...",
			target: loadingPage,
			overlayColor: 'none',
			cenrerY: true,
			boxed: true						
		});
		*/
		
		$('#tab_pegawai').load($('#profil .active a').attr("data-url"),function(response, status, xhr){
			if(status == "success")
				// App.unblockUI(loadingPage);
				$('.active a').tab('show');
			if(status == "error")
				// App.unblockUI(loadingPage);
				alert("Status: " + xhr.status + ": " + xhr.statusText);
		});
		
		/*
		// contoh penggunaan load yang benar
		$('#tab_user').load($('#akun .active a').attr("data-url"),function(result){
			$('.active a').tab('show');
		});
		*/
	}

	var handleUserPass = function() {
		// Validasi Form User
		$('.userpass-form').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				user: { required: true },
				pass: { required: {
						depends: function(element) {
							return ($('#pass').val() != '')
						}
					}, minlength: 5 },
				rpass: { equalTo: "#pass" }
			},
			messages: {
				user: { required: "Nama User tidak boleh kosong, emang mau login pake apa ???" },
				pass: {
					minlength: "Minimal 5 Karakter ( Saran: gunakan kombinasi karakter+angka, contoh B3b3B )."
				},
				rpass: { equalTo: "Isian tidak sama dengan Password diatas, monggo diulang... tapi isinya yang bener."}
			},
			invalidHandler: function (event, validator) {
				$('.alert')
					.removeClass('alert-success alert-info alert-danger alert-warning')
					.addClass('alert-danger')
					.html('<button class="close" data-close="alert"></button><i class="fa-lg fa fa-warning"></i> Terdapat kotak input yang belum diisi / belum memenuhi persyaratan. Coba di teliti lagi.')
					.fadeIn();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},
			submitHandler: function (form) {
				var formData = $('.userpass-form').serialize();
				$('#submituser')
					.attr('disabled','disabled')
					.html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');					
				$.ajax ({
					url: modul,
					type: "POST",
					timeout: 20000,
					data: formData,
					dataType: 'json'
				}).done(function(response, textStatus, jqXHR) {	
					if (response.status == 'sukses') {
						$('.alert').hide();
						$('#pass').val('');
						$('#rpass').val('');
						$('#submituser').removeAttr('disabled').html('<i class="fa fa-check"></i> Simpan');
						const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 1500 });
						toast({ type: 'success', title: response.pesan });
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.responseText);
				});
				return false;
			}
		});
		
		$('.userpass-form input').keypress(function (e) {
			if (e.which == 13) {
				if ($('.userpass-form').validate().form()) {
					$('.userpass-form').submit();
				}
				return false;
			}			
		});		
	}
	
	var handleFoto = function() {
		// foto
		$('#userfoto-form').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				user: { required: true },
				pass: { required: {
						depends: function(element) {
							return ($('#pass').val() != '')
						}
					}, minlength: 5 },
				rpass: { equalTo: "#pass" }
			},
			messages: {
				user: { required: "Nama User tidak boleh kosong, emang mau login pake apa ???" },
				pass: {
					minlength: "Minimal 5 Karakter ( Saran: gunakan kombinasi karakter+angka, contoh B3b3B )."
				},
				rpass: { equalTo: "Isian tidak sama dengan Password diatas, monggo diulang... tapi isinya yang bener."}
			},
			invalidHandler: function (event, validator) {
				$('.alert')
					.removeClass('alert-success alert-info alert-danger alert-warning')
					.addClass('alert-danger')
					.html('<button class="close" data-close="alert"></button><i class="fa-lg fa fa-warning"></i> Terdapat kotak input yang belum diisi / belum memenuhi persyaratan. Coba di teliti lagi.')
					.fadeIn();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				error.insertAfter(element.closest('.input-icon'));
			},
			submitHandler: function (form) {
				var formData = $('.userpass-form').serialize();
				$('#submituser')
					.attr('disabled','disabled')
					.html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');					
				$.ajax ({
					url: modul,
					type: "POST",
					timeout: 20000,
					data: formData,
					dataType: 'json'
				}).done(function(response, textStatus, jqXHR) {	
					if (response.status == 'sukses') {
						$('.alert').hide();
						$('#pass').val('');
						$('#rpass').val('');
						$('#submituser').removeAttr('disabled').html('<i class="fa fa-check"></i> Simpan');
						const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 1500 });
						toast({ type: 'success', title: response.pesan });
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					alert(jqXHR.responseText);
				});
				return false;
			}
		});		
		/*
		$('#userfoto-form').on('submit',function(e) {
			e.preventDefault();
			var formData = new FormData($('#userfoto-form')[0]);
				
			$('.progress')
				.addClass('active')
				.show();
				
			$.ajax({
				xhr: function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(e) {
						if (e.lengthComputable) {
							console.log('Bytes Loaded: ' + e.loaded);
							console.log('Total Size: ' + e.total);
							console.log('Persen: ' + (e.loaded / e.total));
							var percent = Math.round((e.loaded / e.total) * 100);
							$('.progress')
								.attr('aria-valuenow', percent)
								.css('width', percent + '%')
								.text(percent + '%');
						}
					});
					return xhr;
				},
				
				type : 'POST',
				url : modul + '&upload=foto',
				data : formData,
				processData : false,
				contentType : false,
				success: function(response) {
					$('#userfoto-form')[0].reset();
					$('.progress').attr('aria-valuenow', 0).css('width', '0%').text('0%');
					$('.progress').hide();
					const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 1500 });						
					if(response == "") {
						toast({ type: 'error', title: 'File Gagal di upload' });
					} else {
						toast({ type: 'success', title: 'File berhasil di upload' });
					}
				}				
			});			
			return false;
		});
		*/
	}
	
	var handleModalShow = function () {
		
		$(document).on("ajaxComplete", function(event, xhr, settings) {
			event.preventDefault();
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			// $('.form-body').scrollTop(0);
		});		
	}
	
	return {
		init: function () {
			handleTab();
			handleTabActiveFirst();
			handleUserPass();
			// handleFoto();
			handleModalShow();
		}
	};
}();