var Login = function () {

	var handleLogin = function() {

		$('.login-form').validate({
			errorElement: 'span', //default input error message container
			errorClass: 'help-block', // default input error message class
			focusInvalid: false, // do not focus the last invalid input
			rules: {
				username: { required: true },
				password: { required: true, minlength: 5 },
				remember: { required: false}
			},
			messages: {
				username: { required: "Nama wajib di isi." },
				password: { required: "Password wajib di isi.", minlength: "Isian minimal 5" }
			},
			 //display error alert on form submit   
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('.login-form')).show();
			},
			// hightlight error inputs
			highlight: function (element) {
				$(element)
					.closest('.form-group')
					.addClass('has-error'); // set error class to the control group
			},
			success: function (label) {
				label
					.closest('.form-group')
					.removeClass('has-error');
				label
					.remove();
			},
			errorPlacement: function (error, element) {
				//error.insertAfter(element.closest('.input-icon'));
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}				
			},
			submitHandler: function () {
			//submitHandler: function (form) {
				//form.submit();
				/*
				// Cara 1 (Cara Cepat)
				$.post('logincek.php', $('.login-form').serialize(),  function(data) {
					if(data == 0) {
						$('.alert').removeClass(' alert alert-success');
						$('.alert').addClass(' alert alert-danger');
						$('.alert').html('Wrong User ID and Password');
						$('.alert').hide();
						$('.alert').fadeIn(1500); //Fade in the data given by the
					} else {
						//$("#message").html("Welcome to RetailMe Admin Panel");
						$('.alert').removeClass(' alert alert-danger');
						$('.alert').addClass(' alert alert-success');
						$('.alert').hide();
						$('.alert').fadeIn(1500); //Fade in the data given by the insert.php file
						window.location.href = "index.php";
					}
				});
				*/
					
				// Cara 2 (Procedural)
				//var data = $('.login-form').serialize(); // Deprecated
				var data = new FormData();
					data.append('username',$("#username").val());
					data.append('password',$("#password").val());

				$.ajax({
					url: 'logincek.php',
					type: 'POST',
					data: data,
					dataType: 'json',
					contentType: false,
					processData: false,
					beforeSend : function(e) {
						$('.alert', $('.login-form')).hide();
						if(e && e.overrideMimeType) {
							e.overrideMimeType("application/json;charset=UTF-8");
						}
						$('.btn')
							.addClass('disabled')
							.html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
					},
					success: function(response) {
						if(response.status == "sukses") {
							$('.alert', $('.login-form')).hide();
							//$('.alert').removeClass('.alert alert-danger').addClass('.alert alert-success').html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>').show();
							//window.location.href = "index.php"; // gunakan ini saat online
							setTimeout('window.location.href = "index.php"',1000); // gunakan ini saat offline
						}
						if(response.status == "gagal") {
							$('.alert')
								.html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>').show();
							$('.btn')
								.removeClass('disabled')
								.html('<i class="fa fa-undo"></i> Coba Lagi');
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						$('.alert').html('<button class="close" data-close="alert"></button><span>'+xhr.responseText+'</span>').show();
						//alert(xhr.responseText);
					},
					complete: function() {}
				});
				return false;
			}
		});

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit();
                }				
                return false;
            }
        });
		
		$('#username').focus();		
	}
	
	/*
	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}

	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="fa fa-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


			$('#select2_sample4').change(function () {
                $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });



         $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
	                    required: true
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                address: {
	                    required: true
	                },
	                city: {
	                    required: true
	                },
	                country: {
	                    required: true
	                },

	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}
    */
	
    return {
		//main function to initiate the module
		init: function () {
			handleLogin();
			//handleForgetPassword();
			//handleRegister();        
		}
	};
}();