var LaporanAuditor = function () {
	
	var modul = 'modul.php?ke=au_laporan';
	
	var modeLaporan = {
		// fungsi ajax load laporan
		tampilOpsi: function(url, data) {
			var dest = $('#dokumen');
			dest.html('<div align="center" class="loader2"><img src="img/loading/loading-orange.gif"><br><span>Loading...</span></div>');
			$.ajax({
				url: url,
				type: 'POST',					
				timeout: 20000,
				data: data,
				dataType: 'html',
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				dest.html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				dest.html('Gagal');
			});
		},
		
		tampilJenis: function(url, data) {
			var dest = $('#jenislaporan');
			dest.html('<div align="center"><img src="img/loading/loading-barsm.gif"></div>');
			$.ajax({
				url: url,
				type: 'POST',					
				timeout: 20000,
				data: data,
				dataType: 'html',
			}).done(function(response, textStatus, jqXHR) {
				// console.log(response);
				dest.html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				dest.html('Gagal');
			});
		}
	}
		
	var handleMenu = function () {

		// Select Tahun dan Option Masa berubah
		$(document).on( 'change', '#tahun, input[type=radio][name=masa]', function(e) {
			
			var thn  = $("#tahun").val();
			var masa = $("input[name='masa']:checked").val();
			var data = { thn: thn, masa: masa };
			
			if((thn == "") || (masa == "")){
				$('#jenislaporan').html('Silahkan pilih Tahun dan Masa Penilaian');
				$('#dokumen').html('');
			} else {
				$('#dokumen').html('');
				modeLaporan.tampilJenis(modul+'&laporan=periode', data);
			}			
			e.preventDefault();
		});
		
		$(document).on('click', '#rekap', function(e) {
			
			var id 	 = $(this).attr('data-id');
			var nip  = $(this).attr('data-nip');			
			var thn  = $(this).attr('data-thn');
			var masa = $(this).attr('data-masa');

			var data = { id: id, nip: nip, thn: thn, masa: masa };
			
			$('#rekap').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			$.ajax({
				url: 'modul.php?ke=au_laporan&laporan=rekap',
				type: 'POST',
				timeout: 20000,
				data: data,
				dataType: 'json'
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if(response.status == 'sukses') {
					$('#rekap').text('Rekap PAK');
					const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
					toast({ type: 'success', title: response.pesan });
				} else if(response.status == 'gagal') {
					$('#rekap').text('Rekap PAK');
					const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
					toast({ type: 'success', title: response.pesan });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				alert(jqXHR.responseText);
			});
			return false;
		});
		
		$('.laporan').each(function() {
			var $this = $(this);
			$this.on('click', function() {
				var url = $this.attr("data-url");
				tampilData(url, $(".modal-body"));
			})
		});
		
		/*
		$(document).on('click',function(e) {
			e.preventDefault();
			if(!$(e.target).is("#duk-modal")) {
				var url = $(this).attr("data-url");
				tampilData(url, $("#duk-modal modal-body"));
			}
		});
		*/
	}
	
	var handlePengawasan = function() {
		// Pengawasan
		$(document).on('click','#was',function(e) {
			var thn  = $('#tahun').val();
			var masa = $('input[name=masa]:checked', '#periode').val();
			var data = { thn: thn, masa: masa };
			
			modeLaporan.tampilOpsi(modul+'&laporan=was', data);
			
			e.preventDefault();
		});
	}
	
	var handleProfesi = function() {
		
		// Load dokumen Profesi
		$(document).on('click','#pp',function(e) {
			var thn  = $('#tahun').val();
			var masa = $('input[name=masa]:checked', '#periode').val();
			var data = { thn: thn, masa: masa };
			modeLaporan.tampilOpsi(modul+'&laporan=pp', data);
			e.preventDefault();
		});
		
		// Dapatkan nama penaggungjawab
		$(document).on('change', '#ttdnip', function() {
			var ttdnama = $('#ttdnip').find("option:selected").text();
			$('#ttdnama').val(ttdnama);
		});
		
		// Simpan informasi penandatangan
		$(document).on('click','#ppttdsimpan',function(e) {
			var data = new FormData($('#ppttd')[0]);
			$('#ppttdsimpan').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			$.ajax({
				url: modul,
				type: 'POST',					
				data: data,
				dataType: 'json',
				processData: false,
				contentType: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				// console.log(response);
				if(response.status == 'sukses') {
					$('#ppttdsimpan').html('Simpan');
				}
				if(response.status == 'gagal') {
					$('#ppttdsimpan').html('Simpan');
					$('#dokumen').html('Gagal')
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('#ppttdsimpan').html('Error');
				alert(jqXHR.responseText);
			});
			e.preventDefault();
		});
	}

	var handlePenunjang = function() {
		
		// Load dokumen Profesi
		$(document).on('click','#penunjang',function(e) {
			
			var thn  = $('#tahun').val();
			var masa = $('input[name=masa]:checked', '#periode').val();
			
			$.ajax({
				url: modul+'&laporan=up',
				type: 'POST',
				timeout: 20000,
				data: { thn: thn, masa: masa },
				dataType: 'html',
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('#dokumen').html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('#dokumen').html('Gagal');
			});
			e.preventDefault();
		});
		
		// Dapatkan nama penaggungjawab
		$(document).on('change', '#upnip', function() {
			var ttdnama = $('#upnip').find("option:selected").text();
			$('#upnama').val(ttdnama);
		});
		
		// Simpan informasi penandatangan
		$(document).on('click','#upsimpan',function(e) {
			var data = new FormData($('#formup')[0]);
			$('#upsimpan').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			$.ajax({
				url: modul,
				type: 'POST',
				data: data,
				dataType: 'json',
				processData: false,
				contentType: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				// console.log(response);
				if(response.status == 'sukses') {
					$('#upsimpan').html('Simpan');
				}
				if(response.status == 'gagal') {
					$('#upsimpan').html('Simpan');
					$('#dokumen').html('Gagal')
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('#upsimpan').html('Error');
				alert(jqXHR.responseText);
			});
			e.preventDefault();
		});
	}

	var handleLAK = function() {
		// Pengawasan
		$(document).on('click','#lak',function(e) {			
			var thn  = $('#tahun').val();
			var masa = $('input[name=masa]:checked', '#periode').val();
			
			$.ajax({
				url: modul+'&laporan=lak',
				type: 'POST',					
				timeout: 20000,
				data: { thn: thn, masa: masa },
				dataType: 'html',
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('#dokumen').html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('#dokumen').html('Gagal');
			});
			e.preventDefault();
		});
	}
	
	var handleDUPAK = function() {
		// Pengawasan
		$(document).on('click','#dupak',function(e) {			
			var thn  = $('#tahun').val();
			var masa = $('input[name=masa]:checked', '#periode').val();
			
			$.ajax({
				url: modul+'&laporan=dupak',
				type: 'POST',					
				timeout: 20000,
				data: { thn: thn, masa: masa },
				dataType: 'html',
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('#dokumen').html(response);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('#dokumen').html('Gagal');
			});
			e.preventDefault();
		});
	}
	
	var handleModalShow = function () {		
		
		// Kode Kegiatan Pegawasan
		$("#duk-modal").on("shown.bs.modal", function() {
			// $('.modal-body').scrollTop(0);
		});
	}
	
	return {
		init: function () {
			handleMenu();
			handlePengawasan();
			handleProfesi();
			handlePenunjang();
			handleLAK();
			handleDUPAK();
			handleModalShow();
		}
	};
}();