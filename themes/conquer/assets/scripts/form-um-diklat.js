var Diklat = function () {
	
	// variabel Modal Form
	var modul = window.location.href;
	// var modul = $('#form-user').attr('action');
	var judul = 'Form Diklat Dalam Jabatan';
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit	= modul+'&form=edit';
	var menuHapus	= modul+'&form=hapus';
		
	// variabel Tabel Data
	var loadUrl 	 = modul+'&lihat=tabeldiklat';
	var loadUrl2 	 = modul+'&lihat=tabeldata';
	var destSelector = $('#tblDiklat tbody');
	var tabel 		 = $("#tableAjax");
	
	var grid = new Datatable();

	var handleRecords = function () {
		
		// AJAX Biasa
		// tampilData(loadUrl, destSelector);		
		
		// DATATABLE
		grid.init({
			src: tabel,
			dataTable: {
				"pageLength": 5,
				"language": { "lengthMenu": "<span class='seperator'>|</span><span class='hidden-480'>Tampil</span> _MENU_" },
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-4 col-sm-12'><'col-md-8 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": loadUrl2 },
				// "ordering": false,
				"columnDefs": [{"targets": [0,1,2,4]}], // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[3, "desc"]],  // Kolom yang di Sortir
			}
		});
			
		// ============ CUSTOM FILTERING ==
		$('#fjns').on('change',function(e) {
			e.preventDefault();
			var jns = $(this).val();
			if (jns == 'fgs') {
				$('#fsub').removeClass('hidden');
			}
			else {
				grid.clearAjaxParams();
				$('#fsub').val('').addClass('hidden');
			}
		});

		grid.getTableWrapper().on("click", ".table-group-action-submit", function (e) { // SUBMIT
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				if($(this).hasClass('hidden') == false) {
					grid.setAjaxParam($(this).attr("name"), $(this).val());
				}
			});
			grid.getDataTable().ajax.reload();
		});

		grid.getTableWrapper().on("click", ".table-group-action-reset", function (e) { // RESET
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				$(this).val("");
			});
			grid.clearAjaxParams();
			grid.getDataTable().ajax.reload();
		});		
		// ============ End Custom Filter
	}
	
	var handleCRUD = function() {

		$('#menubaru').on('click', function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var aksiVal = 'diklat_simpan';		// mengisi value elemen #aksi
			formModal.tambahData ( menuBaru+'&nip='+nip, aksiVal, judul );
			e.preventDefault();
		});
		
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');	// ambil dan isi value element #uid
			var data = { uid: uid };			// posting data untuk SQL
			var aksiVal = 'diklat_update';		// mengisi value elemen #aksi
			formModal.editData ( menuEdit, uid, aksiVal, judul, data );
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var uid = $(this).attr("data-id");
			var data = { uid: uid }
			formModal.hapusData ( menuHapus, data, true, tabel );
			e.preventDefault();
		});	
	}
	
	var handleRecordsPegawai = function () {
		
		// Setting Datatable
		var tblPegawai = $("#tblPegawai").dataTable({
			"language": { "search":"", "lengthMenu":"_MENU_ data", "zeroRecords":"Hasil pencarian tidak menemukan apa-apa", "info":"_START_ s/d _END_ dari _TOTAL_ data pns", "infoEmpty":"", "infoFiltered":"(Total _MAX_ data)" },
			"sort": false,
			"pageLength": 5,
			"lengthMenu": [[5, 10, 30, 50, -1],[5, 10, 30, 50, "Semua"]],
		});
		//.parent().removeClass('table-scrollable');
		$('#tblPegawai_filter .form-control').attr('placeholder','Pencarian').addClass('input-small input-sm');
		$('#tblPegawai_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm');
		$('#tblPegawai tr').css('cursor','pointer');		
		
		$(document).on('click','#tblPegawai tr.pilih', function(e) {
			e.preventDefault();
			var datanip = $(this).attr('data-nip');
			var datanama = $(this).attr('data-nama');
			
			// Tutup Modal pegawai yang dipilih
			$("#modal-pegawai").modal('hide');
			
			// Tampilkan gambar, nama dan nip pegawai bersangkutan
			var tmp;
			$tmp='<span class="pegawaifoto">';
			if (datanip.substr(14,1) == 1) {
				$tmp+='<img src="img/placeholder_man.png" style="padding-bottom:10px"><br>';
			} else {
				$tmp+='<img src="img/placeholder_woman.png" style="padding-bottom:10px"><br>';
			}
			$tmp+='</span>';
			$tmp+='<span class="pegawainama">'+datanama+'</span><br>';
			$tmp+='NIP. <span class="pegawainip">'+spasiNip(datanip)+'</span>';
			$('.info-pegawai').html($tmp);
			
			$('#fnip').val(datanip);		
			// Tampilkan informasi pangkat pegawai bersangkutan
			// dataTables
			grid.setAjaxParam('fnip', datanip);
			grid.getDataTable().ajax.reload();
			
			// Ajax Biasa
			// tampilData(loadUrl+'&nip='+datanip, destSelector);
		});		
	}

	var handleValidasi = function () {

		var form1 = $('#form-user');
		var error1 = $('#form-modal').find('.alert-danger'); // $('#form-modal .alert-danger', form1);
		
		form1.validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				jenis: { required: true },
				nama: { required: true },
				tglmulai: { required: true }
			},
			messages: {
				jenis:	{ required: "Jenis belum ditentukan" },
				nama: { required: "Uraian diklat belum di isi" },
				tglmulai: { required: "Tanggal mulai wajib di isi " }
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				var data = new FormData(form1[0]);
				formModal.submitData ( modul, data, true, tabel );
			}
		});
		
		$(document).on('keypress', '#form-user input', function(e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}

	var handleTabelMenu = function () {
		$('#menurefresh').on('click', function(e) {
			grid.getDataTable().ajax.reload();
			// tampilData(loadUrl, destSelector);
			e.preventDefault();
		});
		
		/*
		$(document).on('click','#export',function(e) {
			swal({
				title: "Export tabel ke format CSV?", text:"", type:"question", showCancelButton:true, confirmButtonText:"Ya", cancelButtonText:"Batal", cancelButtonColor:"#d33"
			}).then(function(result) {
				if(result.value) window.open("'.$modul.'&stgswas=export","_blank");
			});
			e.preventDefault();		
		});
		*/
	}
	
	var formDiklat = {
		formResetJenis: function() {
			$('#subjenis').val('');
			$('#sts').val('');
			$('#tglsts').val('');
			$('#kegiatan').val('');
			$('#kode').val('');
			$('#pp_peran').val('');
			$('#angkatan').val('');
			$('#penyelenggara').val('');
			$('#tglmulai').val('');
			$('#tglslsai').val('');
			$('#lokasi').val('');
			$('#sertifikat').val('');
			$('#tglsert').val('');
			$('#predikat').val('');
			$('#pp_jmlhari').val('0');
			$('#pp_jmljam').val('0');
			$('#pp_sak').val('0');
			$('#pp_jak').val('0');
			$('#ket').val('');
		},
		formResetSub: function() {
			$('#kode').val('');
			$('#pp_jmlhari').val('0');
			$('#pp_jmljam').val('0');
			$('#pp_sak').val('0');
			$('#pp_jak').val('0');
		},
		formInputJenis: function(jenis) {			
			if(jenis == 'str') { // Diklat Struktural
				$('#fg-subjenis').addClass('hidden');
				$('#fg-sts').addClass('hidden');
					$('#fg-kegiatan').removeClass('hidden');
						$('#kegiatan').attr('readonly',true);
					$('#fg-kode').removeClass('hidden');
					$('#fg-peran').addClass('hidden');
					$('#fg-tempat').removeClass('hidden');
					$('#fg-angkatan').removeClass('hidden');
					$('#fg-penyelenggara').removeClass('hidden');					
					$('#fg-sd').removeClass('hidden');
					$('#fg-lokasi').removeClass('hidden');
					$('#fg-predikat').removeClass('hidden');
					$('#fg-sert').removeClass('hidden');
					$('#fg-tglsert').removeClass('hidden');
					$('#fg-jumlah').removeClass('hidden');
				$('#fg-jmlhari').addClass('hidden');
				$('#fg-kp').addClass('hidden');		
					$('#fg-ket').removeClass('hidden');
			}			
			else if(jenis == 'fgs') { // Diklat Fungsional
					$('#fg-subjenis').removeClass('hidden');
					$('#fg-sts').removeClass('hidden');
					$('#fg-kegiatan').removeClass('hidden');
						$('#kegiatan').attr('readonly',false);
					$('#fg-peran').removeClass('hidden');
				$('#fg-tempat').addClass('hidden');
				$('#fg-angkatan').addClass('hidden');
					$('#fg-penyelenggara').removeClass('hidden');
					$('#fg-sd').removeClass('hidden');
					$('#fg-lokasi').removeClass('hidden');
					$('#fg-sert').removeClass('hidden');
				$('#fg-predikat').addClass('hidden');
					$('#fg-jmlhari').removeClass('hidden');
					$('#spinhari').removeClass('hidden');
					$('#spinjumlah').removeClass('hidden');
					$('#fg-kp').removeClass('hidden');
					$('#fg-ket').removeClass('hidden');
			}		
			else if(jenis == 'tek') { // Diklat Teknis
				$('#fg-subjenis').addClass('hidden');
				$('#fg-sts').addClass('hidden');
				$('#fg-kegiatan').removeClass('hidden');
					$('#kegiatan').attr('readonly',false);
				$('#fg-peran').addClass('hidden');
				$('#fg-tempat').removeClass('hidden');
				$('#fg-angkatan').removeClass('hidden');
				$('#fg-penyelenggara').removeClass('hidden');
				$('#fg-sd').removeClass('hidden');
				$('#fg-lokasi').removeClass('hidden');
				$('#fg-sert').removeClass('hidden');			
				$('#fg-predikat').addClass('hidden');
				$('#fg-jmlhari').addClass('hidden');
				$('#fg-kp').addClass('hidden');
				$('#fg-ket').removeClass('hidden');
			}		
			else if(jenis == 'prj') {// Diklat Prajabatan
				$('#fg-subjenis').addClass('hidden');
				$('#fg-sts').addClass('hidden');
				$('#fg-kegiatan').removeClass('hidden');
					$('#kegiatan').attr('readonly',true);
				$('#fg-kode').removeClass('hidden');
				$('#fg-peran').addClass('hidden');
				$('#fg-tempat').addClass('hidden');
				$('#fg-angkatan').removeClass('hidden');
				$('#fg-penyelenggara').removeClass('hidden');				
				$('#fg-sd').removeClass('hidden');
				$('#fg-lokasi').removeClass('hidden');
				$('#fg-sert').removeClass('hidden');
				$('#fg-predikat').addClass('hidden');
				$('#fg-jmlhari').removeClass('hidden');
				$('#fg-kp').addClass('hidden');				
				$('#fg-ket').removeClass('hidden');
			}
			else{
				$('#fg-subjenis').addClass('hidden');
				$('#fg-sts').addClass('hidden');
				$('#fg-kegiatan').addClass('hidden');
					$('#kegiatan').attr('readonly',false);
				$('#fg-peran').addClass('hidden');
				$('#fg-tempat').addClass('hidden');
				$('#fg-angkatan').addClass('hidden');
				$('#fg-penyelenggara').addClass('hidden');								
				$('#fg-sd').addClass('hidden');
				$('#fg-lokasi').addClass('hidden');				
				$('#fg-sert').addClass('hidden');
				$('#fg-predikat').addClass('hidden');
				$('#fg-jmlhari').addClass('hidden');
				$('#fg-kp').addClass('hidden');				
				$('#fg-ket').addClass('hidden');
			}
		},
		formInputSub: function(jenis, sub) {
			if ((jenis == 'str') || (jenis == 'prj')) {
				$('#fg-kode').removeClass('hidden');
			}			
			else if ((jenis == 'fgs') && (sub == 'pp') || (sub == 'up')) {
				$('#fg-kode').removeClass('hidden');
			}
			else {
				$('#fg-kode').addClass('hidden');
			}
		}
	}
	
	var handleJenisDiklat = function () {
		
		$(document).on('change','#jenis',function(e) {
			e.preventDefault();		
			var jns = this.value;
			var judul = $("#jenis").find("option:selected").text();
			$('#modal-kegiatan .modal-title').text(judul);
			formDiklat.formResetJenis();
			formDiklat.formInputJenis(jns);
		});
		
		$(document).on('change','#subjenis',function(e) {
			e.preventDefault();
			var jenis = $('#jenis').val();
			var sub = this.value;
			formDiklat.formResetSub();
			formDiklat.formInputSub(jenis,sub);
		});
	}
	
	var handleKegiatan = function () {

		$(document).on('click','#modalkode',function(e) { // Pilih Mode Tabel Kegiatan
			e.preventDefault();
			
			var jenis 	 = $('#jenis').val();
			var sub = $("#subjenis").val();
			var kode 	= $("#kode").val();
			
			if ((jenis == 'str') || (jenis == 'tek') || (jenis == 'prj')) { // Diklat Struktural / Teknis / Prajabatan
				var judul = $("#jenis").find("option:selected").text();
			} else {
				var judul = $("#subjenis").find("option:selected").text();	// Diklat Fungsional
			}			
			$('#modal-kegiatan .modal-title').text(judul); // Judul Tabel Diklat
			
			// Load isi Tabel Kegiatan
			// tampilData(modul + '&lihat=tabelkegiatan&jenis=' + jenis + '&subjenis=' + sub, $('#tblKegiatan tbody'));
			
			var data = { kode: kode };
			loadData(modul + '&lihat=tabelkegiatan&jenis=' + jenis + '&subjenis=' + sub, $('#tblKegiatan tbody'), data);
		});
		
		// Pilih Kegiatan
		$(document).on('click','#tblKegiatan tr.pilih',function(e) {
			e.preventDefault();
			
			var jenis 	 = $('#jenis').val();
			var sub = $("#subjenis").val();
			
			var kod = $(this).attr('data-kod');
			var keg = $(this).attr('data-keg');
			var hsl	= $(this).attr('data-hsl');
			var sak	= $(this).attr('data-sak');
			
			if((jenis == "str") || jenis == 'prj') { // Struktural / Prajabatan
				$('#kode').val(kod);
				$('#kegiatan').val(keg);
				$('#pp_sak').val(sak);
			} else if ((jenis == 'fgs') && (sub == 'pp') || (sub == 'up')) {
				$('#kode').val(kod);
				$('#hasil').val(hsl);
				$('#pp_sak').val(sak);
			}
			
			$("#modal-kegiatan").modal('hide');
			
		});
	}

	var hitung = {
		kreditPoin: function(jenis, sub) {
			
			if((jenis == 'fgs') && (sub == 'pj')) { // Diklat Fungsional -> Penjenjangan				
				var jml	= $('#jumlah').val();
				var sat = $('#pp_sak');
				var pnt = $('#pp_jak');				
				var nil;
				if((jml >= 30)  && (jml <= 80))  nil = 1;
				if((jml >= 81)  && (jml <= 160)) nil = 2;
				if((jml >= 161) && (jml <= 480)) nil = 3;
				if((jml >= 481) && (jml <= 640)) nil = 6;
				if((jml >= 641) && (jml <= 960)) nil = 9;
				if(jml >= 960) nil = 15;
				
				sat.val(nil);
				pnt.val(nil.toFixed(3)); // decimal / 3 angka dibelakang koma
			}
			
			if((jenis == 'fgs') && (sub == 'pp') || (sub == 'up')) {// Diklat Fungsional -> Profesi / Penunjang
				var jml	= $('#jumlah').val();
				var sat = $('#pp_sak').val();
				var pnt = $('#pp_jak');
				var tot = jml * sat;
				pnt.val(tot.toFixed(3)); // decimal 3 angka dibelakang koma
			}
		}
	}
	
	var handleKreditPoin = function () {
		// Kedua submisi dibawah fungsinya sama
		$(document).on("keyup","#jmljamkeg", function(e) { // Isi manual jumlah
			var jenis 	= $('#jenis').val();
			var sub		= $('#subjenis').val();
			hitung.kreditPoin(jenis,sub);
			e.preventDefault();
		});		
		$(document).on("click","#spinjumlah .spinner-up, #spinjumlah .spinner-down", function(e) { // Tombol Up dan tombol Down
			var jenis 	= $('#jenis').val();
			var sub		= $('#subjenis').val();
			hitung.kreditPoin(jenis,sub);
			/*
			var jml = $("#jmljamkeg").val();
			var sat = $("#satuankp").val();
			var pnt = $("#kreditpoin");
			var tot = jml * sat;
			pnt.val(tot.toFixed(3));
			*/
			e.preventDefault();
		});
	}
	
	var handleModalShow = function () {
		
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			$("#tglsts").inputmask({ "mask": "99-99-9999" });
			$("#tglmulai").inputmask({ "mask": "99-99-9999" });
			$("#tglslsai").inputmask({ "mask": "99-99-9999" });
			$("#tglsert").inputmask({ "mask": "99-99-9999" });
			
			$("#spinhari").spinner({ value: 0, min: 0 });
			$("#spinjam").spinner({ value: 0, min: 0 });
			$("#spinjumlah").spinner({
				// step: 0.01,
				value: 0, min: 0
			});
			
			// Select2
			// $("#subjenis").select2({ allowClear: true });	
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			// Untuk Edit
			var jenis = $('#jenis').val();
			var sub	  = $('#subjenis').val();
			formDiklat.formInputJenis(jenis);
			formDiklat.formInputSub(jenis, sub);
			
			$('#form-modal .form-body').scrollTop(0);
		});
		
		$('#modal-kegiatan').on('shown.bs.modal',function() {
			$('#modal-kegiatan #modal-body').scrollTop(0);
		});
	}
	
	return {
		init: function () {
			handleRecords();
			handleCRUD();
			handleRecordsPegawai();
			handleValidasi();
			handleTabelMenu();
			handleJenisDiklat();
			handleKegiatan();
			handleKreditPoin();
			handleModalShow();
		}
	};
}();
