var DataTableReload = function( refID ) {
	refID.DataTable().ajax.reload();
};

var refreshData = function ( refID ) {
	refID.DataTable().ajax.reload();
}

var loadData = function(url, refID, data={}) {
	$.ajax({
		url: url,
		type: 'POST',
		timeout: 20000,
		data: data,
		dataType: 'html',
		beforeSend: function() {
			refID.html('<div align="center" class="loader2"><img src="img/loading/loading-orange.gif"><br><span>LOADING</span></div>');
		},
		success: function(response) {
			// setTimeout(function() {
			refID.html(response);	
			// }, 1500);			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(xhr.responseText);
		}
	});
};

function hapusSpasi(nip) { return nip.replace(/\s/g,''); }

function spasiNip(nip) { return nip.substr(0,8) + ' ' + nip.substr(8,6) + ' ' + nip.substr(14,1) + ' ' + nip.substr(15,3); }

function tampilData(url, dest) {
	dest.html('<div align="center" class="loader2"><img src="img/loading/loading-orange.gif"><br><span>Loading...</span></div>');
	$.ajax({
		url: url,
		type: 'GET',
		timeout: 20000,
		dataType: 'html',		
	}).done(function(response, textStatus, jqXHR) {
		dest.html(response);
	}).fail(function(jqXHR, textStatus, errorThrown) {
		alert(jqXHR.responseText);
	});
}

var formModal = {
	tambahData: function( url, submitval, judul = 'Form Tambah Data', data={}, scroll=true, wide=false ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		submitval: value dari [button submit]
		judul: Caption Modal
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		scroll: isi modal form bisa di scroll dan tidak memanjang
		========================================================================================*/
		var modalID		= $('#form-modal');
		var theForm		= $('#form-user');
			theForm[0].reset();
		
		var theTitle = modalID.find('.modal-title');
			theTitle.html(judul);
	    var theWide = modalID.find('.modal-dialog')
		    if(wide) {
				theWide.addClass('modal-wide');
			} else {
			    theWide.removeClass('modal-wide');
			}
		var theBody	= theForm.find('.form-body');
			theBody.html('');
		var theUid = theForm.find('#uid');
			theUid.val('');
		var theAksi = theForm.find('#aksi');
			theAksi.val(submitval);
		var theSubmit = theForm.find(':submit');
			theSubmit.text('Simpan');
		var theSpin = theForm.find('.modal-loading');
			theSpin.show();
			
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'html'
		}).done(function(response, textStatus, jqXHR) {
			theSpin.hide();
			theBody.html(response);
		}).fail(function(jqXHR, textiStatus, errorThrown) {			
			theSpin.hide();
			alert(jqXHR.responseText);
			
		})
		return false;
	},
	editData: function ( url, uid, submitval, judul = 'Form Edit Data', data={}, scroll=true, wide=false ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		uid: pilihan primary key / kata kunci penghubung ke database
		submitval: value dari [button submit]
		judul: Caption Modal
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		scroll: isi modal form bisa di scroll dan tidak memanjang
		========================================================================================*/
		var modalID		= $('#form-modal');
		var theForm		= $('#form-user');
			theForm[0].reset();
		
		var theTitle = modalID.find('.modal-title');
			theTitle.html(judul);
	    var theWide = modalID.find('.modal-dialog')
		    if(wide) {
				theWide.addClass('modal-wide');
			} else {
			    theWide.removeClass('modal-wide');
			}
		var theBody	= theForm.find('.form-body');
			theBody.html('');
		var theUid = theForm.find('#uid');
			theUid.val(uid);
		var theAksi = theForm.find('#aksi');
			theAksi.val(submitval);
		var theSubmit = theForm.find(':submit');
			theSubmit.text('Perbaharui');
		var theSpin = theForm.find('.modal-loading');
			theSpin.show();
		
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}
		
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'html',
		}).done(function(response, textStatus, jqXHR) {
			theSpin.hide();
			theBody.html(response);
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		})
		return false;
		/*
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'html',
			cache: false,
			beforeSend: function() {
				theForm[0].reset();
				theTitle.html(judul);
				theBody.html('');
				theUid.val(uid);
				theSubmit.val(submitval).text('Perbaharui');
				theSpin.show();
			},
			success: function(response) {
				theBody.html(response);
				theSpin.hide();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				theSpin.hide();
				alert(xhr.responseText);
			}
		})		
		return false;
		*/
	},
	hapusData: function ( url, data, bDataTable = true, dstSel='', srcUrl='' ) {
		/*========================================================================================
		url			: lokasi skrip php dijalankan
		data		: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		bDataTable	: apakah menggunakan DataTable
		refID		: [table ID = DataTable] atau [div ID = ajaxBiasa] sebagai isi dari response
		========================================================================================*/
		swal({
			title: "Yakin ingin menghapus data ini?",
			html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
			type: "question",
			showCancelButton: true,
			confirmButtonColor: "#AA0000",
			confirmButtonText: "Hapus",
			cancelButtonColor: "#F0AD4E",
			cancelButtonText: "Jangan"
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: url,
					type: "POST",
					timeout: 20000,
					data: data,
					dataType: "json",
					processData: true,
					cache: false,
					success: function(response) {
						// console.log(response);
						if(response.status == "sukses") {
							if (bDataTable) {					
								dstSel.DataTable().ajax.reload();
							} else {
								loadData(srcUrl, dstSel);
							}
							// swal({ title: "Data telah terhapus!", text: response.pesan, type: "success", showConfirmButton: false, timer: 1500 });
							const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
							toast({ type: 'success', title: 'Data telah terhapus' });
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(xhr.responseText)
					}
				});
			}
		})
		return false;
	},
	submitData: function ( url, data, bDataTable = true, dstSel='', srcUrl='' ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		bDataTable: apakah menggunakan DataTable
		refID: ID untuk reload data contoh: DataTable = $('#tableAjax')
		========================================================================================*/
		var theModal	= $('#form-modal');
		var theErr		= theModal.find('.alert-danger');
			theErr.hide();
		
		var theForm		= $('#form-user');
		var theBody		= theForm.find('.form-body');
		var theSubmit	= theForm.find(':submit');

		theSubmit.html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			cache: false
		}).done(function(response, textStatus, jqXHR) {
			if (response.status == 'sukses') { // Sukses
				theSubmit.removeClass('disabled').text('Selesai...');
				theModal.modal('hide'); // Tutup Modal Form Simpan/Perbaharui					
				if (bDataTable) {					
					dstSel.DataTable().ajax.reload();
				} else {
					loadData(srcUrl, dstSel);
				}
				// Infokan Kondisi
				swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
			}
			if (response.status == 'gagal') { // Gagal
				theForm.find('.alert')
					.removeClass('alert-success alert-danger alert-info')
					.addClass('alert-warning')
					.html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>')
					.show();
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		})
		return false;
	}
}

var customModal = {
	loadData: function( url, modalId, data, scroll=true ) {			
		var theSpin = modalId.find('.modal-loading');
		var theBody = modalId.find('.form-body');
		
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}		
		$.ajax({
			url: url,
			type: 'GET',
			timeout: 20000,
			data: data,
			dataType: 'html'
		}).done(function(response, textStatus, jqXHR) {
			theSpin.hide();
			theBody.html(response).fadeIn();
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		});
	},
	tambahData: function( url, modalID, theForm, submitval, judul, data, scroll=true ) {
		theForm[0].reset();
		
		var theTitle = modalID.find('.modal-title');
			theTitle.html(judul);
		var theBody = modalID.find('.form-body');
			theBody.html('');
		var theUid = theForm.find('input[name=uid]');
			theUid.val('');
		var theAksi = theForm.find('#aksi');
			theAksi.val(submitval);
		var theSubmit = theForm.find(':submit');
			theSubmit.val(submitval).text('Simpan');
		var theSpin = modalID.find('.modal-loading');
			theSpin.show();
			
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}

		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			dataType: 'html'
		}).done(function(response, textStatus, jqXHR) {
			theSpin.hide();
			theBody.html(response);
		}).fail(function(jqXHR, textStatus, errorThrown) {			
			theSpin.hide();
			alert(jqXHR.responseText);
			
		});
		/*
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			dataType: 'html',
			beforeSend: function() {
				theTitle.html(judul);
				theForm[0].reset();
				theBody.html('');
				theUid.val('');
				theSubmit.val(submitval).text('Simpan');
				theSpin.show();
			},
			success: function( response ){
				theSpin.hide();
				theBody.html(response);				
			},
			error: function(xhr, ajaxOption, thrownError) {
				theSpin.hide();
				alert(xhr.responseText);
			}
		});
		*/
	},
	editData: function( url, uid, modalID, theForm, submitval, judul, data, scroll=true ) {
		
		theForm[0].reset();
		var theTitle = modalID.find('.modal-title').html(judul);
		var theBody = theForm.find('.form-body');
			theBody.html('');
		var theUid = theForm.find('#uid'); //'input[name=uid]'
			theUid.val(uid);
		var theAksi = theForm.find('#aksi');
			theAksi.val(submitval);
		var theSubmit = theForm.find(':submit');
			theSubmit.val(submitval).text('Perbaharui');
		var theSpin = theForm.find('.modal-loading');
			theSpin.show();
		
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}		
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'html',	
		}).done(function(response, textStatus, jqXHR) {
			theSpin.hide();
			theBody.html(response);
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		});
	},
	submitData: function ( url, theModal, theForm, data, bDataTable = false, dstSel = '', srcUrl = '' ) {
		/*========================================================================================
		url			: lokasi skrip php dijalankan
		theModal	: Modal
		theForm		: Form
		data		: penyertaan post [nama dan value] contoh: var data = new FormData(form[0]) atau var data = { name: value, dst... }
		bDataTable	: apakah menggunakan DataTable
		dstSel		: Selector DataTable ID (ID untuk reload data contoh: DataTable = $('#tableAjax')) atau Tujusn SrcUrl ditampilkan
		srcUrl		: jika tidak menggunakan database, tampilkan data sesuai URL
		========================================================================================*/
		theForm.find(':submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			cache: false
		}).done(function(response, textStatus, jqXHR) {
			if (response.status == 'sukses') { // Sukses
				theModal.modal('hide'); // Tutup Modal Form Simpan/Perbaharui					
				if (bDataTable) {					
					dstSel.DataTable().ajax.reload();
				} else {
					loadData(srcUrl, dstSel);
				}
				// Infokan Kondisi
				swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
			}
			if (response.status == 'gagal') { // Gagal
				theForm.find('.alert')
					.removeClass('alert-success alert-danger alert-info')
					.addClass('alert-warning')
					.html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>')
					.show();
			}
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		});
		return false;
	},
	hapusData: function( url, data, bDataTable = false, dstSel='', srcUrl='' ) {
		swal({
			title: "Yakin ingin menghapus data ini?",
			html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
			type: "question",
			showCancelButton: true,
			confirmButtonColor: "#AA0000",
			confirmButtonText: "Hapus",
			cancelButtonColor: "#F0AD4E",
			cancelButtonText: "Jangan"
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: url,
					type: 'POST',
					dataType: "json",
					data: data,
					processData: true,
					cache: false,
					success: function(response) {
						// console.log(response);
						if(response.status == "sukses") {
							if (bDataTable) {					
								dstSel.DataTable().ajax.reload();
							} else {
								loadData(srcUrl, dstSel);
							}
							// swal({ title: "Data telah terhapus!", text: response.pesan, type: "success", showConfirmButton: false, timer: 1500 });
							const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
							toast({ type: 'success', title: 'Data telah terhapus' });
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(xhr.responseText)
					}
				});
			}
		});
		return false;
	}
}