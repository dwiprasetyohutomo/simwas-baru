var STWas = function () {
	var modul = 'modul.php?ke=surat_tugas_was';
	
/*	
	var handlePickers = function () {
		$('.date-picker').datepicker({
			rtl: App.isRTL(),
			format: 'yyyy-mm-dd',				
			todayBtn: "linked",
			clearBtn: true,
			language: "id",
			autoclose: true,			
			daysOfWeekHighlighted: "0",
			todayHighlight: true		
		});
	}
*/	
	var handleRecords = function () {		
		var grid = new Datatable();		
		var tabel = $("#tableAjax");
		
		grid.init({
			src: tabel,
			dataTable: {
				//"pageLength": 5,
				"ajax": {
					"url": modul+"&stgswas=tabeldata"
				},
				"columnDefs": [
					{
						// Kolom yang dimatikan Fungsi Sortir nya
						"targets": [0,2,3,4,5,7]
					}
				],				
				"order": [
					[6, "desc"] // Kolom pertama yang di Sortir
				],
			}
		});
		
		var $inputcari = $('div.dataTables_filter input');		
		var $btncari = $('<span class="input-group-btn"><button class="btn btn-warning" type="button" style="margin-top:-1px"><i class="fa fa-search"></i></button></span>');
		$inputcari.contents().unwrap();
		$inputcari.attr('placeholder', 'Cari Nomor / Kegiatan')
			.removeClass('input-small')
			.wrap('<div class="input-group input-group-sm"></div>').after($btncari);
		$inputcari.off();
		$inputcari.on("keypress", function (e) {
			if (e.which == 13) {
				tabel.DataTable().search($inputcari.val()).draw();
			}
		});
		$btncari.on("click", function() {
			tabel.DataTable().search($inputcari.val()).draw();
		});		
	}
	
	var handleBaru = function() {
		$(document).on('click', '#baru', function(e) {
			var pageTitle = 'Surat Tugas Pengawasan';
			
			$('#form-user')[0].reset();
		
			$('.modal-title').html('<i class="fa fa-plus"></i> ' + pageTitle);
			$('.modal-loading').show();
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			$(".form-body").html('');
		
			$('#uid').val('');
			$('#aksi').val('stgs_simpan');
			$('#submit').text('Simpan');
		
			$.ajax({
				//url: modul+'&isian=baru',
				url: modul+'&stgswas=baru',
				type: 'POST',
				timeout: 20000,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();
		});		
	}
	
	var handleEdit = function () {
		$(document).on('click', '#edit', function(e) {
			var pageTitle = 'Surat Tugas Pengawasan';
			
			$('#form-user')[0].reset();
			
			$('.modal-title').html('<i class="fa fa-edit"></i> ' + pageTitle);
			$('.modal-loading').show();
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			//$(".form-body").wrap(\'<div class="scroller" style="height:70vh" data-always-visible="1" data-rail-visible="1" data-handle-color="#555" data-handle-size="10px"></div>\');
			$('.form-body').html('');
		
			var uid = $(this).attr('data-id');
			$('#uid').val(uid);
			$('#aksi').val('stgs_update');
			$('#submit').text('Perbaharui');
		
			$.ajax({
				//url: modul+'&isian=edit',
				url: modul+'&stgswas=edit',
				type: 'POST',
				timeout: 20000,
				data: 'uid='+uid,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeOut digunakan sebagai percobaan/testing saat online
				//setTimeout(function(){
				console.log(response);
				//$(".form-body").html(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
				//}, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: '+textStatus+': '+jqXHR.responseText+'<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();
		});
	}
	
	var handleValidasi = function () {
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				nomor: 	{ required: true },
				jenis: {required: true},
				kegiatan: {required: true }
			},
			messages: {
				nomor:	{ required: "Nomor harus di isi" },
				jenis:  { required: "Jenis kegiatan harus di isi!"},
				kegiatan:	{ required: "Uraian kegiatan harus di isi" }
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				var data = new FormData($('#form-user')[0]);
				$('#submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
				$.ajax({
					url: modul,
					type: 'POST',
					data: data,
					dataType: 'json',
					contentType: false,
					processData: false,
					cache: false
				}).done(function(response, textStatus, jqXHR) {
					if (response.status == 'sukses') {
						$('#form-modal').modal('hide');
						refreshData();
						swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
					} else {
						$('#submit').text('Coba Lagi');
						$('.alert')
							.removeClass('alert-danger alert-info')
							.addClass('alert-warning')
							.html('<span>'+response.pesan+'</span>').show();
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					$('#submit').hide();
					alert(jqXHR.responseText);
				});
			}
		});
		
		$('.form-user input').keypress(function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleHapus = function () {
		$(document).on('click', '#hapus', function(e) {
			var uid = $(this).attr('data-id');
			swalDelete(modul+'&stgswas=hapus', uid);
			e.preventDefault();
		});		
	}

	var handleModalShow = function () {
		
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			//$.extend($.inputmask.defaults, { "autounmask": true} );
		
			$("#spinlama").spinner({ value: 20 });
			$("#spinpos").spinner({ value: 1 });
			
			if ($("#anggaran").val() == "Lain") {
				$("#anggaran2").attr("style","margin-top:15px").val("").show();
			} else {
				$("#anggaran2").hide();
			}			
		
			// Menggunakan Currency
			//$("#anggaran").inputmask({ alias: "currency", prefix: "", groupSeparator: ".", digits:0 });  // Rupiah
			//$("#anggaran").inputmask({ alias: "currency", prefix: "", digits: 3 }); // Decimal
			
			$("#tahun").inputmask({ "mask": "9999" });
			$("#tgl_brkt").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			$("#tgl_kmbl").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			$("#tgl_srt").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });			
			$("#insp_nip").inputmask({ "mask": "99999999 999999 9 999" });
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			//$(".form-body").animate({ scrollTop: 0 }, \'slow\');			
			$('.form-body').scrollTop(0);
			$('#nomor').focus();
		});
	}
	
	var handleExport = function () {
		$(document).on('click','#export',function(e) {
			swal({
				title: "Export tabel ke format CSV?", text:"", type:"question", showCancelButton:true, confirmButtonText:"Ya", cancelButtonText:"Batal", cancelButtonColor:"#d33"
			}).then(function(result) {
				if(result.value) window.open("'.$modul.'&stgswas=export","_blank");
			});
			e.preventDefault();		
		});
	}
	
	var handleInspekturInfo = function () {
		$(document).on("change", "#insp", function () {
			var inspnama = $(this).find("option:selected").text();
			var inspnip = this.value;
			$("#insp_nama").val(inspnama)
			$("#insp_nip").val(inspnip);
			/*
			App.blockUI({ message: 'Mohon bersabar...', target: '.form-body', overlayColor: 'none', cenrerY: true, boxed: true });
			$.ajax({
				url: modul+'&personil=info',
				type: 'POST',
				timeout: 20000,
				dataType: 'json',
				//contentType: 'application/x-www-form-urlencoded',
				async: false,
				data: { nip : nip },
				success: function(data) {
					// Hilangkan setTimeout saat sudah online
					//setTimeout(function() {					
						App.unblockUI('.form-body'); // Tutup loading
						userData = data;
						$("#nama").val(userData.nama);
						$("#nip").val(userData.nip);
					//}, 500);
				}
			});
			*/
		});
	}

	var handleAnggaran = function () {
		$(document).on("change", "#anggaran", function() {
			var aggr = this.value;
			if ($(this).val() == "Lain") {
				$("#anggaran2").attr("style","margin-top:15px")
					.val("").show()
					.focus();
			} else {
				$("#anggaran2").hide();
			}			
		});
	}
	
	return {
		init: function () {
			//handlePickers();
			handleRecords();
			handleBaru();
			handleEdit();
			handleValidasi();
			handleHapus();
			handleModalShow();
			handleInspekturInfo();
			handleAnggaran();
		}
	};
}();
