var STWasPersonil = function () {	
	
	var modul = 'modul.php?ke=surat_tugas_was_tim';
	
	var handleInfoPegawai = function () {

		$(document).on("change","#namap", function(e) {			
			var nip = $(this).val();
			/*
			if( vnip != 0 ) {
				var data_string;
				data_string = "nip="+vnip;
				$.post("modul.php?ke=surat_tugas_was_tim&personil=info",data_string,function(data) {
					var data = data;
					$("#nama").val(data.nama);
					$("#nip").val(data.nip);
					$("#golru").val(data.pangkat);
				});
			} else {
				$("#nama").val("");
			}
			*/
			
			// Munculkan loading
			App.blockUI({ message: 'Mohon bersabar...', target: '.form-body', overlayColor: 'none', cenrerY: true, boxed: true });
			$.ajax({
				url: modul+'&personil=info',
				type: 'POST',
				timeout: 20000,
				dataType: 'json',
				//contentType: 'application/x-www-form-urlencoded',
				async: false,
				data: { nip : nip },
				success: function(data) {
					// Hilangkan setTimeout saat sudah online
					//setTimeout(function() {					
						App.unblockUI('.form-body'); // Tutup loading
						userData = data;
						$("#nama").val(userData.nama);
						$("#nip").val(userData.nip);
						$("#golru").val(userData.pangkat);					
					//}, 500);
				}
			});
			e.preventDefault();		
		});
	}
	
	var handleBaru = function() {
		$(document).on('click', '#personil', function(e) {
			var uid = $(this).attr('data-id');
			var nomor = $(this).attr('data-nomor');
			var tahun = $(this).attr('data-tahun');
				
			$('#form-user')[0].reset();
		
			$('.modal-title').html('<i class="fa fa-plus"></i> Personil');
			$('.modal-loading').show();
			$('.form-body').html('');
			//$('#uid').val(uid);
			$('#aksi').val('simpan');
			$('#submit').text('Simpan');
			
			$.ajax({
				url: modul+'&personil=tambah',
				type: 'POST',
				timeout: 20000,
				data: 'uid='+uid,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeOut digunakan sebagai percobaan/testing saat online
				//setTimeout(function(){
				console.log(response);
				//$('.form-body').html(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
				//}, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: '+textStatus+':'+jqXHR.responseText+'<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();		
		});	
	}
	
	var handleEdit = function () {
		$(document).on('click', '#edit', function(e) {
			var pageTitle = 'Surat Tugas Pengawasan';
			//var modul = 'modul.php?ke=surat_tugas_was';
			
			$('#form-user')[0].reset();		
			$('.modal-title').html('<i class="fa fa-edit"></i> Edit Data ' + pageTitle);
			$('.modal-loading').show();
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			$('.form-body').html('');
		
			var uid = $(this).attr('data-id');
			$('#uid').val(uid);
			$('#aksi').val('update');
			$('#submit').text('Update');
		
			$.ajax({
				url: modul+'&isian=edit',
				type: 'POST',
				timeout: 20000,
				data: 'uid='+uid,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeOut digunakan sebagai percobaan/testing saat online
				//setTimeout(function(){
				console.log(response);
				//$(".form-body").html(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
				//}, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: '+textStatus+': '+jqXHR.responseText+'<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();
		});
	}
	
	var handleValidasi = function () {
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				nomor: 	{ required: true },
				jenis: {required: true},
				kegiatan: {required: true }
			},
			messages: {
				nomor:	{ required: "Nomor harus di isi" },
				jenis:  { required: "Jenis kegiatan harus di isi!"},
				kegiatan:	{ required: "Uraian kegiatan harus di isi" }
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				var modul = 'modul.php?ke=surat_tugas_was';
				var data = new FormData($('#form-user')[0]);
				$('#submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
				$.ajax({
					url: modul,
					type: 'POST',
					data: data,
					dataType: 'json',
					contentType: false,
					processData: false,
					cache: false
				}).done(function(response, textStatus, jqXHR) {
					if (response.status == 'sukses') {
						$('#form-modal').modal('hide');
						refreshData();
						swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
					} else {
						$('#submit').text('Coba Lagi');
						$('.alert')
							.removeClass(' alert-danger alert-info')
							.addClass(' alert-warning').html('<span>'+response.pesan+'</span>').show();
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					$('#submit').attr('disabled','disabled');
					alert(jqXHR.responseText);
				});
			}
		});
		
		$('#form-user input').keypress(function (e) {
			if (e.which == 13) {
				if ($('.login-form').validate().form()) {
					$('.login-form').submit();
                }
                return false;
            }
        });
	}
	
	var handleHapus = function () {
		$(document).on('click', '#hapus', function(e) {
			//var modul = 'modul.php?ke=surat_tugas_was';
			var uid = $(this).attr('data-id');
			swalDelete(modul+'&lihat=hapus', uid);
			e.preventDefault();
		});		
	}

	var handleModalShow = function () {
		$('#form-modal').on('shown.bs.modal',function() {
			//$(".form-body").animate({ scrollTop: 0 }, \'slow\');
			$('.form-body').scrollTop(0);
			$('#nomor').focus();
		});
	}
		
	return {
		init: function () {
			handleInfoPegawai();
			handleBaru();
			handleEdit();
			handleValidasi();
			handleHapus();
			handleModalShow();
		}
	};
}();
