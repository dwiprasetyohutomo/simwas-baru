var kepangkatan = function () {
	
	var modul = 'modul.php?ke=kepangkatan';
	var pageTitle = 'Form Kepangkatan';
	
	var handleRecords = function () {
		
		tampilData();
		
		function tampilData() {
			$.ajax({
				type : 'GET',
				url : 'modul.php?ke=kepangkatan&lihat=tabelpangkat',
				async : false,
				dataType : 'html',
				success : function(data) {
					console.log(data);
					$('#tampilData').html(data);
				}
			});
		}
		
		
		/*
		// skrip menampilkan data menggunakan tipe data json
		function tampilData_json() {
			$.ajax({
				type : 'ajax',
				url : 'modul.php?ke=kepangkatan&lihat=tabelpangkatjson',
				async : false,
				dataType : 'json',
				success : function(data) {
					var html = '';
					var i;
					for(i=0; i<data.length; i++){
						html += '<tr>'+
								'<td>'+[i+1]+'.</td>'+
								'<td><center>'+data[i].golru+'</center></td>'+
								'<td></td>'+
								'<td></td>'+
								'<td></td>'+
								'<td></td>'+
								'<td></td>'+
								'</tr>';
					}
					$('#showdata').html(html);
				}
			});
		}
		*/		
	}
	
	var handleBaru = function () {		
		$(document).on('click', '#baru', function(e) {
			$('#form-user')[0].reset();

			$('.modal-title').html('Tambah ' + pageTitle);
			$('.modal-loading').show();
			//$('.modal-dialog').addClass('modal-wide');
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			$('.form-body').html('');

			$('#uid').val('');
			$('#aksi').val('pangkat_simpan');
			$('#submit').val('pangkat_simpan').text('Simpan');
			
			$.ajax({
				url: modul + '&pangkat=baru',
				type: 'POST',
				timeout: 20000,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});		
			e.preventDefault();
		});
	}

	var handleEdit = function () {		
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');
			$('#form-user')[0].reset();
			$('.modal-title').html('<i class="fa fa-edit"></i> ' + pageTitle);
			$('.modal-loading').show();
			// $('.modal-dialog').addClass('modal-wide');
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			// $(".form-body").wrap(\'<div class="scroller" style="height:70vh" data-always-visible="1" data-rail-visible="1" data-handle-color="#555" data-handle-size="10px"></div>\');
			$('.form-body').html('');		
			$('#uid').val(uid);
			$('#aksi').val('pangkat_update');
			$('#submit').val('pangkat_update').text('Update');
			
			$.ajax({
				url: modul+'&pangkat=edit',
				type: 'POST',
				timeout: 20000,
				data: 'uid='+uid,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeOut digunakan sebagai percobaan/testing saat online
				// setTimeout(function(){
				console.log(response);
				// $(".form-body").html(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
				// }, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: '+textStatus+': '+jqXHR.responseText+'<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();
		});
	}
	
	var handleValidasi = function (){
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				golru: 	{ required: true }
			},
			messages: {
				golru:	{ required: "Golru harus di isi" }
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				var data = new FormData($('#form-user')[0]);
				$('#submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
				$.ajax({
					url: modul,
					type: 'POST',
					data: data,
					dataType: 'json',
					contentType: false,
					processData: false,
					cache: false
				}).done(function(response, textStatus, jqXHR) {
					if (response.status == 'sukses') {
						$('#form-modal').modal('hide');
						// $('#tab_pangkat').html('');
						refreshData();
						// $('#tblPangkat').DataTable.ajax.reload();
						// tabel.ajax.reload();
						swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
					} else {
						$('#submit').text('Coba Lagi');
						$('.alert')
							.removeClass('alert-success alert-danger alert-info')
							.addClass('alert-warning')
							.html('<span>'+response.pesan+'</span>').show();
						$('.form-body').scrollTop(0);
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					// $('#submit').hide();
					alert(jqXHR.responseText);
				});
			}
		});
	
		$('.form-user input').keypress(function (e) {
		// $(document).on('keypress', '#form-user input', function(e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
				}
				return false;
			}
		});
	}
	
	var handleModalShow = function () {	
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			
			// $("#tahun").inputmask({ "mask": "9999" });
			// $("#lahirtgl").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			// $("#niplama").inputmask({ "mask": "999 999 999" });
			// $("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			// $('#spinmkt').spinner({value:1, min: 1, max: 10});
			$("#tmt").inputmask({ "mask": "99-99-9999" });
			$("#masath").inputmask({ "mask": "99" });
			$("#masabl").inputmask({ "mask": "99" });
			$("#gapok").inputmask({ alias: "currency", prefix: "", groupSeparator: ".", digits:0 });  // Rupiah
			$("#tglsk").inputmask({ "mask": "99-99-9999" });
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
			// $('#nip').focus();
		});
	}
	
	return {
		init: function () {
			handleRecords();
			handleBaru();
			handleEdit();
			handleValidasi();
			// handleHapus();
			handleModalShow();
		}
	};
}();
tampilData();