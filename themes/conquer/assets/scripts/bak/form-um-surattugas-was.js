var SuratTugasWas = function () {
	
	// Modal Form
	var modul = 'modul.php?ke=um_surattugas_was';
	var judul = 'Form Surat Tugas (Pengawasan)';
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit 	= modul+'&form=edit';
	var menuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	var importData	= '';
	var exportData	= modul+'&tabel=export';
	var emptyData	= modul+'&tabel=kosongkan';
	
	// Tabel Data
	var sourceUrl	 = modul+'&lihat=tabeldata';
	var destSelector = $('#tableAjax');
	
	var handleRecords = function () {
		
		var grid = new Datatable();
		var tabel = $("#tableAjax");		
		grid.init({
			src: tabel,
			dataTable: {
				//"pageLength": 5,
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'table-group-actions pull-right'>>r>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>",
				"ajax": { "url": sourceUrl },
				"columnDefs": [ {"targets": [0,2,3,4,5,7]} ], // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[6, "desc"]], // Kolom pertama yang di Sortir
			}
		});

		$('#tableAjax_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm');
				
		var $inputcari = $('div.dataTables_filter input');
		var $btncari = $('<span class="input-group-btn"><button class="btn btn-warning" type="button" style="margin: -2px 0 0 4px"><i class="fa fa-search"></i></button></span>');
		
		$inputcari.contents()
			.unwrap();
		
		$inputcari.attr('placeholder', 'Cari Surat Tugas / Kegiatan')
			.removeClass('input-small')
			.wrap('<div class="input-group input-group-sm"></div>')
			.after($btncari);

		$inputcari.off();
		
		$inputcari.on("keypress", function (e) {
			if (e.which == 13) {
				tabel.DataTable().search($inputcari.val()).draw();
			}
		});
		
		$btncari.on("click", function() {
			tabel.DataTable().search($inputcari.val()).draw();
		});
	}

	var handleDataBaru = function () {
		$('#menubaru').on('click', function(e) {
			modalForm.tambahData( menuBaru, 'simpan', judul );
			e.preventDefault();
		});
	}
	
	var handleDataEdit = function () {
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');
			modalForm.editData( menuEdit, uid, 'update', judul );
			e.preventDefault();
		});
	}
	
	var handleHapusData = function () {
		$(document).on("click", "#hapus", function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var uid = $(this).attr("data-id");
			var tabel = $("#tableAjax");
			modalForm.hapusData( menuHapus, uid, sourceUrl+'&nip='+nip, tabel, true );
			e.preventDefault();
		});
	}
	
	var handleValidate = function () {
		
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				jenis: { required: true },
				nomor: { required: true },
				kegiatan: { required: true },
				tglsrt: { required: true }
			},
			messages: {
				// nosk: { required: "Anda belum mengisi Nomor SK" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function () {
				var nip = hapusSpasi($('.pegawainip').text());
				var data = new FormData($('#form-user')[0]);
				var tabel = $("#tableAjax");
				modalForm.submitData(modul, data, sourceUrl+'&nip='+nip, tabel, true);
			}
		});
		
		$('#form-user input').keypress(function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleTableMenu = function () {
		
		$('#menurefresh').on('click',function(e) {
			e.preventDefault();
			var tabel = $('#tableAjax');
			refreshData(tabel);
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&form=importform', 'Import data kepangkatan (.csv)', 'pangkat_import');
			e.preventDefault();
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = exportData;
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';
			var tabel = $("#tableAjax");
			
			modalForm.kosongkanData(emptyData, judul1, judul2, 'Hapus dah', 'Tidak Jadi', '', tabel, true);
		});		
	}

	var handleSubmisi = function () {

		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&tabel=importupload',
				type: method,
				data: data,
				mimeType: 'multipart/form-data',
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(sourceUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});
	}
	
	var handleModalShow = function () {
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			
			// $('#tim').multiSelect();
			
			$("#spinlama").spinner({ value: 20 });
			// $("#spinpos").spinner({ value: 1 });
			// $("#tahun").inputmask({ "mask": "9999" });
			// $("#tglbrkt").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			$("#tglbrkt").inputmask({ "mask": "99-99-9999" });
			$("#tglkmbl").inputmask({ "mask": "99-99-9999" });
			$("#tglsrt").inputmask({ "mask": "99-99-9999" });
			$("#inspnip").inputmask({ "mask": "99999999 999999 9 999" });
			
			if ($("#anggaran").val() == "Lain") {
				$("#anggaran2")
					.attr("style","margin-top:15px")
					.show();
			} else {
				$("#anggaran2").hide();
			}
			
			var inspnama = $('#insp').find("option:selected").text();
			var inspnip = $('#insp').val();
			$("#inspnama").val(inspnama);
			$("#inspnip").val(inspnip);
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
		});		
	}
	
	var handleModalReport = function () {
		$(document).on('click', '#view', function(e) {
			e.preventDefault();
			var uid = $(this).attr('data-id');
			reportModal.viewReport('modul.php?ke=um_surattugas_was_rpt&uid='+uid, 'Hallo', true, true);			
		});
	}
	
	var handleTambahan = function () {
		
		$(document).on("change", "#anggaran", function() {
			var aggr = this.value;
			if ($(this).val() == "Lain") {
				$("#anggaran2").attr("style","margin-top:15px")
					.val('')
					.show()
					.focus();
			} else {
				$("#anggaran2").hide();
			}			
		});
		
		$(document).on("change", "#insp", function () {
			var inspnip = this.value;
			var inspnama = $(this).find("option:selected").text();
			$("#inspnama").val(inspnama)
			$("#inspnip").val(inspnip);
		});
		
	}
	
	return {
		init: function () {
			handleRecords();
			handleDataBaru();
			handleDataEdit();
			handleHapusData();
			handleTableMenu();
			handleValidate();
			handleSubmisi();
			handleModalShow();
			handleModalReport();
			handleTambahan();
		}
	};
}();