var Datatable = function () {
	
	var tableOptions;	// opsi utama
	var dataTable; 		// objek datatable
	var table; 			// objek aktual tabel jquery
	var tableContainer; // objek aktual tabel kontainer
	var tableWrapper;	// objek aktual tabel wrapper
	var tableInitialized = false;
	var ajaxParams = {};// Atur Filter
	var the;

    var countSelectedRecords = function () {
        var selected = $('tbody > tr > td:nth-child(1) input[type="checkbox"]:checked', table).size();
        var text = tableOptions.dataTable.language.customGroupActions;
        if (selected > 0) {
            $('.table-group-actions > span', tableWrapper).text(text.replace("_TOTAL_", selected));
        } else {
            $('.table-group-actions > span', tableWrapper).text("");
        }
    }

	return {
		
		init: function (options) {

			if (!$().dataTable) {
				return;
			}
			
			the = this;
				
			options = $.extend(true, {
				src: "",
                filterApplyAction: "filter",
                filterCancelAction: "filter_cancel",
                resetGroupActionInputOnSuccess: true,
				loadingMessage: 'Loading...',
				dataTable: {
					/*
					l - Length changing
					f - Filtering input
					t - The Table!
					i - Information
					p - Pagination
					r - pRocessing
					< and > - div elements
					<"#id" and > - div with an id
					<"class" and > - div with a class
					<"#id.class" and > - div with an id and class
					*/
					/*
					Penjelasan dari "dom"
					<div class="row">
						<div class="col-md-8 col-sm-12">
							{pagination}
							{length}
							{information}
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="table-group-actions pull-right">
							{filtering input}
							</div>
						</div>
						{processing}
					</div>
					<div class="table-scrollable">
						{table}
					</div>
					<div class="row">
						<div class="col-md-8 col-sm-12">
							{pagination}
							{length}
							{information}
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="table-group-actions pull-right">
							</div>
						</div>
						{processing}
					</div>
					*/					
					//"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>f>r><'table-scrollable't><'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>", // datatable layout
					//"dom": "<\'row\'<\'col-md-8 col-sm-12\'pli><\'col-md-4 col-sm-12\'<\'table-group-actions pull-right\'f>>r><\'tableAjax-scrollable\'t><\'row\'<\'col-md-8 col-sm-12\'pli><\'col-md-4 col-sm-12\'<\'table-group-actions pull-right\'>>>",
					"responsive": true,
					"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'f>>r><'tableAjax-wraptext't><'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>>",
					"pageLength": 5, // default records per page					
					//"pageLength": displayLength == null ? 5 : displayLength,
					//"stateSave": true,
					"lengthMenu": [
						[5, 10, 20, 50, -1],
						[5, 10, 20, 50, "Semua"]
					], // change per page values here
					"language": { // language settings
						// metronic spesific
						"customGroupActions": "_TOTAL_ records selected:  ",
						// "customAjaxRequestGeneralError": "Tidak dapat menyelesaikan permintaan, adapun kesalahan yang mungkin terjadi diantaranya:<br><ul><li>\"ajax\": \"url\": ... salah ketik / tidak ada (jQuery).</li><li>\"columnDefs\": \"targets\":... tidak sesuai (jQuery).</li><li>$columns = array(...) belum ditentukan (PHP).</li></ul>",
						// data tables spesific
						"lengthMenu": "<span class=\'seperator\'>|</span><span class=\'hidden-480\'>Tampil</span> _MENU_",
						"info": "<span class=\'seperator\'></span><span class=\'hidden-480\'>data (total _TOTAL_)</span>",
						"loadingRecords": "<span class=\"glyphicon glyphicon-refresh glyphicon-refresh-animate\"></span> Sedang mengambil data...",
                        "infoEmpty": "Tidak ada informasi untuk ditampilkan", //"No records found to show",
                        "emptyTable": "Tidak ada data", //"No data available in table",
                        //"zeroRecords": "No matching records found",
						"zeroRecords": "Hasil pencarian tidak menemukan apa-apa",
						"search": "",
                        "paginate": {"previous": "Sebelum","next": "Lanjut","last": "Akhir","first": "Awal","page": "","pageOf": "dari"}
					},
					"orderCellsTop": true,
                    "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
						'orderable': false,
						'targets': [0]
						//'searchable': false
                    }],
					/*
					// nomor otomatis
					"rowCallback": function (row, data, iDisplayIndex) {
						var index = this.fnPagingInfo().iPage * this.fnPagingInfo().iLength + (iDisplayIndex + 1);
						$('td:eq(0)',row).html('<div style="text-align:right">' + index + '.</div>');
					},
					*/
					/* simple, simple_number, full, full_number, first_last_number */
					"pagingType": "bootstrap_extended", // pagination type(bootstrap, bootstrap_full_number or bootstrap_extended)
					"autoWidth": false, // disable fixed width and enable fluid table
					"processing": false, // enable/disable display message box on record load
					"serverSide": true, // enable/disable server side ajax loading
					
					"ajax": { // define ajax settings
						"url": "", // Ajax URL
						"type": "POST", // Tipe permintaan
						"timeout": 20000,
						"data": function (data) { // Tambahkan paremeter permintaan sebelum kirim
							$.each(ajaxParams, function (key, value) {
								data[key] = value;
							});							
							App.blockUI({
								message: tableOptions.loadingMessage,
								target: tableContainer,
								overlayColor: 'none',
								cenrerY: true,
								boxed: true
							});
						},
						"dataSrc": function (res) { // Manipulate data yang dikembalikan server
							if (res.customActionMessage) {
								App.alert({
									type: (res.customActionStatus == 'OK' ? 'success' : 'danger'),
									icon: (res.customActionStatus == 'OK' ? 'check' : 'warning'),
									message: res.customActionMessage,
									container: tableWrapper,
									place: 'prepend'
								});
							}

                            if (res.customActionStatus) {
                                if (tableOptions.resetGroupActionInputOnSuccess) {
                                    $('.table-group-action-input', tableWrapper).val("");
                                }
                            }

                            if ($('.group-checkable', table).size() === 1) {
                                $('.group-checkable', table).attr("checked", false);
                                $.uniform.update($('.group-checkable', table));
                            }

                            if (tableOptions.onSuccess) {
                                tableOptions.onSuccess.call(undefined, the);
                            }

							App.unblockUI(tableContainer);
							
							return res.data;
						},
						"error": function (xhr, error, thrown) { // tangani beberapa koneksi error
							if (tableOptions.onError) {
								tableOptions.onError.call(undefined, the);
							}
							
							App.alert({
								type: 'danger',
								icon: 'warning',
								// message: tableOptions.dataTable.language.customAjaxRequestGeneralError,
								message: xhr.responseText,
								container: tableWrapper,
								place: 'prepend'
							});
							
							App.unblockUI(tableContainer);
						}
					},
					"drawCallback": function(oSettings) { // Jalankan beberapa kode pada saat table di tulis ulang
						if (tableInitialized === false) { // check if table has been initialized
                            tableInitialized = true; // set table initialized
                            table.show(); // display table
                        }
						App.initUniform($('input[type="checkbox"]', table));
						countSelectedRecords(); // reset selected records indicator
					}
				}
			}, options);

			tableOptions = options;
			
			// create table's jquery object
			table = $(options.src);
			tableContainer = table.parents(".table-container");

			// apply the special class that used to restyle the default datatable
			var tmp = $.fn.dataTableExt.oStdClasses;

			$.fn.dataTableExt.oStdClasses.sWrapper = $.fn.dataTableExt.oStdClasses.sWrapper + " dataTables_extended_wrapper";
            $.fn.dataTableExt.oStdClasses.sFilterInput = "form-control input-small input-sm input-inline";
            $.fn.dataTableExt.oStdClasses.sLengthSelect = "form-control input-xsmall input-sm input-inline";

			// initialize a datatable
			dataTable = table.DataTable(options.dataTable);

			// revert back to default
            $.fn.dataTableExt.oStdClasses.sWrapper = tmp.sWrapper;
            $.fn.dataTableExt.oStdClasses.sFilterInput = tmp.sFilterInput;
            $.fn.dataTableExt.oStdClasses.sLengthSelect = tmp.sLengthSelect;

			// get table wrapper
			tableWrapper = table.parents('.dataTables_wrapper');
			
			// ===============================================================
			// Tambahan Sendiri
			// ===============================================================
			
			// Panjangkan DataTable Length Menu
			//tableWrapper.find('.dataTables_length select').removeClass('input-xsmall').addClass('input-xsmall input-sm');
			
			// Filter/Pencarian berdasarkan Key Enter
			var inputcari = tableWrapper.find('.dataTables_filter input');
			var btncari = $('<span class="input-group-btn"><button class="btn btn-default" type="button" style="margin: -2px 0 0 -1px"><i class="fa fa-search"></i></button></span>');
			inputcari.contents().unwrap();
			inputcari
				.attr('placeholder','Pencarian...')
				// .removeClass('input-small')
				.wrap('<div class="input-group input-group-sm"></div>')
				.after(btncari);
			inputcari.off();
			inputcari.on("keypress", function (e) {
				if(e.which == 13) {
					table.DataTable().search(inputcari.val()).draw();
				}
			});
			btncari.on("click", function() {
				table.DataTable().search(inputcari.val()).draw();
			});
			// ===============================================================
						
			// build table group actions panel
            if ($('.table-actions-wrapper', tableContainer).size() === 1) {
                $('.table-group-actions', tableWrapper).html($('.table-actions-wrapper', tableContainer).html()); // place the panel inside the wrapper
                $('.table-actions-wrapper', tableContainer).remove(); // remove the template container
            }

            // handle filter submit button click
            table.on('click', '.filter-submit', function (e) {
                e.preventDefault();
                the.submitFilter();
            });

            // handle filter cancel button click
            table.on('click', '.filter-cancel', function (e) {
                e.preventDefault();
                the.resetFilter();
            });			
		},

        submitFilter: function () {
            the.setAjaxParam("action", tableOptions.filterApplyAction);

            // get all typeable inputs
            $('textarea.form-filter, select.form-filter, input.form-filter:not([type="radio"],[type="checkbox"])', table).each(function () {
                the.setAjaxParam($(this).attr("name"), $(this).val());
            });

            // get all checkboxes
            $('input.form-filter[type="checkbox"]:checked', table).each(function () {
                the.addAjaxParam($(this).attr("name"), $(this).val());
            });

            // get all radio buttons
            $('input.form-filter[type="radio"]:checked', table).each(function () {
                the.setAjaxParam($(this).attr("name"), $(this).val());
            });

            dataTable.ajax.reload();
        },

        resetFilter: function () {
            $('textarea.form-filter, select.form-filter, input.form-filter', table).each(function () {
                $(this).val("");
            });
            $('input.form-filter[type="checkbox"]', table).each(function () {
                $(this).attr("checked", false);
            });
            the.clearAjaxParams();
            the.addAjaxParam("action", tableOptions.filterCancelAction);
            dataTable.ajax.reload();
        },

        setAjaxParam: function (name, value) {
            ajaxParams[name] = value;
        },

        addAjaxParam: function (name, value) {
            if (!ajaxParams[name]) {
                ajaxParams[name] = [];
            }

            skip = false;
            for (var i = 0; i < (ajaxParams[name]).length; i++) { // check for duplicates
                if (ajaxParams[name][i] === value) {
                    skip = true;
                }
            }

            if (skip === false) {
                ajaxParams[name].push(value);
            }
        },

        clearAjaxParams: function (name, value) {
            ajaxParams = {};
        },
		
        getDataTable: function () {
            return dataTable;
        },
		
		getTableWrapper: function () {
			return tableWrapper;
		}
	};
};