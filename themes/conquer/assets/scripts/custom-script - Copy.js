var DataTableReload = function( refID ) {
	refID.DataTable().ajax.reload();
};

var refreshData = function ( refID ) {
	refID.DataTable().ajax.reload();
}

var loadData = function( url, refID, data={} ) {
	$.ajax({
		url: url,
		type: 'POST',
		timeout: 20000,
		data: data,
		dataType: 'html',
		beforeSend: function() {
			refID.html('<div align="center" class="loader2"><img src="img/loading/loading-orange.gif"><br><span>LOADING</span></div>');
		},
		success: function(response) {
			// setTimeout(function() {
			refID.html(response);	
			// }, 1500);			
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(xhr.responseText);
		}
	});
};

var formModal = {
	tambahData: function( url, submitval, judul = 'Form Tambah Data', data={}, scroll=true ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		submitval: value dari [button submit]
		judul: Caption Modal
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		scroll: isi modal form bisa di scroll dan tidak memanjang
		========================================================================================*/
		var modalID		= $('#form-modal');
		var theTitle	= modalID.find('.modal-title');
		
		var theForm		= $('#form-user');
		var theSpin		= theForm.find('.modal-loading');
		var theBody		= theForm.find('.form-body');
		var theUid		= theForm.find('#uid');
		var theSubmit	= theForm.find(':submit');
				
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}		
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			timeout: 20000,
			dataType: 'html',
			cache: false,
			beforeSend: function() {
				theForm[0].reset();
				theTitle.html(judul);
				theBody.html('');
				theUid.val('');
				theSubmit.val(submitval).text('Simpan');
				theSpin.show();
			},
			success: function(response) {
				theSpin.hide();
				theBody.html(response);
			},
			error: function(xhr, ajaxOptions, thrownError) {
				theSpin.hide();
				alert(xhr.responseText);
			}
		})
		return false;
	},
	editData: function ( url, uid, submitval, judul = 'Form Edit Data', data={}, scroll=true ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		uid: pilihan primary key / kata kunci penghubung ke database
		submitval: value dari [button submit]
		judul: Caption Modal
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		scroll: isi modal form bisa di scroll dan tidak memanjang
		========================================================================================*/
		var modalID		= $('#form-modal');
		var theTitle	= modalID.find('.modal-title');
		
		var theForm		= $('#form-user');
		var theSpin		= theForm.find('.modal-loading');
		var theBody		= theForm.find('.form-body');
		var theUid		= theForm.find('#uid');
		var theSubmit	= theForm.find(':submit');
				
		if( scroll ) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}		
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'html',
			cache: false,
			beforeSend: function() {
				theForm[0].reset();
				theTitle.html(judul);
				theBody.html('');
				theUid.val(uid);
				theSubmit.val(submitval).text('Perbaharui');
				theSpin.show();
			},
			success: function(response) {
				theBody.html(response);
				theSpin.hide();
			},
			error: function(xhr, ajaxOptions, thrownError) {
				theSpin.hide();
				alert(xhr.responseText);
			}
		})
		return false;
	},
	hapusData: function ( url, data, bDataTable = true, refID ) {
		/*========================================================================================
		url			: lokasi skrip php dijalankan
		data		: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		bDataTable	: apakah menggunakan DataTable
		refID		: [table ID = DataTable] atau [div ID = ajaxBiasa] sebagai isi dari response
		========================================================================================*/
		swal({
			title: "Yakin ingin menghapus data ini?",
			html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
			type: "question",
			showCancelButton: true,
			confirmButtonColor: "#AA0000",
			confirmButtonText: "Hapus",
			cancelButtonColor: "#F0AD4E",
			cancelButtonText: "Jangan"
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: url,
					type: "POST",
					timeout: 20000,
					data: data,
					dataType: "json",
					processData: true,
					cache: false,
					success: function(response) {
						console.log(response);
						if(response.status == "sukses") {
							// swal({ title: "Data telah terhapus!", text: response.pesan, type: "success", showConfirmButton: false, timer: 1500 });
							const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
							toast({ type: 'success', title: 'Data telah terhapus' })
							if (bDataTable) {
								refID.DataTable().ajax.reload();
							} else {
								// tampilData(loadurl, refreshID);
							}
						}
					},
					error: function(xhr, ajaxOptions, thrownError) {
						alert(xhr.responseText)
					}
				});
			}
		})
		return false;
	},
	submitData: function ( url, data, bDataTable = true, refID ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		bDataTable: apakah menggunakan DataTable
		refID: ID untuk reload data contoh: DataTable = $('#tableAjax')
		========================================================================================*/
		var theModal	= $('#form-modal');
		var theErr		= theModal.find('.alert-danger');
		
		var theForm		= $('#form-user');
		var theBody		= theForm.find('.form-body');
		var theSubmit	= theForm.find(':submit');

		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			cache: false,
			beforeSend: function(e) {
				theErr.hide();
				if(e && e.overrideMimeType) { e.overrideMimeType("application/json;charset=UTF-8"); }
				theSubmit.addClass('disabled').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			},
			success: function(response) {
				if (response.status == 'sukses') {
					theSubmit.removeClass('disabled').text('Selesai...');
					theModal.modal('hide');
					if (bDataTable == true) {
						refID.DataTable().ajax.reload();
					}
					swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
				}
				if (response.status == 'gagal') {
					theSubmit.attr('disabled').text('Error!');
					theErr
						.removeClass('alert-success alert-danger alert-info')
						.addClass('alert-warning')
						.html('<span>'+response.pesan+'</span>')
						.show();
					theBody.scrollTop(0);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				theSubmit.hide();
				alert(xhr.responseText);
			}
		})
	}
}

var simpleModal = {
	loadData: function( url, modalId, data, scroll=true ) {	
		
		var theSpin = modalId.find('.modal-loading');
		var theBody = modalId.find('.form-body');
		
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}		
		$.ajax({
			url: url,
			type: 'GET',
			timeout: 20000,
			data: data,
			// processData: true,
			// cache: false,
			dataType: 'html'
		}).done(function(response, textStatus, jqXHR) {
			theSpin.hide();
			theBody.html(response).fadeIn();
		}).fail(function(jqXHR, textStatus, errorThrown) {
			alert(jqXHR.responseText);
		});
	},
	tambahData: function( url, modalID, theForm, submitval, data={}, scroll=true ) {

		var theSpin 	= modalID.find('.modal-loading');
		var theBody 	= modalID.find('.form-body');
		var theUid		= theForm.find('input[name=uid]');
		var theSubmit	= theForm.find(':submit');
		
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}
		
		$.ajax({
			url: url,
			type: 'POST',
			data: data,
			dataType: 'html',
			beforeSend: function() {
				theForm[0].reset();
				theBody.html('');
				theUid.val('');
				theSubmit.val(submitval).text('Simpan');
				theSpin.show();
			},
			success: function( response ){
				theSpin.hide();
				theBody.html(response);				
			},
			error: function(xhr, ajaxOption, thrownError) {
				theSpin.hide();
				alert(xhr.responseText);
			}
		});
	},
	editData: function( url, modalID, theForm, uid, submitval, data, scroll=true ) {
		
		var theSpin 	= modalID.find('.modal-loading');
		
		var theSpin		= theForm.find('.modal-loading');
		var theBody 	= theForm.find('.form-body');		
		var theUid		= theForm.find('input[name=uid]');
		var theSubmit 	= theForm.find(':submit');
		
		if(scroll) {
			theBody.attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
		} else {
			theBody.removeAttr('style');
		}
		
		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'html',
			beforeSend: function() {
				theForm[0].reset();
				theBody.html('');
				theUid.val(uid);
				theSubmit.val(submitval).text('Perbaharui');
				theSpin.show();
			},
			success: function( response ){
				theSpin.hide();
				theBody.html(response);
			},
			error: function(xhr, ajaxOption, thrownError) {
				theSpin.hide();
				alert(xhr.responseText);
			}
		});
	},
	hapusData: function( url, refID, data, bDataTable = true ) {
		var tid  = $(this).attr('data-timid');			
			swal({
				title: "Yakin ingin menghapus data ini?",
				html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
				type: "question",
				showCancelButton: true,
				confirmButtonColor: "#AA0000",
				confirmButtonText: "Hapus",
				cancelButtonColor: "#F0AD4E",
				cancelButtonText: "Jangan"
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: url,
						type: 'POST',
						dataType: "json",
						data: data,
						success: function(response) {
							// console.log(response);
							if(response.status == "sukses") {
								// swal({ title: "Data telah terhapus!", text: response.pesan, type: "success", showConfirmButton: false, timer: 1500 });
								const toast = swal.mixin({ toast: true, position: 'top', showConfirmButton: false, timer: 3000 });
								toast({ type: 'success', title: 'Data telah terhapus' })
								
								if (bDataTable) {
									refID.DataTable().ajax.reload();
								} else {
									loadData( url, refID, data );
								}
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(xhr.responseText)
						}
					});
				}
			});
		return false;
	},
	submitData: function ( url, theModal, theForm, data, bDataTable = true, refID ) {
		/*========================================================================================
		url: lokasi skrip php dijalankan
		data: penyertaan post [nama dan value] contoh: data = { name: value, dst... }
		bDataTable: apakah menggunakan DataTable
		refID: ID untuk reload data contoh: DataTable = $('#tableAjax')
		========================================================================================*/
		var theErr		= theModal.find('.alert-danger');

		var theBody		= theForm.find('.form-body');
		var theSubmit	= theForm.find(':submit');

		$.ajax({
			url: url,
			type: 'POST',
			timeout: 20000,
			data: data,
			dataType: 'json',
			processData: false,
			contentType: false,
			cache: false,
			beforeSend: function(e) {
				theErr.hide();
				if(e && e.overrideMimeType) { e.overrideMimeType("application/json;charset=UTF-8"); }
				theSubmit.addClass('disabled').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			},
			success: function(response) {
				if (response.status == 'sukses') {
					theSubmit.removeClass('disabled').text('Selesai...');
					theModal.modal('hide');
					if (bDataTable == true) {
						refID.DataTable().ajax.reload();
					}
					swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
				}
				if (response.status == 'gagal') {
					theSubmit.attr('disabled').text('Error!');
					theErr
						.removeClass('alert-success alert-danger alert-info')
						.addClass('alert-warning')
						.html('<span>'+response.pesan+'</span>')
						.show();
					theBody.scrollTop(0);
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				theSubmit.hide();
				alert(xhr.responseText);
			}
		})
	}
}