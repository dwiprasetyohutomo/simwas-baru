var umst = function () {

    // var modul = 'modul.php?ke=um_pegawai';
    var modul = window.location.href;
    var pageTitle = 'Data Surat Tugas';

    // CRUD
    var menuBaru = modul + '&st=baru';
    var menuSTBaru = modul + '&was=stbaru';
    var menuDetailST = modul + '&was=detailst';
    var menuEdit = modul + '&was=edit';
    var menuHapus = modul + '&was=hapus';

    // Table Data
    var srcUrl = modul + '&was=tabeldata';
    var destSel = $("#tableAjax");

    var handleRecords = function () {
        var grid = new Datatable();
        var tabel = $("#tableAjax");
        grid.init({
            src: tabel,
            dataTable: {
                "pageLength": 20,
                "dom": "<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
                    "<'tableAjax-scrollable't>" +
                    "<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'table-group-actions pull-right'>>r>",
                "ajax": {
                    "url": modul + "&was=tabeldata"
                },
                // "ordering": false
                "columnDefs": [{
                    "targets": [0, 5, 6, 7, 8, 9, 11]
                }], // Kolom yang dimatikan Fungsi Sortir nya
                "order": [[1, "asc"], [3, "asc"]], // Kolom yang di Sortir
            }
        });

        /*
        $('#tableAjax_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm'); // Besarkan Filter
        var inputcari 	= $('div.dataTables_filter input');
        var btncari 	= $('<span class="input-group-btn"><button class="btn btn-warning" type="button" style="margin-top:-1px"><i class="fa fa-search"></i></button></span>');
        inputcari.contents().unwrap();
        inputcari.attr('placeholder', 'Cari nip/nama/kelahiran...').removeClass('input-small').wrap('<div class="input-group input-group-sm"></div>').after(btncari);
        inputcari.off();
        inputcari.on("keypress", function (e) {
        	if (e.which == 13) {
        		tabel.DataTable().search(inputcari.val()).draw();
        	}
        });
        btncari.on("click", function() {
        	tabel.DataTable().search(inputcari.val()).draw();
        });
        */

        // == CUSTOM FILTERING ==
        grid.getTableWrapper().on("click", ".table-group-action-submit", function (e) { // SUBMIT
            e.preventDefault();
            $("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
                grid.setAjaxParam($(this).attr("name"), $(this).val());
            });
            grid.getDataTable().ajax.reload();
        });
        grid.getTableWrapper().on("click", ".table-group-action-reset", function (e) { // RESET
            e.preventDefault();
            $("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
                $(this).val("");
            });
            grid.clearAjaxParams();
            //$("#form-filter")[0].reset(); // Optional
            grid.getDataTable().ajax.reload();
        });

        // Set Status Filter
        var filterby = $('#status');
        filterby.val('Y');
        grid.clearAjaxParams();
        grid.setAjaxParam(filterby.attr("name"), filterby.val());
        grid.getDataTable().ajax.reload();
    }

    var handleSuratTugasBaru = function () {

        $(document).on('click', '#stbaru', function (e) {

            $('#form-user')[0].reset();

            $('.modal-title').html('Form Tambah ' + pageTitle);
            $('.modal-loading').show();
            //$('.modal-dialog').addClass('modal-wide');
            $('.form-body').attr('style', 'max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
            $('.form-body').html('');

            var uid = $(this).attr('data-id');
            $('#uid').val(uid);
            var jenis = $(this).attr('jenis-id');
            $('#jenis').val(jenis);
            $('#aksi').val('was_stsimpan');
            $('#submit').val('was_stsimpan').text('Simpan');

            $.ajax({
                url: menuSTBaru,
                type: 'POST',
                timeout: 20000,
                data: 'uid=' + uid + '&jenis=' + jenis,
                dataType: 'html'
            }).done(function (response, textStatus, jqXHR) {
                console.log(response);
                $('.form-body').html(response);
                $('.modal-loading').hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
                $('.modal-loading').hide();
                $('#submit').hide();
            });
            e.preventDefault();
        });
    }

    var handleDetailST = function () {

        $(document).on('click', '#detailst', function (e) {

            $('#form-user')[0].reset();

            $('.modal-title').html('Detail ST ' + pageTitle);
            $('.modal-loading').show();
            //$('.modal-dialog').addClass('modal-wide');
            $('.form-body').attr('style', 'max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
            $('.form-body').html('');

            var uid = $(this).attr('data-id');
            $('#uid').val(uid);
            var jenis = $(this).attr('jenis-id');
            $('#jenis').val(jenis);
            $('#aksi').val('was_stsimpan');
            $('#submit').val('was_stsimpan').text('Simpan');
            document.getElementById('submit').style = 'display:none;';

            $.ajax({
                url: menuDetailST,
                type: 'POST',
                timeout: 20000,
                data: 'uid=' + uid,
                dataType: 'html'
            }).done(function (response, textStatus, jqXHR) {
                console.log(response);
                $('.form-body').html(response);
                $('.modal-loading').hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
                $('.modal-loading').hide();
                $('#submit').hide();
            });
            e.preventDefault();
        });
    }

    var handleDataBaru = function () {

        $(document).on('click', '#baru', function (e) {

            $('#form-user')[0].reset();

            $('.modal-title').html('Form Tambah ' + pageTitle);
            $('.modal-loading').show();
            //$('.modal-dialog').addClass('modal-wide');
            $('.form-body').attr('style', 'max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
            $('.form-body').html('');

            $('#uid').val('');
            $('#aksi').val('st_simpan');
            $('#submit').val('st_simpan').text('Simpan Data ST');

            $.ajax({
                url: menuBaru,
                type: 'POST',
                timeout: 20000,
                dataType: 'html'
            }).done(function (response, textStatus, jqXHR) {
                console.log(response);
                $('.form-body').html(response);
                $('.modal-loading').hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
                $('.modal-loading').hide();
                $('#submit').hide();
            });
            e.preventDefault();
        });
    }

    var handleDataEdit = function () {

        $(document).on('click', '#edit', function (e) {

            $('#form-user')[0].reset();

            $('.modal-title').html('<i class="fa fa-edit"></i> ' + pageTitle);
            $('.modal-loading').show();
            //$('.modal-dialog').addClass('modal-wide');
            $('.form-body').attr('style', 'max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
            $('.form-body').html('');

            var uid = $(this).attr('data-id');
            $('#uid').val(uid);
            $('#aksi').val('was_update');
            $('#submit').val('was_update').text('Update');

            $.ajax({
                url: menuEdit,
                type: 'POST',
                timeout: 20000,
                data: 'uid=' + uid,
                dataType: 'html'
            }).done(function (response, textStatus, jqXHR) {
                // setTimeOut digunakan sebagai percobaan/testing saat online
                // setTimeout(function(){
                // console.log(response);
                $('.form-body').html(response);
                $('.modal-loading').hide();
                // }, 2000);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
                $('.modal-loading').hide();
                $('#submit').hide();
            });
            e.preventDefault();
        });
    }

    var handleDataHapus = function () {
        $(document).on('click', '#hapus', function (e) {
            var uid = $(this).attr('data-id');
            swal({
                title: 'Yakin ingin menghapus data ini?',
                html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: "#AA0000",
                confirmButtonText: "Hapus",
                cancelButtonColor: "#F0AD4E",
                cancelButtonText: "Jangan"
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: menuHapus,
                        type: "POST",
                        timeout: 20000,
                        data: 'uid=' + uid,
                        dataType: "json",
                        success: function (response) {
                            if (response.status == "sukses") {
                                swal({
                                    title: "Data telah terhapus!",
                                    text: response.pesan,
                                    type: "success",
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                DataTableReload($('#tableAjax'));
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText)
                        }
                    });
                }
            })
            e.preventDefault();
        });
    }

    var handleValidasi = function () {
        $('#form-user').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                nip: {
                    required: true
                },
                nama: {
                    required: true
                }
            },
            messages: {
                nip: {
                    required: "Nomor harus di isi"
                },
                nama: {
                    required: "Nama harus di isi"
                },
            },
            invalidHandler: function (event, validator) {
                $('.alert-danger', $('#form-user')).show();
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error')
            },
            success: function (label) {
                label.closest(".form-group").removeClass("has-error");
                label.remove();
            },
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').size() > 0) {
                    error.insertAfter(element.parent('.input-group'));
                } else if (element.attr('data-error-container')) {
                    error.appendTo(element.attr('data-error-container'));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            submitHandler: function (form) {
                var data = new FormData($('#form-user')[0]);
                $('#submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
                $.ajax({
                    url: modul,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    cache: false
                }).done(function (response, textStatus, jqXHR) {
                    if (response.status == 'sukses') {
                        $('#form-modal').modal('hide');
                        DataTableReload($('#tableAjax'));
                        swal({
                            type: 'success',
                            title: 'Sukses!',
                            text: response.pesan,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        $('#submit').text('Coba Lagi');
                        $('.alert')
                            .removeClass('alert-success alert-danger alert-info')
                            .addClass('alert-warning')
                            .html('<button class="close" data-close="alert"></button><span>' + response.pesan + '</span>')
                            .show();
                        $('.form-body').scrollTop(0);
                    }
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    $('#submit').hide();
                    alert(jqXHR.responseText);
                });
            }
        });

        $('#form-user .form-control').keypress(function (e) {
            if (e.which == 13) {
                if ($('#form-user').validate().form()) {
                    $('#form-user').submit();
                }
                return false;
            }
        });
    }

    var handleSuratTugasBaru = function () {

        $(document).on('click', '#stbaru', function (e) {

            $('#form-user')[0].reset();

            $('.modal-title').html('Form Tambah ' + pageTitle);
            $('.modal-loading').show();
            //$('.modal-dialog').addClass('modal-wide');
            $('.form-body').attr('style', 'max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
            $('.form-body').html('');

            var uid = $(this).attr('data-id');
            $('#uid').val(uid);
            var jenis = $(this).attr('jenis-id');
            $('#jenis').val(jenis);
            $('#aksi').val('was_stsimpan');
            $('#submit').val('was_stsimpan').text('Simpan');

            $.ajax({
                url: menuSTBaru,
                type: 'POST',
                timeout: 20000,
                data: 'uid=' + uid + '&jenis=' + jenis,
                dataType: 'html'
            }).done(function (response, textStatus, jqXHR) {
                console.log(response);
                $('.form-body').html(response);
                $('.modal-loading').hide();
            }).fail(function (jqXHR, textStatus, errorThrown) {
                $('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
                $('.modal-loading').hide();
                $('#submit').hide();
            });
            e.preventDefault();
        });
    }

    var handleModalShow = function () {

        $(document).on("ajaxComplete", function (e) {
            e.preventDefault();
            $("#lahirtgl").inputmask({
                "mask": "99-99-9999",
                "placeholder": "dd-mm-yyyy"
            });
            $("#niplama").inputmask({
                "mask": "999 999 999"
            });
            $("#nip").inputmask({
                "mask": "99999999 999999 9 999"
            });

        });

        $('#form-modal').on('shown.bs.modal', function () {
            $('.form-body').scrollTop(0);
            $('#nip').focus();
        });
    }

    var handleExport = function () {
        $(document).on('click', '#export', function (e) {
            swal({
                title: "Export tabel ke format CSV?",
                text: "",
                type: "question",
                showCancelButton: true,
                confirmButtonText: "Ya",
                cancelButtonText: "Batal",
                cancelButtonColor: "#d33"
            }).then(function (result) {
                if (result.value) window.open("'.$modul.'&stgswas=export", "_blank");
            });
            e.preventDefault();
        });
    }

    var handleInspekturInfo = function () {
        $(document).on("change", "#insp", function () {
            var inspnama = $(this).find("option:selected").text();
            var inspnip = this.value;
            $("#insp_nama").val(inspnama)
            $("#insp_nip").val(inspnip);
            /*
            App.blockUI({ message: 'Mohon bersabar...', target: '.form-body', overlayColor: 'none', cenrerY: true, boxed: true });
            $.ajax({
            	url: modul+'&personil=info',
            	type: 'POST',
            	timeout: 20000,
            	dataType: 'json',
            	//contentType: 'application/x-www-form-urlencoded',
            	async: false,
            	data: { nip: nip },
            	success: function(data) {
            		// Hilangkan setTimeout saat sudah online
            		//setTimeout(function() {
            			App.unblockUI('.form-body'); // Tutup loading
            			userData = data;
            			$("#nama").val(userData.nama);
            			$("#nip").val(userData.nip);
            		//}, 500);
            	}
            });
            */
        });
    }

    var handleFormat = function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('format2').style = 'display:block;';
                document.getElementById('format1').style = 'display:none;';
            } else {
                document.getElementById('format1').style = 'display:block;';
                document.getElementById('format2').style = 'display:none;';
            }
        });
    }
    var handleDasar = function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('dsr2b').style = 'display:block;';
                document.getElementById('dsr2a').style = 'display:none;';
                document.getElementById('dsr3b').style = 'display:block;';
                document.getElementById('dsr3a').style = 'display:none;';
            } else {
                document.getElementById('dsr2b').style = 'display:none;';
                document.getElementById('dsr2a').style = 'display:block;';
                document.getElementById('dsr3b').style = 'display:none;';
                document.getElementById('dsr3a').style = 'display:block;';
            }
        });
    }
    /*
     awal isisan pegawai dropdown atau text
    */
    var handlePeg0= function () {
        $(document).on('change', "#peg0", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota0').style = 'display:block;';
            } else {
                document.getElementById('dataanggota0').style = 'display:none;';
            }
        });
    }
   var handlePeg1= function () {
        $(document).on('change', "#peg1", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota1').style = 'display:block;';
            } else {
                document.getElementById('dataanggota1').style = 'display:none;';
            }
        });
    }
    var handlePeg2= function () {
        $(document).on('change', "#peg2", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota2').style = 'display:block;';
            } else {
                document.getElementById('dataanggota2').style = 'display:none;';
            }
        });
    }
    var handlePeg3= function () {
        $(document).on('change', "#peg3", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota3').style = 'display:block;';
            } else {
                document.getElementById('dataanggota3').style = 'display:none;';
            }
        });
    }
    var handlePeg4= function () {
        $(document).on('change', "#peg4", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota4').style = 'display:block;';
            } else {
                document.getElementById('dataanggota4').style = 'display:none;';
            }
        });
    }
    var handlePeg5= function () {
        $(document).on('change', "#peg5", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota5').style = 'display:block;';
            } else {
                document.getElementById('dataanggota5').style = 'display:none;';
            }
        });
    }
    var handlePeg6= function () {
        $(document).on('change', "#peg6", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota6').style = 'display:block;';
            } else {
                document.getElementById('dataanggota6').style = 'display:none;';
            }
        });
    }
    var handlePeg7= function () {
        $(document).on('change', "#peg7", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota7').style = 'display:block;';
            } else {
                document.getElementById('dataanggota7').style = 'display:none;';
            }
        });
    }
    var handlePeg8= function () {
        $(document).on('change', "#peg8", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota8').style = 'display:block;';
            } else {
                document.getElementById('dataanggota8').style = 'display:none;';
            }
        });
    }
    var handlePeg9= function () {
        $(document).on('change', "#peg9", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota9').style = 'display:block;';
            } else {
                document.getElementById('dataanggota9').style = 'display:none;';
            }
        });
    }
    var handlePeg10= function () {
        $(document).on('change', "#peg10", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota10').style = 'display:block;';
            } else {
                document.getElementById('dataanggota10').style = 'display:none;';
            }
        });
    }
    var handlePeg11= function () {
        $(document).on('change', "#peg11", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota11').style = 'display:block;';
            } else {
                document.getElementById('dataanggota11').style = 'display:none;';
            }
        });
    }
    //kode pegawai 11
    var handlePeg12= function () {
        $(document).on('change', "#peg12", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota12').style = 'display:block;';
            } else {
                document.getElementById('dataanggota12').style = 'display:none;';
            }
        });
    }
    var handlePeg13= function () {
        $(document).on('change', "#peg13", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota13').style = 'display:block;';
            } else {
                document.getElementById('dataanggota13').style = 'display:none;';
            }
        });
    }
    var handlePeg14= function () {
        $(document).on('change', "#peg14", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota14').style = 'display:block;';
            } else {
                document.getElementById('dataanggota14').style = 'display:none;';
            }
        });
    }
    var handlePeg15= function () {
        $(document).on('change', "#peg15", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota15').style = 'display:block;';
            } else {
                document.getElementById('dataanggota15').style = 'display:none;';
            }
        });
    }
    var handlePeg16= function () {
        $(document).on('change', "#peg16", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota16').style = 'display:block;';
            } else {
                document.getElementById('dataanggota16').style = 'display:none;';
            }
        });
    }
    var handlePeg17= function () {
        $(document).on('change', "#peg17", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota17').style = 'display:block;';
            } else {
                document.getElementById('dataanggota17').style = 'display:none;';
            }
        });
    }
    var handlePeg18= function () {
        $(document).on('change', "#peg18", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota18').style = 'display:block;';
            } else {
                document.getElementById('dataanggota18').style = 'display:none;';
            }
        });
    }
    var handlePeg19= function () {
        $(document).on('change', "#peg19", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota19').style = 'display:block;';
            } else {
                document.getElementById('dataanggota19').style = 'display:none;';
            }
        });
    }
    var handlePeg20= function () {
        $(document).on('change', "#peg20", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota20').style = 'display:block;';
            } else {
                document.getElementById('dataanggota20').style = 'display:none;';
            }
        });
    }
    var handlePeg21= function () {
        $(document).on('change', "#peg21", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota21').style = 'display:block;';
            } else {
                document.getElementById('dataanggota21').style = 'display:none;';
            }
        });
    }
     //kode pegawai 21   
    var handlePeg22= function () {
        $(document).on('change', "#peg22", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota22').style = 'display:block;';
            } else {
                document.getElementById('dataanggota22').style = 'display:none;';
            }
        });
    }
    var handlePeg23= function () {
        $(document).on('change', "#peg23", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota23').style = 'display:block;';
            } else {
                document.getElementById('dataanggota23').style = 'display:none;';
            }
        });
    }
    var handlePeg24= function () {
        $(document).on('change', "#peg24", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota24').style = 'display:block;';
            } else {
                document.getElementById('dataanggota24').style = 'display:none;';
            }
        });
    }
    var handlePeg25= function () {
        $(document).on('change', "#peg25", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota25').style = 'display:block;';
            } else {
                document.getElementById('dataanggota25').style = 'display:none;';
            }
        });
    }
    var handlePeg26= function () {
        $(document).on('change', "#peg26", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota26').style = 'display:block;';
            } else {
                document.getElementById('dataanggota26').style = 'display:none;';
            }
        });
    }
    var handlePeg27= function () {
        $(document).on('change', "#peg27", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota27').style = 'display:block;';
            } else {
                document.getElementById('dataanggota27').style = 'display:none;';
            }
        });
    }
    var handlePeg28= function () {
        $(document).on('change', "#peg28", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota28').style = 'display:block;';
            } else {
                document.getElementById('dataanggota28').style = 'display:none;';
            }
        });
    }
    var handlePeg29= function () {
        $(document).on('change', "#peg29", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota29').style = 'display:block;';
            } else {
                document.getElementById('dataanggota29').style = 'display:none;';
            }
        });
    }
    var handlePeg30= function () {
        $(document).on('change', "#peg30", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota30').style = 'display:block;';
            } else {
                document.getElementById('dataanggota30').style = 'display:none;';
            }
        });
    }
    var handlePeg31= function () {
        $(document).on('change', "#peg31", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota31').style = 'display:block;';
            } else {
                document.getElementById('dataanggota31').style = 'display:none;';
            }
        });
    }
    var handlePeg32= function () {
        $(document).on('change', "#peg32", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota32').style = 'display:block;';
            } else {
                document.getElementById('dataanggota32').style = 'display:none;';
            }
        });
    }
    var handlePeg33= function () {
        $(document).on('change', "#peg33", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota33').style = 'display:block;';
            } else {
                document.getElementById('dataanggota33').style = 'display:none;';
            }
        });
    }
    var handlePeg33= function () {
        $(document).on('change', "#peg33", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota33').style = 'display:block;';
            } else {
                document.getElementById('dataanggota33').style = 'display:none;';
            }
        });
    }
    var handlePeg34= function () {
        $(document).on('change', "#peg34", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota34').style = 'display:block;';
            } else {
                document.getElementById('dataanggota34').style = 'display:none;';
            }
        });
    }
    var handlePeg35= function () {
        $(document).on('change', "#peg35", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota35').style = 'display:block;';
            } else {
                document.getElementById('dataanggota35').style = 'display:none;';
            }
        });
    }
    var handlePeg36= function () {
        $(document).on('change', "#peg36", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota36').style = 'display:block;';
            } else {
                document.getElementById('dataanggota36').style = 'display:none;';
            }
        });
    }
    //kode pegawai 36  
    var handlePeg37= function () {
        $(document).on('change', "#peg37", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota37').style = 'display:block;';
            } else {
                document.getElementById('dataanggota37').style = 'display:none;';
            }
        });
    }
    var handlePeg38= function () {
        $(document).on('change', "#peg38", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota38').style = 'display:block;';
            } else {
                document.getElementById('dataanggota38').style = 'display:none;';
            }
        });
    }
    var handlePeg39= function () {
        $(document).on('change', "#peg39", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota39').style = 'display:block;';
            } else {
                document.getElementById('dataanggota39').style = 'display:none;';
            }
        });
    }
    var handlePeg40= function () {
        $(document).on('change', "#peg40", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota40').style = 'display:block;';
            } else {
                document.getElementById('dataanggota40').style = 'display:none;';
            }
        });
    }
    var handlePeg41= function () {
        $(document).on('change', "#peg41", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota41').style = 'display:block;';
            } else {
                document.getElementById('dataanggota41').style = 'display:none;';
            }
        });
    }
    var handlePeg42= function () {
        $(document).on('change', "#peg42", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota42').style = 'display:block;';
            } else {
                document.getElementById('dataanggota42').style = 'display:none;';
            }
        });
    }
    var handlePeg43= function () {
        $(document).on('change', "#peg43", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota43').style = 'display:block;';
            } else {
                document.getElementById('dataanggota43').style = 'display:none;';
            }
        });
    }
    var handlePeg44= function () {
        $(document).on('change', "#peg44", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota44').style = 'display:block;';
            } else {
                document.getElementById('dataanggota44').style = 'display:none;';
            }
        });
    }
    var handlePeg45= function () {
        $(document).on('change', "#peg45", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota45').style = 'display:block;';
            } else {
                document.getElementById('dataanggota45').style = 'display:none;';
            }
        });
    }
    var handlePeg46= function () {
        $(document).on('change', "#peg46", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota46').style = 'display:block;';
            } else {
                document.getElementById('dataanggota46').style = 'display:none;';
            }
        });
    }
    var handlePeg47= function () {
        $(document).on('change', "#peg47", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota47').style = 'display:block;';
            } else {
                document.getElementById('dataanggota47').style = 'display:none;';
            }
        });
    }
    var handlePeg48= function () {
        $(document).on('change', "#peg48", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota48').style = 'display:block;';
            } else {
                document.getElementById('dataanggota48').style = 'display:none;';
            }
        });
    }
    var handlePeg49= function () {
        $(document).on('change', "#peg49", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota49').style = 'display:block;';
            } else {
                document.getElementById('dataanggota49').style = 'display:none;';
            }
        });
    }
    var handlePeg50= function () {
        $(document).on('change', "#peg50", function () {
            if ($(this).find(":selected").val() == "0") {
                document.getElementById('dataanggota50').style = 'display:block;';
            } else {
                document.getElementById('dataanggota50').style = 'display:none;';
            }
        });
    }
    /*
    isisan pegawai dropdown atau text
    */

    
    //awal kode posisi anggota pkpt
    var handlePKPT0= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos0').style = 'display:block;';
            } else {
                document.getElementById('pos0').style = 'display:none;';
            }
        });
    }
    var handlePKPT1= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos1').style = 'display:block;';
            } else {
                document.getElementById('pos1').style = 'display:none;';
            }
        });
    }
    var handlePKPT2= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos2').style = 'display:block;';
            } else {
                document.getElementById('pos2').style = 'display:none;';
            }
        });
    }
    var handlePKPT3= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos3').style = 'display:block;';
            } else {
                document.getElementById('pos3').style = 'display:none;';
            }
        });
    }
    var handlePKPT4= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos4').style = 'display:block;';
            } else {
                document.getElementById('pos4').style = 'display:none;';
            }
        });
    }
    var handlePKPT5= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos5').style = 'display:block;';
            } else {
                document.getElementById('pos5').style = 'display:none;';
            }
        });
    }
    var handlePKPT6= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos6').style = 'display:block;';
            } else {
                document.getElementById('pos6').style = 'display:none;';
            }
        });
    }
    var handlePKPT7= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos7').style = 'display:block;';
            } else {
                document.getElementById('pos7').style = 'display:none;';
            }
        });
    }
    var handlePKPT8= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos8').style = 'display:block;';
            } else {
                document.getElementById('pos8').style = 'display:none;';
            }
        });
    }
    var handlePKPT9= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos9').style = 'display:block;';
            } else {
                document.getElementById('pos9').style = 'display:none;';
            }
        });
    }
    var handlePKPT10= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos10').style = 'display:block;';
            } else {
                document.getElementById('pos10').style = 'display:none;';
            }
        });
    }
    var handlePKPT11= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos11').style = 'display:block;';
            } else {
                document.getElementById('pos11').style = 'display:none;';
            }
        });
    }
    var handlePKPT12= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos12').style = 'display:block;';
            } else {
                document.getElementById('pos12').style = 'display:none;';
            }
        });
    }
    var handlePKPT13= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos13').style = 'display:block;';
            } else {
                document.getElementById('pos13').style = 'display:none;';
            }
        });
    }
    var handlePKPT14= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos14').style = 'display:block;';
            } else {
                document.getElementById('pos14').style = 'display:none;';
            }
        });
    }
    var handlePKPT15= function () {
        $(document).on('change', "#jenis", function () {
            if ($(this).find(":selected").val() == "2") {
                document.getElementById('pos15').style = 'display:block;';
            } else {
                document.getElementById('pos15').style = 'display:none;';
            }
        });
    }
    
    //kelar kode pkpt 15
    
    
    //akhir kode anggota pkpt
    var handleTombolDasar0 = function () {
        $(document).on('click', '#add0', function (e) {
            var x = $(this).attr('kolom');
            var y = parseInt(x) + 1;
            var dasar = "dasar" + y;
            document.getElementById(dasar).style = 'display:block;';
            document.getElementById('tombol' + x).style = 'display:none;';
        });
        $(document).on('click', '#remove0', function (e) {
            var x = $(this).attr('kolom');
            var y = x - 1;
            document.getElementById('dasar' + x).style = 'display:none;';
            document.getElementById('tombol' + y).style = 'display:block;';
        });

    }
     var handleTombolAnggota0 = function () {
         $(document).on('click', '#add1', function (e) {
            var x = $(this).attr('kolom1');
            var y = parseInt(x) + 1;
            var anggota = "anggota" + y;
            document.getElementById(anggota).style = 'display:block;';
            document.getElementById('tblanggota' + x).style = 'display:none;';
        });
        $(document).on('click', '#remove1', function (e) {
            var x = $(this).attr('kolom1');
            var y = x - 1;
            document.getElementById('anggota' + x).style = 'display:none;';
            document.getElementById('tblanggota' + y).style = 'display:block;';
        });
    }

    var handleAnggaran = function () {
        $(document).on("change", "#anggaran", function () {
            var aggr = this.value;
            if ($(this).val() == "Lain") {
                $("#anggaran2").attr("style", "margin-top:15px").val("").show().focus();
            } else {
                $("#anggaran2").hide();
            }
        });
    }

    return {
        init: function () {
            handleRecords();
            handleDataBaru();
            handleSuratTugasBaru();
            handleDetailST();
            handleDataEdit();
            handleValidasi();
            handleDataHapus();
            handleModalShow();
            handleFormat();
            //awal handel posisi pkpt
            handleDasar();
            handlePKPT0();
            handlePKPT1();
            handlePKPT2();
            handlePKPT3();
            handlePKPT4();
            handlePKPT5();
            handlePKPT6();
            handlePKPT7();
            handlePKPT8();
            handlePKPT9();
            handlePKPT10();
            handlePKPT11();
            handlePKPT12();
            handlePKPT13();
            handlePKPT14();
            handlePKPT15();
            //akhir handle pegawai pkpt
            
            //awal handel seleksi dropdown pegawai
            handlePeg0();
            handlePeg1();
            handlePeg2();
            handlePeg3();
            handlePeg4();
            handlePeg5();
            handlePeg6();
            handlePeg7();
            handlePeg8();
            handlePeg9();
            handlePeg10();
            handlePeg11();
            handlePeg12();
            handlePeg13();
            handlePeg14();
            handlePeg15();
            handlePeg16();
            handlePeg17();
            handlePeg18();
            handlePeg19();
            handlePeg20();
            handlePeg21();
            handlePeg22();
            handlePeg23();
            handlePeg24();
            handlePeg25();
            handlePeg26();
            handlePeg27();
            handlePeg28();
            handlePeg29();
            handlePeg30();
            handlePeg31();
            handlePeg32();
            handlePeg33();
            handlePeg34();
            handlePeg35();
            handlePeg36();
            handlePeg37();
            handlePeg38();
            handlePeg39();
            handlePeg40();
            handlePeg41();
            handlePeg42();
            handlePeg43();
            handlePeg44();
            handlePeg45();
            handlePeg46();
            handlePeg47();
            handlePeg48();
            handlePeg49();
            handlePeg50();

            //akhir handle seleksi dropdown pegawai
            handleTombolDasar0();
            handleTombolAnggota0();
        }
    };
}();
