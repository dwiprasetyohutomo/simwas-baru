var DiklatStruktural = function () {
	
	// Modal Form
	var modul = 'modul.php?ke=um_diklatstruktural';
	var judul = 'Form Diklat Struktural';
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit 	= modul+'&form=edit';
	var menuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	var importData	= '';
	var exportData	= modul+'&tabel=export';
	var emptyData	= modul+'&tabel=kosongkan';
	
	// Tabel Data
	var sourceUrl	 = modul+'&lihat=tabeldata';
	var destSelector = $('#tbldata tbody');
	
	var handleRecords = function () {

		tampilData(sourceUrl, destSelector);
		
		$('#menubaru').on('click', function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			modalForm.tambahData( menuBaru+'&nip='+nip, 'simpan', judul, true );
			e.preventDefault();
		});
		
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');
			modalForm.editData( menuEdit, uid, 'update', judul);
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var uid = $(this).attr("data-id");
			modalForm.hapusData( menuHapus, uid, sourceUrl+'&nip='+nip, destSelector, false );
			e.preventDefault();
		});		
	}

	var handleValidate = function () {
		
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				// kdtingkat: { required: true },
			},
			messages: {
				// nosk: { required: "Anda belum mengisi Nomor SK" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function () {
				var nip = hapusSpasi($('.pegawainip').text());
				var data = new FormData($('#form-user')[0]);
				modalForm.submitData(modul, data, sourceUrl+'&nip='+nip, destSelector);
			}
		});
		
		$('#form-user input').keypress(function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleTableMenu = function () {
		
		$('#menurefresh').on('click',function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			tampilData(sourceUrl+'&nip='+nip, destSelector);
			e.preventDefault();
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&form=importform', 'Import data kepangkatan (.csv)', 'pangkat_import');
			e.preventDefault();
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = exportData;
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';			
			modalForm.kosongkanData(emptyData, judul1, judul2, 'Hapus dah', 'Tidak Jadi', sourceUrl, destSelector);
		});		
	}

	var handleSubmisi = function () {

		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&tabel=importupload',
				type: method,
				data: data,
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(sourceUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});
	}
	
	var handleModalShow = function () {
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();			
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			$("#angkatan").inputmask({ "mask": "9999" });
			$("#tglmulai").inputmask({ "mask": "99-99-9999" });
			$("#tglselesai").inputmask({ "mask": "99-99-9999" });
			$('#spinjmljam').spinner({value:0, min: 0, max: 999});
			$('#spinjmlhari').spinner({value:0, min: 0, max: 999});			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
		});		
	}
	
	var handleTambahan = function () {

		var tblPegawai = $('#tblPegawai').dataTable({
			"responsive": true,
			"language": {
				"search": "",
				"zeroRecords": "Hasil pencarian tidak menemukan apa-apa",
				"lengthMenu": "_MENU_ data",
				"info": "_START_ s/d _END_ dari _TOTAL_ data pns",
				"infoFiltered": "(Total _MAX_ data)"
			},
			"sort": false,
			"pageLength": 5,
			"lengthMenu": [
				[5, 10, 20, 50, -1],
				[5, 10, 20, 50, "Semua"]
			],
			// "pagingType": "simple"
		})
		// .parent().removeClass('table-scrollable');		
		$('#tblPegawai_filter .form-control').attr('placeholder','Pencarian').addClass('input-sm');
		$('#tblPegawai_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm');
		
		
		$(document).on('click','#tblPegawai tr.pilih', function(e) {
			e.preventDefault();
			var nip = $(this).attr('data-nip');
			var datanama = $(this).attr('data-nama');
			
			// Tutup Modal pegawai yang dipilih
			$("#modal-pegawai").modal('hide');
			
			// Tampilkan gambar, nama dan nip pegawai bersangkutan
			var pns;
			$pns='<span class="pegawaifoto">';
			if (nip.substr(14,1) == 1) {
				$pns+='<img src="img/placeholder_man.png" style="padding-bottom:10px"><br>';
			} else {
				$pns+='<img src="img/placeholder_woman.png" style="padding-bottom:10px"><br>';
			}
			$pns+='</span>';
			$pns+='<span class="pegawainama">'+datanama+'</span><br>';
			$pns+='NIP. <span class="pegawainip">'+spasiNip(nip)+'</span>';
			$('.info-pegawai').html($pns);
			
			// Tampilkan informasi pangkat pegawai bersangkutan
			// tampilData(sourceUrl+'&nip='+nip, destSelector);
			tampilData('modul.php?ke=um_diklatstruktural&lihat=tabeldata&nip='+nip, destSelector);
			
			
		});		
	}
	
	return {
		init: function () {
			handleRecords();
			handleTableMenu();
			handleValidate();
			handleSubmisi();
			handleModalShow();
			handleTambahan();
		}
	};
}();