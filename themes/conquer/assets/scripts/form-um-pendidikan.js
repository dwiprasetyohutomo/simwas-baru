var PendidikanFormal = function () {
	
	// Modal Form
	var modul = 'modul.php?ke=um_pendidikan';
	var judul = 'Form Pendidikan Formal';
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit 	= modul+'&form=edit';
	var menuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	var importData	= '';
	var exportData	= modul+'&tabel=export';
	var emptyData	= modul+'&tabel=kosongkan';
	
	// Tabel Data
	var loadUrl		 = modul+'&lihat=tabeldata';
	var destSelector = $('#tbldata tbody');
	
	var tabel		 = $("#tableAjax");
	var grid 		 = new Datatable();
	
	var handleRecords = function () {

		// tampilData(sourceUrl, destSelector);
		grid.init({
			src: tabel,
			dataTable: {
				"pageLength": 5,
				"language": { "lengthMenu": "<span class='seperator'>|</span><span class='hidden-480'>Tampil</span> _MENU_" },
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-6 col-sm-12'pli><'col-md-6 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": loadUrl },
				// "ordering": false,
				"columnDefs": [{"targets": [0,1,2,3,5,6]}], // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[4, "desc"]],  // Kolom yang di Sortir
			}
		});
	}

	var handleCRUD = function () {

		$('#menubaru').on('click', function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var aksiVal = 'simpan'; // mengisi value elemen #aksi
			formModal.tambahData ( menuBaru+'&nip='+nip, aksiVal, judul );
			e.preventDefault();
		});
		
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');	// ambil dan isi value element #uid
			var data = { uid: uid }; // posting data untuk SQL
			var aksiVal = 'update';	// mengisi value elemen #aksi
			formModal.editData ( menuEdit, uid, aksiVal, judul, data );
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var uid = $(this).attr("data-id");
			var data = { uid: uid }
			formModal.hapusData ( menuHapus, data, true, tabel );
			e.preventDefault();
		});

	}
	
	var handleFileCrud = function () {
		
	}
	
	var handlePegawai = function () {

		var tblPegawai = $("#tblPegawai").dataTable({
			"language": { "search":"", "lengthMenu":"_MENU_ data", "zeroRecords":"Hasil pencarian tidak menemukan apa-apa", "info":"_START_ s/d _END_ dari _TOTAL_ data pns", "infoEmpty":"", "infoFiltered":"(Total _MAX_ data)" },
			"sort": false,
			"pageLength": 5,
			"lengthMenu": [[5, 10, 30, 50, -1],[5, 10, 30, 50, "Semua"]],
		});
		// .parent().removeClass('table-scrollable');		
		$('#tblPegawai_filter .form-control').attr('placeholder','Pencarian').addClass('input-small input-sm');
		$('#tblPegawai_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm');
		$('#tblPegawai tr').css('cursor','pointer');

		$(document).on('click','#tblPegawai tr.pilih', function(e) {
			e.preventDefault();
			var nip = $(this).attr('data-nip');
			var datanama = $(this).attr('data-nama');
			
			// Tutup Modal pegawai yang dipilih
			$("#modal-pegawai").modal('hide');
			
			// Tampilkan gambar, nama dan nip pegawai bersangkutan
			var pns;
			$pns='<span class="pegawaifoto">';
			if (nip.substr(14,1) == 1) {
				$pns+='<img src="img/placeholder_man.png" style="padding-bottom:10px"><br>';
			} else {
				$pns+='<img src="img/placeholder_woman.png" style="padding-bottom:10px"><br>';
			}
			$pns+='</span>';
			$pns+='<span class="pegawainama">'+datanama+'</span><br>';
			$pns+='NIP. <span class="pegawainip">'+spasiNip(nip)+'</span>';
			$('.info-pegawai').html($pns);
			
			// Tampilkan informasi pangkat pegawai bersangkutan
			// tampilData(sourceUrl+'&nip='+nip, destSelector);
			grid.setAjaxParam('nip', nip);
			grid.getDataTable().ajax.reload();
		});
	}

	var handleValidate = function () {
		
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				kd: { required: true },
				nama: { required: true },
				thnlulus: { required: true },
				lokasi: { required: true },
			},
			messages: {
				// nosk: { required: "Anda belum mengisi Nomor SK" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function () {
				var data = new FormData($('#form-user')[0]);
				formModal.submitData ( modul, data, true, tabel );
			}
		});
		
		/*
		$('#form-user input').keypress(function (e) {
		// $(document).on("keypress","#form-user input", function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
		*/
	}
	
	var handleTableMenu = function () {
		
		$('#menurefresh').on('click',function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			tampilData(sourceUrl+'&nip='+nip, destSelector);
			e.preventDefault();
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&form=importform', 'Import data kepangkatan (.csv)', 'pangkat_import');
			e.preventDefault();
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = exportData;
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';			
			modalForm.kosongkanData(emptyData, judul1, judul2, 'Hapus dah', 'Tidak Jadi', sourceUrl, destSelector);
		});		
	}

	var handleSubmisi = function () {

		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&tabel=importupload',
				type: method,
				data: data,
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(sourceUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});
	}
	
	var handleModalShow = function () {
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();			
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			$("#thnmasuk").inputmask({ "mask": "9999" });
			$("#thnlulus").inputmask({ "mask": "9999" });
			$("#tglijazah").inputmask({ "mask": "99-99-9999" });
			$("#tglijin").inputmask({ "mask": "99-99-9999" });
			$("#tglpenyesuaian").inputmask({ "mask": "99-99-9999" });
			
			// Select2
			// $("#kd").select2({ allowClear: true });
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
		});		
	}
	
	var handleJenjang = function() {
		// Kredit Poin berdasarkan Jenjang Pendidikan
		$(document).on('change', '#kd', function() {
			var naik = $('#kdtk').val();
			if (naik == '') { // Jika tidak ada peningkatan ijazah sesuaikan kredit poin dengan tingkat ijazah
				var kreditpoin = $(this).find(':selected').attr('data-kp');
				$('#kreditpoin').val(kreditpoin);
			}
		});
		
		// Kenaikan Jenjang Pendidikan
		$(document).on('change','#kdtk', function() {
			// var naik = $(this).value;
			var naik = $('#kdtk').val();
			if (naik != '') { // Sesuaikan Kredit Poin dengan tabel peningkatan ijazah
				var kreditpoin = $(this).find(':selected').attr('data-kp');
				$('#kreditpoin').val(kreditpoin);
			}			
			if (naik == '') { // Jika tidak ada peningkatan ijazah sesuaikan kredit poin dengan tingkat ijazah
				var kreditpoin = $('#kd').find(':selected').attr('data-kp');
				$('#kreditpoin').val(kreditpoin);
			}
		});
	}
	
	return {
		init: function () {
			handleRecords();
			handleCRUD();
			handleTableMenu();
			handleValidate();
			handleSubmisi();
			handleModalShow();
			handlePegawai();
			handleJenjang();
		}
	};
}();