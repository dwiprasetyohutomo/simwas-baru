	var Jabatan = function () {
	
	// Modal Form
	// var modul = $('#form-user').attr('action');
	var modul = 'modul.php?ke=um_jabatan';
	var judul = 'Form Jabatan';
	
	// CRUD
	var menuBaru	= modul+'&form=baru';
	var menuEdit 	= modul+'&form=edit';
	var menuHapus 	= modul+'&form=hapus';
	
	// Tabel Menu
	var importData	= '';
	var exportData	= modul+'&tabel=export';
	var emptyData	= modul+'&tabel=kosongkan';
	
	// Tabel Data
	// var sourceUrl	 = modul+'&lihat=tabeldata';
	var loadUrl		 = modul+'&lihat=tabeldata';
	var destSelector = $('#tbldata tbody');
	
	var tabel		 = $("#tableAjax");
	var grid 		 = new Datatable();
	
	var kdjab = '';
	
	var handleRecords = function () {

		// tampilData(sourceUrl, destSelector);
		
		// DATATABLE
		grid.init({
			src: tabel,
			dataTable: {
				"pageLength": 5,
				"language": { "lengthMenu": "<span class='seperator'>|</span><span class='hidden-480'>Tampil</span> _MENU_" },
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-6 col-sm-12'pli><'col-md-6 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": loadUrl },
				"ordering": false,
				// "columnDefs": [{"targets": [0]}], // Kolom yang dimatikan Fungsi Sortir nya
				// "order": [[6, "desc"]],  // Kolom yang di Sortir
			}
		});
	}

	var handleCRUD = function () {
		
		$('#menubaru').on('click', function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			var aksiVal = 'simpan';		// mengisi value elemen #aksi
			formModal.tambahData ( menuBaru+'&nip='+nip, aksiVal, judul );
			/*
			var nip = hapusSpasi($('.pegawainip').text());
			modalForm.tambahData( menuBaru+'&nip='+nip, 'simpan', judul, true );
			*/
			e.preventDefault();
		});
		
		$(document).on('click', '#edit', function(e) {
			var uid = $(this).attr('data-id');	// ambil dan isi value element #uid
			var data = { uid: uid };			// posting data untuk SQL
			var aksiVal = 'update';		// mengisi value elemen #aksi
			formModal.editData ( menuEdit, uid, aksiVal, judul, data );
			/*
			var uid = $(this).attr('data-id');
			modalForm.editData( menuEdit, uid, 'update', judul);
			*/
			e.preventDefault();
		});

		$(document).on("click", "#hapus", function(e) {
			var uid = $(this).attr("data-id");
			var data = { uid: uid }
			formModal.hapusData ( menuHapus, data, true, tabel );
			/*
			var nip = hapusSpasi($('.pegawainip').text());
			var uid = $(this).attr("data-id");
			// modalForm.hapusData( menuHapus, uid, sourceUrl+'&nip='+nip, destSelector, false );
			modalForm.hapusData( menuHapus, uid, sourceUrl+'&nip='+nip, tabel, true );
			*/
			e.preventDefault();
		});
		
	}
	
	var handlePegawai = function () {

		var tblPegawai = $("#tblPegawai").dataTable({
			"language": { "search":"", "lengthMenu":"_MENU_ data", "zeroRecords":"Hasil pencarian tidak menemukan apa-apa", "info":"_START_ s/d _END_ dari _TOTAL_ data pns", "infoEmpty":"", "infoFiltered":"(Total _MAX_ data)" },
			"sort": false,
			"pageLength": 5,
			"lengthMenu": [[5, 10, 30, 50, -1],[5, 10, 30, 50, "Semua"]],
		});
		// .parent().removeClass('table-scrollable');		
		$('#tblPegawai_filter .form-control').attr('placeholder','Pencarian').addClass('input-small input-sm');
		$('#tblPegawai_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm');
		$('#tblPegawai tr').css('cursor','pointer');

		$(document).on('click','#tblPegawai tr.pilih', function(e) {
			e.preventDefault();
			var nip = $(this).attr('data-nip');
			var datanama = $(this).attr('data-nama');
			
			// Tutup Modal pegawai yang dipilih
			$("#modal-pegawai").modal('hide');
			
			// Tampilkan gambar, nama dan nip pegawai bersangkutan
			var pns;
			$pns='<span class="pegawaifoto">';
			if (nip.substr(14,1) == 1) {
				$pns+='<img src="img/placeholder_man.png" style="padding-bottom:10px"><br>';
			} else {
				$pns+='<img src="img/placeholder_woman.png" style="padding-bottom:10px"><br>';
			}
			$pns+='</span>';
			$pns+='<span class="pegawainama">'+datanama+'</span><br>';
			$pns+='NIP. <span class="pegawainip">'+spasiNip(nip)+'</span>';
			$('.info-pegawai').html($pns);
			
			// Tampilkan informasi pangkat pegawai bersangkutan
			// tampilData(sourceUrl+'&nip='+nip, destSelector);
			grid.setAjaxParam('nip', nip);
			grid.getDataTable().ajax.reload();
		});
	}

	var handleValidate = function () {
		
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				// kdtingkat: { required: true },
			},
			messages: {
				// nosk: { required: "Anda belum mengisi Nomor SK" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function () {
				var data = new FormData($('#form-user')[0]);
				formModal.submitData ( modul, data, true, tabel );
				// formSubmit();
			}
		});
		
		/*
		function formSubmit() {
			var nip = hapusSpasi($('.pegawainip').text());
			var data = new FormData($('#form-user')[0]);
			// modalForm.submitData(modul, data, sourceUrl+'&nip='+nip, destSelector);
			modalForm.submitData(modul, data, loadUrl+'&nip='+nip, tabel, true);
		}
		*/
	}
	
	var handleTableMenu = function () {
		
		$('#form-import').submit(function(e) {
			e.preventDefault();
			
			var method = $(this).attr('method');
			var data = new FormData(this);
			
			var fileType = '.csv';
			var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + fileType + ")$");
			if(!regex.test($('#uploadfile').val().toLowerCase())) {
				$('.alert').html('<button class="close" data-close="alert"></button>File yang Anda pilih bukan format ('+fileType+'), import tidak valid.').show();
				$('#import-modal #import').text('Import');
				return false;
			} else {
				$('#import-modal #import').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
			}
			$.ajax({
				url: modul+'&tabel=importupload',
				type: method,
				data: data,
				dataType: 'json',
				contentType: false,
				processData: false,
				cache: false
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				if (response.status == 'sukses') {
					tampilData(sourceUrl, destSelector);
					$('#import-modal').modal('hide');
					swal({ type: 'success', title: 'Sukses!', pesan: 'Sukses import data', showConfirmButton: false, timer: 1500 });
				}
			}).fail(function(jqXHR, textStatus, errorThrown) {				
				alert(jqXHR.responseText);
			});
			return false;
		});

		$('#menurefresh').on('click',function(e) {
			var nip = hapusSpasi($('.pegawainip').text());
			tampilData(sourceUrl+'&nip='+nip, destSelector);
			e.preventDefault();
		});
		
		$('#menuimport').on('click', function(e) {
			// modalForm.importData( modul+'&form=importform', 'Import data kepangkatan (.csv)', 'pangkat_import');
			e.preventDefault();
		});
		
		$('#menuexport').on('click', function(e) {
			e.preventDefault();
			window.location.href = exportData;
		});
		
		$('#menukosongkan').on('click',function(e) {
			e.preventDefault();
			var judul1 = 'Kosongkan semua data ?';
			var judul2 = '<strong>Ingat !!</strong> data tidak dapat dikembalikan lagi! (Kosong Tabelnya)';			
			modalForm.kosongkanData(emptyData, judul1, judul2, 'Hapus dah', 'Tidak Jadi', sourceUrl, destSelector);
		});		
	}

	var formJabatan = {
		resetJenis: function () {
			$('#jabatan').val('');
			$('#eselon').val('');
			$('#nosk').val('');
			$('#tglsk').val('');
			$('#golru').val('');
			$('#tmtjab').val('');
			$('#tmtpel').val('');
			$('#nonotadinas').val('');
			$('#tmtnotadinas').val('');
			$('#ket').val('');			
		},
		inputJenis: function(jenis) {
			if(jenis == 'jst') {
				$('#fg-jabatan').removeClass('hidden');
				$('#fg-eselon').removeClass('hidden');
				$('#fg-unker').removeClass('hidden');
				$('#fg-nosk').removeClass('hidden');
				$('#fg-tglsk').removeClass('hidden');
				$('#fg-golru').removeClass('hidden');
				$('#fg-tmtjab').removeClass('hidden');
				$('#fg-tmtpel').removeClass('hidden');
				$('#fg-nonotadinas').addClass('hidden');
				$('#fg-tmtnotadinas').addClass('hidden');
				$('#fg-ket').removeClass('hidden');				
			}
			else if(jenis == 'jft') {
				$('#fg-jabatan').removeClass('hidden');
				$('#fg-eselon').addClass('hidden');
				$('#fg-unker').removeClass('hidden');
				$('#fg-nosk').removeClass('hidden');
				$('#fg-tglsk').removeClass('hidden');
				$('#fg-golru').removeClass('hidden');
				$('#fg-tmtjab').removeClass('hidden');
				$('#fg-tmtpel').addClass('hidden');
				$('#fg-nonotadinas').addClass('hidden');
				$('#fg-tmtnotadinas').addClass('hidden');
				$('#fg-ket').removeClass('hidden');				
			}
			else if(jenis == 'jpl') {
				$('#fg-jabatan').removeClass('hidden');
				$('#fg-eselon').addClass('hidden');
				$('#fg-unker').removeClass('hidden');
				$('#fg-nosk').addClass('hidden');
				$('#fg-tglsk').addClass('hidden');
				$('#fg-golru').removeClass('hidden');
				$('#fg-tmtjab').removeClass('hidden');
				$('#fg-tmtpel').addClass('hidden');
				$('#fg-nonotadinas').removeClass('hidden');
				$('#fg-tmtnotadinas').removeClass('hidden');
				$('#fg-ket').removeClass('hidden');
			}
			else {
				$('#fg-jabatan').addClass('hidden');
				$('#fg-eselon').addClass('hidden');
				$('#fg-unker').addClass('hidden');
				$('#fg-nosk').addClass('hidden');
				$('#fg-tglsk').addClass('hidden');
				$('#fg-golru').addClass('hidden');
				$('#fg-tmtjab').addClass('hidden');
				$('#fg-tmtpel').addClass('hidden');
				$('#fg-nonotadinas').addClass('hidden');
				$('#fg-tmtnotadinas').addClass('hidden');
				$('#fg-ket').addClass('hidden');
			}
		}
	}
	
	var handleJenisJabatan = function () {
		// Jenis Jabatan
		$(document).on('change','#jenis', function(e) {
			e.preventDefault();		
			var jenis = this.value;
			var judul = $("#jenis").find("option:selected").text();
			$('#modal-jabatan .modal-title').text(judul);
			// formJabatan.resetJenis();
			formJabatan.inputJenis(jenis);
			tampilData(modul + '&lihat=tabeljabatan&jabatan=' + jenis, $('#tblJabatan tbody'));
		});
	}
	
	var handleModalJabatan = function () {

		$(document).on('click','#tblJabatan tr.pilih', function(e) {
			e.preventDefault();
			
			var jenis = $('#jenis').val();
			
			if (jenis == 'jst') {
				var jabatan = $(this).attr('data-jabatan');
				var kodejab = $(this).attr('data-kodejab');
				var eselon 	= $(this).attr('data-eselon');				
				$('#jabatan').val(jabatan);
				$('#kdjabatan').val(kodejab);
				$('#eselon').val(eselon);				
				$("#modal-jabatan").modal('hide');
			}

			else if (jenis == 'jft') {
				var kodejab = $(this).attr('data-kodejab');
				var jabatan = $(this).attr('data-jabatan');
				var tingkat = $(this).attr('data-tingkat');

				if(tingkat > 0) {
					tampilData(modul + '&lihat=tabeltingkat&kode=' + kodejab, $('#tblTingkat tbody'));
				} else {
					$('#jabatan').val(jabatan);
					$('#kdjabatan').val(kodejab);
					$("#modal-jabatan").modal('hide');
				}
			}

			else if (jenis == 'jpl') {
				var jabatan = $(this).attr('data-jabatan');
				var kodejab = $(this).attr('data-kodejab');
				$('#jabatan').val(jabatan);
				$('#kdjabatan').val(kodejab);
				$("#modal-jabatan").modal('hide');
			}
		});		
	}
	
	var handleModalTingkat = function () {
		
		$(document).on('click','#tblTingkat tr.pilih', function(e) {
			e.preventDefault();
			var tingkat = $(this).attr('data-tingkat');
			$('#jabatan').val(tingkat);
			$("#modal-tingkat").modal('hide');
			$("#modal-jabatan").modal('hide');
		});		
	}

	var handleModalShow = function () {
		$(document).on("ajaxComplete", function(e){
			// e.preventDefault();
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			$("#tglsk").inputmask({ "mask": "99-99-9999" });
			$("#tmtjab").inputmask({ "mask": "99-99-9999" });
			$("#tmtpel").inputmask({ "mask": "99-99-9999" });
			$("#tmtnotadinas").inputmask({ "mask": "99-99-9999" });			
		});
			
		$('#form-modal').on('shown.bs.modal',function() {
			var jenis = $('#jenis').val();
			var judul = $("#jenis").find("option:selected").text();
			$('#modal-jabatan .modal-title').text(judul);
			formJabatan.inputJenis(jenis);
			$('.form-body').scrollTop(0);
		});
		
		$('#modal-jabatan').on('shown.bs.modal',function() {
			var jenis = $('#jenis').val();
			// tampilData(modul + '&lihat=tabeljabatan&jabatan=' + jenis, $('#tblJabatan tbody'));
		});
	}
	
	return {
		init: function () {
			handleRecords();
			handleCRUD();
			handlePegawai();
			handleValidate();
			handleTableMenu();
			handleJenisJabatan();
			handleModalJabatan();
			handleModalTingkat();
			handleModalShow();
		}
	};
}();