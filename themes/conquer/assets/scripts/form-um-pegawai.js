var pegawai = function () {
	
	// var modul = 'modul.php?ke=um_pegawai';	
	var modul = window.location.href;
	var pageTitle = 'Form Data Pegawai';
	
	// CRUD
	var menuBaru 	= modul+'&pegawai=baru';
	var menuEdit	= modul+'&pegawai=edit';
	var menuHapus 	= modul+'&pegawai=hapus';
	
	// Table Data
	var srcUrl 	= modul+'&pegawai=tabeldata';
	var destSel	= $("#tableAjax");
	
	var handleRecords = function () {
		var grid = new Datatable();		
		var tabel = $("#tableAjax");
		grid.init({
			src: tabel,
			dataTable: {
				"pageLength": 5,
				"dom":
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'pull-right'f>>>" +
					"<'tableAjax-scrollable't>" +
					"<'row'<'col-md-7 col-sm-12'pli><'col-md-5 col-sm-12'<'table-group-actions pull-right'>>r>",
				"ajax": { "url": modul+"&pegawai=tabeldata" },
				// "ordering": false
				"columnDefs": [{"targets": [0,1,4,5,6,7]}], // Kolom yang dimatikan Fungsi Sortir nya
				"order": [[3, "asc"]],  // Kolom yang di Sortir
			}
		});
		
		/*
		$('#tableAjax_length .form-control').removeClass('input-xsmall').addClass('input-small input-sm'); // Besarkan Filter
		var inputcari 	= $('div.dataTables_filter input');
		var btncari 	= $('<span class="input-group-btn"><button class="btn btn-warning" type="button" style="margin-top:-1px"><i class="fa fa-search"></i></button></span>');		
		inputcari.contents().unwrap();
		inputcari.attr('placeholder', 'Cari nip/nama/kelahiran...').removeClass('input-small').wrap('<div class="input-group input-group-sm"></div>').after(btncari);
		inputcari.off();
		inputcari.on("keypress", function (e) {
			if (e.which == 13) {
				tabel.DataTable().search(inputcari.val()).draw();
			}
		});
		btncari.on("click", function() {
			tabel.DataTable().search(inputcari.val()).draw();
		});
		*/
		
		// == CUSTOM FILTERING ==
		grid.getTableWrapper().on("click", ".table-group-action-submit", function (e) { // SUBMIT
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				grid.setAjaxParam($(this).attr("name"), $(this).val());
			});
			grid.getDataTable().ajax.reload();
		});
		grid.getTableWrapper().on("click", ".table-group-action-reset", function (e) { // RESET
			e.preventDefault();
			$("textarea.form-filter, select.form-filter, input.form-filter").each(function () {
				$(this).val("");
			});
			grid.clearAjaxParams();
			//$("#form-filter")[0].reset(); // Optional
			grid.getDataTable().ajax.reload();
		});
		
		// Set Status Filter
		var filterby = $('#status');
		filterby.val('Y');
		grid.clearAjaxParams();
		grid.setAjaxParam(filterby.attr("name"), filterby.val());
		grid.getDataTable().ajax.reload();
	}
	
	var handleDataBaru = function() {

		$(document).on('click', '#baru', function(e) {
			
			$('#form-user')[0].reset();

			$('.modal-title').html('Form Tambah ' + pageTitle);
			$('.modal-loading').show();
			//$('.modal-dialog').addClass('modal-wide');
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			$('.form-body')	.html('');
		
			$('#uid').val('');
			$('#aksi').val('pegawai_simpan');
			$('#submit').val('pegawai_simpan').text('Simpan');
		
			$.ajax({
				url: menuBaru,
				type: 'POST',
				timeout: 20000,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				console.log(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});
			e.preventDefault();
		});		
	}
	
	var handleDataEdit = function () {

		$(document).on('click', '#edit', function(e) {			
			
			$('#form-user')[0].reset();
			
			$('.modal-title').html('<i class="fa fa-edit"></i> ' + pageTitle);
			$('.modal-loading').show();
			//$('.modal-dialog').addClass('modal-wide');
			$('.form-body').attr('style','max-height: calc(105vh - 205px); overflow-y: auto; overflow-x: hidden');
			$('.form-body').html('');
		
			var uid = $(this).attr('data-id');
			$('#uid').val(uid);
			// $('#aksi').val('pegawai_update');
			$('#submit').val('pegawai_update').text('Update');

			$.ajax({
				url: menuEdit,
				type: 'POST',
				timeout: 20000,
				data: 'uid='+uid,
				dataType: 'html'
			}).done(function(response, textStatus, jqXHR) {
				// setTimeOut digunakan sebagai percobaan/testing saat online
				// setTimeout(function(){
				// console.log(response);
				$('.form-body').html(response);
				$('.modal-loading').hide();
				// }, 2000);
			}).fail(function(jqXHR, textStatus, errorThrown) {
				$('.form-body').html('<i class="glyphicon glyphicon-info-sign"></i> Terjadi kesalahan: ' + textStatus + ': ' + jqXHR.responseText + '<br><a href="http://api.jquery.com/jQuery.ajax" target="_blank">http://api.jquery.com/jQuery.ajax</a>');
				$('.modal-loading').hide();
				$('#submit').hide();
			});		
			e.preventDefault();
		});
	}
	
	var handleDataHapus = function () {
		$(document).on('click', '#hapus', function(e) {			
			var uid = $(this).attr('data-id');
			swal({
				title: 'Yakin ingin menghapus data ini?',
				html: '<strong style="color:#dd0000">Perhatian!</strong> data yang telah dihapus tidak dapat dikembalikan lagi!',
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: "#AA0000",
				confirmButtonText: "Hapus",
				cancelButtonColor: "#F0AD4E",
				cancelButtonText: "Jangan"
			}).then((result) => {
				if (result.value) {
					$.ajax({
						url: menuHapus,
						type: "POST",
						timeout: 20000,
						data: 'uid='+uid,
						dataType: "json",
						success: function(response) {
							if(response.status == "sukses") {
								swal({ title: "Data telah terhapus!", text: response.pesan, type: "success", showConfirmButton: false, timer: 1500 });
								DataTableReload($('#tableAjax'));
							}
						},
						error: function(xhr, ajaxOptions, thrownError) {
							alert(xhr.responseText)
						}
					});
				}
			})
			e.preventDefault();
		});		
	}

	var handleValidasi = function () {
		$('#form-user').validate({
			errorElement: 'span',
			errorClass: 'help-block',
			focusInvalid: false,
			rules: {
				nip: 	{ required: true },
				nama: { required: true }
			},
			messages: {
				nip:	{ required: "Nomor harus di isi" },
				nama:	{ required: "Nama harus di isi" },
			},
			invalidHandler: function (event, validator) {
				$('.alert-danger', $('#form-user')).show();
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error')
			},
			success: function (label) { 
				label.closest(".form-group").removeClass("has-error");
				label.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').size() > 0) {
					error.insertAfter(element.parent('.input-group'));
				} else if (element.attr('data-error-container')) { 
					error.appendTo(element.attr('data-error-container'));
				} else if (element.parents('.radio-list').size() > 0) { 
					error.appendTo(element.parents('.radio-list').attr("data-error-container"));
				} else if (element.parents('.radio-inline').size() > 0) { 
					error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
				} else if (element.parents('.checkbox-list').size() > 0) {
					error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
				} else if (element.parents('.checkbox-inline').size() > 0) { 
					error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
				} else {
					error.insertAfter(element); // for other inputs, just perform default behavior
				}
			},
			submitHandler: function (form) {
				var data = new FormData($('#form-user')[0]);
				$('#submit').html('<span class="glyphicon glyphicon-refresh glyphicon-animate"></span> Proses...');
				$.ajax({
					url: modul,
					type: 'POST',
					data: data,
					dataType: 'json',
					contentType: false,
					processData: false,
					cache: false
				}).done(function(response, textStatus, jqXHR) {
					if (response.status == 'sukses') {
						$('#form-modal').modal('hide');
						DataTableReload($('#tableAjax'));
						swal({ type: 'success', title: 'Sukses!', text: response.pesan, showConfirmButton: false, timer: 1500 });
					} else {
						$('#submit').text('Coba Lagi');
						$('.alert')
							.removeClass('alert-success alert-danger alert-info')
							.addClass('alert-warning')
							.html('<button class="close" data-close="alert"></button><span>'+response.pesan+'</span>')
							.show();
						$('.form-body').scrollTop(0);
					}
				}).fail(function(jqXHR, textStatus, errorThrown) {
					$('#submit').hide();
					alert(jqXHR.responseText);
				});
			}
		});
		
		$('#form-user .form-control').keypress(function (e) {
			if (e.which == 13) {
				if ($('#form-user').validate().form()) {
					$('#form-user').submit();
                }
                return false;
            }
        });
	}
	
	var handleModalShow = function () {
		
		$(document).on("ajaxComplete", function(e){
			e.preventDefault();
			$("#lahirtgl").inputmask({ "mask": "99-99-9999", "placeholder": "dd-mm-yyyy" });
			$("#niplama").inputmask({ "mask": "999 999 999" });
			$("#nip").inputmask({ "mask": "99999999 999999 9 999" });
			
		});
		
		$('#form-modal').on('shown.bs.modal',function() {
			$('.form-body').scrollTop(0);
			$('#nip').focus();
		});
	}
	
	var handleExport = function () {
		$(document).on('click','#export',function(e) {
			swal({
				title: "Export tabel ke format CSV?", text:"", type:"question", showCancelButton:true, confirmButtonText:"Ya", cancelButtonText:"Batal", cancelButtonColor:"#d33"
			}).then(function(result) {
				if(result.value) window.open("'.$modul.'&stgswas=export","_blank");
			});
			e.preventDefault();		
		});
	}
	
	var handleInspekturInfo = function () {
		$(document).on("change", "#insp", function () {
			var inspnama = $(this).find("option:selected").text();
			var inspnip = this.value;
			$("#insp_nama").val(inspnama)
			$("#insp_nip").val(inspnip);
			/*
			App.blockUI({ message: 'Mohon bersabar...', target: '.form-body', overlayColor: 'none', cenrerY: true, boxed: true });
			$.ajax({
				url: modul+'&personil=info',
				type: 'POST',
				timeout: 20000,
				dataType: 'json',
				//contentType: 'application/x-www-form-urlencoded',
				async: false,
				data: { nip: nip },
				success: function(data) {
					// Hilangkan setTimeout saat sudah online
					//setTimeout(function() {					
						App.unblockUI('.form-body'); // Tutup loading
						userData = data;
						$("#nama").val(userData.nama);
						$("#nip").val(userData.nip);
					//}, 500);
				}
			});
			*/
		});
	}

	var handleAnggaran = function () {
		$(document).on("change", "#anggaran", function() {
			var aggr = this.value;
			if ($(this).val() == "Lain") {
				$("#anggaran2").attr("style","margin-top:15px").val("").show().focus();
			} else {
				$("#anggaran2").hide();
			}			
		});
	}
	
	return {
		init: function () {
			handleRecords();
			handleDataBaru();
			handleDataEdit();
			handleValidasi();
			handleDataHapus();
			handleModalShow();
		}
	};
}();
