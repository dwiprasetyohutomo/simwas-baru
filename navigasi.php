<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu">
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<div class="clearfix">
					</div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li class="sidebar-search-wrapper">
					<form class="search-form" role="form" action="http://www.keenthemes.com/preview/conquer/index.html" method="get">
						<div class="input-icon right">
							<i class="fa fa-search"></i>
							<input type="text" class="form-control input-sm" name="query" placeholder="Search...">
						</div>
					</form>
				</li>
				<li class="start ">
					<a href="index.html">
					<i class="icon-home"></i>
					<span class="title">Dashboard</span>
					</a>
				</li>
				<?php
				    $query = "SELECT judul, alamat, icon, nav_1 FROM navigasi GROUP BY nav_1 ORDER BY nav_1 ASC";
       				$stmt = $con->prepare($query);
        			$stmt->execute();
        			$num = $stmt->rowCount();
					if($num>0){
						while ($nav1= $stmt->fetch(PDO::FETCH_ASSOC)){
							extract($nav1);
							$query = "SELECT judul, alamat, icon, nav_2 FROM navigasi WHERE nav_1='{$nav_1}'and nav_2>'0' GROUP BY nav_2 ORDER BY nav_2 ASC";
							$idnav_1=$nav_1;
       						$stmt1 = $con->prepare($query);
        					$stmt1->execute();
        					$num1 = $stmt1->rowCount();
							if($num1>0){
								echo "<li>
								<a href=\"javascript:;\">
								<i class=\"".$icon."\"></i>
								<span class=\"title\">".$judul."</span>
								<span class=\"arrow \"></span>
								</a>
								<ul class=\"sub-menu\">";								
								while ($nav2= $stmt1->fetch(PDO::FETCH_ASSOC)){
									extract($nav2);
									$query = "SELECT judul, alamat, icon, nav_3 FROM navigasi WHERE nav_1='$idnav_1'and nav_2='{$nav_2}' and nav_3>'0' GROUP BY nav_3 ORDER BY nav_3 ASC";
									$idnav_2=$nav_2;
       								$stmt2 = $con->prepare($query);
        							$stmt2->execute();
        							$num2 = $stmt2->rowCount();
									if($num2>0){
										echo "<li>
											<a href=\"javascript:;\">
											<i class=\"".$icon."\"></i>".$judul."<span class=\"arrow\"></span>
											</a>
											<ul class=\"sub-menu\">";
										while ($nav3= $stmt2->fetch(PDO::FETCH_ASSOC)){
											extract($nav3);
											$query = "SELECT judul, alamat, icon, nav_4 FROM navigasi WHERE nav_1='$idnav_1'and nav_2='$idnav_2' and nav_3={$nav_3} and nav_4>'0' GROUP BY nav_4 ORDER BY nav_4 ASC";
											$idnav_3=$nav_3;
       										$stmt3 = $con->prepare($query);
        									$stmt3->execute();
        									$num3 = $stmt3->rowCount();
											if($num3>0){
											echo "<li>
												<a href=\"javascript:;\">
												<i class=\"".$icon."\"></i>".$judul."<span class=\"arrow\"></span>
												</a>
												<ul class=\"sub-menu\">";
											while ($nav4= $stmt3->fetch(PDO::FETCH_ASSOC)){
												extract($nav4);
												echo "<li>
													<a href=\"".$alamat."\">
													<i class=\"".$icon."\"></i>
													".$judul."</a>
													</li>";
											}
											echo "</ul>
												</li>";	
											}
											else {
												echo "<li>
												<a href=\"".$alamat."\">
												<i class=\"".$icon."\"></i>
												".$judul."</a>
												</li>";
											}
										}
										echo "</ul>
											</li>";
									}
									else {
									echo "<li>
										<a href=\"".$alamat."\">
										<i class=\"".$icon."\"></i>
										".$judul."</a>
										</li>";
									}
								}
								echo "</ul>
									</li>";
							}
							else {
								echo "<li>
									<a href=\"".$alamat."\"><i class=\"".$icon."\"></i>".$judul."</a>
									</li>";
							} 
        				}
        			}
        		?>
				<li class="last ">
					<a href="login.html">
					<i class="icon-user"></i>
					<span class="title">Login</span>
					</a>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>
	<!-- END SIDEBAR -->