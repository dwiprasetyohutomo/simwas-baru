<?php
error_reporting(9);
include "lib/template.class.php";

// Periksa kondisi login
session_start();
if(!isset($_SESSION['nip']) && empty($_SESSION['nip'])) {
	header('location: login.php');
	exit();
}

$pageTitle = 'Halaman Tidak Ditemukan 404';
$pageBreadcrumb = '
<li>
	<i class="fa fa-home"></i> 
	<a href="index.php">Beranda</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">Halaman</a>
	<i class="fa fa-angle-right"></i>
</li>
<li>
	<a href="#">404</a>
</li>
';

$isi ='
<div class="col-md-12 page-404">
	<div class="number">
		404
	</div>
	<div class="details">
		<h3>Oops! Anda tersesat?</h3>
		<p>
			Kami tidak menemukan halaman yang anda cari..<br/>
			<button class="btn btn-small" onclick="javascript:history.go(-1)"><i class="fa fa-arrow-left"></i></button> Kembali ke Beranda<br/>
			atau coba pencarian dibawah ini.
		</p>
		<form action="#">
			<div class="input-group input-medium">
				<input type="text" class="form-control" placeholder="cari...">
				<span class="input-group-btn">
				<button type="submit" class="btn btn-info"><i class="fa fa-search"></i></button>
				</span>
			</div>
			<!-- /input-group -->
		</form>
	</div>
</div>';

$tpl = new Template;
$tpl->load('themes/conquer/404.html');
$tpl->set('theme',$tpl->theme);
$tpl->set('css-tambahan','<link href="'.$theme.'/assets/css/pages/error.css" rel="stylesheet" type="text/css"/>');
$tpl->set('page-title',$tpl->pageTitle);
$tpl->set('page-breadcrumb',$tpl->pageBreadcrumb);
$tpl->set('page-kontent',$isi);
$tpl->set('page-plugin-script',$tpl->pagePlugins);
$tpl->set('page-styles-script',$tpl->pageStyles);
$tpl->set('initA',$tpl->initA);
$tpl->set('initB',$tpl->initB);
$tpl->set('initC',$tpl->initC);
$tpl->publish();
?>