<?php
include '/lib/tcpdf/tcpdf.php';
include '/lib/tglindonesia.php';

//decrypt key
$secret_key = 'hasgroup.id';
$secret_iv = 'hasgroup.id';
$decrypt = false;
$encrypt_method = "AES-256-CBC";
$key = hash( 'sha256', $secret_key );
$iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );
$st_id=$_GET['key'];
$stkey=$st_id;
$st_id = openssl_decrypt( base64_decode( $st_id ), $encrypt_method, $key, 0, $iv );
echo $st_id;
 
if ($st_id === false){
    echo "data tidak ditemukan";
}
else {
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "1n5p3k7o12a7";
    $conn = new mysqli($servername, $username, $password, $dbname);
    $pdf = new TCPDF('P','mm',array(215,330), true, 'UTF-8', false);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetMargins(15, 10, 15, true);
    $pdf->AddPage(); 
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
    
    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, 10);
    
    $pdf->SetFont('helvetica', '', 12);

    $tbl ="
    <table cellspacing=\"0\" cellpadding=\"1\" >
        <tr>
            <td rowspan=\"5\" width=\"20%\"></td>
            <td width=\"80%\" align=\"center\" style=\"font-size:11px\">PEMERINTAH PROVINSI KALIMANTAN UTARA</td>
        </tr>
        <tr>
            <td width=\"80%\" align=\"center\"><b>INSPEKTORAT</b></td>
        </tr>
        <tr>
            <td width=\"80%\" align=\"center\" style=\"font-size:10px\">Jalan Rambutan Nomor 001 Kode Pos 77212 Telp/Fax (0552) 23126</td>
        </tr>
        <tr>
            <td width=\"80%\" align=\"center\" style=\"font-size:11px\">TANJUNG SELOR</td>
        </tr>
        <tr>
            <td width=\"80%\" align=\"right\" style=\"font-size:6px\">website: www.inspektorat.kaltaraprov.go.id ; email: itwilkaltara@gmail.com</td>
        </tr>
        
    </table>
    <br>
    ";
    $pdf->writeHTML($tbl, true, false, false, false, '');
     $pdf->Image('@'.file_get_contents('img/logo kaltara.jpg'),25,10,0,25);

    // Query data surat tugas
   
    $sql="SELECT
              *
            FROM
              um_st
            Where st_id=$st_id";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    // ------------------------------------ "KOP SURAT" -----------------------------------------
    
    
    // ------------------------------------- Nomor Surat Tugas ----------------------------------------
    //$pdf->Write(0, 'isi', 'hyperlink', 0, 'align', true, 0, false, false, 0);
    
    $pdf->SetFont('helvetica', 'U,B', 14);
    $pdf->Write(0, 'SURAT PERINTAH TUGAS', '', 0, 'C', true, 0, false, false, 0);
    $pdf->SetFont('helvetica', 'B', 12);
    $pdf->Write(0, 'NOMOR : '.$row["st_kode"], '', 0, 'C', true, 0, false, false, 0);
    $pdf->WriteHTMLCell(0, 0, '','', '<br />', 0, 1, 0, true, '', true);
    
    // ---------------------------------- Dasar Surat Tugas-------------------------------------------
    $pdf->SetFont('helvetica', '', 12);
    $tbl ="
    <table cellspacing=\"0\" cellpadding=\"1\" border=\"0\" >
        <tr>
            <td rowspan=\"6\" width=\"20%\">Dasar</td>
            <td rowspan=\"6\" width=\"2%\">:</td>
            <td width=\"3%\">1.</td>
            <td width=\"75%\" align=\"justify\">".$row["st_dasar1"]."</td>
        </tr>";
    for ($i = 2; $i < 7; $i++) {
        if (!empty($row["st_dasar".$i])) { 
            $tbl.="
            <tr>
                <td width=\"3%\">".$i.".</td>
                <td width=\"75%\" align=\"justify\">".$row["st_dasar".$i]."</td>
            </tr>";
        }
    }   
    $tbl.="
    </table>
    <br>
    ";
    $pdf->writeHTML($tbl, true, false, false, false, '');
    $pdf->SetFont('helvetica', 'B', 12);
    $pdf->Write(0, 'MEMERINTAHKAN :', '', 0, 'C', true, 0, false, false, 0);
    $pdf->WriteHTMLCell(0, 0, '','', '<br />', 0, 1, 0, true, '', true);
    
     // ------------------------------------ "Menugaskan" -----------------------------------------
    $pdf->SetFont('helvetica', '', 12);
    $tbl ="
    <table cellspacing=\"0\" cellpadding=\"1\" border=\"0\" >
        <tr>
            <td rowspan=\"6\" width=\"20%\">Menugaskan</td>
            <td width=\"2%\">:</td>
        </tr>
    </table>
    <br>
    ";
    $pdf->writeHTML($tbl, true, false, false, false, '');
    
    // ---------------------------------- Nama Anggota -------------------------------------------
    $tahun=$row["st_tahun"];
    $no=$row["st_no"];
    $jenis=$row["st_jenis"];
    
    $sql1="SELECT
              *
            FROM
              um_st_anggota
            Where anggota_tahun=$tahun
                 and anggota_st=$no";
    $result1 = $conn->query($sql1);
    $i=1;
    $tbl ="<table cellspacing=\"0\" cellpadding=\"1\" border=\"0\">";
    while($row1 = $result1->fetch_assoc()) {
        if ($jenis==1){
              $tbl .="
                    <tr>
                        <td rowspan=\"4\" width=\"5%\">".$i.".</td>
                        <td width=\"15%\">Nama</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\"><b>".$row1["anggota_nama"]."</b></td>
                    </tr>
                    <tr>
                        <td width=\"15%\">Pangkat/Gol.</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\">".$row1["anggota_golru"]."</td>
                    </tr>
                    <tr>
                        <td width=\"15%\">NIP</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\">".$row1["anggota_nip"]."</td>
                    </tr>
                    <tr>
                        <td width=\"15%\">Jabatan</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\">".$row1["anggota_jabatan"]."</td>
                    </tr>
                    ";
            $i=$i+1;
        }
        elseif ($jenis==2) {
            $posisi=$row1["anggota_posisi"];
            $sql2="SELECT
                      posisi_ket
                    FROM
                      was_st_posisi
                    Where posisi_id=$posisi";
            $result2 = $conn->query($sql2);
            $row2 = $result2->fetch_assoc();
             $tbl .="
                    <tr>
                        <td rowspan=\"4\" width=\"5%\">".$i.".</td>
                        <td width=\"15%\">Nama</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\"><b>".$row1["anggota_nama"]."</b></td>
                    </tr>
                    <tr>
                        <td width=\"15%\">Pangkat/Gol.</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\">".$row1["anggota_golru"]."</td>
                    </tr>
                    <tr>
                        <td width=\"15%\">NIP</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\">".$row1["anggota_nip"]."</td>
                    </tr>
                    <tr>
                        <td width=\"15%\">Jabatan</td>
                        <td width=\"2%\">:</td>
                        <td width=\"78%\" align=\"justify\">".$row2["posisi_ket"]."</td>
                    </tr>
                    ";
            $i=$i+1;
        }
    }
    $tbl .="</table>";
            $pdf->writeHTML($tbl, true, false, false, false, '');
    
    // ---------------------------------- Kegiatan -------------------------------------------
     $tbl ="<table cellspacing=0 cellpadding=1 border=\"0\">
                <tr>
                    <td width=\"20%\">Untuk</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".$row["st_kegiatan"]."</td>
                </tr>
            
                <tr>
                    <td width=\"20%\">Tempat Berangkat</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".$row["st_asal"]."</td>
                </tr>
            
                <tr>
                    <td width=\"20%\">Tempat Tujuan</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".$row["st_tujuan"]."</td>
                </tr>
    
                <tr>
                    <td width=\"20%\">Lamanya</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".$row["st_lama"]."</td>
                </tr>
            
                <tr>
                    <td width=\"20%\">Tanggal Berangkat</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".tgl_indo($row["st_tglmulai"])."</td>
                </tr>
           
                <tr>
                    <td width=\"20%\">Tanggal Kembali</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".tgl_indo($row["st_tglselesai"])."</td>
                </tr>
    
                <tr>
                    <td width=\"20%\">Beban Biaya</td>
                    <td width=\"2%\">:</td>
                    <td width=\"78%\" align=\"justify\">".$row["st_dpa"]."</td>
                </tr>
            </table>";
    $pdf->writeHTML($tbl, true, false, false, false, ''); 
    
    // ---------------------------------- Beban Biaya -------------------------------------------
     $tbl ="<table cellspacing=\"0\" cellpadding=\"1\" border=\"0\" nobr=\"true\">
                <tr>
                    <td colspan=\"2\" width=\"100%\">Setelah melaksanakan tugas segera membuat laporan</td>
                </tr>
                <tr>
                    <td colspan=\"2\" width=\"100%\">Demikian Surat Perintah Tugas ini diberikan agar dipergunakan sebagaimana mestinya <br></td>
                </tr>
                <tr>
                    <td width=\"50%\"> </td>
                    <td width=\"50%\" align=\"left\"> 
                    &nbsp;Dikeluarkan di Tanjung Selor <br> 
                    &nbsp;Pada Tanggal ".tgl_indo($row["st_tgl"])." <br>
                    <b> ".$row["st_jabatan"]." </b><br>
                    <br>
                    <br>
                    <br>
                    <b>".$row["st_nama"]."</b><br>
                    <b> ".$row["st_golru"]."</b><br>
                    <b> ".$row["st_nip"]."</b><br>
                    </td>
                </tr>
            </table>";
    $pdf->writeHTML($tbl, true, false, false, false, '');
    // new style
   // new style
    $style = array(
        'border' => false,
        'padding' => 0,
        'fgcolor' => array(0, 0, 255),
        'bgcolor' => false
    );

    // QRCODE,H : QR-CODE Best error correction
    $qr="192.168.1.143/simwas.id/cetakst.php?key=".$stkey;
    $pdf->write2DBarcode($qr, 'QRCODE,H', 15, 290, 30, 30, $style, 'N');

    // Clean any content of the output buffer
    ob_end_clean();
    //output file
    $pdf->Output('test st.pdf', 'I');
    
    
}

?>
