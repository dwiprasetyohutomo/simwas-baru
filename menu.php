<li class="start">
	<a href="index.php">
		<i class="icon-home"></i>
		<span class="title">Beranda</span>
	</a>
</li>
<!--
<li>
	<a href="javascript:;">
		<i class="icon-notebook"></i>
		<span class="title"><em>e</em>-Perencanaan</span>
	</a>
</li>
-->
<li>
	<a href="javascript:;">
		<i class="icon-briefcase"></i>
		<span class="title"><em>e</em>-Office</span>
		<span class="arrow "></span>
	</a>
	<ul class="sub-menu">
		<!--<li><a href="javascript:;">Surat Masuk</a></li>-->
		<li><a href="modul.php?ke=um_st">Surat Tugas</a></li>
		<li><a href="modul.php?ke=um_masuk">Surat Masuk</a></li>
		<li><a href="modul.php?ke=um_keluar">Surat Keluar</a></li>
		<!--
		<li><a href="javascript:;">Produk Hukum</a></li>
		<li><a href="javascript:;">Galeri</a></li>
	  -->
	</ul>
</li>
<li>
	<a href="javascript:;">
		<i class="icon-briefcase"></i>
		<span class="title"><em>e</em>-Konsultasi</span>
		<span class="arrow "></span>
	</a>
	<ul class="sub-menu">
		<!--<li><a href="javascript:;">Surat Masuk</a></li>-->
		<li><a href="modul.php?ke=kon_baru">Belum Terjawab</a></li>
		<li><a href="modul.php?ke=kon_admin">Sudah Terjawab</a></li>
		<!--
		<li><a href="javascript:;">Produk Hukum</a></li>
		<li><a href="javascript:;">Galeri</a></li>
	  -->
	</ul>
</li>
<!--
<li>
	<a href="javascript:;">
		<i class="icon-users"></i>
		<span class="title"><em>e</em>-Personal</span>
		<span class="arrow "></span>
	</a>
	<ul class="sub-menu">
		<li><a href="modul.php?ke=um_pegawai">Daftar Pengawai</a></li>
		<li><a href="javascript:;">Pendidikan & Pelatihan</a></li>
		<li><a href="javascript:;">Kenaikan Gaji Berkala</a></li>
		<li><a href="javascript:;">Kenaikan Pangkat</a></li>
		<li><a href="javascript:;">Satya Lencana Karya Satya</a></li>
		<li><a href="modul.php?ke=um_cuti">Cuti</a></li>
	</ul>
</li>
<li>
	<a href="javascript:;">
		<i class="icon-social-dropbox"></i>
		<span class="title"><em>e</em>-Persediaan</span>
	</a>
</li>
<li>
	<a href="javascript:;">
		<i class="icon-home"></i>
		<span class="title"><em>e</em>-Aset</span>
	</a>
</li>
-->
<li>
	<a href="javascript:;">
		<i class="icon-rocket"></i>
		<span class="title"><em>e</em>-Pengawasan</span>
		<span class="arrow "></span>
	</a>
	<ul class="sub-menu">
		<!--
		<li><a href="javascript:;">Program Kerja Pengawasan</a></li>
		<li><a href="javascript:;">Kertas Kerja Pengawasan</a></li>
		<li><a href="javascript:;">P2HP</a></li>
		<li><a href="javascript:;">Laporan Hasil Pemeriksaan</a></li>
        -->
        <li>
			<a href="javascript:;">Laporan
				<span class="arrow "></span>
			</a>
			<ul class="sub-menu">
				<li><a href="modul.php?ke=laporan_temuan">Temuan</a></li>
				<li><a href="modul.php?ke=laporan_rekomendasi">Rekomendasi</a></li>
				<li><a href="modul.php?ke=laporan_rekap">Rekapitulasi</a></li>
			</ul>
		</li>
        
		<li><a href="modul.php?ke=was_data">Data Pemeriksaan</a></li>
		<li><a href="modul.php?ke=was_rekam">Rekam Jejak</a></li>
		<li><a href="modul.php?ke=was_sts">Data STS</a></li>
		<li><a href="modul.php?ke=was_spi">Data Bukti SPI</a></li>
		<!--
		<li><a href="javascript:;">Konsultasi</a></li>
		<li><a href="javascript:;">Laporan Inspektorat</a></li>
	  -->
	</ul>
</li>

<?php
// Atur Koneksi
/*
$db = new PDO("mysql:host=localhost;dbname=1nsp3kt0r4t", 'root','');
$isi = '';
$res = $db->prepare("SELECT judul, alamat, icon, nav_1 FROM op_menu GROUP BY nav_1 ORDER BY nav_1 ASC");
$res->execute();
$jum = $res->rowCount();
if($jum>0) {
	while($nav1 = $res->fetch(PDO::FETCH_ASSOC)) {
		extract($nav1);
		$idnav_1 = $nav_1;
		$res1 = $db->prepare("SELECT judul, alamat, icon, nav_2 FROM op_menu WHERE nav_1='{$nav_1}' AND nav_2>'0' GROUP BY nav_2 ORDER BY nav_2 ASC");
		$res1->execute();
		$jum1 = $res1->rowCount();
		if($jum1>0)  {
			$isi.='<li><a href="'.$alamat.'">'.((!$icon == '') ? '<i class="'.$icon.'"></i>' : '').' <span class="title">'.$judul.'</span><span class="arrow "></span></a><ul class="sub-menu">';
			while($nav2 = $res1->fetch(PDO::FETCH_ASSOC)) {
				extract($nav2);
				$idnav_2 = $nav_2;
				$res2 = $db->prepare("SELECT judul, alamat, icon, nav_3 FROM op_menu WHERE nav_1='".$idnav_1."' AND nav_2='{$nav_2}' AND nav_3>'0' GROUP BY nav_3 ORDER BY nav_3 ASC");
				$res2->execute();
				$jum2 = $res2->rowCount();
				if($jum2>0) {
					$isi.='<li><a href="'.$alamat.'">'.((!$icon == '') ? '<i class="'.$icon.'"></i>' : '').' <span class="title">'.$judul.'</span><span class="arrow "></span></a><ul class="sub-menu">';
					while($nav3 = $res2->fetch(PDO::FETCH_ASSOC)) {
						extract($nav3);
						$idnav_3 = $nav_3;
						$res3 = $db->prepare("SELECT judul, alamat, icon, nav_4 FROM op_menu WHERE nav_1='$idnav_1' AND nav_2='$idnav_2' AND nav_3='{$nav_3}' AND nav_4>'0' GROUP BY nav_4 ORDER BY nav_4 ASC");
						$res3->execute();
						$jum3 = $res3->rowCount();
						if($jum3>0) {
							$isi.='<li><a href="'.$alamat.'">'.((!$icon == '') ? '<i class="'.$icon.'"></i>' : '').' <span class="title">'.$judul.'</span><span class="arrow "></span></a><ul class="sub-menu">';
							while($nav4 = $res3->fetch(PDO::FETCH_ASSOC)) {
								extract($nav4);
								$isi.='<li><a href="'.$alamat.'">'.((!$icon == '') ? '<i class="'.$icon.'"></i>' : '').' <span class="title">'.$judul.'</span></a></li>';
							}
							$isi.='</ul></li>';
						}
						else {
							$isi.='<li><a href="'.$alamat.'">'.((!$icon == '') ? '<i class="'.$icon.'"></i>' : '').' <span class="title">'.$judul.'</span></a></li>';
						}
					}
					$isi.='</ul></li>';
				}
				else {
					$isi.='<li><a href="'.$alamat.'">'.((!$icon == '') ? '<i class="'.$icon.'"></i>' : '').' <span class="title">'.$judul.'</span></a></li>';
				}
			}
			$isi.='</ul></li>';
		}
		else {
			$isi.='<li><a href="'.$alamat.'"><i class="'.$icon.'"></i> <span class="title">'.$judul.'</span></a>
			</li>';
		}
	}
}
echo $isi;
*/
?>
<li>
	<a href="modul.php?ke=um_profil">
		<i class="icon-user"></i>
		<span class="title">Profil User</span>
	</a>
</li>
<!--
<li>
	<a href="javascript">
		<i class="icon-wrench"></i>
		<span class="title"> Pemeliharaan</span>
		<span class="arrow "></span>
	</a>
	<ul class="sub-menu">
		<li><a href="modul.php?ke=op_menu"><i class="icon-layers"></i> <span class="title">Menu <em>e</em>-Aplikasi</span></a></li>
		<li>
			<a href="modul.php?ke=op_users">
				<i class="icon-user"></i>
				<span class="title">Users</span>
			</a>
		</li>
	</ul>
</li>
-->
